<?php

namespace App\Http\Controllers\Auth;

use App\Jobs\SendFoundPostEmail;
use App\Jobs\SendLostPostEmail;
use App\Jobs\SendNewPostEmail;
use App\Mail\FoundPostCronEmail;
use App\Mail\LostPostCronEmail;
use App\Mail\NewPostCronEmail;
use App\Models\PetsInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class PawMawMailController extends Controller
{
    public function sendNewPostEmail()
    {
//        DB::connection()->enableQueryLog();
//        $pets  = PetsInfo::with('user')
//        ->whereRaw(DB::raw('(thirtyMinuteMailSend = 0  and DATE_ADD(created_at,INTERVAL 30 MINUTE)<Now()) or (thirtyMinuteMailSend = 1 and oneDayMailSend = 0  and DATE_ADD(created_at,INTERVAL 1440 MINUTE)<Now())'))
//            ->limit(10)
//            ->get();
//        if(count($pets)>0) {
//            foreach ($pets as $pet) {
//                if($pet->user==null){
//                    dd($pet);
//                }
//            }
//        }
//        return $pets;
//        dd(DB::getQueryLog());
//        dd($pets);

    }


    public function sendNewLostPostEmail()
    {
        $pets  = PetsInfo::whereRaw(DB::raw('counter<3 and  DATE(DATE_ADD(email_send_date, INTERVAL 24 HOUR)) <= DATE(NOW()) and type = "lost" and user_id is not null'))
            ->with('user')
            ->limit(10)
            ->get();

        if(count($pets)>0){
            foreach ($pets as $pet) {
                if($pet->user!=null) {
                    SendLostPostEmail::dispatch($pet);

                    $pet->email_send_date = $pet->email_send_date->addDays(1);
                    $pet->counter = $pet->counter + 1;

                    $pet->save();
                }

            }
        }

    }


    public function sendNewFoundPostEmail()
    {
        $pets  = PetsInfo::whereRaw(DB::raw('counter<3 and  DATE(DATE_ADD(email_send_date, INTERVAL 24 HOUR)) <= DATE(NOW()) and type = "found" and user_id is not null'))
            ->with('user')
            ->limit(10)
            ->get();

        if(count($pets)>0){
            foreach ($pets as $pet) {
                if($pet->user!=null) {
                    SendFoundPostEmail::dispatch($pet);

                    $pet->email_send_date = $pet->email_send_date->addDays(1);
                    $pet->counter = $pet->counter + 1;

                    $pet->save();
                }

            }
        }

    }

    public function lostExample()
    {
        $pet = PetsInfo::where('id',162)->where('type','lost')->with('user')->get();
        Mail::to("pawmawalert@gmail.com ")->send(new LostPostCronEmail($pet));
    }




    public function foundExample()
    {
        $pet = PetsInfo::where('id',150)->where('type','found')->with('user')->get();
        Mail::to("pawmawalert@gmail.com ")->send(new NewPostCronEmail($pet));
    }



    public function postExample()
    {
        $pet = PetsInfo::where('id',150)->where('type','lost')->with('user')->get();
        Mail::to("pawmawalert@gmail.com ")->send(new LostPostCronEmail($pet));
    }


}
