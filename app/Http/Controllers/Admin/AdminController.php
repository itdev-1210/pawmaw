<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Models\Role;
use Auth;
use Mail;
use Session;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $admin = Auth::guard('admin')->user();
        $users = Admin::orderBy('id','desc')->get();

        return view('admin.admins.list',compact('users'));
    }

    public function create()
    {
        $admin = Auth::guard('admin')->user();
        $roles = Role::where('guard_name','admin')->orderBy('name','ASC')->get();
        return view('admin.admins.add',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $admin = Auth::guard('admin')->user();

        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email|max:255|unique:admins',
            'password' => 'required|min:6|confirmed'
        ]);

        $user = new Admin;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        $user->assignRole($request->role);

        return redirect()->action('Admin\AdminController@index')->with('success','Successfully Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Admin::find($id);
        return view('admin.admins.view',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Auth::guard('admin')->user();
        $user = Admin::find($id);
        $roles = Role::where('guard_name','admin')->orderBy('name','ASC')->get();

        $assigned_role = $user->roles->pluck('id');
        return view('admin.admins.edit',compact('user','roles','assigned_role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = Auth::guard('admin')->user();
        $this->validate($request,[
            'name' => 'required',
        ]);

        $user = Admin::find($id);
        if($request->email != $user->email){
            $this->validate($request,[
                'email' => 'required|email|max:255|unique:users'
            ]);
            $user->email = $request->email;
        }
        $user->name = $request->name;
        if($request->password){
            $this->validate($request,[
                'password' => 'required|min:6|confirmed'
            ]);
            $user->password = bcrypt($request->password);
        }       
        $user->syncRoles($request->role);
        $user->update();

        return redirect()->action('Admin\AdminController@index')->with('success','Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Admin::whereId($id)->delete();
        return redirect()->action('Admin\AdminController@index')->with('success','Successfully Deleted!');
    }

    public function edit_password($id)
    {
        $user = Admin::find($id);
        return view('admin.admins.password',compact('user'));
    }

    public function change_password(Request $request, $id)
    {
        $admin = Auth::guard('admin')->user();
        $this->validate($request,[
            'password' => 'required|min:6|confirmed'
        ]);

        $user = Admin::find($id);
        $user->password = bcrypt($request->password);
        $user->update();

        return redirect()->action('Admin\AdminController@index')->with('success','Successfully Updated!');

    }
    
    public function unsubscribe() {
        
        return view('admin.newsletters.unsubscribe');
    }
    
        public function addunsubscribe(Request $request) {
            
      
            if($request->email) {
                
                $checkEmail = \DB::table('users')
                       ->select('email')
                       ->where('email', $request->email)->get();
                
                if(count($checkEmail) < 1) {
                  return redirect()->back()->with("error", "Invalid user email");  
                }
                
                $data = [ 'address'=>$request->email, 'tag'=>'newsletter_mail'];
                $api_key = env('MAILGUN_SECRET');
                $DomainName = env('MAILGUN_DOMAIN');
                
                $options=['--user'=>'api:'.$api_key];
                $url = "https://api.mailgun.net/v3/$DomainName/unsubscribes";
                
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($curl, CURLOPT_USERPWD, "api:".$api_key);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                $result = curl_exec($curl);
                curl_close($curl);
                return redirect()->back()->with("success", "Email Unsubscribed Successfully !");
                
            } else {
                
                return redirect()->back()->with("error", "Something went wrong try again");
            }
        
    }
}
