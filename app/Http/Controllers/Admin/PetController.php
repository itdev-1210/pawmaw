<?php

namespace App\Http\Controllers\Admin;
use App\Mail\AdminEmail;
use App\Models\Contact;
use App\Models\OwnersEmail;
use App\Models\PetsEmail;
use App\Models\EmailTime;
use App\User;
use App\Models\PetsInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Mail\DeletePost;
use Illuminate\Support\Facades\Mail;
use App\Mail\OwnerEmail;
use Session;
use Illuminate\Support\Str;

class PetController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = Auth::guard('admin')->user();
        $pets = PetsInfo
            ::when(@$_GET['search'], function($query){
                if(strpos($_GET['search'], '@') != false){
                    $query
                        ->where(
                            'contact_email',
                            $_GET['search']
                        );
                } else if(is_numeric($_GET['search'])){
                    $query
                        ->where(
                            'id',
                            $_GET['search']
                        );
                } else{
                    $query
                        ->where(
                            'name',
                            'like',
                            '%' . $_GET['search'] . '%'
                        );
                }
            })
            ->orderBy('id','desc')
            ->paginate(20);

        return view('admin.pets.list',compact('pets'));
    }


    public function adminmessage()
    {
        $admin = Auth::guard('admin')->user();
        $contacts = PetsEmail::orderBy('id','desc')->get();
        return view('admin.contacts.list',compact('contacts'));
    }



    public function webmessage()
    {
        $admin = Auth::guard('admin')->user();
        $contacts = Contact::orderBy('id','desc')->get();
        return view('admin.contacts.weblist',compact('contacts'));
    }


    public function petmessage()
    {
        $admin = Auth::guard('admin')->user();
        $contacts = OwnersEmail::orderBy('id','desc')->get();
        return view('admin.contacts.pet_details',compact('contacts'));
    }


    public function adminmessageshow($id)
    {
        $contacts = PetsEmail::find($id);
        return view('admin.contacts.view',compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pets.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = Auth::guard('admin')->user();


        $info = new PetsInfo;
        $info->type = $request->action;
        $info->user_id = $user->id;
        $info->name = $request->name;
        $info->specie = $request->specie;
        $info->color = $request->color;
        $info->last_seen = $request->address;
        $info->contact_email = $user->email;
        $info->postal = $request->postal;
        $info->date = $request->lost_found_date;
        $info->gender = $request->sex;
        $info->description = $request->description;
        if($request->action == 'found'){
            $info->circumstance = $request->circumstances;
        }
        $info->breed = $request->breed;


        $info->save();

        if($request->pet_photo != ''){
            $destinationPath =   public_path('/uploads/pets_image');
            $file = $request->pet_photo;
            $ext = $request->pet_photo->getClientOriginalExtension();
            $fileName = $request->action.$info->id.'.'.$ext;
            if(file_exists($destinationPath.'/'.$fileName)){
                @unlink($destinationPath.'/'.$fileName);
            }
            $file->move($destinationPath, $fileName);
            $info->photo = $fileName;
            $info->update();
        }  $user = Auth::guard('user')->user();


        $info = new PetsInfo;
        $info->type = $request->action;
        $info->user_id = $user->id;
        $info->name = $request->name;
        $info->specie = $request->specie;
        $info->color = $request->color;
        $info->last_seen = $request->address;
        $info->contact_email = $user->email;
        $info->postal = $request->postal;
        $info->date = $request->lost_found_date;
        $info->gender = $request->sex;
        $info->description = $request->description;
        if($request->action == 'found'){
            $info->circumstance = $request->circumstances;
        }
        $info->breed = $request->breed;


        $info->save();

        if($request->pet_photo != ''){
            $destinationPath =   public_path('/uploads/pets_image');
            $file = $request->pet_photo;
            $ext = $request->pet_photo->getClientOriginalExtension();
            $fileName = $request->action.$info->id.'.'.$ext;
            if(file_exists($destinationPath.'/'.$fileName)){
                @unlink($destinationPath.'/'.$fileName);
            }
            $file->move($destinationPath, $fileName);
            $info->photo = $fileName;
            $info->update();
        }



        return redirect()->action('Admin\PetController@index')->with('success','Successfully Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pet = PetsInfo::find($id);
        return view('admin.pets.view',compact('pet'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pet = PetsInfo::find($id);
        return view('admin.pets.edit',compact('pet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::guard('admin')->user();

        $info = PetsInfo::find($id);
        $info->type = $request->action;

        $info->name = $request->name;
        $info->specie = $request->specie;
        $info->color = $request->color;
        $info->last_seen = $request->address;

        $info->street = $request->street;
        $info->postal = $request->postal;
        $info->date = $request->lost_found_date;
        $info->gender = $request->sex;
        $info->description = $request->description;
        $info->meta_description = $request->meta_description;
        $info->meta_keywords = $request->meta_keywords;

        $info->meta_title = $request->meta_title;
        if($request->action == 'found'){
            $info->circumstance = $request->circumstances;
        }
        $info->breed = $request->breed;
        
        if ( $info->name != null ) {
			$temp = Str::slug( $request->name, '-' );
		} else {
			$temp = Str::slug( 'found pet', '-' );
		}

    	$maxSlugNumber = 0;
		$petResults = PetsInfo::where('slug', 'like', $temp. '-%')->orderBy( 'id', 'desc' )->get();
		if ( ! $petResults->isEmpty() ) {
			$slugNumbers = array();
			foreach ($petResults->all() as $petResult) {
				if (preg_match('/'. preg_quote($temp, '/'). '-([0-9]+)$/', $petResult->slug, $matches)) {
				    $slugNumbers[] = $matches[1];
				}
		    }
			if (count($slugNumbers)) {
				$maxSlugNumber = max($slugNumbers);
			}
		}
		
		if ( ! PetsInfo::all()->where( 'slug', $temp )->isEmpty() ) {
			$i = $maxSlugNumber + 1;
			$newslug = $temp . '-' . $i;
			while ( ! PetsInfo::all()->where( 'slug', $newslug )->isEmpty() ) {
				$i ++;
				$newslug = $temp . '-' . $i;
			}
			$temp = $newslug;
		}
		$info->slug = $temp;
        $info->update();
        if($request->pet_photo != ''){
            $destinationPath =   public_path('/uploads/pets_image');
            $file = $request->pet_photo;
            $ext = $request->pet_photo->getClientOriginalExtension();
            $fileName = $request->action.$info->id.'.'.$ext;
            if(file_exists($destinationPath.'/'.$fileName)){
                @unlink($destinationPath.'/'.$fileName);
            }
            $file->move($destinationPath, $fileName);
            $info->photo = $fileName;
            $info->update();
        }



        return redirect()->action('Admin\PetController@index')->with('success','Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    
        $pet = PetsInfo::find($id);
        $admin = Auth::guard('admin')->user();
        PetsInfo::whereId($id)->delete();
        EmailTime::where('pet_id', $id)->delete();
        Mail::to($pet->contact_email)->send(new DeletePost($pet));
        return redirect()->action('Admin\PetController@index')->with('success','Successfully Deleted!');
    }

    public function statusUpdate($id)
    {
        $admin = Auth::guard('admin')->user();
        $pet = PetsInfo::find($id);

        if($pet->status == 0){
            $pet->status = 1;
            $status = 'published';
        }else{
            $pet->status = 0;
            $status = 'unpublished';
            //Mail::to($pet->contact_email)->send(new DeletePost($pet));
            
        }
        $pet->update();

    }
    
    public function packageUpdate($id, $packageName)
    {
        $admin = Auth::guard('admin')->user();
        $pet = PetsInfo::find($id);

        if($pet){
        	if ($packageName) {
                $pet->package = json_encode(['name' => $packageName, 'updated_at' => time()]);
        	}else {
        	    $pet->package = null;  
        	}
            $pet->update();
        }
    }


    public function email($id)
    {
        $pet = PetsInfo::getPetInformationById($id);


        return view('admin.pets.email',compact('pet'));
    }


    public function emailstore(Request $request)
    {


        $user = Auth::guard('admin')->user();

        $email = new PetsEmail;
        $email->pet_id = $request->pet_id;
        $email->user_id = $request->user_id;
        //$email->shareonfacebook = $request->shareonfacebook;
        $email->viewfacebookpost = $request->viewfacebookpost;

        $email->save();

        $email->load('pet');


        $info = User::where('id',$request->user_id)->first();
        Mail::to($info->email)->send(new AdminEmail($email));

        return redirect()->action('Admin\PetController@index')->with('success','Successfully Created!');
    }
    
    public function ownermailstatus(Request $request){
      
        $admin = Auth::guard('admin')->user();
        if($request->id != '' && $request->status != ''){
            
            $ownermail = OwnersEmail::find($request->id);
           
            if($ownermail){
                $petid = $ownermail->pet_id;
                $pet =  \DB::table('pets_info')->where('id', $petid)->first();

                if($pet){
                    $email = (object) [
                        'email' => $ownermail->email,
                        'message' => $ownermail->message,
                        'name' => $ownermail->name
                    ];
                    
                    $ownermail->status = $request->status;
                    $ownermail->update();
                    if($request->status == 1){
                        Mail::to($pet->contact_email)->send(new OwnerEmail($email));
                    }
                    echo "true";
                }
                else{
                    echo "false";
                }
            }
            else{
                echo "false";
            }
        }
        else{
            echo "false";   
        }
    }
}
