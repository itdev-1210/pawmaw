<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class FacebookPageController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('admin');
    }
    
	public function index()
	{
		$pages = DB
			::table('facebook_pages')
			->when(@$_GET['search'], function($query){
				$query->where(
					'name', 
					'like',
					'%' . $_GET['search'] . '%'
				);
			})
			->orderBy('id', 'DESC')
			->paginate(20);

		return view('admin.facebook_pages.list', compact('pages'));
	}

	public function create()
	{
		return view('admin.facebook_pages.add');
	}

	public function store()
	{
		$zip_codes = explode(',', request('zip_codes'));

		$id = DB
			::table('facebook_pages')
			->insertGetId([
				'name'        => request('name'),
			    'country'     => request('country'),
				'link'        => request('link'),
				'facebook_id' => request('facebook_id'),
				'app_id'      => request('app_id'),
				'app_secret'  => request('app_secret')
			]);

		foreach($zip_codes as $zip_code){
			DB
				::table('facebook_zip_codes')
				->insert([
					'facebook_id' => $id,
					'zip_code'    => $zip_code
				]);
		}

		return redirect('admin/facebook');
	}

	public function edit($id)
	{
		$facebook = DB
			::table('facebook_pages')
			->select(
				'facebook_pages.*',
				DB::raw("(
					SELECT GROUP_CONCAT(facebook_zip_codes.zip_code)
					FROM facebook_zip_codes
					WHERE facebook_zip_codes.facebook_id = facebook_pages.id
					GROUP BY facebook_pages.id
				) as zip_codes") 
			)
			->where('id', $id)
			->first();

		$zip_codes = json_decode(json_encode(DB
			::table('facebook_zip_codes')
			->select('zip_code')
			->where('facebook_zip_codes.facebook_id', $id)
			->pluck('zip_code')
			->toArray()), true);

		$facebook->zip_codes = implode(',', $zip_codes);

		return view('admin.facebook_pages.edit', compact('facebook'));
	}

	public function update($id)
	{
		$zip_codes = explode(',', request('zip_codes'));

		DB
			::table('facebook_pages')
			->where('id', $id)
			->update([
				'name'        => request('name'),
				'link'        => request('link'),
				'facebook_id' => request('facebook_id'),
				'app_id'      => request('app_id'),
				'app_secret'  => request('app_secret'),
			    'country'  => request('country'),
			    'access_token'  => request('access_token')
			]);

		DB
			::table('facebook_zip_codes')
			->where('facebook_zip_codes.facebook_id', $id)
			->delete();

		if(count($zip_codes))
			foreach($zip_codes as $key => $zip_code){
				if($zip_code)
					DB
						::table('facebook_zip_codes')
						->insert([
							'facebook_id' => $id,
							'zip_code'    => $zip_code
						]);
			}

		return redirect()->back();
	}

	public function destroy($id)
	{
		DB::table('facebook_pages')->where('id', $id)->delete();
		DB::table('facebook_zip_codes')->where('facebook_id', $id)->delete();

		return redirect('/admin/facebook');
	}
}
