<?php

namespace App\Http\Controllers\Admin;
use App\BlogPost;
use App\Category;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;

class BlogController extends Controller
{
	
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = BlogPost::orderBy('id', 'DESC')->get();

        return view('admin.blogs.list',compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categories = Category::all();

        return view('admin.blogs.add',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'category_id' => 'required',
            'image' => 'required|image',
            'excerpt' => 'required'
        ]);

        $detailsWork = $request->all();

        if(!$detailsWork['slug'])
            unset($detailsWork['slug']);

        $destinationPath = public_path('img/');
        $file = request('image');
        $unique = uniqid();
        $fileName = $unique.'-'.$file->getClientOriginalName();

        $file->move($destinationPath, $fileName);

        $detailsWork['image'] = $fileName;

        $this->checkOnRotate($destinationPath . '/' . $fileName);

/*         $detail=$request->content;

        $dom = new \domdocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $images = $dom->getElementsByTagName('img');

        foreach($images as $k => $img){
            $data = $img->getAttribute('src');

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);

            $data = base64_decode($data);
            $image_name= time().$k.'.png';
           // $path =$destinationPath.'/'. $image_name;
           // echo'$path<pre>';print_r($path); exit;
            file_put_contents($destinationPath, $data);

            $img->removeAttribute('src');
            $img->setAttribute('src', $image_name);
        }

        $detail = $dom->savehtml();

        $detailsWork['content'] = $detail; */

        BlogPost::create($detailsWork);

        return redirect()->route('adminblog');
    }

    private function checkOnRotate($image)
    {

        $exif = @exif_read_data($image);

        if(!@$exif['Orientation'])
            return;

        $orientation = $exif['Orientation'];
        $source = @imagecreatefromjpeg($image);

        if(!$source){
            $source = imagecreatefromstring(file_get_contents($image));
        }

        switch($orientation){
            case 3:
                $rotate = imagerotate($source, 90, 0);
                break;
            case 6:
                $rotate = imagerotate($source, 270, 0);
            break;
        }

        imagejpeg($rotate, $image);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['post'] = BlogPost::find($id);
        $data['categories'] = Category::all();


        return view('admin.blogs.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $udpateArray = [
            'title'       => request('title'),
            'excerpt'     => request('excerpt'),
            'content'     => request('content'),
            'category_id' => request('category_id')
        ];

        if(request('slug'))
            $udpateArray['slug'] = request('slug');

        if(request('image')){
            $destinationPath = public_path('img/');
            $file = request('image');
            $ext = $file->getClientOriginalExtension();
            $unique = uniqid();
            $fileName = $unique . '.' . $ext;
            
            if(file_exists($destinationPath . '/' . $fileName)){
                 @unlink($destinationPath . '/' . $fileName);
            }

            $file->move($destinationPath, $fileName);

            $udpateArray['image'] = $fileName;

            $this->checkOnRotate($destinationPath . '/' . $fileName);
        }

        BlogPost
            ::where('id', $id)
            ->update($udpateArray);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BlogPost::destroy($id);

        return response(200);
    }

    public function comment()
    {
        $data['comments'] = Comment::where('checked', 0)->paginate(20);

        return view('admin.blogs.comment', $data);
    }

    public function commentCheck()
    {
        if(request('type')){
            Comment::where('id', request('id'))->update(['checked' => 1]);
        } else{
            Comment::destroy(request('id'));
        }

        return response(200);
    }
    
    public function uploadEditorImage(Request $request) {
       // $destinationPath = public_path('pawmaw/img/blog-img');
       // echo $destinationPath;exit;
        $this->validate($request, [
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
    
        if ($request->hasFile('file')) {
            
            $image = $request->file('file');
            $name = time().'-'.$image->getClientOriginalName();
            $destinationPath = public_path('pawmaw/img/blog-img');
            $image->move($destinationPath, $name);
            echo url('pawmaw/img/blog-img')."/".$name;
            exit;
  
        }
    }
}
