<?php

namespace App\Http\Controllers\Admin;
use App\PagePost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;

class PageController extends Controller
{
	
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = PagePost::orderBy('id', 'DESC')->get();
        return view('admin.pages.list',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'slug' => 'required',
            'content' => 'required',
            'metatitle' => 'required',
            'metadesc' => 'required'
        ]);

        $detailsWork = $request->all();

        if(!$detailsWork['slug']) {
            unset($detailsWork['slug']);
        }else{
            if ($this->isSlugExists($detailsWork['slug'])) {
                return redirect()->back()->withErrors(['Url already exists']);
            }
        }    

        PagePost::create($detailsWork);

        return redirect()->route('adminpage');
    }

    private function checkOnRotate($image)
    {

        $exif = @exif_read_data($image);

        if(!@$exif['Orientation'])
            return;

        $orientation = $exif['Orientation'];
        $source = @imagecreatefromjpeg($image);

        if(!$source){
            $source = imagecreatefromstring(file_get_contents($image));
        }

        switch($orientation){
            case 3:
                $rotate = imagerotate($source, 90, 0);
                break;
            case 6:
                $rotate = imagerotate($source, 270, 0);
            break;
        }

        imagejpeg($rotate, $image);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['post'] = PagePost::find($id);
        return view('admin.pages.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $udpateArray = [
            'title'       => request('title'),
            'content'     => request('content'),
            'slug'     => request('slug'),
            'metatitle' => request('metatitle'),
            'metadesc' => request('metadesc'),
        ];

        if ($this->isSlugExists($udpateArray['slug'], $id)) {
            return redirect()->back()->withErrors(['Url already exists']);
        }
            
        PagePost
            ::where('id', $id)
            ->update($udpateArray);

        return redirect()->back();
    }

    function isSlugExists($slug, $pageId = null) {
        $query = PagePost::where('slug', $slug);
		if($pageId){
			$query->where('id', '!=', $pageId);
		}
		$results = $query->get();
        return !$results->isEmpty(); 
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PagePost::destroy($id);

        return response(200);
    }
}
