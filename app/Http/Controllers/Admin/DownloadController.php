<?php
namespace App\Http\Controllers\Admin;
use App\User;
use App\Models\PetsInfo;
use App\Modeels\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Session;
use Response;


class DownloadController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index() {
        
        return redirect('admin');
    }
    
    public function sitemap(){
        
        $pets = PetsInfo::select('slug')->orderBy('created_at', 'DESC')->get();
        $blogs =  \DB::table('blog_entries')->select('slug')->orderBy('created_at', 'DESC')->get();
        $pages =  \DB::table('page_entries')->select('slug')->orderBy('created_at', 'DESC')->get();
        //return view('admin.sitemap.sitemap');
        $content = response()->view('admin.sitemap.sitemap', compact('pets','blogs'));
        
        $content = '';
      $content .='<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    
    $content .='<url>';
        $content .='<loc>'.url("/").'</loc>';
    $content .='</url>';
    $content .='<url>';
        $content .='<loc>'.url("blog").'</loc>';
    $content .='</url>';
    $content .='<url>';
        $content .='<loc>'.url("faqs").'</loc>';
    $content .='</url>';
    $content .='<url>';
        $content .='<loc>'.url("about-us").'</loc>';
    $content .='</url>';
    $content .='<url>';
        $content .='<loc>'.url("contact").'</loc>';
    $content .='</url>';
    $content .='<url>';
        $content .='<loc>'.url("join-our-network").'</loc>';
   $content .=' </url>';
    $content .='<url>';
        $content .='<loc>'.url("privacy-policy").'</loc>';
    $content .='</url>';
    $content .='<url>';
        $content .='<loc>'.url("refund-policy").'</loc>';
    $content .='</url>';
    
    foreach($pages as $page){
        $content .='<url>';
            $content .='<loc>'. url($page->slug).'</loc>';
        $content .='</url>';
    }

    foreach($pets as $pet){
        $content .='<url>';
            $content .='<loc>'. url('pet-details').'/'.$pet->slug.'</loc>';
        $content .='</url>';
    }
    foreach($blogs as $blog){
        $content .='<url>';
            $content .='<loc>'. url('blog').'/'.$blog->slug.'</loc>';
        $content .='</url>';
    }
  
$content .='</urlset>';
       
        
        $file=fopen(base_path().'/public/sitemap.xml','w');
        fwrite($file,$content);
        fclose($file);
        
        $headers = array(
              'Content-Type: text/xml',
            );

        return Response::download(base_path().'/public/sitemap.xml', 'sitemap.xml', $headers);
    }
  
   public function emails(Request $request){ 

        
        $fromdate = $request->get('fromdate');
        $user = \DB::table('users')->select("email","created_at");
        $newslatter =  \DB::table('newsletters')->select('email');
        
        if($request->get('fromdate')) {
            $fromdate = $request->get('fromdate');
            $user->whereDate('created_at',' >= ',$fromdate);
            $newslatter->whereDate('created_at',' >= ',$fromdate);
        }
        if($request->get('todate')) {
            $todate = $request->get('todate');
            $user->whereDate('created_at',' <= ',$todate);
            $newslatter->whereDate('created_at',' <= ',$todate);
        }
        
        $user = $user->orderBy('created_at', 'ASC')->get();
        $newslatter = $newslatter->orderBy('created_at', 'ASC')->get();
        $dataArray = array();  
        
        foreach ($user as $key => $val)
        { 
          $uemail = $val->email;    
          
          $newsEmail = (isset($newslatter[$key])) ? $newslatter[$key]->email : '';
          
          if(!empty($newsEmail)) {
              
              unset($newslatter[$key]);
          }
          
          $std = new \stdClass();
          $std->useremail = $uemail;
          $std->newsemail = $newsEmail;
          array_push($dataArray,$std);
          
        }
         
       
         
        foreach ($newslatter as $val)
        {
    
          $std = new \stdClass();
          $std->useremail = '';
          $std->newsemail = $val->email;
          array_push($dataArray,$std);
         
        }
        
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="emails.csv"');
        header('Pragma: no-cache');
        header('Expires: 0');

        $file = fopen('php://output', 'w');
        fputcsv($file, array('User Email','Newsletter Emails'));
        foreach($dataArray as $val) {
            fputcsv($file, array($val->useremail,$val->newsemail));
        }
         
        exit();
      
   } 
   
} // class close


