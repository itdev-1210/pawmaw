<?php

namespace App\Http\Controllers\Admin;
use App\PagePost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use App\Models\Notifications;
use App\Models\SiteSettings;


class NotificationsController extends Controller
{
	
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.notifications.list', [
            'notifications' => Notifications::get(),
            'settings' => SiteSettings::first()
        ]);
    }

    public function store(Request $request){
        $path = public_path('uploads/');
        $imageName = 'notification-'.time().'.'.$request->image->extension();
        Image::make($request->image)->save($path.$imageName);
        $res = Notifications::create([
            'name' => $request->name,
            'image' => $imageName,
            'lost_or_found' => $request->lost_or_found
        ]);
        return redirect()->back();
    }

    public function delete($id){
        $notif = Notifications::find($id);
        $imagePath = public_path('uploads/'.$notif->image);
        if(\file_exists($imagePath)){
            unlink($imagePath);
        }
        $notif->delete();
        return redirect()->back();
    }

    public function changeSetting($value){
        $setting = SiteSettings::first();
        $setting->enable_notifications = $value;
        $setting->save();
        return redirect()->back();
    }
}
