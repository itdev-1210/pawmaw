<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use DB;
use Artisan;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles_all = Role::where('guard_name','admin')->get();
        $roles = array();
        foreach ($roles_all as $key => $value) {
            $permissions = array();
            $perms=DB::table("role_has_permissions")->where("role_has_permissions.role_id",$value->id)
            ->pluck('role_has_permissions.permission_id')->toArray();
            $permissions = Permission::whereIn("id",$perms)->get();
            $roles[]=array( 
                'id'=>$value->id,
                'name'=>$value->name,
                'guard_name'=>$value->guard_name,
                'permissions' => $permissions
            );
        }
        return view('admin.roles.list',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::where('guard_name','admin')->orderBy('name','ASC')->get();
        return view('admin.roles.add',compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:roles,name',
            'permissions' => 'required'
        ]);

        $role = new Role;
        $role->name = $request->name;
        $role->guard_name = 'admin';
        $role->save();

        foreach ($request->input('permissions') as $key => $value) {
            $role->givePermissionTo($value);
        }
        
        return redirect()->action('Admin\RoleController@index')->with('success','Successfully Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id')->toArray();
        $permissions = Permission::whereIn('id',$rolePermissions)->get();

        return view('admin.roles.view',compact('role','permissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $permissions = Permission::where('guard_name','admin')->orderBy('name','ASC')->get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')->toArray();

        return view('admin.roles.edit',compact('role','permissions','rolePermissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'permissions' => 'required'
        ]);
        $role = Role::find($id);
        $role->name = $request->name;
        $role->update();

        DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)->delete();

        foreach ($request->input('permissions') as $key => $value) {
            $role->givePermissionTo($value);
        }
        return redirect()->action('Admin\RoleController@index')->with('success','Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role::whereId($id)->delete();
        DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)->delete();
        return redirect()->action('Admin\RoleController@index')->with('success','Successfully Deleted!');
    }
}
