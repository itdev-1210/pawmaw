<?php

namespace App\Http\Controllers\Admin;
use App\Models\Payment;
use App\User;
use App\Models\PetsInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Session;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {


        $user = Auth::guard('admin')->user();

        $payment = Payment::orderBy('id','desc')->get();

        return view('admin.orders.list',compact('user', 'payment'));



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payment = Payment::find($id);
        return view('admin.orders.view',compact('payment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pet = PetsInfo::find($id);
        return view('admin.pets.edit',compact('pet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::guard('admin')->user();

        $info = PetsInfo::find($id);
        $info->type = $request->action;

        $info->name = $request->name;
        $info->specie = $request->specie;
        $info->color = $request->color;
        $info->last_seen = $request->address;

        $info->postal = $request->postal;
        $info->date = $request->lost_found_date;
        $info->gender = $request->sex;
        $info->description = $request->description;
        if($request->action == 'found'){
            $info->circumstance = $request->circumstances;
        }
        $info->breed = $request->breed;
        $info->update();

        if($request->pet_photo != ''){
            $destinationPath =   public_path('/uploads/pets_image');
            $file = $request->pet_photo;
            $ext = $request->pet_photo->getClientOriginalExtension();
            $fileName = $request->action.$info->id.'.'.$ext;
            if(file_exists($destinationPath.'/'.$fileName)){
                @unlink($destinationPath.'/'.$fileName);
            }
            $file->move($destinationPath, $fileName);
            $info->photo = $fileName;
            $info->update();
        }



        return redirect()->action('Admin\PetController@index')->with('success','Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pet = PetsInfo::find($id);
        $admin = Auth::guard('admin')->user();


        PetsInfo::whereId($id)->delete();
        return redirect()->action('Admin\PetController@index')->with('success','Successfully Deleted!');
    }

    public function statusUpdate($id)
    {
        $admin = Auth::guard('admin')->user();
        $pet = PetsInfo::find($id);

        if($pet->status == 0){
            $pet->status = 1;
            $status = 'published';
        }else{
            $pet->status = 0;
            $status = 'unpublished';
        }
        $pet->update();


    }







}
