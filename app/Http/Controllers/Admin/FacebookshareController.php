<?php

namespace App\Http\Controllers\Admin;

use App\Models\Facebookshare;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Models\Role;


use Auth;
use Mail;
use Session;

class FacebookshareController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $agent = Auth::guard('admin')->user();
        $facebookshares = Facebookshare::orderBy('id','desc')->get();
        return view('admin.facebooks.list',compact('facebookshares', 'agent'));
    }

    public function create()
    {
        return view('admin.facebooks.add');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $agent = Auth::guard('admin')->user();
        $this->validate($request,[
            'zip_code' => 'required',
            'pageid' => 'required',
            'app_id' => 'required',
            'app_secret' => 'required',
            'access_token' => 'required'
            
        ]);

        $facebookshare = new Facebookshare;
        $facebookshare->zip_code = $request->zip_code;
        $facebookshare->pageid = $request->pageid;
        $facebookshare->app_id = $request->app_id;
        $facebookshare->app_secret = $request->app_secret;
        $facebookshare->access_token = $request->access_token;
      

        $facebookshare->save();



        return redirect()->action('Admin\FacebookshareController@index')->with('success','Successfully Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $channel = Channel::with(['added_by_data','status_by_data','updated_by_data'])->find($id);
        return view('admin.facebooks.view',compact('channel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     *
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit($id)
    {
        $facebookshare = Facebookshare::find($id);
        return view('admin.facebooks.edit',compact('facebookshare'));
    }



    public function update(Request $request, $id)
    {
        $agent = Auth::guard('admin')->user();

        $this->validate($request,[
            'zip_code' => 'required',
            'pageid' => 'required',
            'app_id' => 'required',
            'app_secret' => 'required',
            'access_token' => 'required'

        ]);
        $facebookshare = Facebookshare::find($id);
        $facebookshare->zip_code = $request->zip_code;
        $facebookshare->pageid = $request->pageid;
        $facebookshare->app_id = $request->app_id;
        $facebookshare->app_secret = $request->app_secret;
        $facebookshare->access_token = $request->access_token;





        $facebookshare->update();



        return redirect()->action('Admin\FacebookshareController@index')->with('success','Successfully Updated!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $facebookshare = Facebookshare::find($id);
        $agent = Auth::guard('admin')->user();



        Facebookshare::whereId($id)->delete();
        return redirect()->action('Admin\FacebookshareController@index')->with('success','Successfully Deleted!');
    }

    public function statusUpdate($id)
    {
        $agent = Auth::guard('agent')->user();
        $channel = Channel::find($id);
        $channel->status_by = $agent->id;

        if($channel->status == 0){
            $channel->status = 1;
            $status = 'published';
        }else{
            $channel->status = 0;
            $status = 'unpublished';
        }
        $channel->update();

        $operation = new CrudOperation;
        $operation->module_type = 'channels';
        $operation->action_type = $status;
        $operation->module_id = $channel->id;
        $operation->login_record = Session::get('login_record');
        $operation->save();
    }


}
