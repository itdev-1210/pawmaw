<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Newsletter;
use App\Admin;
use App\Models\Role;

use Mail;
use Session;
use Auth;

class NewsletterController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index()
    {
        $agent = Auth::guard('admin')->user();
        $newsletters = Newsletter::orderBy('id','desc')->get();
        return view('admin.newsletters.list',compact('newsletters', 'agent'));
    }


    public function store(Request $request)
    {
        $request->validate([
            
            'email' => 'required|email|unique:newsletters,email',
         
        ]);




        $email = new Newsletter;


        
        $email->email = $request->email;
      


        $email->save();




        return redirect()->back()->with('success','Successfully Added!');

    }



}
