<?php

namespace App\Http\Controllers\UserAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Hesto\MultiAuth\Traits\LogsoutGuard;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, LogsoutGuard {
        LogsoutGuard::logout insteadof AuthenticatesUsers;
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    public $redirectTo = 'user/my-account';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user.guest', ['except' => 'logout']);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        // Auth::loginUsingId(3127);

        // dd(auth()->user());

        return view('user.auth.login');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {   
     
        return Auth::guard('user');
    }

    // protected function validateLogin(Request $request)
    // {
    //     $this->validate($request, [
    //         $this->username() => 'required|exists:users,' . $this->username() . ',verified,1',
    //         'password' => 'required',
    //     ], [
    //         $this->username() . '.exists' => 'The email address or password is incorrect. Please try again'
    //     ]);
    // }

    public function login(Request $request)
    {

        if (
            Auth::guard('user')->attempt(['email' => request('email'), 'password' => request('password')])
        ){
            $user = User::where('email', request('email'))->first();   

            Auth::login($user);
            if(request('redirectulr')) {
                return redirect(request('redirectulr'));
            } else {
                return redirect('/user/my-account');
            }
            
        }

        return redirect()->back()->with(['message' => 'Email or password is incorrect']);
    }
}
