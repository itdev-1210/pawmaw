<?php

namespace App\Http\Controllers\UserAuth;

use App\Exceptions\AppErrorException;
use App\Mail\UserCreated;
use App\Rules\EmailExitsWorld;
use App\Rules\ReCaptchaRule;
use App\User;
use App\Models\PetsInfo;
use App\Models\PetAdresses;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/user/next';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->validEmailCheck(); 
        $this->middleware('user.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {   
        
        if($data['request'] == 1){
            return Validator::make($data, [
                'email' => ['required', 'email','max:255','unique:users', new EmailExitsWorld],
                'password' => 'required|min:6',
                'pet_photo' => 'mimes:jpeg,jpg,png,gif|required|max:5120'
            ]);
        }else{
            return Validator::make($data, [
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|min:6|confirmed',
                'g-recaptcha-response' => new ReCaptchaRule
            ]);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {   
        if($data['request'] == 1){
        	
        	if(!empty($data['pet_photo'])){
        		if (!in_array(strtolower($data['pet_photo']->getClientOriginalExtension()), array('jpg', 'jpeg', 'png', 'gif'))) {
        		
        		}
        	}
            $info = new PetsInfo;
            $data['name'] = $data['name'] ? $data['name'] : 'Unknown';
            $maxOrder = PetsInfo
                ::orderBy('order_priority', 'DESC')
                ->first();

            $maxOrder = $maxOrder
                ? $maxOrder->order_priority
                : 0;

            $info->type = $data['action'];
            $info->name = $data['name'];

            $info->street = $data['street'];
            $info->postal = $data['postal'];
            $info->last_seen = $data['address'];
            $info->contact_email = $data['email'];
            $info->full_inf = 0;
            $info->order_priority = $maxOrder;
            $info->date_list_update = date("Y-m-d H:i:s");
    
            if($data['name'] != null){
                $temp = Str::slug($data['name'], '-');
            } else{
                $temp = Str::slug('found pet', '-');
            }

            if(!PetsInfo::all()->where('slug',$temp)->isEmpty()){
                $i = 1;
                $newslug = $temp . '-' . $i;
                while(!PetsInfo::all()->where('slug',$newslug)->isEmpty()){
                    $i++;
                    $newslug = $temp . '-' . $i;
                }
                $temp =  $newslug;
            }
            
            $info->slug = $temp;

            $info->save();

            PetAdresses::insert([
                'pet_info_id'    => $info->id,
                'street_address' => @request('street'),
                'city'           => @request('locality'),
                'state'          => @request('administrative_area_level_1'),
                'country'        => @request('country'),
                'zip_code'       => @request('postal'),
                'country_short'  => @request('country_short'),
                'lat'            => @request('lat'),
                'lng'            => @request('lng')
            ]);

            if(!empty($data['pet_photo'])){
                $destinationPath = public_path('/uploads/pets_image');
                $file = $data['pet_photo'];
                $ext = $data['pet_photo']->getClientOriginalExtension();
                $fileName = $data['action'].'_'.$info->id.'.'.$ext;
                
                if(file_exists($destinationPath.'/'.$fileName)){
                     @unlink($destinationPath.'/'.$fileName);
                }

                $file->move($destinationPath, $fileName);

                $info->photo = $fileName;
                $info->update();

                $this->checkOnRotate($destinationPath . '/' . $fileName);
            }

            if(!auth()->user()){
                $user =  User::create([
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'verified' => User::UNVERIFIED_USER,
                    'verification_token' => User::generateVerificationCode(),
                    'name' => $data['name'],
                    'phone' => $data['phone'],
                    'zip' => $data['postal'],
                ]);
                $info->user_id = $user->id;
                $info->update();

                Auth::login($user);

                //Mail::to($user)->send(new UserCreated($user));
                Session::put('email_success', 1);

                session()->put('new_user', 1);
            } else{
                $info->user_id = auth()->user()->id;
                $info->update();
            }

            return $user;
        } else{

            $user =  User::create([
                'name' => $data['name'],
                'phone' => $data['phone'],
                'zip' => $data['postal'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'verified' => User::UNVERIFIED_USER,
                'verification_token' => User::generateVerificationCode(),
            ]);
            //Mail::to($user)->send(new UserCreated($user));
            Session::put('email_success', 1);

            Auth::login($user);

            session()->put('new_user', 1);

            return $user;
        }


    }
    
    public function validEmailCheck() {
             
             $email = Input::get('email');
             $type = Input::get('request');
             if(!empty($email)) {
             $url = 'https://api.thechecker.co/v2/verify?email='.$email.'&api_key=f98685e55023ddafbb81dbcd5d6f060c613bd692631ce9b1a05503bbb697fc53';
        
             $ch = curl_init();
             curl_setopt($ch, CURLOPT_URL, $url);
             curl_setopt($ch, CURLOPT_POST, 0);
             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
             $response = curl_exec ($ch);
             curl_close ($ch);
             
             $result = json_decode($response);
   
                 if($result->result == 'deliverable') {
               
                     return 1;
                     
                 } else {
               
                        if($type == 1){ 
                           
                            header('Location:'.url('/?invalidemail=1'));
                            exit;
                            
                        } else {
                          
                           header('Location:'.url('user/register?invalidemail=1'));
                           exit;
                        }
                 }
             }
            
     }
     
    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('user.auth.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('user');
    }

    private function checkOnRotate($image)
    {
        $exif = @exif_read_data($image);

        if(!@$exif['Orientation'])
            return;

        $orientation = $exif['Orientation'];
        $source = @imagecreatefromjpeg($image);

        if(!$source){
            $source = imagecreatefromstring(file_get_contents($image));
        }

        switch($orientation){
            case 3:
                $rotate = imagerotate($source, 90, 0);
                break;
            case 6:
                $rotate = imagerotate($source, 270, 0);
            break;
        }

        if(@$rotate)
            imagejpeg(@$rotate, $image);
        else
            return;
    }
}
