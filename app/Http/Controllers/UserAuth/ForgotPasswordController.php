<?php

namespace App\Http\Controllers\UserAuth;

use App\Exceptions\AppErrorException;
use App\Http\Controllers\Controller;
use App\Mail\UserCreated;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user.guest');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('user.auth.passwords.email');
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('users');
    }

    public function resendActivationCode()
    {

        return view('user.auth.passwords.resend');
    }
    public function resendActivationCodePost(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ]);
        $user = User::where('email',$request->email)->first();
        if($user==null){
            throw new AppErrorException("user not found");
        }
        if($user->verified==User::VERIFIED_USER){
            throw new AppErrorException("user already verified");
        }
        $user->verification_token = User::generateVerificationCode();
        $user->save();
        Mail::to($user)->send(new UserCreated($user));

        return redirect('/')->with('status', 'New email varification link send your account');
    }


}
