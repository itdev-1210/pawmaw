<?php

namespace App\Http\Controllers\Front;

use App\Models\Payment;
use App\User;
use App\Models\PetsInfo;
use App\Models\PetAdresses;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use PDF;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Image;
use DB;
use App\Models\EmailTime;
use App\Models\PetsEmail;

//use Mail;

class UserAccountController extends Controller
{
    public function __construct()
    {
        //$this->middleware( 'user' );
    }

    public function index()
    {
        $user = Auth::guard('user')->user();
        $users = Auth::guard('user')->user();
        $pets = PetsInfo::where('user_id', $user->id)->orderBy('id', 'desc')->get();
        $pets_email = '';
        foreach ($pets as $key => $row) {
            $pet_id = $row->id;
            $pets_email = PetsEmail::where('pet_id', $pet_id)->get();
            $pets[$key]['fb_share'] = count($pets_email) ? $pets_email[0]->viewfacebookpost : '';
        }

        return view('user.home', compact('pets', 'users', 'pets_email'));
    }

    public function petDetails($id)
    {
        $pet = PetsInfo::getPetInformationBySlug($id);
        if (!$pet) {
            return response()->view('errors.404', [], 404);
        }
        $check = PetsInfo::checkUserPetId(Auth::guard('user')->user()->id, $pet->id);
        if (!$check) {
            return redirect('user/login');
        }
        $payments = $pet->payments->where('updated_at', '>', date('Y-m-d', strtotime('-3 days')))->sortByDesc('updated_at')->toArray();
        $payments = array_values($payments);

        return view('user.details', compact('pet', 'payments'));
    }

    public function download_flayer($id)
    {
        if (!Auth::guard('user')->user()) {
            return redirect('user/login?redirecturl='.url()->current());
        }

        $check = PetsInfo::checkUserPetId(Auth::guard('user')->user()->id, $id);
        if (!$check) {
            return redirect('user/login');
        }

        $pet = PetsInfo::getPetInformationById($id);
        if (!$pet) {
            return response()->view('errors.404', [], 404);
        }
        $payments = $pet->payments->where('updated_at', '>', date('Y-m-d', strtotime('-3 days')))->sortByDesc('updated_at')->toArray();
        $payments = array_values($payments);

        return view('user.flayer', compact('pet', 'payments'));
    }

    public function alerts($id)
    {
        if (!Auth::guard('user')->user()) {
            return redirect('user/login?redirecturl='.url()->current());
        }
        $check = PetsInfo::checkUserPetId(Auth::guard('user')->user()->id, $id);
        if (!$check) {
            return redirect('user/login');
        }
        $pet = PetsInfo::getPetInformationById($id);
        if (!$pet) {
            return response()->view('errors.404', [], 404);
        }

        $payments = $pet->payments->where('updated_at', '>', date('Y-m-d', strtotime('-3 days')))->sortByDesc('updated_at')->toArray();
        $payments = array_values($payments);

        return view('user.alerts', compact('pet', 'payments'));
    }

    public function faq($id)
    {
        $check = PetsInfo::checkUserPetId(Auth::guard('user')->user()->id, $id);
        if (!$check) {
            return redirect('user/login');
        }
        $pet = PetsInfo::getPetInformationById($id);
        if (!$pet) {
            return response()->view('errors.404', [], 404);
        }

        return view('user.faq', compact('pet'));
    }

    public function facebook_tools($id)
    {
        $check = PetsInfo::checkUserPetId(Auth::guard('user')->user()->id, $id);
        if (!$check) {
            return redirect('user/login');
        }
        $pet = PetsInfo::getPetInformationById($id);
        if (!$pet) {
            return response()->view('errors.404', [], 404);
        }
        $petAddress = DB::table('pet_adresses')->where('pet_adresses.pet_info_id', $pet->id)->first();
        $fbData = $this->getFacebookPageByName($petAddress->country);
        if (!$fbData) {
            $fbData = $this->getFacebookPage($pet->zip_code);
        }
        $facebookShare = PetsEmail::where('pet_id', $pet->id)->orderBy('id', 'desc')->first();
        $payments = $pet->payments->where('updated_at', '>', date('Y-m-d', strtotime('-3 days')))->sortByDesc('updated_at')->toArray();
        $payments = array_values($payments);

        return view('user.facebook_tools', compact('pet', 'payments', 'fbData', 'facebookShare'));
    }

    public function join_network($id = null)
    {
        $check = PetsInfo::checkUserPetId(Auth::guard('user')->user()->id, $id);
        if (!$check) {
            return redirect('user/login');
        }
        $pet = PetsInfo::getPetInformationById($id);
        if (!$pet) {
            return response()->view('errors.404', [], 404);
        }
        $payments = $pet->payments->where('updated_at', '>', date('Y-m-d', strtotime('-3 days')))->sortByDesc('updated_at')->toArray();
        $payments = array_values($payments);

        if (@$_GET['zip_code']) {
            $facebook = DB::table('facebook_pages')
            ->leftJoin(
                'facebook_zip_codes', 'facebook_zip_codes.facebook_id', 'facebook_pages.id'
                )
                ->where('facebook_zip_codes.zip_code', $_GET['zip_code'])
                ->first();
        }

        return view('user.join_network', compact('pet', 'payments', 'facebook', 'id'));
    }

    public function join_network_without_map()
    {
        if (@$_GET['zip_code']) {
            $facebook = DB::table('facebook_pages')
            ->leftJoin(
                'facebook_zip_codes', 'facebook_zip_codes.facebook_id', 'facebook_pages.id'
                )
                ->where('facebook_zip_codes.zip_code', $_GET['zip_code'])
                ->first();
        }

        return view('user.join_network-without-map', compact('facebook'));
    }

    public function update_listing($id)
    {
        if (!Auth::guard('user')->user()) {
            return redirect('user/login?redirecturl='.url()->current());
        }
        $check = PetsInfo::checkUserPetId(Auth::guard('user')->user()->id, $id);
        if (!$check) {
            return redirect('user/login');
        }
        $pet = PetsInfo::getPetInformationById($id);
        $payments = $pet->payments->where('updated_at', '>', date('Y-m-d', strtotime('-3 days')))->sortByDesc('updated_at')->toArray();
        $payments = array_values($payments);

        return view('user.update_listing', compact('pet', 'payments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function report()
    {
        $user = Auth::guard('user')->user();

        $isMobile = $this->isMobile();

        return view('user.report', compact('user', 'isMobile'));
    }

    public function history()
    {
        $user = Auth::guard('user')->user();

        $payment = Payment::where('user_id', $user->id)->orderBy('id', 'desc')->get();

        return view('user.history', compact('user', 'payment'));
    }

    public function promote($id)
    {
        $user = Auth::guard('user')->user();
        $petinfo = PetsInfo::find($id);

        return view('user.promote', compact('user', 'petinfo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store_report(Request $request)
    {
        $this->validate($request, [
            'pet_photo' => 'mimes:jpeg,jpg,png,gif|required|max:5120',
         ]);

        $required = [
            'administrative_area_level_1',
            'street',
            'locality',
            'country',
            'postal',
            'country_short',
            'lat',
            'lng',
        ];

        foreach ($required as $req) {
            $val = $request->get($req);
            if (empty($val)) {
                //var_dump($req);exit();
                return redirect()->action('Front\UserAccountController@report')->with('error', 'Fields must be filled!');
            }
        }

        $maxOrder = PetsInfo
            ::orderBy('order_priority', 'DESC')
            ->first();

        $request->name = $request->name ? $request->name : 'Unknown';

        $maxOrder = $maxOrder
            ? $maxOrder->order_priority
            : 0;

        $user = Auth::guard('user')->user();

        $info = new PetsInfo();
        $info->type = $request->action;
        $info->user_id = $user->id;
        $info->name = $request->name;
        $info->last_seen = (string) $request->address;
        $info->contact_email = $user->email;
        $info->postal = $request->postal;
        $info->postal = $request->postal;
        $info->street = $request->street;
        $info->order_priority = $maxOrder;
        $info->date_list_update = date('Y-m-d H:i:s');
        $info->full_inf = 0;

        if ($request->name != null) {
            $temp = Str::slug($request->name, '-');
        } else {
            $temp = Str::slug('found pet', '-');
        }

        $maxSlugNumber = 0;
        $petResults = PetsInfo::where('slug', 'like', $temp.'-%')->orderBy('id', 'desc')->get();
        if (!$petResults->isEmpty()) {
            $slugNumbers = array();
            foreach ($petResults->all() as $petResult) {
                if (preg_match('/'.preg_quote($temp, '/').'-([0-9]+)$/', $petResult->slug, $matches)) {
                    $slugNumbers[] = $matches[1];
                }
            }
            if (count($slugNumbers)) {
                $maxSlugNumber = max($slugNumbers);
            }
        }

        if (!PetsInfo::all()->where('slug', $temp)->isEmpty()) {
            $i = $maxSlugNumber + 1;
            $newslug = $temp.'-'.$i;
            while (!PetsInfo::all()->where('slug', $newslug)->isEmpty()) {
                ++$i;
                $newslug = $temp.'-'.$i;
            }
            $temp = $newslug;
        }
        $info->slug = $temp;
        $info->user_id = $user->id;
        $info->save();

        if (@$request->phone) {
            User
                ::where('id', Auth::guard('user')->user()->id)
                ->update([
                              'phone' => $request->phone,
                          ]);
        }

        PetAdresses::insert([
                                 'pet_info_id' => $info->id,
                                 'street_address' => @request('street'),
                                 'city' => @request('locality'),
                                 'state' => @request('administrative_area_level_1'),
                                 'country' => @request('country'),
                                 'zip_code' => @request('postal'),
                                 'country_short' => @request('country_short'),
                                 'lat' => @request('lat'),
                                 'lng' => @request('lng'),
                             ]);

        if (!empty($request->pet_photo)) {
            $destinationPath = public_path('/uploads/pets_image');
            $file = $request->pet_photo;
            $ext = $request->pet_photo->getClientOriginalExtension();
            $fileName = $request['action'].'_'.$info->id.'.'.$ext;

            if (file_exists($destinationPath.'/'.$fileName)) {
                @unlink($destinationPath.'/'.$fileName);
            }

            try {
                $file->move($destinationPath, $fileName);
            } catch (Exception $e) {
                return redirect()->action('Front\UserAccountController@reg_next')->with('error', $e->getMessage());
            }

            $info->photo = $fileName;
            $info->update();

            $this->checkOnRotate($destinationPath.'/'.$fileName);
        }

        return redirect()->action('Front\UserAccountController@reg_next')->with('success', 'Successfully Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::guard('user')->user();
        $pet = PetsInfo::getPetInformationById($id);
        if (!$pet) {
            return response()->view('errors.404', [], 404);
        }
        $payments = $pet->payments->where('updated_at', '>', date('Y-m-d', strtotime('-3 days')))->sortByDesc('updated_at')->toArray();
        $payments = array_values($payments);

        return view('user.edit-report', compact('pet', 'user', 'payments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::guard('user')->user();
        $info = PetsInfo::find($id);
        $info->type = $request->type;
        $info->user_id = $user->id;
        $info->specie = $request->specie;
        $info->color = $request->color;
        $info->contact_email = $user->email;
        $info->date = $request->lost_found_date;
        $info->gender = $request->sex;
        $info->description = $request->description;
        if ($request->action == 'found') {
            $info->circumstance = $request->circumstances;
        }
        $info->breed = $request->breed;

        $info->update();

        // if($request->pet_photo != ''){
        //     $destinationPath =   public_path('/uploads/pets_image');
        //     $file = $request->pet_photo;
        //     $ext = $request->pet_photo->getClientOriginalExtension();
        //     $fileName = $request->action.$info->id.'.'.$ext;
        //     if(file_exists($destinationPath.'/'.$fileName)){
        //         @unlink($destinationPath.'/'.$fileName);
        //     }
        //     $file->move($destinationPath, $fileName);
        //     $info->photo = $fileName;
        //     $info->update();
        // }

        return redirect()->action('Front\UserAccountController@index')->with('success', 'Pet Details Updated Succesfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $petinfo = PetsInfo::find($id);
        $petinfo::whereId($id)->delete();
        Mail::to($pet->contact_email)->send(new DeletePost());

        return redirect()->action('Front\UserAccountController@index')->with('success', 'Successfully Deleted!');
    }

    public function pdf($id)
    {
        $data['petinfo'] = PetsInfo::getPetInformationById($id);

        $pdf = PDF::loadView('user.layout.pdf', $data);

        return $pdf->download();
    }

    public function pdfTest($id)
    {
        $data['petinfo'] = PetsInfo::with(['user'])->find($id);

        return view('user.layout.pdf', $data);
    }

    public function reunited($id)
    {
        $petinfo = PetsInfo::find($id);

        if (file_exists(public_path('uploads/converting-image/pet-'.$id.'.png'))) {
            unlink(public_path('uploads/converting-image/pet-'.$id.'.png'));
        }
        if (file_exists(public_path('uploads/converting-image/pet-big-'.$id.'.png'))) {
            unlink(public_path('uploads/converting-image/pet-big-'.$id.'.png'));
        }

        $petinfo->status = 1;
        $petinfo->type = 'reunited';
        $petinfo->reunited_date = time();
        $petinfo->update();

        return redirect()->action('Front\UserAccountController@index')->with('success', 'Successfully Reunited!');
    }

    public function showChangePasswordForm()
    {
        $user = Auth::guard('user')->user();

        return view('user.change-password', compact('user'));
    }

    public function edit_profile()
    {
        $user = Auth::guard('user')->user();

        /*if(Session::has('user_email')){
            $info = PetsInfo::where('contact_email',Session::get('user_email'))->first();

            $temp = PetsInfo::find($info->id);
            $temp->user_id = $user->id;
            $temp->update();

            Session::forget('user_email');
        }*/

        return view('user.edit-profile', compact('user'));
    }

    public function reg_next()
    {
        $user = Auth::guard('user')->user();
        if (!$user) {
            return redirect('/');
        }
        $info = PetsInfo::getPetInformationForNextStep($user->id);
        if (empty($info)) {
            return redirect()->action('Front\UserAccountController@index')->with('error', 'Post Incomplete!');
        }

        return view('user.reg_next', compact('user', 'info'));
    }

    public function updatepetinfo(Request $request)
    {
        $user = Auth::guard('user')->user();
        if (empty($request->pet_id)) {
            return redirect('/my-account');
        }
        $pet = PetsInfo::find($request->pet_id);
        $pet->specie = $request->specie;
        $pet->color = $request->color;
        $pet->breed = $request->breed;
        $pet->date = $request->lost_found_date;
        $pet->gender = $request->sex;
        $pet->description = $request->description;
        $pet->full_inf = 1;

        $pet->update();
        $mapImageName = $request->pet_id.'.png';
        if (!empty($request->mapUrl)) {
            $destinationPath = public_path('/uploads/maps_image/');
            $file = $request->mapUrl;
            $fileDirectory = $destinationPath.$mapImageName;
            $image = file_get_contents($file);
            $fp = fopen($fileDirectory, 'w+');

            fputs($fp, $image);
            fclose($fp);
            unset($image);
        }

        DB::table('pet_adresses')
                ->where('pet_info_id', $request->pet_id)
                ->update(['map_img' => $mapImageName]);

        $this->sendEmail($pet);

        return view('user.package', compact('user', 'pet'));

        //return redirect()->action('Front\UserAccountController@index')->with('success','Info updated successfully!');
    }

    public function sendEmail($pet)
    {
        // Type 1 : One time Email
        $this->setScheduledMails($pet, 'flyer', 1, 14400);
        $this->setScheduledMails($pet, 'scam', 1, 21600);
        if ($pet->type == 'lost') {
            $this->setScheduledMails($pet, 'lost', 1, 36000);
        } else {
            $this->setScheduledMails($pet, 'found', 1, 36000);
        }
        // Type 2 : Repetitive
        $this->setScheduledMails($pet, 'package', 2, 43200);
        $this->setScheduledMails($pet, 'missing', 2, 172800);
    }

    public function setScheduledMails($pet, $model, $type, $delay)
    {
        $pet->model = $model;
        $timedMail = new EmailTime();
        $timedMail->object = json_encode($pet);
        $timedMail->type = $type;
        $timedMail->user_id = $pet->user_id;
        $timedMail->pet_id = $pet->id;
        $timedMail->time = time() + $delay;
        $timedMail->save();
    }

    /*   public function postOnFacebook($petInfo) {
          $petAddress = DB::table('pet_adresses')->where('pet_adresses.pet_info_id', $petInfo->id)->first();
          $fbData = $this->getFacebookPageByName($petAddress->country);
          if (! $fbData) {
              $fbData = $this->getFacebookPage($petInfo->postal);
          }
          if(!$fbData){
              $accessToken = env('FACEBOOK_ACCESS');
              $pageId = env('FACEBOOK_PAGE');
          }else{
          $accessToken = $fbData->access_token;
          $pageId = $fbData->facebook_id;
          }
          if($petInfo->name){
          $petName =$petInfo->name;
          }else{
           $petName ='This Pet';
          }

          if($petInfo->type=='lost'){
              $dateType='Lost';
              $type='LOST';
              $contact="$petName's owner";
          }else{
              $dateType='Found';
              $type='FOUND';
              $contact='the finder';
          }

          $date = $petInfo->date;
          $address = $petInfo->last_seen;
          $street=$petInfo->street;
          $petDescription = $petInfo->description;
          $species = $petInfo->specie;
          $color = $petInfo->color;
          $gender = $petInfo->gender;
          $breed = $petInfo->breed;
          $slug=$petInfo->slug;

          $pic = "https://pawmaw.com/uploads/converting-image/pet-$petInfo->id.png";

          $attachment = array(
              'access_token' => $accessToken,
              'caption' => "
              Let your nearby people know by SHARE/COMMENT/LIKE " . $petName . " was ".$type." on " . $date . " in " . $address . "

              ➤Pet Description:
              ".$petDescription."

              ➤Information from the Owner:
              Area last seen: " . $address . "
              Cross street:" . $street . "
              Species: " . $species . "
              Color: " . $color . "
              Gender: " . $gender . "
              Breed: " . $breed . "
              ".$dateType." Date: " . $date . "

              ➤To contact ".$contact.", click on the link:
              https://pawmaw.com/pet-details/$slug

              ➤Lost or found a pet? Please report it here:
              https://www.pawmaw.com/report/  ",
              'url' => $pic,
          );
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/' . $pageId . '/photos');
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $attachment);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          $result=curl_exec($ch);
          curl_close($ch);

      } */

    public function update_profile(Request $request)
    {
        $user = Auth::guard('user')->user();

        $change = User::find($user->id);
        $change->fname = $request->fname;
        $change->lname = $request->lname;
        $change->phone = $request->phone;
        $change->address = $request->address;
        $change->update();

        return redirect()->action('Front\UserAccountController@index')->with('success', 'Profile updated successfully!');
    }

    public function changePassword(Request $request)
    {
        if (!(Hash::check($request->get('current-password'), Auth::guard('user')->user()->password))) {
            // The passwords matches
            return redirect()->back()->with('error', 'Your current password does not matches with the password you provided. Please try again.');
        }

        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            return redirect()->back()->with('error', 'New Password cannot be same as your current password. Please choose a different password.');
        }

        $validatedData = $request->validate([
                                                 'current-password' => 'required',
                                                 'new-password' => 'required|string|min:6|confirmed',
                                             ]);

        //Change Password
        $user = Auth::guard('user')->user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with('success', 'Password changed successfully !');
    }

    private function checkOnRotate($image)
    {
        $exif = @exif_read_data($image);

        if (!@$exif['Orientation']) {
            return;
        }

        $orientation = $exif['Orientation'];
        $source = @imagecreatefromjpeg($image);

        if (!$source) {
            $source = imagecreatefromstring(file_get_contents($image));
        }

        switch ($orientation) {
            case 3:
                $rotate = imagerotate($source, 90, 0);
                break;
            case 6:
                $rotate = imagerotate($source, 270, 0);
                break;
        }

        if (@$rotate) {
            imagejpeg(@$rotate, $image);
        } else {
            return;
        }
    }

    public function getFacebookPage($zipcode)
    {
        $facebook = DB::table('facebook_pages')->leftJoin('facebook_zip_codes', 'facebook_zip_codes.facebook_id', 'facebook_pages.id')
            ->select('facebook_pages.*')
            ->where('facebook_zip_codes.zip_code', $zipcode)
            ->first();

        return $facebook;
    }

    public function getFacebookPageByName($country)
    {
        $facebook = DB::table('facebook_pages')->where('facebook_pages.country', $country)->first();

        return $facebook;
    }

    public function isMobile()
    {
        $useragent = $_SERVER['HTTP_USER_AGENT'];

        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
            return true;
        } else {
            return false;
        }
    }
}
