<?php

namespace App\Http\Controllers\Front;

use App\Mail\ContactEmail;
use App\Mail\OwnerEmail;
use App\Models\Contact;
use App\Models\OwnersEmail;
use App\Models\PetsInfo;
use App\Models\PetAdresses;
use App\Rules\ReCaptchaRule;
use App\BlogPost;
use App\PagePost;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Image;
use App\Models\PetsEmail;

DB::enableQueryLog();

class SiteController extends Controller
{
    public function index($pageSlug = false)
    {
        $pets = PetsInfo
                ::with(['user'])
                ->where('type', '!=', 'reunited')
                ->where('status', '=', '1')
                ->where('full_inf', '=', '1')
                ->orderBy('pets_info.order_priority', 'DESC')
                ->orderBy('id', 'desc')
                ->take(9)
                ->get();
        // dd(DB::getQueryLog());exit;
        $posts = BlogPost::orderBy('id', 'DESC')->limit(3)->get();
        $metaData = [
            'title' => 'PawMaw - Lost and Found Cats, Dogs, and Pets Rescue Service',
              'description' => 'Helping lost pets to get back to their family. Report lost pets on the website. Print Lost Pet flyers, send an alert on the area where your pet went missing & find your missing pet quickly.',
            //'description' => "If your pet goes missing and the best ways to get help is create a free listing with PawMaw.Search for Lost/Found pets in your area. PawMaw can help you find your lost pet nationwide.",
            'image' => url('pawmaw/img/fevi.png'),
        ];
        $page = null;
        if ($pageSlug) {
            $page = PagePost::where('slug', $pageSlug)->first();
            if (!$page) {
                return response()->view('errors.404', [], 404);
            } else {
                $metaData['title'] = $page->metatitle;
                $metaData['description'] = $page->metadesc;
            }
        }
        $isMobile = $this->isMobile();

        return view('pawmawFront.site.index', compact('pets', 'posts', 'metaData', 'page', 'isMobile'));
    }

    public function setAddressData()
    {
        $pets = PetsInfo::offset(3000)->limit(500)->get();
        $street_address = null;

        foreach ($pets as $index => $pet) {
            $data = [
                'address' => $pet['last_seen'],
                'key' => 'AIzaSyDLiWS9-LYSJbnYpL-TvgHL-kWMYtSK3Gk',
            ];

            $query = http_build_query($data);
            $autocomplete = json_decode(@file_get_contents(
                            'https://maps.googleapis.com/maps/api/geocode/json?'
                            .$query
                    ), true);

            $addressDetails = @$autocomplete['results'][0]['address_components'];
            $addressInf = [];

            if (!$addressDetails) {
                continue;
            }

            foreach ($addressDetails as $details) {
                $key = $details['types'][0];

                if ($key == 'country') {
                    $addressInf[$key] = $details['long_name'];
                    $addressInf[$key.'_short'] = $details['short_name'];
                } else {
                    $addressInf[$key] = $details['long_name'];
                }
            }

            if (@$addressInf['street_number']) {
                $street_address = $addressInf['street_number'];
            }

            if ($street_address) {
                $street_address .= ', '.@$addressInf['route'];
            } else {
                $street_address = @$addressInf['route'];
            }

            $databaseData = [
                'pet_info_id' => $pet['id'],
                'street_address' => @$street_address,
                'city' => @$addressInf['locality'],
                'state' => @$addressInf['administrative_area_level_1'],
                'country' => @$addressInf['country'],
                'zip_code' => @$addressInf['postal_code'],
                'country_short' => @$addressInf['country_short'],
                'lat' => @$autocomplete['results'][0]['geometry']['location']['lat'],
                'lng' => @$autocomplete['results'][0]['geometry']['location']['lng'],
            ];

            PetAdresses::insert($databaseData);
            Petsinfo::where('id', $pet['id'])->update([
                'street' => $street_address,
            ]);
        }
    }

    public function putFacebookData()
    {
        return;

        $facebookName = 'Lost & Found Pets -  District of Columbia';
        $zipCodes = '';

        $zipCodes = explode(',', $zipCodes);

        $id = DB::table('facebook_pages')->insertGetId(['name' => $facebookName]);

        foreach ($zipCodes as $key => $value) {
            DB
                    ::table('facebook_zip_codes')
                    ->insert([
                        'zip_code' => $value,
                        'facebook_id' => $id,
            ]);
        }

        dd('good');
    }

    public function reunited(Request $request)
    {
        $pets = PetsInfo
                ::where('type', 'reunited')
                ->where('status', '=', '1')
                ->orderBy('updated_at', 'desc')
                ->paginate(10);

        return view('pawmawFront.site.reunited', compact('pets'));
    }

    public function petDetails($id)
    {
        $pet = PetsInfo::getPetInformationBySlug($id);
        if (!$pet) {
            return response()->view('errors.404', [], 404);
        }
        $petAddress = PetAdresses::where('pet_info_id', $pet->id)->first();
        $fbpages = array();
        if ($petAddress) {
            $fbpages = $this->getFacebookPageByName($petAddress->country);
        }

        if (!$fbpages) {
            $fbpages = $this->getFacebookPage($pet->zip_code);
        }
        $facebookShare = PetsEmail::where('pet_id', $pet->id)->orderBy('id', 'desc')->first();
        $metaData = [
            'title' => ($pet->name == null ? "Found Pet - A {$pet->gender} {$pet->specie} ".ucfirst($pet->type)." - {$pet->last_seen} - PawMaw" : ucfirst($pet->name)." - A {$pet->gender} {$pet->specie} ".ucfirst($pet->type)." - {$pet->last_seen} - PawMaw"),
            'description' => "{$pet['name']} a {$pet['gender']} {$pet['specie']} {$pet['type']} in {$pet['city']}, {$pet['state']} {$pet->zip_code} on {$pet->date}, Colour: {$pet->color}, Breed: {$pet['breed']}, Pet description: {$pet->description}",
            'image' => url('uploads/pets_image/'.$pet['photo']),
        ];

        $metaData['facebook'] = [
            'title' => 'HELP! '.ucfirst($pet->name).' is '.ucfirst($pet->type),
            'description' => "A {$pet['gender']} {$pet['specie']} {$pet->type} in ({$pet['city']}, {$pet['state']}), {$pet->zip_code}",
        ];

        if ($pet->type == 'reunited') {
            $metaData['facebook']['title'] = 'Hurray! '.ucfirst($pet->name).' is '.ucfirst($pet->type);
        }

        $pets_email = PetsEmail::where('pet_id', $pet->id)->get();
        $pet['fb_share'] = count($pets_email) ? $pets_email[0]->viewfacebookpost : '';

        $ismobile = $this->isMobile();

        return view('pawmawFront.site.pet_details', compact('pet', 'metaData', 'fbpages', 'facebookShare', 'ismobile'));
    }

    public function mapWaterMark($id)
    {
        $pet = PetsInfo::getPetInformationBySlug($id);
        if (!$pet) {
            return response()->view('errors.404', [], 404);
        }
        $petAddress = DB::table('pet_adresses')->where('pet_adresses.pet_info_id', $pet->id)->first();
        $fbpages = array();
        if ($petAddress) {
            $fbpages = $this->getFacebookPageByName($petAddress->country);
        }
        if (!$fbpages) {
            $fbpages = $this->getFacebookPage($pet->zip_code);
        }
        $facebookShare = PetsEmail::where('pet_id', $pet->id)->orderBy('id', 'desc')->first();
        $metaData = [
            'title' => ($pet->name == null ? "Found Pet - A {$pet->gender} {$pet->specie} ".ucfirst($pet->type)." - {$pet->last_seen} - PawMaw" : ucfirst($pet->name)." - A {$pet->gender} {$pet->specie} ".ucfirst($pet->type)." - {$pet->last_seen} - PawMaw"),
            'description' => "{$pet['name']} a {$pet['gender']} {$pet['specie']} {$pet['type']} in {$pet['city']}, {$pet['state']} {$pet->zip_code} on {$pet->date}, Colour: {$pet->color}, Breed: {$pet['breed']}, Pet description: {$pet->description}",
            'image' => url('uploads/pets_image/'.$pet['photo']),
        ];

        $metaData['facebook'] = [
            'title' => 'HELP! '.ucfirst($pet->name).' is '.ucfirst($pet->type),
            'description' => "A {$pet['gender']} {$pet['specie']} {$pet->type} in ({$pet['city']}, {$pet['state']}), {$pet->zip_code}",
        ];

        if ($pet->type == 'reunited') {
            $metaData['facebook']['title'] = 'Hurray! '.ucfirst($pet->name).' is '.ucfirst($pet->type);
        }
        $ismobile = $this->isMobile();
        $mapFlag = 1;

        return view('pawmawFront.site.image_watermark', compact('pet', 'metaData', 'fbpages', 'facebookShare', 'ismobile', 'mapFlag'));
    }

    public function imageWaterMark($id)
    {
        $pet = PetsInfo::getPetInformationBySlug($id);
        if (!$pet) {
            return response()->view('errors.404', [], 404);
        }
        $petAddress = DB::table('pet_adresses')->where('pet_adresses.pet_info_id', $pet->id)->first();
        $fbpages = array();
        if ($petAddress) {
            $fbpages = $this->getFacebookPageByName($petAddress->country);
        }
        if (!$fbpages) {
            $fbpages = $this->getFacebookPage($pet->zip_code);
        }
        $facebookShare = PetsEmail::where('pet_id', $pet->id)->orderBy('id', 'desc')->first();
        $metaData = [
            'title' => ($pet->name == null ? "Found Pet - A {$pet->gender} {$pet->specie} ".ucfirst($pet->type)." - {$pet->last_seen} - PawMaw" : ucfirst($pet->name)." - A {$pet->gender} {$pet->specie} ".ucfirst($pet->type)." - {$pet->last_seen} - PawMaw"),
            'description' => "{$pet['name']} a {$pet['gender']} {$pet['specie']} {$pet['type']} in {$pet['city']}, {$pet['state']} {$pet->zip_code} on {$pet->date}, Colour: {$pet->color}, Breed: {$pet['breed']}, Pet description: {$pet->description}",
            'image' => url('uploads/pets_image/'.$pet['photo']),
        ];

        $metaData['facebook'] = [
            'title' => 'HELP! '.ucfirst($pet->name).' is '.ucfirst($pet->type),
            'description' => "A {$pet['gender']} {$pet['specie']} {$pet->type} in ({$pet['city']}, {$pet['state']}), {$pet->zip_code}",
        ];

        if ($pet->type == 'reunited') {
            $metaData['facebook']['title'] = 'Hurray! '.ucfirst($pet->name).' is '.ucfirst($pet->type);
        }
        $ismobile = $this->isMobile();

        $mapFlag = 0;

        return view('pawmawFront.site.image_watermark', compact('pet', 'metaData', 'fbpages', 'facebookShare', 'ismobile'.'mapFlag'));
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $name = $request->name;
        $status = $request->status;
        $address = $request->address;
        $species = $request->species;
        $cityAlias = config('cityAlias');
        $additionalAddress = null;
        if (@$request->address) {
            foreach ($cityAlias as $key => $value) {
                if (strtoupper($key) == strtoupper($request->address)) {
                    $additionalAddress = $key;
                    break;
                } elseif (strtoupper($value) == strtoupper($request->address)) {
                    $additionalAddress = $key;
                    break;
                }
            }
        }

        if ($search != 1) {
            $status = array(
                'lost',
                'found',
            );
        }

        if (!@$request->status) {
            $status = array(
                'lost',
                'found',
            );
        }

        //echo'$additionalAddress<pre>'; print_r($additionalAddress); exit;
        $pets = PetsInfo::select('pets_info.*')->when(@$request->name, function ($query) use ($request) {
            if (is_numeric($request->name)) {
                $query->where('pets_info.id', $request->name);
            } else {
                $query->where('pets_info.name', 'like', "%{$request->name}%");
            }
        })
            ->when(@$request->species, function ($query) use ($request) {
                $query->where('pets_info.specie', $request->species);
            })
            ->when(@$request->address, function ($query) use ($request, $additionalAddress) {
                if ($additionalAddress) {
                    $query->leftJoin('pet_adresses', 'pet_adresses.pet_info_id', 'pets_info.id')
                    ->where('pet_adresses.state', $additionalAddress)
                    ->orderBy('pet_adresses.pet_info_id', 'DESC');
                }            // else if($additionalAddress){
                // $query
                // ->leftJoin(
                // 'pet_adresses',
                // 'pet_adresses.pet_info_id',
                // 'pets_info.id'
                // )
                // ->where('pet_adresses.state', $additionalAddress);
                // }
                elseif (strlen($request->address) > 3 && is_numeric($request->address)) {
                    $query->addSelect(DB::raw("
                            ABS(pet_adresses.zip_code * (pet_adresses.zip_code - {$request->address})) as zip_order
                        "))
                    ->leftJoin('pet_adresses', 'pet_adresses.pet_info_id', 'pets_info.id')
                    ->where('pet_adresses.zip_code', '>=', $request->address - 100)
                    ->where('pet_adresses.zip_code', '<=', $request->address + 100)
                    ->orderBy('pet_adresses.pet_info_id', 'DESC');
                } else {
                    $query->where('pets_info.last_seen', 'like', '%'.$request->address.'%');
                }
            })
            ->when(!empty($status), function ($query) use ($status) {
                $query->whereIn('pets_info.type', $status);
                if (in_array('reunited', $status)) {
                    $query->orderBy('pets_info.reunited_date', 'desc');
                    $query->orderBy('pets_info.order_priority', 'desc');
                } else {
                    $query->orderBy('pets_info.order_priority', 'desc');
                    $query->orderBy('pets_info.id', 'desc');
                }
            })
            ->where('pets_info.status', '=', '1')
            ->where('pets_info.full_inf', '=', '1')
            ->paginate(12);

        $metaData = [
            'title' => 'All Lost Pet Database with lost dogs, cats, and other pets - PawMaw',
            'description' => 'Search lost dogs, cats, and other pets from our PawMaw Lost/Found pet database. Report a lost/found pet in your area. PawMaw can help you find your lost pet quickly.',
            'image' => url('pawmaw/img/fevi.png'),
        ];
        // echo'$request<pre>'; print_r($pets); exit;

        $facebook = DB::table('facebook_pages')->join('facebook_zip_codes', 'facebook_zip_codes.facebook_id', 'facebook_pages.id')
            ->select('facebook_pages.*')
            ->where('facebook_zip_codes.zip_code', $request->address)
            ->first();

        //dd(DB::getQueryLog());exit;
        return view('pawmawFront/site/found', compact('pets', 'name', 'status', 'species', 'address', 'metaData', 'facebook'));
    }

    public function lostSearch(Request $request)
    {
        $search = $request->search;
        $name = $request->name;
        $status = $request->status;
        $address = $request->address;
        $species = $request->species;
        $cityAlias = config('cityAlias');
        $additionalAddress = null;

        if (@$request->address) {
            foreach ($cityAlias as $key => $value) {
                if (strtoupper($value) == strtoupper($request->address)) {
                    $additionalAddress = $key;
                    break;
                }
            }
        }

        $status = array(
            'lost',
        );

        $pets = PetsInfo
                ::select('pets_info.*')
                ->when(@$request->name, function ($query) use ($request) {
                    $query
                    ->where('pets_info.name', 'like', "%{$request->name}%")
                    ->orWhere('pets_info.id', $request->name);
                })
                ->when(@$request->species, function ($query) use ($request) {
                    $query
                    ->where('pets_info.specie', $request->species);
                })
                ->when(@$request->address, function ($query) use ($request, $additionalAddress) {
                    if (strlen($request->address) == 2) {
                        $query
                        ->leftJoin(
                                'pet_adresses', 'pet_adresses.pet_info_id', 'pets_info.id'
                        )
                        ->where('pet_adresses.state', $request->address);
                    } elseif ($additionalAddress) {
                        $query
                        ->leftJoin(
                                'pet_adresses', 'pet_adresses.pet_info_id', 'pets_info.id'
                        )
                        ->where('pet_adresses.state', $additionalAddress);
                    } elseif (
                            strlen($request->address) > 3 && is_numeric($request->address)
                    ) {
                        $query
                        ->addSelect(DB::raw("
                            ABS(pet_adresses.zip_code * (pet_adresses.zip_code - {$request->address})) as zip_order
                        "))
                        ->leftJoin(
                                'pet_adresses', 'pet_adresses.pet_info_id', 'pets_info.id'
                        )
                        ->where('pet_adresses.zip_code', '>=', $request->address - 100)
                        ->where('pet_adresses.zip_code', '<=', $request->address + 100)
                        ->orderBy('pet_adresses.pet_info_id');
                    } else {
                        $query
                        ->where(
                                'pets_info.last_seen', 'like', '%'.$request->address.'%'
                        );
                    }
                })
                ->when(!empty($status), function ($query) use ($status) {
                    $query->whereIn('pets_info.type', $status);
                })
                ->where('pets_info.status', '=', '1')
                ->where('pets_info.full_inf', '=', '1')
                ->orderBy('pets_info.order_priority', 'DESC')
                ->orderBy('pets_info.id', 'desc')
                ->paginate(12);

        $metaData = [
            'title' => 'All Lost Pet Database with lost dogs, cats, and other pets - PawMaw',
            'description' => 'Search lost dogs, cats, and other pets from our PawMaw Lost/Found pet database. Report a lost/found pet in your area. PawMaw can help you find your lost pet quickly.',
            'image' => url('pawmaw/img/fevi.png'),
        ];

        $facebook = DB
                ::table('facebook_pages')
                ->leftJoin(
                        'facebook_zip_codes', 'facebook_zip_codes.facebook_id', 'facebook_pages.id'
                )
                ->where('facebook_zip_codes.zip_code', $request->address)
                ->first();

        return view(
                'pawmawFront/site/found', compact('pets', 'name', 'status', 'species', 'address', 'metaData', 'facebook')
        );
    }

    public function foundSearch(Request $request)
    {
        $search = $request->search;
        $name = $request->name;
        $status = $request->status;
        $address = $request->address;
        $species = $request->species;
        $cityAlias = config('cityAlias');
        $additionalAddress = null;

        if (@$request->address) {
            foreach ($cityAlias as $key => $value) {
                if (strtoupper($value) == strtoupper($request->address)) {
                    $additionalAddress = $key;
                    break;
                }
            }
        }

        $status = array(
            'found',
        );

        $pets = PetsInfo
                ::select('pets_info.*')
                ->when(@$request->name, function ($query) use ($request) {
                    $query
                    ->where('pets_info.name', 'like', "%{$request->name}%")
                    ->orWhere('pets_info.id', $request->name);
                })
                ->when(@$request->species, function ($query) use ($request) {
                    $query
                    ->where('pets_info.specie', $request->species);
                })
                ->when(@$request->address, function ($query) use ($request, $additionalAddress) {
                    if (strlen($request->address) == 2) {
                        $query
                        ->leftJoin(
                                'pet_adresses', 'pet_adresses.pet_info_id', 'pets_info.id'
                        )
                        ->where('pet_adresses.state', $request->address);
                    } elseif ($additionalAddress) {
                        $query
                        ->leftJoin(
                                'pet_adresses', 'pet_adresses.pet_info_id', 'pets_info.id'
                        )
                        ->where('pet_adresses.state', $additionalAddress);
                    } elseif (
                            strlen($request->address) > 3 && is_numeric($request->address)
                    ) {
                        $query
                        ->addSelect(DB::raw("
                            ABS(pet_adresses.zip_code * (pet_adresses.zip_code - {$request->address})) as zip_order
                        "))
                        ->leftJoin(
                                'pet_adresses', 'pet_adresses.pet_info_id', 'pets_info.id'
                        )
                        ->where('pet_adresses.zip_code', '>=', $request->address - 100)
                        ->where('pet_adresses.zip_code', '<=', $request->address + 100)
                        ->orderBy('pet_adresses.pet_info_id');
                    } else {
                        $query
                        ->where(
                                'pets_info.last_seen', 'like', '%'.$request->address.'%'
                        );
                    }
                })
                ->when(!empty($status), function ($query) use ($status) {
                    $query->whereIn('pets_info.type', $status);
                })
                ->where('pets_info.status', '=', '1')
                ->where('pets_info.full_inf', '=', '1')
                ->orderBy('pets_info.order_priority', 'DESC')
                ->orderBy('pets_info.id', 'desc')
                ->paginate(12);

        $metaData = [
            'title' => 'All Lost Pet Database with lost dogs, cats, and other pets - PawMaw',
            'description' => 'Search lost dogs, cats, and other pets from our PawMaw Lost/Found pet database. Report a lost/found pet in your area. PawMaw can help you find your lost pet quickly.',
            'image' => url('pawmaw/img/fevi.png'),
        ];

        $facebook = DB
                ::table('facebook_pages')
                ->leftJoin(
                        'facebook_zip_codes', 'facebook_zip_codes.facebook_id', 'facebook_pages.id'
                )
                ->where('facebook_zip_codes.zip_code', $request->address)
                ->first();

        return view(
                'pawmawFront/site/found', compact('pets', 'name', 'status', 'species', 'address', 'metaData', 'facebook')
        );
    }

    public function reunitedSearch(Request $request)
    {
        $search = $request->search;
        $name = $request->name;
        $status = $request->status;
        $address = $request->address;
        $species = $request->species;
        $cityAlias = config('cityAlias');
        $additionalAddress = null;

        if (@$request->address) {
            foreach ($cityAlias as $key => $value) {
                if (strtoupper($value) == strtoupper($request->address)) {
                    $additionalAddress = $key;
                    break;
                }
            }
        }

        $status = array(
            'reunited',
        );

        $pets = PetsInfo::select('pets_info.*')->when(@$request->name, function ($query) use ($request) {
            $query->where('pets_info.name', 'like', "%{$request->name}%")
                ->orWhere('pets_info.id', $request->name);
        })
            ->when(@$request->species, function ($query) use ($request) {
                $query->where('pets_info.specie', $request->species);
            })
            ->when(@$request->address, function ($query) use ($request, $additionalAddress) {
                if (strlen($request->address) == 2) {
                    $query->leftJoin('pet_adresses', 'pet_adresses.pet_info_id', 'pets_info.id')
                    ->where('pet_adresses.state', $request->address);
                } elseif ($additionalAddress) {
                    $query->leftJoin('pet_adresses', 'pet_adresses.pet_info_id', 'pets_info.id')
                    ->where('pet_adresses.state', $additionalAddress);
                } elseif (strlen($request->address) > 3 && is_numeric($request->address)) {
                    $query->addSelect(DB::raw("
                            ABS(pet_adresses.zip_code * (pet_adresses.zip_code - {$request->address})) as zip_order
                        "))
                    ->leftJoin('pet_adresses', 'pet_adresses.pet_info_id', 'pets_info.id')
                    ->where('pet_adresses.zip_code', '>=', $request->address - 100)
                    ->where('pet_adresses.zip_code', '<=', $request->address + 100)
                    ->orderBy('pet_adresses.pet_info_id');
                } else {
                    $query->where('pets_info.last_seen', 'like', '%'.$request->address.'%');
                }
            })
            ->when(!empty($status), function ($query) use ($status) {
                $query->whereIn('pets_info.type', $status);
            })
            ->where('pets_info.status', '=', '1')
            ->where('pets_info.full_inf', '=', '1')
            ->orderBy('pets_info.order_priority', 'DESC')
            ->orderBy('pets_info.id', 'desc')
            ->paginate(12);

        $metaData = [
            'title' => 'All Lost Pet Database with lost dogs, cats, and other pets - PawMaw',
            'description' => 'Search lost dogs, cats, and other pets from our PawMaw Lost/Found pet database. Report a lost/found pet in your area. PawMaw can help you find your lost pet quickly.',
            'image' => url('pawmaw/img/fevi.png'),
        ];

        $facebook = DB::table('facebook_pages')->leftJoin('facebook_zip_codes', 'facebook_zip_codes.facebook_id', 'facebook_pages.id')
            ->select('facebook_pages.*')
            ->where('facebook_zip_codes.zip_code', $request->address)
            ->first();

        return view('pawmawFront/site/found', compact('pets', 'name', 'status', 'species', 'address', 'metaData', 'facebook'));
    }

    public function saveConvertingImage()
    {
        $image_name = 'pet-'.request('id').'.png';
        $path = public_path('uploads/converting-image');

        $image = Image::make(
                        file_get_contents(
                                mb_convert_encoding(request('image'), 'UTF-8', 'UTF-8')
                        )
                )->save($path.'/'.$image_name);

        return response()->json(
                        ['image' => '/uploads/converting-image/'.$image_name], 200
        );
    }

    public function saveWatermarkConvertingImage()
    {
        $image_name = 'pet-big-'.request('id').'.png';
        $path = public_path('uploads/converting-image');

        $image = Image::make(
                        file_get_contents(
                                mb_convert_encoding(request('image'), 'UTF-8', 'UTF-8')
                        )
                )->save($path.'/'.$image_name);

        return response()->json(
                        ['image' => '/uploads/converting-image/'.$image_name], 200
        );
    }

    // public function search(Request $request)
    // {
    //     $search = $request->search;
    //     $name = $request->name;
    //     $status = $request->status;
    //     $address = $request->address;
    //     $species = $request->species;
    //     $cityAlias = config('cityAlias');
    //     $additionalAddress = null;
    //     if(@$cityAlias[strtoupper($address)]){
    //         $additionalAddress = $cityAlias[strtoupper($address)];
    //     } else {
    //         foreach ($cityAlias as $key => $value){
    //             if(strtoupper($value) == strtoupper($address)){
    //                 $additionalAddress = $key;
    //                 break;
    //             }
    //         }
    //     }
    //     if($search != 1){
    //         $status = array(
    //             'lost',
    //             'found'
    //         );
    //     }
    //     $name_result = array();
    //     $status_result = array();
    //     $address_result = array();
    //     $species_result = array();
    //     $final_result = array();
    //     $pet_ids = PetsInfo
    //         ::where('status','=','1')
    //         ->pluck('id')
    //         ->toArray();;
    //     if($name != null || $name != ''){
    //         $name_result = PetsInfo
    //             ::where('status','=','1')
    //             ->where('name', 'like', "%{$name}%")
    //             ->orWhere('id',$name)
    //             ->pluck('id')
    //             ->toArray();
    //     } else{
    //         $name_result = $pet_ids;
    //     }
    //     if(!empty($status)){
    //         foreach ($status as $key => $value) {
    //             $temp = PetsInfo
    //                 ::where('status','=','1')
    //                 ->where('type', 'like', "%{$value}%")
    //                 ->pluck('id')
    //                 ->toArray();
    //             if($temp){
    //                 foreach ($temp as $key => $id) {
    //                     array_push($status_result, $id);
    //                 }
    //             }
    //         }
    //     } else{
    //         $status_result = $pet_ids;
    //         $status = array();
    //     }
    //     if($species != '0'){
    //         $species_result = PetsInfo
    //             ::where('status','=','1')
    //             ->where('specie',$species)
    //             ->pluck('id')
    //             ->toArray();
    //     } else{
    //         $species_result = $pet_ids;
    //     }
    //     if($address != null || $address != ''){
    //         $address_result = PetsInfo
    //             ::where('status','=','1')
    //             ->where(function($query) use($address, $additionalAddress){
    //                 if($additionalAddress)
    //                     $query
    //                         ->where('last_seen', 'like', '%' . $address . '%')
    //                         ->orWhere('postal', 'like', '%' . $address . '%')
    //                         ->orWhere('last_seen', 'like', '%' . $additionalAddress . '%');
    //                 else
    //                     $query
    //                         ->where('last_seen', 'like', '%' . $address . '%')
    //                         ->orWhere('postal', 'like', '%' . $address . '%');
    //             })
    //             ->pluck('id')
    //             ->toArray();
    //     } else{
    //         $address_result = $pet_ids;
    //     }
    //     $final_result = array_intersect(
    //         $pet_ids,
    //         $name_result,
    //         $status_result,
    //         $species_result,
    //         $address_result
    //     );
    //     $pets = PetsInfo
    //         ::whereIn('id', $final_result)
    //         ->orderBy('id','desc')
    //         ->paginate(15);
    //     return view(
    //         'pawmawFront/site/found',
    //         compact('pets', 'name', 'status', 'species', 'address')
    //     );
    // }

    public function owneremail($id)
    {
        $pet = PetsInfo::find($id);

        return view('pawmawFront.site.pet_details', compact('pet'));
    }

    public function about()
    {
        $data['metaData'] = [
            'title' => "Here's Our Story to Find Your Pet - PawMaw",
            'description' => 'PawMaw is actively helping to search for your lost pets in your local area. Learn more about us here. We are here to create a happy reunion with your family member.',
            'image' => url('pawmaw/img/fevi.png'),
        ];

        return view('pawmawFront.site.about', $data);
    }

    public function faq()
    {
        $metaData = [
            'title' => 'Frequently Asked Questions - PawMaw',
            'description' => 'Our F&Q page is committed to providing each customer with the highest standard of customer service. Have a question about how PawMaw works? Read our F&Q page as many questions can be answered here.',
            'image' => url('pawmaw/img/fevi.png'),
        ];

        return view('pawmawFront.site.faq', compact('metaData'));
    }

    public function howitworks()
    {
        $metaData = [
            'title' => 'How it Works - Post, Promote and Reunite with your Lost Dog or Cat Quickly-PawMaw',
            'description' => 'Get your pet back home Quickly. We are always here to help you reunite with your lost pet - Over 50000 pets happy reunions and counting! Learn how we are helping you to find your lost pet.',
            'image' => url('pawmaw/img/fevi.png'),
        ];

        return view('pawmawFront.site.howitworks', compact('metaData'));
    }

    public function join_network()
    {
        $metaData = [
            'title' => 'Join Our Network to Get Lost Pet Alert In Your Area- PawMaw',
            'description' => 'Want more lost Pet alert from your area? Join in your local area local area Facebook page or group to receive a lost pet alert. Help your neighbours to find the lost pet quickly.',
            'image' => url('pawmaw/img/fevi.png'),
        ];

        if (@$_GET['zip_code']) {
            $facebook = DB
                    ::table('facebook_pages')
                    ->leftJoin(
                            'facebook_zip_codes', 'facebook_zip_codes.facebook_id', 'facebook_pages.id'
                    )
                    ->where('facebook_zip_codes.zip_code', $_GET['zip_code'])
                    ->first();
        }

        return view('pawmawFront.site.join_network', compact('metaData', 'facebook'));
    }

    public function owneremailstore(Request $request)
    {
        // $email = (object) [
        //     'email' => $request->email,
        //     'message' => $request->message,
        //     'name' => $request->name
        // ];

        // Mail::to($request->contact_email)->send(new OwnerEmail($email));

        $OwnerEmail = new OwnersEmail();

        $OwnerEmail->pet_id = $request->pet_id;
        $OwnerEmail->name = $request->name;
        $OwnerEmail->email = $request->email;
        $OwnerEmail->message = $request->message;

        $OwnerEmail->save();

        return redirect()->back()->with('success', 'Message Send Successfully !');
    }

    public function contactemailstore(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'message' => 'required',
            'g-recaptcha-response' => ['required', new ReCaptchaRule()],
        ]);
        $email = new Contact();

        $email->name = $request->name;
        $email->phone = $request->phone;
        $email->email = $request->email;
        $email->address = $request->address;
        $email->message = $request->message;

        $email->save();

        Mail::to('info@pawmaw.com')->send(new ContactEmail($email));
        // Mail::to('cosmin.popescu21@gmail.com')->send(new ContactEmail($email));
        return redirect()->back()->with('success', 'Message Send Successfully !');
    }

    public function getFacebookPage($zipcode)
    {
        $facebook = DB::table('facebook_pages')
                ->leftJoin(
                        'facebook_zip_codes', 'facebook_zip_codes.facebook_id', 'facebook_pages.id'
                )
                ->select('facebook_pages.*')
                ->where('facebook_zip_codes.zip_code', $zipcode)
                ->first();

        return $facebook; //response()->json(['facebook' => $facebook], 200);
    }

    public function getFacebookPageByName($country)
    {
        $facebook = DB
        ::table('facebook_pages')
        ->where('facebook_pages.country', $country)
        ->first();

        return $facebook;
    }

    public function notifications()
    {
        $settings = \App\Models\SiteSettings::first();
        if (!$settings->enable_notifications) {
            return response()->json([]);
        }
        $notifications = \App\Models\Notifications::inRandomOrder()->get();

        return response()->json($notifications);
    }

    public function updatePetListing()
    {
        $maxOrder = PetsInfo
                ::orderBy('order_priority', 'DESC')
                ->first();

        $maxOrder = $maxOrder ? $maxOrder->order_priority : 0;

        PetsInfo
                ::where('id', request('id'))
                ->update([
                    'order_priority' => ++$maxOrder,
                    'date_list_update' => date('Y-m-d H:i:s'),
        ]);

        return response()->json($maxOrder, 200);
    }

    public function unsubscribe(Request $request)
    {
        if ($request->user_email) {
            $data = ['address' => $request->user_email, 'tag' => 'newsletter_mail'];
            $api_key = env('MAILGUN_SECRET');
            $DomainName = env('MAILGUN_DOMAIN');

            $options = ['--user' => 'api:'.$api_key];
            $url = "https://api.mailgun.net/v3/$DomainName/unsubscribes";

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, 'api:'.$api_key);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            $result = curl_exec($curl);

            curl_close($curl);

            return redirect()->back()->with('success', 'Email Unsubscribed Successfully !');
        }
    }

    public function isMobile()
    {
        $useragent = $_SERVER['HTTP_USER_AGENT'];

        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
            return true;
        } else {
            return false;
        }
    }
}
