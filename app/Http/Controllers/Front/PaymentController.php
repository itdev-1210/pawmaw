<?php

namespace App\Http\Controllers\Front;

use App\Models\Payment;
use App\Models\PetsInfo;
use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{


	public function checkout()
	{

		return view( 'user.checkout', compact( 'pets' ) );

	}

	public function ins( Request $request )
	{
		file_put_contents( dirname( __FILE__ ) . '/payment.txt', PHP_EOL . 'POST: ' . json_encode( $_POST ), FILE_APPEND );
		file_put_contents( dirname( __FILE__ ) . '/payment.txt', PHP_EOL . 'GET: ' . json_encode( $_GET ), FILE_APPEND );
	}

	function hmac( $key, $data )
	{
		$b = 64; // byte length for md5
		if ( strlen( $key ) > $b ) {
			$key = pack( "H*", md5( $key ) );
		}

		$key    = str_pad( $key, $b, chr( 0x00 ) );
		$ipad   = str_pad( '', $b, chr( 0x36 ) );
		$opad   = str_pad( '', $b, chr( 0x5c ) );
		$k_ipad = $key ^ $ipad;
		$k_opad = $key ^ $opad;

		return md5( $k_opad . pack( "H*", md5( $k_ipad . $data ) ) );
	}

	public function checkPaymentStatus( $ref )
	{
		$code   = env( 'TWOCHECKOUT_MERCHANT' );
		$secret = env( 'TWOCHECKOUT_SECRET' );
		$date   = gmdate( 'Y-m-d H:i:s' );
		$str    = strlen( $code ) . $code . strlen( $date ) . $date;
		$hash   = $this->hmac( $secret, $str );

		$curl = curl_init();

		curl_setopt_array( $curl, array(
			CURLOPT_URL            => "https://api.avangate.com/rest/4.0/orders/{$ref}/",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_TIMEOUT        => 5,
			CURLOPT_HTTPHEADER     => array(
				"Accept: application/json",
				"X-Avangate-Authentication: code='{$code}' date='{$date}' hash='{$hash}'",
				"content-type: application/json"
			),
		) );

		$response = curl_exec( $curl );
		$err      = curl_error( $curl );

		curl_close( $curl );

		$response = json_decode( $response, true );

		return $response;
	}

	public function callback()
	{
		$ref      = $_GET['refno'];
		$response = [];
		$tried    = 0;

		while ( empty( $response['CustomerDetails'] ) ) {
			$response = $this->checkPaymentStatus( $ref );

			if ( empty( $response['CustomerDetails'] ) ) {
				$tried ++;

				if ( $tried > 5 ) {
					return redirect()->action( 'Front\UserAccountController@index' )->with( 'danger', "Payment processing failed, please contact support with reference no. {$ref}" );
				}

				sleep( 5 );
			}
		}

		$package         = $response['Items'][0]['ProductDetails'];
		$price           = $response['Items'][0]['Price'];
		$code            = $response['Items'][0]['Code'];
		$billing         = $response['BillingDetails'];
		$payment_details = $response['PaymentDetails'];
		$payload         = $response['CustomerDetails']['ExternalCustomerReference'];
		$payload         = json_decode( $payload, true );
		$status          = $response['Status'];
		$success         = in_array( $response['Status'], [ 'COMPLETE', 'AUTHRECEIVED' ] );

		$already_paid = Payment::where( 'sid', $ref )->exists();

		if ( $already_paid ) {
			$success = false;
		}

		if ( empty( $payload['pet_id'] ) || empty( $payload['user_id'] ) || ! $success ) {
			$message = 'Payment failed! Please try again.';

			if ( $already_paid ) {
				$message = 'Transaction already done.';
			}

			//var_dump( empty( $payload['pet_id'] ), empty( $payload['user_id'] ), $success, $response );
			//die;

			return redirect()->action( 'Front\UserAccountController@index' )->with( 'danger', $message );
		}

		$request = (object) [
			'pet_id'                => $payload['pet_id'],
			'user_id'               => $payload['user_id'],
			'li_0_name'             => $package['Name'],
			'sid'                   => $ref,
			'key'                   => '',
			'state'                 => $billing['State'],
			'email'                 => $billing['Email'],
			'li_0_type'             => 'product',
			'order_number'          => $response['OrderNo'],
			'currency_code'         => $response['PayoutCurrency'],
			'lang'                  => $response['Language'],
			'invoice_id'            => $ref,
			'li_0_price'            => $price['NetPrice'],
			'total'                 => $response['AdditionalFields']['NetPrice'],
			'credit_card_processed' => 'Y',
			'zip'                   => $billing['Zip'],
			'li_0_quantity'         => 1,
			'li_0_description'      => '',
			'li_0__description'     => '',
			'cart_weight'           => '',
			'fixed'                 => '',
			'last_name'             => $billing['LastName'],
			'li_0_product_id'       => $code,
			'street_address'        => $billing['Address1'],
			'city'                  => $billing['City'],
			'li_0_tangible'         => '',
			'merchant_order_id'     => '',
			'country'               => $billing['CountryCode'],
			'ip_country'            => $billing['CountryCode'],
			'demo'                  => $response['TestOrder'],
			'pay_method'            => '',
			'cart_tangible'         => '',
			'phone'                 => $billing['Phone'],
			'street_address2'       => $billing['Address2'],
			'first_name'            => $billing['FirstName'],
			'card_holder_name'      => $billing['FirstName'] . ' ' . $billing['LastName'],
			'middle_initial'        => '',
			'paypal_direct'         => '',
			'status'                => $response['Status']
		];

		$payment = new Payment;

		$payment->pet_id                = $request->pet_id;
		$payment->user_id               = $request->user_id;
		$payment->middle_initial        = $request->middle_initial;
		$payment->li_0_name             = $request->li_0_name;
		$payment->sid                   = $request->sid;
		$payment->key                   = $request->key;
		$payment->state                 = $request->state;
		$payment->email                 = $request->email;
		$payment->li_0_type             = $request->li_0_type;
		$payment->order_number          = $request->order_number;
		$payment->currency_code         = $request->currency_code;
		$payment->lang                  = $request->lang;
		$payment->invoice_id            = $request->invoice_id;
		$payment->li_0_price            = $request->li_0_price;
		$payment->total                 = $request->total;
		$payment->credit_card_processed = $request->credit_card_processed;
		$payment->zip                   = $request->zip;
		$payment->li_0_quantity         = $request->li_0_quantity;
		$payment->li_0__description     = $request->li_0__description;
		$payment->cart_weight           = $request->cart_weight;
		$payment->fixed                 = $request->fixed;
		$payment->last_name             = $request->last_name;
		$payment->li_0_product_id       = $request->li_0_product_id;
		$payment->street_address        = $request->street_address;
		$payment->city                  = $request->city;
		$payment->li_0_tangible         = $request->li_0_tangible;
		$payment->li_0_description      = $request->li_0_description;
		$payment->merchant_order_id     = $request->merchant_order_id;
		$payment->country               = $request->country;
		$payment->ip_country            = $request->ip_country;
		$payment->demo                  = $request->demo;
		$payment->pay_method            = $request->pay_method;
		$payment->cart_tangible         = $request->cart_tangible;
		$payment->phone                 = $request->phone;
		$payment->street_address2       = $request->street_address2;
		$payment->first_name            = $request->first_name;
		$payment->card_holder_name      = $request->card_holder_name;


		$payment->save();

		return redirect()->action( 'Front\UserAccountController@index' )->with( 'success', 'Successfully Payment!' );
	}


}
