<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\BlogPost;
use App\Category;

class BlogController extends Controller
{
    
	public function index()
	{
		$data['posts'] = BlogPost
			::orderBy('id', 'DESC')
			->when(request('name'), function($query){
				$query
					->where('title', 'like', '%' . request('name') . '%');
			})
			->paginate(4);
		$data['categories'] = Category::all();
		$data['popular_post'] = BlogPost::getPopularPosts();
		$data['recent_post'] = BlogPost::orderBy('id', 'DESC')->limit(4)->get();
		$data['metaData'] = [
            'title' => "Welcome to Our Blog-PawMaw",
            'description' => "Read our blog to know more about, lost & found pet tips, pet safety, pet health and many more. So, learn here about pet-related tips.",
            'image' => url('pawmaw/img/fevi.png') 
        ];

		return view('pawmawFront/site/blog', $data);
	}

	public function post($slug)
	{
		$data['post'] = BlogPost
			::where('slug', $slug)
			->first();

		if(!$data['post'])
            return response()->view('errors.404', [], 404);

		$data['recent_posts'] = BlogPost
			::orderBy('id', 'DESC')
			->limit(2)
			->get();
		$data['categories'] = Category::all();
		$data['metaData'] = [
			'title' => $data['post']['title'],
			'description' => $data['post']['excerpt'],
			'image' => url('img/' . $data['post']['image'])
		];
		$data['comments'] = Comment
			::where('checked', 1)
			->where('comment_id', 0)
			->where('post_id', $data['post']['id'])
			->get();
		$data['comments']->map(function($element){
			$element->replies = $element->getReplies();
		});
		$data['popular_post'] = BlogPost::getPopularPosts();
		$data['recent_post_blog'] = BlogPost::orderBy('id', 'DESC')->limit(4)->get();

		return view('pawmawFront/site/guide-for-found-pets', $data);
	}

	public function category($slug)
	{
		$data['posts'] = BlogPost
			::select(
				'categories.*',
				'blog_entries.*'
			)
			->leftJoin(
				'categories',
				'categories.id',
				'blog_entries.category_id'
			)
			->when(request('name'), function($query){
				$query
					->where('blog_entries.title', 'like', '%' . request('name') . '%');
			})
			->where('categories.slug', $slug)
			->orderBy('blog_entries.id', 'DESC')
			->paginate(4);
		$data['categories'] = Category::all();
		$data['active_category'] = Category::where('slug', $slug)->first();
		$data['popular_post'] = BlogPost::getPopularPosts();
		$data['recent_post'] = BlogPost::orderBy('id', 'DESC')->limit(4)->get();

		return view('pawmawFront/site/blog-category', $data);
	}

	public function createComment()
	{
		$requestData = request()->all();

		unset($requestData['_token']);

		Comment::create($requestData);

		return redirect()
			->back()
			->with(
				'message', 
				'You comment was successfully created. Administration need review it.'
			);
	}

}
