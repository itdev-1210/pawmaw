<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\PetsInfo;
use Route;
use Auth;
class CheckOnFinishPost
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guard( 'user' )->user())
            return redirect('user/login');

        $check = PetsInfo::checkOnUserFinishPost(Auth::guard( 'user' )->user()->id);
     
        if($check && Route::getCurrentRoute()->uri != "user/next")
            return redirect('/user/next');

        return $next($request);
    }
}
