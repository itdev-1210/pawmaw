<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Belts extends Model
{

	public $timestamps = false;
    
	protected $fillable = [
		'id',
		'name',
		'order'
	];

}
