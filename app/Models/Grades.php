<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grades extends Model
{

	public $timestamps = false;
    
	protected $fillable = [
		'id',
		'name',
		'order'
	];

}
