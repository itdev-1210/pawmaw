<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OwnersEmail extends Model
{


    protected $table = "owners_email";

    public function user(){
    	return $this->belongsTo('App\User','user_id','id');
    }


    public function pet()
    {

        return $this->belongsTo('App\Models\PetsInfo');

    }


}
