<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PetsInfo extends Model
{
	use SoftDeletes;

	protected $dates = [
		"email_send_date"
	];

	protected $table = "pets_info";

	protected $appends = [
		'package_name'
	];

	public function user()
	{
		return $this->belongsTo( 'App\User', 'user_id', 'id' );
	}

	public function payments()
	{
		return $this->hasMany( 'App\Models\Payment', 'pet_id', 'id' );
	}

	public function getPackageNameAttributeOld()
	{
		$payments = $this->payments->where( 'updated_at', '>', date( 'Y-m-d', strtotime( "-10 days" ) ) )
		                           ->sortByDesc( 'updated_at' )->toArray();
		$payments = array_values( $payments );

		if ( count( $payments ) > 0 ) {
			$package_name = trim( str_replace( 'package', '', strtolower( $payments[0]['li_0_name'] ) ) );

			if ( $payments[0]['status'] != 'COMPLETE' ) {
				//$package_name = 'processing';
			}

			return $package_name;
		}

		return false;
	}
	
	public function getPackageNameAttribute()
	{
		$payments = $this->payments->where( 'updated_at', '>', date( 'Y-m-d', strtotime( "-10 days" ) ) )
		                           ->sortByDesc( 'updated_at' )->toArray();
		$payments = array_values( $payments );
		$packageName = null;
		$packageExists = false;
		if (count($payments)) {
		    $packageExists = true;
			foreach ($payments as $payment) {
			    $currentPackageName = trim( str_replace( 'package', '', strtolower( $payment['li_0_name'] ) ) );
		  	    $daysPassed = (time() - strtotime($payment['updated_at'])) / 3600 / 24;
			    if (($currentPackageName == 'basic' && $daysPassed < 3) || ($currentPackageName == 'standard' && $daysPassed < 7) || ($currentPackageName == 'premium' && $daysPassed < 10)) {
		            $packageName = $currentPackageName;
		        }
		        if ($packageName) break;
	        }
		}
	    if (!$packageName) {
		    $package = json_decode($this->package); 
		    if ($package) {
		    	$packageExists = true;
		        $daysPassed = (time() - $package->updated_at) / 3600 / 24;
		        if (($package->name == 'basic' && $daysPassed < 3) || ($package->name == 'standard' && $daysPassed < 7) || ($package->name == 'premium' && $daysPassed < 10)) {
		            $packageName = $package->name;
		        }
		    }  
		}
		if ($packageName) $packageName = strtoupper(substr($packageName, 0, 1)). substr($packageName, 1, strlen($packageName)); 
		if (!$packageName && $packageExists) $packageName = 'Alerts Expired!';
        return $packageName;
	}
	
	public function getPackageList()
	{
		$return = array();
		$payments = $this->payments->where( 'updated_at', '>', date( 'Y-m-d', strtotime( "-500 days" ) ) )
		                           ->sortByDesc( 'updated_at' )->toArray();
		$payments = array_values( $payments );

		if ( count( $payments ) > 0 ) {
			foreach ($payments as $payment) {
			    $return[strtotime($payment['updated_at'])] = trim(str_replace('package', '', strtolower($payments[0]['li_0_name'])));
			}
		}

		return $return;
	}

	protected function getPetInformationBySlug( $slug )
	{
		return $this
			->select(
				'pet_adresses.*',
				'pets_info.*',
				'users.phone'
			)
			->with( 'user' )
			->leftJoin(
				'pet_adresses',
				'pet_adresses.pet_info_id',
				'pets_info.id'
			)
			->leftJoin(
				'users',
				'users.id',
				'pets_info.user_id'
			)
			->where( 'pets_info.slug', $slug )
			->first();
	}

	protected function getPetInformationById( $id )
	{
		return $this
			->select(
				'pet_adresses.*',
				'pets_info.*'
			)
			->with( 'user' )
			->leftJoin(
				'pet_adresses',
				'pet_adresses.pet_info_id',
				'pets_info.id'
			)
			->where( 'pets_info.id', $id )
			->first();
	}

	protected function getPetInformationByUser( $id )
	{
		return $this
			->select(
				'pet_adresses.*',
				'pets_info.*'
			)
			->with( 'user' )
			->leftJoin(
				'pet_adresses',
				'pet_adresses.pet_info_id',
				'pets_info.id'
			)
			->where( 'pets_info.user_id', $id )
			->first();
	}

	protected function getPetInformationForNextStep( $id )
	{
		return $this
			->select(
				'pet_adresses.*',
				'pets_info.*'
			)
			->with( 'user' )
			->leftJoin(
				'pet_adresses',
				'pet_adresses.pet_info_id',
				'pets_info.id'
			)
			->where( 'pets_info.user_id', $id )
			->where( 'pets_info.full_inf', 0 )
			->first();
	}

	protected function checkOnUserFinishPost( $user_id )
	{
		return $this
			->where( 'user_id', $user_id )
			->where( 'full_inf', 0 )
			->exists();
	}
    
    protected function checkUserPetId($user_id,$pet_id) {
        
        return $this->select('id')->where(['user_id' => $user_id,'id'=>$pet_id])->count();
    
    }
    
}
