<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{

	protected function getIdByName($name)
	{
		return $this->select('id')->where('name', $name)->first();
	}

}
