<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TournamentAgeGroupWeight extends Model
{

	public $timestamps = false;
    
	protected $fillable = [
		'id',
		'tournament_id',
		'age_group_id',
		'weight',
		'direction',
		'sex'
	];

	protected function getWeightsByGroupId($id)
	{
		return $this
			->select('tournament_age_group_weights.*')
			->selectRaw('COUNT(tournament_athletes.id) as countParticipant')
			->leftJoin('tournament_athletes', function($query){
				$query
					->on(
						'tournament_athletes.age_group_id',
						'tournament_age_group_weights.age_group_id'
					)
					->on(
						'tournament_athletes.weight_id',
						'tournament_age_group_weights.id'
					);
			})
			->leftJoin(
				'tournament_match_settings as settings',
				'settings.weight_id',
				'tournament_age_group_weights.id'
			)
			->where('tournament_age_group_weights.age_group_id', $id)
			->orderBy('tournament_age_group_weights.weight')
			->orderBy('tournament_age_group_weights.direction')
			->groupBy('tournament_age_group_weights.id')
			->groupBy('tournament_athletes.weight_id')
			->get();
	}

	protected function getTournamentWeightsByGroupId($id)
	{
		return $this
			->select('tournament_age_group_weights.*')
			// ->selectRaw('COUNT(tournament_athletes.id) as countParticipant')
			->selectRaw("
				(
					SELECT COUNT(*)
					FROM tournament_athletes
					LEFT JOIN athletes
					ON athletes.id = tournament_athletes.athlete_id
					WHERE tournament_athletes.weight_id = tournament_age_group_weights.id
					AND athletes.id > 0
					GROUP BY tournament_age_group_weights.id
				) as countParticipant
			")
			->leftJoin('tournament_athletes', function($query){
				$query
					->on(
						'tournament_athletes.age_group_id',
						'tournament_age_group_weights.age_group_id'
					)
					->on(
						'tournament_athletes.weight_id',
						'tournament_age_group_weights.id'
					);
			})
			// ->leftJoin(
			// 	'athletes',
			// 	'athletes.id',
			// 	'tournament_athletes.athlete_id'
			// )
			->leftJoin(
				'tournament_match_settings as settings',
				'settings.weight_id',
				'tournament_age_group_weights.id'
			)
			->where('tournament_age_group_weights.age_group_id', $id)
			// ->where(function($query){
			// 	$query
			// 		->whereRaw('athletes.id != null')
			// 		->orWhereRaw('COUNT(tournament_athletes.id) = 0');
			// })
			->orderBy('tournament_age_group_weights.weight')
			->orderBy('tournament_age_group_weights.direction')
			->groupBy('tournament_age_group_weights.id')
			// ->groupBy('tournament_athletes.weight_id')
			->get();
	}

	protected function getDrawWeightsByGroupId($id)
	{
		return $this
			->select(
				'tournament_age_group_weights.*'
			)
			->selectRaw('COUNT(tournament_athletes.id) as countParticipant')
			->leftJoin('tournament_athletes', function($query){
				$query
					->on(
						'tournament_athletes.age_group_id',
						'tournament_age_group_weights.age_group_id'
					)
					->on(
						'tournament_athletes.weight_id',
						'tournament_age_group_weights.id'
					);
			})
			->leftJoin(
				'tournament_match_settings as settings',
				'settings.weight_id',
				'tournament_age_group_weights.id'
			)
			->leftJoin('tournament_draw_shows as drawShow', function($query){
				$query
					->on(
						'drawShow.tournament_id',
						'settings.tournament_id'
					)
					->on(
						'drawShow.day',
						'settings.day'
					);
			})
			->where('settings.id', '!=', null)
			->where(function($query){
				if(auth()->user() && auth()->user()->id != Tournament::find(request('id'))->creator_id){
					$query->where('drawShow.show', 1);
					$query->where('drawShow.id', '!=', null);
				}
			})
			->where(
				DB::raw("
					(
						SELECT COUNT(*)
						FROM tournament_match_athlete_relations as subrelation
						LEFT JOIN tournament_match_settings as subsettings
						ON subsettings.id = subrelation.tournament_match_settings_id
						WHERE subsettings.weight_id = tournament_age_group_weights.id
						GROUP BY subrelation.tournament_match_settings_id
						LIMIT 1
					)
				"),
				'>=', 
				1
			)
			->where('tournament_age_group_weights.age_group_id', $id)
			->orderBy('tournament_age_group_weights.weight')
			->groupBy('tournament_age_group_weights.id')
			->groupBy('tournament_athletes.weight_id')
			->get();
	}


	protected function getMatchSettingsAgeGroups($tournamentId)
	{
		return $this
			->select('age_groups.*')
			->leftJoin('tournament_athletes', function($query){
				$query
					->on(
						'tournament_athletes.age_group_id',
						'tournament_age_group_weights.age_group_id'
					)
					->on(
						'tournament_athletes.weight_id',
						'tournament_age_group_weights.id'
					);
			})
			->leftJoin('tournament_match_settings', function($query){
				$query
					->on(
						'tournament_match_settings.age_group_id',
						'tournament_age_group_weights.age_group_id'
					)
					->on(
						'tournament_match_settings.weight_id',
						'tournament_age_group_weights.id'
					);
			})
			->leftJoin(
				'age_groups',
				'age_groups.id',
				'tournament_age_group_weights.age_group_id'
			)
			// ->where(
			// 	'tournament_age_group_weights.age_group_id', 
			// 	$data['ageGroupId']
			// )
			// ->where(
			// 	'tournament_age_group_weights.tournament_id', 
			// 	$data['tournamentId']
			// )
			->where('tournament_match_settings.id', null)
            ->where('age_groups.tournament_id', $tournamentId)
			// ->orderBy('tournament_age_group_weights.weight')
			// ->groupBy('tournament_age_group_weights.id')
			->groupBy('age_groups.id')
			->get();
	}

	protected function getMatchSettingsWeights($data)
	{
		return $this
			->select('tournament_age_group_weights.*')
			->selectRaw('COUNT(tournament_athletes.id) as countParticipant')
			->leftJoin('tournament_athletes', function($query){
				$query
					->on(
						'tournament_athletes.age_group_id',
						'tournament_age_group_weights.age_group_id'
					)
					->on(
						'tournament_athletes.weight_id',
						'tournament_age_group_weights.id'
					);
			})
			->leftJoin('tournament_match_settings', function($query){
				$query
					->on(
						'tournament_match_settings.age_group_id',
						'tournament_age_group_weights.age_group_id'
					)
					->on(
						'tournament_match_settings.weight_id',
						'tournament_age_group_weights.id'
					);
			})
			->where(
				'tournament_age_group_weights.age_group_id', 
				$data['ageGroupId']
			)
			// ->where(
			// 	'tournament_age_group_weights.tournament_id', 
			// 	$data['tournamentId']
			// )
			->where('tournament_match_settings.id', null)
			->orderBy('tournament_age_group_weights.weight')
			->groupBy('tournament_age_group_weights.id')
			->groupBy('tournament_athletes.weight_id')
			->get();
	}

}
