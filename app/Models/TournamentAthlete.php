<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TournamentAthlete extends Model
{

	protected $table = "tournament_athletes";

	protected $fillable = [
		'tournament_id',
		'athlete_id',
		'age_group_id',
		'weight_id'
	];

	protected function getTournamentParticipants($data, $search)
	{
		return $this
			->select(
				DB::raw('DISTINCT athletes.id as athleteId'),
				'athletes.first_name as athlete_first_name',
				'athletes.last_name as athlete_last_name',
				'athletes.image as athlete_image',
				'users.first_name as trainer_first_name',
				'users.last_name as trainer_last_name',
				'users.id as trainer_id',
				'organizations.name as organization',
				'users.id as trainer_id',
				'cities.name as city',
				'tournament_athletes.id as recordId'
			)
			->leftJoin(
				'athletes',
				'athletes.id',
				'tournament_athletes.athlete_id'
			)
			->when(auth()->user() != null, function($query){
				$query->leftJoin('athletes as trainerAthletes', function($query){
					$query->where('athletes.trainer_id', auth()->user()->id);
				});
			})
			->leftJoin(
				'users',
				'users.id',
				'athletes.trainer_id'
			)
			->leftJoin(
				'cities',
				'cities.id',
				'users.city_id'
			)
			->leftJoin(
				'organizations',
				'organizations.id',
				'users.organization_id'
			)
			->where('tournament_athletes.tournament_id', $data['tournamentId'])
			->where('tournament_athletes.age_group_id', $data['age_group_id'])
			->where('tournament_athletes.weight_id', $data['weight_id'])
			->where('athletes.id', '!=', null)
			->when($search, function($query) use ($search){
				$query->where(
					DB::raw(
						"CONCAT(athletes.first_name, ' ', athletes.last_name)"
					),
					'LIKE',
					'%' . $search . '%'
				);
			})
			->get();
	}

	protected function getTeams($tournamentId)
	{
		return $this
			->select(
				'organizations.name as organization',
				'cities.name',
				'users.id',
				'users.first_name',
				'users.last_name'
			)
			->selectRaw('COUNT(*) as countParticipant')
			->leftJoin(
				'athletes',
				'athletes.id',
				'tournament_athletes.athlete_id'
			)
			->leftJoin(
				'users',
				'users.id',
				'athletes.trainer_id'
			)
			->leftJoin(
				'cities',
				'cities.id',
				'users.city_id'
			)
			->leftJoin(
				'organizations',
				'organizations.id',
				'users.organization_id'
			)
			->when(auth()->user(), function($query){
				$query
					->leftJoin('users as subUser', function($query){
						$query
							->on('subUser.id', 'users.id')
							->where('subUser.id', auth()->user()->id);
					})
					->orderBy('subUser.id', 'DESC');
			})
			->where('tournament_id', $tournamentId)
			->where('organizations.id', '!=', null)
			->groupBy('users.id')
			->orderBy('countParticipant', 'DESC')
			->get();
	}

	protected function getTeamAtheltes($userId, $tournamentId, $search = null)
	{
		return $this
			->select(
				'athletes.*', 
				'athletes.id as athlete_id', 
				'users.first_name as trainer_first_name',
				'users.last_name as trainer_last_name',
				'age_groups.name as age_group_name',
				'tournament_age_group_weights.weight as weight',
				'tournament_age_group_weights.direction as direction'
			)
			->leftJoin('athletes', function($query){
				$query
					->on('athletes.id', 'tournament_athletes.athlete_id');
			})
			->leftJoin(
				'users',
				'users.id',
				'athletes.trainer_id'
			)
			->leftJoin(
				'age_groups',
				'age_groups.id',
				'tournament_athletes.age_group_id'
			)
			->leftJoin(
				'tournament_age_group_weights',
				'tournament_age_group_weights.id',
				'tournament_athletes.weight_id'
			)
			->when($search, function($query) use ($search){
				$query->where(
					DB::raw(
						"CONCAT(athletes.first_name, ' ', athletes.last_name)"
					),
					'LIKE',
					'%' . $search . '%'
				);
			})
			->when(auth()->user(), function($query) use ($userId, $tournamentId){
				if(auth()->user()->id != $userId){
					$query->where(
						'tournament_athletes.tournament_id', 
						$tournamentId
					);
				}
			})
			// ->where(
			// 	'tournament_athletes.tournament_id', 
			// 	$tournamentId
			// )
			->where('athletes.trainer_id', $userId)
			->groupBy('athletes.id')
			->groupBy('age_groups.id')
			->groupBy('tournament_age_group_weights.id')
			->get();
	}

	protected function getMatchSettingsWeights($data)
	{
		return $this
			->select('tournament_age_group_weights.*')
			->selectRaw('COUNT(*)')
			->leftJoin(
				'tournament_age_group_weights',
				'tournament_age_group_weights.id',
				'tournament_athletes.age_group_id'
			)
			->where('tournament_athletes.tournament_id', $data['tournamentId'])
			->where('tournament_athletes.age_group_id', $data['ageGroupId'])
			->groupBy('tournament_athletes.age_group_id')
			->get();
	}

}
