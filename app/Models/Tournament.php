<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Tournament extends Model
{

	protected $fillable = [
		'name',
		'creator_id',
		'image',
		'country_id',
		'region_id',
		'city_id',
		'address',
		'map_lat',
		'map_lng',
		'description',
		'schedule',
		'tournament_level_id',
		'sport_kind_id',
		'rings_count',
		'participants_limit',
		'date_from',
		'date_to',
		'register_from',
		'register_to',
		'judge',
		'secretary'
	];

	protected function getTrainerTournamentsYears()
	{
		return $this
			->selectRaw('YEAR(date_from) as year')
			->where('tournaments.creator_id', auth()->user()->id)
			->orderBy('year', 'DESC')
			->groupBy('year')
			->get();
	}

	public function getTournamentsByDate()
	{
		return $this
			->selectRaw("
				(SELECT COUNT(id)
				FROM tournament_athletes
				WHERE tournament_athletes.tournament_id = tournaments.id
				GROUP BY tournament_athletes.tournament_id) as count_athletes,
				tournaments.*
			")
			->whereRaw("YEAR(date_from) = '" . $this->year . "'")
			->where('tournaments.creator_id', auth()->user()->id)
			->get();
	}

	protected function getTournamentById($id)
	{
		return $this
			->select('tournaments.*')
			->addSelect('cities.name as city')
			->addSelect('users.phone as user_phone')
			->addSelect('users.email as user_email')
			->addSelect(
				DB::raw("
					(
						SELECT COUNT(*)
						FROM tournament_athletes
						WHERE tournament_athletes.tournament_id = tournaments.id
					) as countParticipant
				")
			)
			->leftJoin(
				'cities',
				'cities.id',
				'tournaments.city_id'
			)
			->leftJoin(
				'users',
				'users.id',
				'tournaments.creator_id'
			)
			->where('tournaments.id', $id)
			->first();
	}

	protected function getTournamentList($params)
	{
		return $this
			->select(
				'tournaments.*',
				'cities.name as city_name',
				'sport_kinds.name as sport_kinds_name',
				DB::raw("
					(
						SELECT COUNT(*)
						FROM tournament_athletes
						WHERE tournament_athletes.tournament_id = tournaments.id
					) as countParticipant
				")
			)
			->leftJoin(
				'cities',
				'cities.id',
				'tournaments.city_id'
			)
			->leftJoin(
				'sport_kinds',
				'sport_kinds.id',
				'tournaments.sport_kind_id'
			)
			->when(@$params['sport_kind_id'], function($query) use ($params){
				$query->where(
					'tournaments.sport_kind_id',
					$params['sport_kind_id']
				);
			})
			->when(@$params['status'], function($query) use ($params){
				$query->where(
					'tournaments.status',
					$params['status']
				);
			})
			->when(@$params['city_id'], function($query) use ($params){
				$query->where(
					'tournaments.city_id',
					$params['city_id']
				);
			})
			->when(@$params['date_from'], function($query) use ($params){
				$query->where(
					'tournaments.date_from',
					'>=',
					$params['date_from']
				);
			})
			->when(@$params['date_to'], function($query) use ($params){
				$query->where(
					'tournaments.date_to',
					'<=',
					$params['date_to']
				);
			})
			->when(@$params['offset'], function($query) use ($params){
				$query->offset($params['offset'] * $params['limit']);
			})
			->when(@$params['limit'], function($query) use ($params){
				$query->limit($params['limit']);
			})
			->when(true, function($query) use ($params){
				if(@$params['nearest'])
					$query
						->where(
							'tournaments.date_from', 
							'>=', 
							DB::raw('CURDATE()')
						)
						->orderBy('tournaments.date_from')
						->orderBy('tournaments.date_to');
				else
					$query
						->orderBy('tournaments.date_from', 'DESC')
						->orderBy('tournaments.date_to', 'DESC');
			})
			->get();
	}

	protected function getTournamentListCount($params)
	{
		return $this
			->leftJoin(
				'cities',
				'cities.id',
				'tournaments.city_id'
			)
			->leftJoin(
				'sport_kinds',
				'sport_kinds.id',
				'tournaments.sport_kind_id'
			)
			->when(@$params['sport_kind_id'], function($query) use ($params){
				$query->where(
					'tournaments.sport_kind_id',
					$params['sport_kind_id']
				);
			})
			->when(@$params['status'], function($query) use ($params){
				$query->where(
					'tournaments.status',
					$params['status']
				);
			})
			->when(@$params['city_id'], function($query) use ($params){
				$query->where(
					'tournaments.city_id',
					$params['city_id']
				);
			})
			->when(@$params['date_from'], function($query) use ($params){
				$query->where(
					'tournaments.date_from',
					'>=',
					$params['date_from']
				);
			})
			->when(@$params['date_to'], function($query) use ($params){
				$query->where(
					'tournaments.date_to',
					'<=',
					$params['date_to']
				);
			})
			->count();
	}

	protected function getTournamnetListForStatus($tournament_id)
	{
		return $this
			->select(
				'register_from',
				'register_to',
				'date_from',
				'date_to',
				'id'
			)
			->where('status', '!=', 5)
			->when($tournament_id, function($query) use ($tournament_id){
				$query->where('tournaments.id', $tournament_id);
			})
			->get();
	}

	protected function getSportKinds()
	{
		return $this
			->select(
				'sport_kinds.id',
				'sport_kinds.name'
			)
			->leftJoin(
				'sport_kinds',
				'sport_kinds.id',
				'tournaments.sport_kind_id'
			)
			->groupBy('tournaments.sport_kind_id')
			->get();
	}

	protected function getCities()
	{
		return $this
			->select(
				'cities.id',
				'cities.name'
			)
			->leftJoin(
				'cities',
				'cities.id',
				'tournaments.city_id'
			)
			->groupBy('tournaments.city_id')
			->get();
	}

	protected function getDrawTournaments($tournamentId)
	{
		return $this
			->when($tournamentId, function($query) use ($tournamentId){
				$query->where('id', $tournamentId);
			})
			->where('status', '>', 2)
			->where('status', '!=', 5)
			->get();
	}

	protected function getTournamentStatistic($tournamentId)
	{
		return $this
			->select(
				DB::raw("
					(
						SELECT COUNT(DISTINCT countries.id)
						FROM tournaments as subtournaments
						LEFT JOIN tournament_athletes 
						ON subtournaments.id = tournament_athletes.tournament_id
						LEFT JOIN athletes 
						ON athletes.id = tournament_athletes.athlete_id
						LEFT JOIN users 
						ON users.id = athletes.trainer_id
						LEFT JOIN countries
						ON countries.id = users.country_id
						WHERE subtournaments.id = tournaments.id
					) as countryCount
				"),
				DB::raw("
					(
						SELECT COUNT(DISTINCT organizations.id)
						FROM tournaments as subtournaments
						LEFT JOIN tournament_athletes 
						ON subtournaments.id = tournament_athletes.tournament_id
						LEFT JOIN athletes 
						ON athletes.id = tournament_athletes.athlete_id
						LEFT JOIN users 
						ON users.id = athletes.trainer_id
                        JOIN organizations
                        ON organizations.id = users.organization_id
						WHERE subtournaments.id = tournaments.id
					) as organizationCount
				"),
				DB::raw("
					(
						SELECT COUNT(DISTINCT users.id)
						FROM tournaments as subtournaments
						LEFT JOIN tournament_athletes 
						ON subtournaments.id = tournament_athletes.tournament_id
						LEFT JOIN athletes 
						ON athletes.id = tournament_athletes.athlete_id
						LEFT JOIN users 
						ON users.id = athletes.trainer_id
						WHERE subtournaments.id = tournaments.id
					) as trainersCount
				"),
				DB::raw("
					(
						SELECT COUNT(tournament_athletes.athlete_id)
						FROM tournaments as subtournaments
						LEFT JOIN tournament_athletes 
						ON subtournaments.id = tournament_athletes.tournament_id
						WHERE subtournaments.id = tournaments.id
					) as athletesCount
				")
			)
			->where('tournaments.id', $tournamentId)
			->first();
	}

}

// SELECT count(*) 
// FROM `organizations`
// JOIN users 
// ON users.organization_id = organizations.id
// JOIN athletes 
// ON athletes.trainer_id = users.id
// JOIN tournament_athletes
// ON tournament_athletes.athlete_id = athletes.id
// WHERE tournament_athletes.tournament_id = 30
// GROUP BY organizations.id
