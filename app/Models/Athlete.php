<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Athlete extends Model
{

	protected $fillable = [
		'first_name',
		'last_name',
		'sex',
		'trainer_id',
		'grade_id',
		'belt_id',
		'image',
		'birthday'
	];

	protected $casts = [
		'trainers' => 'array'
	];

	protected function getTrainerAthletes($id, $search = null, $page = null)
	{
		return $this
			->select(
				'athletes.id',
				'athletes.image',
				'athletes.first_name',
				'athletes.last_name',
				'athletes.birthday as birthday'
				// DB::raw('YEAR(athletes.birthday) as birthday')
			)
			// ->leftJoin('age_groups', function($join){
			// 	return $join->where(
			// 		'age_groups.date_from',
			// 		'<=',
			// 		DB::raw('YEAR(athletes.birthday)')
			// 	)
			// 	->where(
			// 		'age_groups.date_to',
			// 		'>=',
			// 		DB::raw('YEAR(athletes.birthday)')
			// 	);
			// })
			->when($search, function($query) use ($search){
				$query->where(
					DB::raw(
						"CONCAT(athletes.first_name, ' ', athletes.last_name)"
					),
					'LIKE',
					'%' . $search . '%'
				);
			})
			->when($page > 1, function($query) use ($page){
				$query->offset(($page - 1) * 15);
			})
			->where("athletes.trainer_id", $id)
			->orderBy('last_name')
			->orderBy('first_name')
			->limit(15)
			->get();
	}

	protected function getTrainerAthletesCount($id, $search)
	{
		return $this
			->when($search, function($query) use ($search){
				return $query->where(
					DB::raw(
						"CONCAT(athletes.first_name, ' ', athletes.last_name)"
					),
					'LIKE',
					'%' . $search . '%'
				);
			})
			->where("athletes.trainer_id", $id)
			->count();
	}

	protected function generalTeamCount($id)
	{
		return $this
			->where("athletes.trainer_id", $id)
			->count();
	}

	protected function getAthleteById($id)
	{
		return $this
			->where('id', $id)
			->first();
	}

	protected function getFullAthleteInformationById($id)
	{
		return $this
			->select(
				'athletes.id',
				'athletes.first_name',
				'athletes.last_name',
				'athletes.image',
				'athletes.birthday',
				'cities.name as city_name',
				'users.id as trainer_id',
				'users.organization as organization',
				'users.first_name as trainer_first_name',
				'users.last_name as trainer_last_name',
				'grades.name as grade_name',
				'belts.name as belt_name',
				'physical_societies.name as physical_society_name'
			)
			->join(
				'users',
				'users.id',
				'athletes.trainer_id'
			)
			->join(
				'cities',
				'cities.id',
				'users.city_id'
			)
			->join(
				'physical_societies',
				'physical_societies.id',
				'users.physical_society_id'
			)
			->join(
				'grades',
				'grades.id',
				'athletes.grade_id'
			)
			->join(
				'belts',
				'belts.id',
				'athletes.belt_id'
			)
			->where('athletes.id', $id)
			->first();
	}

	protected function getOwnParticipantForAge($userId, $data, $search)
	{
		return $this
			->select(
				'athletes.id',
				'athletes.image',
				'athletes.first_name',
				'athletes.last_name',
				'users.first_name as trainer_first_name',
				'users.last_name as trainer_last_name'
			)
			->leftJoin('tournament_athletes', function($query) use ($data){
				$query
					->on(
						'tournament_athletes.athlete_id',
						'athletes.id'
					)
					->where(
						'tournament_athletes.tournament_id', 
						$data['tournamentId']
					)
					->where(
						'tournament_athletes.age_group_id', 
						$data['age_group_id']
					);
			})
			->leftJoin('age_groups', function($join) use ($data){
				$join
					->where(
						'age_groups.date_from',
						'<=',
						DB::raw('YEAR(athletes.birthday)')
					)
					->where(
						'age_groups.date_to',
						'>=',
						DB::raw('YEAR(athletes.birthday)')
					);
			})
			->leftJoin(
				'users',
				'users.id',
				'athletes.trainer_id'
			)
			->when($search, function($query) use ($search){
				$query->where(
					DB::raw(
						"CONCAT(athletes.first_name, ' ', athletes.last_name)"
					),
					'LIKE',
					'%' . $search . '%'
				);
			})
			->where(function($query) use ($data, $userId){
				if($data['status'] < 3)
					$query->where('athletes.trainer_id', $userId);
			})
			->where('athletes.sex', $data['sex'])
			->where('tournament_athletes.id', '=', null)
			->where('age_groups.id', $data['age_group_id'])
			->groupBy('athletes.id')
			->get();
	}

	protected function getProfileData($id)
	{
		return $this
			->select(
				'cities.name as city_name',
				'organizations.name as organization_name',
				'grades.name as grade_name',
				'belts.name as belt_name',
				'organization_types.name as organization_types_name',
				'users.first_name as trainer_first_name',
				'users.last_name as trainer_last_name',
				'athletes.*'
			)
			->leftJoin(
				'users',
				'users.id',
				'athletes.trainer_id'
			)
			->leftJoin(
				'cities',
				'cities.id',
				'users.city_id'
			)
			->leftJoin(
				'organizations',
				'organizations.id',
				'users.organization_id'
			)
			->leftJoin(
				'grades',
				'grades.id',
				'athletes.grade_id'
			)
			->leftJoin(
				'belts',
				'belts.id',
				'athletes.belt_id'
			)
			->leftJoin(
				'organization_types',
				'organization_types.id',
				'users.physical_society_id'
			)
			->where('athletes.id', $id)
			->first();
	}

	protected function getTeamAtheltes($userId, $tournamentId, $search = null)
	{
		return $this
			->select(
				'athletes.*', 
				'users.first_name as trainer_first_name',
				'users.last_name as trainer_last_name',
				'users.id as trainer_id',
				'age_groups.name as age_group_name',
				'tournament_age_group_weights.weight as weight',
				'tournament_age_group_weights.direction as direction',
				'athletes.id as athlete_id',
				DB::raw("(tournament_age_group_weights.id IS NOT NULL) as checkOnAdded")
			)
			->leftJoin('tournament_athletes', function($query) use ($tournamentId){
				$query
					->on('tournament_athletes.athlete_id', 'athletes.id')
					->where('tournament_athletes.tournament_id', $tournamentId);
			})
			->leftJoin(
				'users',
				'users.id',
				'athletes.trainer_id'
			)
			->leftJoin(
				'age_groups',
				'age_groups.id',
				'tournament_athletes.age_group_id'
			)
			->leftJoin(
				'tournament_age_group_weights',
				'tournament_age_group_weights.id',
				'tournament_athletes.weight_id'
			)
			->when($search, function($query) use ($search){
				$query->where(
					DB::raw(
						"CONCAT(athletes.first_name, ' ', athletes.last_name)"
					),
					'LIKE',
					'%' . $search . '%'
				);
			})
			->when(true, function($query) use ($userId, $tournamentId){
				if(!auth()->user() || auth()->user()->id != $userId){
					$query->where(
						'tournament_athletes.tournament_id', 
						$tournamentId
					);
				}
			})
			->where('athletes.trainer_id', $userId)
			->groupBy('athletes.id')
			->groupBy('age_groups.id')
			->groupBy('tournament_age_group_weights.id')
			->orderBy('checkOnAdded', 'DESC')
			->orderBy('athletes.last_name')
			->orderBy('athletes.first_name')
			->get();
	}
}
