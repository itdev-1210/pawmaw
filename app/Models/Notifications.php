<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Notifications extends Model
{
    protected $table = "notifications";
    
    protected $fillable = [
        'name',
        'image',
        'lost_or_found'
    ];
    public $timestamps = false;
}
