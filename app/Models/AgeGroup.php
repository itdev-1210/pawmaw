<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AgeGroup extends Model
{
    
    public $timestamps = false;

    protected $fillable = [
    	'name',
    	'date_from',
    	'date_to',
    	'creator_id',
        'tournament_id'
    ];

    protected function getAdminAgeGroupsList()
    {
    	return $this
            ->where('tournament_id', 0)
            ->orderBy('date_from', 'DESC')
    		->get();
    }

    protected function getAgeGroup($id)
    {
    	return $this
    		->leftJoin(
    			'tournament_age_group_restricts',
    			'tournament_age_group_restricts.age_group_id',
    			'age_groups.id'
    		)
    		->where('age_groups.id', $id)
    		->first();
    }

    protected function getClientAgeGroups()
    {
        return $this
            ->select(
                'id',
                'name', 
                'date_from', 
                'date_to', 
                'creator_id'
            )
            ->where('tournament_id', 0)
            // ->where('creator_id', auth()->user()->id)
            // ->orWhere('creator_id', 1)
            ->orderBy('creator_id', 'DESC')
            ->orderBy('date_from', 'DESC')
            // ->groupBy('name', 'date_from', 'date_to', 'creator_id')
            ->get();
    }

    protected function getAgeGroupsByTournamentId($id)
    {
        return $this
            ->where('tournament_id', 0)
            ->orderBy('date_from', 'DESC')
            ->get();
    }

    protected function getSelectedAgeGroupsByTournamentId($id)
    {
        return $this
            ->where('tournament_id', $id)
            ->orderBy('date_from', 'DESC')
            ->get();
    }

    protected function getTournamentAgeGroup($tournamentId)
    {
        return $this
            ->where('tournament_id', $tournamentId)
            ->get();
    }

    protected function getAgeGroupsSidebar($tournamentId)
    {
        return $this
            ->select(
                'age_groups.*',
                DB::raw("
                    (
                        SELECT COUNT(*)
                        FROM age_groups as sub_age_groups
                        LEFT JOIN tournament_athletes as athletes
                        ON athletes.age_group_id = sub_age_groups.id
                        LEFT JOIN tournament_age_group_weights as weights
                        ON weights.id = athletes.weight_id
                        WHERE weights.sex = 1
                        AND sub_age_groups.id = age_groups.id
                        GROUP BY sub_age_groups.id
                        LIMIT 1
                    ) as manCount
                "),
                DB::raw("
                    (
                        SELECT COUNT(*)
                        FROM age_groups as sub_age_groups
                        LEFT JOIN tournament_athletes as athletes
                        ON athletes.age_group_id = sub_age_groups.id
                        LEFT JOIN tournament_age_group_weights as weights
                        ON weights.id = athletes.weight_id
                        WHERE weights.sex = 2
                        AND sub_age_groups.id = age_groups.id
                        GROUP BY sub_age_groups.id
                        LIMIT 1
                    ) as womanCount
                ")
            )
            ->where('age_groups.tournament_id', $tournamentId)
            ->get();
    }

}
