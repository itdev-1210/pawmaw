<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FacebookPage extends Model {

    use SoftDeletes;

    public function fbPageSlug($zipcode) {
        return $this
                        ->select(
                                'facebook_pages.*', 'facebook_zip_codes.*'
                        )
                        ->rightJoin(
                                'facebook_zip_codes', 'facebook_zip_codes.facebook_id', 'facebook_pages.id'
                        )
                        ->where('facebook_zip_codes.zip_code', $zipcode)
                        ->first();
    }

}
