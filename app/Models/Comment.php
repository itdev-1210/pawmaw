<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    
	protected $fillable = [
		'post_id', 
		'comment_id', 
		'comment', 
		'name', 
		'email', 
		'checked', 
		'created_at', 
		'updated_at'
	];

	public function getReplies()
	{
		return $this
			->where('comment_id', $this->id)
			->get();
	}

}
