<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TournamentAgeGroupRestrict extends Model
{
    
	public $timestamps = false;

	protected $fillable = [
		'age_group_id',
		'grade_id',
		'belt_id'
	];

}
