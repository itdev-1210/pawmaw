<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PetAdresses extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'street_address',
        'city',
        'state',
        'zip_code',
        'country',
        'country_short',
        'lat',
        'lng',
        'map_img',
    ];
}
