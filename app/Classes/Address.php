<?php

namespace App\Classes;

use App\Models\Country;
use App\Models\Region;
use App\Models\City;

class Address{

	public static function createAndGetCountryId($name)
	{
		$id = Country::getIdByName($name);

		if(!$id){
			$id = Country::insertGetId([
				'name' => $name
			]);

			return $id;
		}

		return $id['id'];
	}

	public static function createAndGetRegionId($name)
	{
		$id = Region::getIdByName($name);

		if(!$id){
			$id = Region::insertGetId([
				'name' => $name
			]);

			return $id;
		}

		return $id['id'];
	}

	public static function createAndGetCityId($name)
	{
		$id = City::getIdByName($name);

		if(!$id){
			$id = City::insertGetId([
				'name' => $name
			]);

			return $id;
		}

		return $id['id'];
	}
}
