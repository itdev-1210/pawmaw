<?php

namespace App\Classes;

use App\Models\TournamentMatchSettings;
use App\Models\TournamentMatchAthleteRelation;
use App\Models\Tournament;
use DateTime;

class SetMatchAthleteRelation{

	private $tournament = [];
	private $athletesCouple = [];
	private $athletes = [];
	private $day;
	private $ring;
	private $deep = 0;
	private $tournamentId = null;
	private $weightId = null;
	private $drawRange = [
		1, 2, 4, 8, 16, 32, 64, 128, 256
	];
	private $countDrawsTypes = [
		32  => 2,
		64  => 4,
		128 => 8
	];

	public function index($tournamentId = null, $weight_id = null)
	{
		$this->tournamentId = $tournamentId;
		$this->weightId = $weight_id;

		$this->deleteOldData();

		foreach ($this->getTournaments() as $this->tournament) {
			foreach ($this->getTournamentDays() as $this->day){
				foreach ($this->getTournamentRings() as $this->ring){
					$this->setMatchRelation();

					$this->deep = 0;
				}
			}
		}
	}

	private function deleteOldData()
	{
		if($this->tournamentId)
			TournamentMatchAthleteRelation
				::leftJoin(
					'tournament_match_settings',
					'tournament_match_settings.id',
					'tournament_match_athlete_relations.tournament_match_settings_id'
				)
				->where('tournament_match_settings.tournament_id', $this->tournamentId)
				->delete();
		else
			TournamentMatchAthleteRelation::truncate();
	}

	private function setMatchRelation()
	{
		$this->getTournamentAthletesByRing();

		if(count($this->athletes) != 0){

			$this->setAthletesCouple();
			
			$this->saveAthletesCoupleInDatabase();
		}
	}

	private function saveAthletesCoupleInDatabase()
	{
		if($this->deep == 0){
			$this->saveAthletesCoupleInDatabaseFirst();
			return;
		}

		foreach ($this->athletesCouple as $weightKey => $weightGroup){
			foreach ($weightGroup as $drawKey => $drawGroup){
				foreach ($drawGroup as $key => $couple) {
					$coupleId = TournamentMatchAthleteRelation
						::orderBy('id', 'DESC')
						->first();

					$coupleId = $coupleId != null
						? $coupleId->id
						: 0;

					$data = [
						'tournament_match_settings_id' => $couple[0]['id'],
						'deep'                         => $this->deep,
						'order'                        => 1,
						'couple_id'					   => $coupleId,
						'parent_couple_id'			   => $couple[0]['couple_id'],
						'draw_group'				   => $drawKey,
						'place'						   => $couple[0]['place'] - 1
					];

					$data['fight_number'] = $this->getFightNumberByRing();

					if($this->deep == 1 && $couple[0]['deep'] == 1){
						TournamentMatchAthleteRelation
							::where('couple_id', $couple[0]['couple_id'])
							->update([
								'fight_number' => $data['fight_number'],
								'place' => $couple[0]['place'] - 1
							]);
						
					} else{
						if(count($couple) == 1){
							$data['fight_number'] = 0;

							TournamentMatchAthleteRelation::create($data);
						} else {

							if($couple[0]['fight_number'] == 0)
								$data['athlete_id'] = $couple[0]['athlete_id'];
							else
								$data['athlete_id'] = 0;

							TournamentMatchAthleteRelation::create($data);

							$data['athlete_id'] = 0;
							$data['parent_couple_id'] = $couple[1]['couple_id'];
							$data['order'] = 2;

							TournamentMatchAthleteRelation::create($data);
						}
					}
				}
			}
		}

		$this->deep++;

		$this->setMatchRelation();
	}

	private function saveAthletesCoupleInDatabaseFirst()
	{
		foreach ($this->athletesCouple as $groupKey => $groupDivide) {
			foreach ($groupDivide as $weightKey => $weightDivide) {
				foreach ($weightDivide as $drawNumberKey => $drawNumber) {
					$drawNumber = array_reverse($drawNumber);

					foreach ($drawNumber as $key => $couple) {
						$fight_number = $this->getFightNumberByRing();

						$coupleId = TournamentMatchAthleteRelation
							::orderBy('id', 'DESC')
							->first();

						$coupleId = $coupleId != null
							? $coupleId->id
							: 0;

						$couple[0]['couple_id'] = $coupleId;
						$couple[1]['couple_id'] = $coupleId;

						if($couple[1]['athlete_id'] == 0 || $couple[0]['deep'] == 1){
							$couple[0]['fight_number'] = 0;
							$couple[1]['fight_number'] = 0;
						} else{

							$couple[0]['fight_number'] = $fight_number;
							$couple[1]['fight_number'] = $fight_number;
						}


						TournamentMatchAthleteRelation::create($couple[0]);
						TournamentMatchAthleteRelation::create($couple[1]);
					}
				}
			}
		}		

		$this->deep++;

		$this->setMatchRelation();
	}

	private function saveOneParticipant($weightGroup)
	{
		$athlete = $weightGroup[0];

		$coupleId = TournamentMatchAthleteRelation
			::orderBy('id', 'DESC')
			->first();

		$coupleId = $coupleId != null
			? $coupleId->id
			: 0;

		$data = [
			'athlete_id'                   => $athlete['athlete_id'],
			'tournament_match_settings_id' => $athlete['id'],
			'draw_group'                   => 1,
			'change_able'                  => 0,
			'fight_number'                 => 0,
			'deep'                         => 2,
			'order'                        => 1,
			'couple_id'					   => $coupleId,
			'place'						   => 1
		];

		TournamentMatchAthleteRelation::create($data);
	}

	private function setAthletesCouple()
	{
		if($this->deep == 0){
			$this->setAthletesCoupleFirst();
			return;
		}

		$dividedAthletes = [];
		$this->athletesCouple = [];

		foreach ($this->athletes as $key => $athlete) {
			$dividedAthletes
				[$athlete['weight_id']][$athlete['draw_group']][] = $athlete;
		}

		foreach ($dividedAthletes as $weightKey => $weightDivide) {
			if(count($weightDivide) > 1){
				$check = 1;
				$counter = 1;
				$couple = 0;

				for($i = 0; $i < count($weightDivide); $i++){
					if(count($weightDivide[array_keys($weightDivide)[$i]]) > 1)
						$check = 0;
				}

				if($check){
					foreach ($weightDivide as $key => $value) {
						if($couple == 0){
							$weightDivide[$key][0] = $value[0];
							$couple = $key;
						} else{
							$weightDivide[$couple][1] = $value[0];
							unset($weightDivide[$key]);
							$couple = 0;
						}

					}
				} else{
					foreach ($weightDivide as $key => $value) {
						if(count($value) == 1)
							unset($weightDivide[$key]);
					}
				}
			}

			foreach ($weightDivide as $drawNumberKey => $drawNumber) {
				$drawGroup = &$this->athletesCouple
					[$weightKey][$drawNumberKey];
				$couple = 1;

				foreach ($drawNumber as $key => $athlete){
					if(
						($athlete['deep'] == 0 && $this->deep == 1)
						|| ($athlete['deep'] >= 1 && $this->deep > 1)
					){
						if($couple == 1){
							$drawGroup[$key][0] = $athlete;
							$couple = 0;
						} else{
							if($athlete['id'] == $drawNumber[$key - 1]['id']){
								$drawGroup[$key - 1][1] = $athlete;
								$couple = 1;
							} else{
								$drawGroup[$key][0] = $athlete;
							}
						}
					} else{
						$drawGroup[$key][0] = $athlete;
						$drawGroup[$key][1] = $athlete;
					}
				}

				if(count($drawGroup) > 1){
					end($drawGroup);
					$key = key($drawGroup);

					if(count($drawGroup[$key]) == 1){
						$drawGroup[$key][] = $drawGroup[$key][0];
					}
				}
			}
		}
	}

	private function setAthletesCoupleFirst()
	{
		$dividedAthletes = [];
		$this->athletesCouple = [];
	
		foreach ($this->athletes as $key => $athlete) {
			$dividedAthletes
				[$athlete['age_group_id']][$athlete['weight_id']][] = $athlete;
		}

		foreach ($dividedAthletes as $groupKey => $groupDivide) {
			foreach ($groupDivide as $weightKey => $weightDivide) {
				
				if(count($weightDivide) == 1 && $this->deep == 0){
					$this->saveOneParticipant($weightDivide);

					continue;
				}

				$place = $this->getPlaceByCount(count($weightDivide));

				$weightDivide = $this->divideSimilarAthletes($weightDivide);

				$weightDivide = $this->divideDraws($weightDivide);

				$this->drawCounts[$weightKey] = count($weightDivide);

				foreach ($weightDivide as $drawNumbeKey => $drawNumber) {
					$minCount = null;
					$maxCount = null;
					$couple = 1;
					$data = [];

					for($i = 0; $i < count($this->drawRange); $i++){
						if(
							count($drawNumber) > $this->drawRange[$i]
							&& count($drawNumber) < @$this->drawRange[$i + 1]
						){
							$minCount = $this->drawRange[$i];
							$maxCount = $this->drawRange[$i + 1];
						}
					}

					foreach ($drawNumber as $athleteKey => $athlete) {
						$weightGroup = &$this->athletesCouple
							[$groupKey][$weightKey][$drawNumbeKey];
						$conditionKey = $athleteKey + 1;
						$countDifference = $maxCount - count($drawNumber);
						$firstCondition = (
							$conditionKey <= $countDifference 
							&& $conditionKey + 1 <= $countDifference
						);
						$secondCondition = (
							$athleteKey % 2 == 1 && $conditionKey <= $countDifference
						);
						$thirdCondition = (
							count($weightDivide) > 1 
							&& (
								array_search(count($drawNumber), $this->drawRange) 
								|| array_search(count($drawNumber) + 1, $this->drawRange)
							)
						);

						$data = [
							'athlete_id' => $athlete['athlete_id'],
							'tournament_match_settings_id' => $athlete['id'],
							'draw_group' => $drawNumbeKey + 1,
							'change_able' => 1,
							'place' => $place
						];

						if($firstCondition || $secondCondition || $thirdCondition)
							$data['deep'] = 1;
						else
							$data['deep'] = 0;

						$data['order'] = $couple;

						if($couple == 1){
							$weightGroup[$athleteKey][0] = $data;

							$couple = 2;
						} else{
							$weightGroup[$athleteKey - 1][1] = $data;

							$couple = 1;
						}
					}

					if(count($drawNumber) % 2 == 1){
						$data['athlete_id'] = 0;
						$data['order'] = 2;
						$weightGroup[count($drawNumber) - 1][1] = $data;
					}
				}
			}
		}
	}

	private function divideDraws($weightDivide)
	{
		$countChunks = 1;
		$sizeInChunk = null;

		foreach ($this->countDrawsTypes as $key => $value) {
			if(
				$key < count($weightDivide)
				&& $key * 2 > count($weightDivide)
			)
				$countChunks = $value;
		}

		$sizeInChunk = ceil(count($weightDivide) / $countChunks);

		return array_chunk(
			$weightDivide,
			$sizeInChunk
		);
	}

	private function getFightNumberByRing()
	{
		return TournamentMatchAthleteRelation::getMaxFigthNumberByRing(
			$this->tournament['id'], $this->day, $this->ring
		) + 1;
	}

	private function getTournamentAthletesByRing()
	{
		$this->athletes = [];

		$parameters = [$this->tournament['id'], $this->day, $this->ring];

		if($this->deep > 1)
			$parameters[] = $this->deep;

		switch($this->deep){
			case 0:
				$this->athletes = TournamentMatchSettings
					::getAthletesByRing(...$parameters);
				break;
			default:
				$this->athletes = TournamentMatchAthleteRelation
					::getAthletesByDeep(...$parameters);
		}
	}

	private function getPlaceByCount($count)
	{
		$places = [
			1 => 1,
			2 => 2,
			3 => 4,
			4 => 8,
			5 => 16,
			6 => 32,
			7 => 64,
			8 => 128
		];

		$place = 0;

		for($i = 1; $i <= count($places); $i++){
			if(
				$count > $places[$i]
				&& $count <= @$places[$i + 1]
			){
				$place = array_keys($places)[$i];

				break;
			}
		}

		return $place;
	}

	private function divideSimilarAthletes($athletes)
	{
		$athletesCount = [];
		$trainerAthletesGroup = [];
		$generalOrder = [];
		$maxCount = 0;
		$dividedAthletes = [];

		foreach ($athletes as $key => $athlete) {
			if(!array_key_exists($athlete['trainer_id'], $athletesCount))
				$athletesCount[$athlete['trainer_id']] = 0;

			$athletesCount[$athlete['trainer_id']]++;
			$trainerAthletesGroup[$athlete['trainer_id']][] = $athlete;
		}

		arsort($athletesCount);

		foreach ($athletesCount as $key => $athleteCount) {
			$generalOrder = array_merge(
				$generalOrder, 
				$trainerAthletesGroup[$key]
			);
		}

		$maxCount = $athletesCount[array_keys($athletesCount)[0]];

		for($i = 0; $i < $maxCount; $i++){
			for($j = $i; $j < count($generalOrder); $j += $maxCount){
				$dividedAthletes[] = $generalOrder[$j];
			}
		}

		return $dividedAthletes;
	}

	private function getTournamentRings()
	{
		return range(1, $this->tournament['rings_count']);
	}

	private function getTournamentDays()
	{
		$dateFrom = new DateTime($this->tournament['date_from']);
		$dateTo = new DateTime($this->tournament['date_to']);

		$difference = $dateFrom->diff($dateTo)->d + 1;

		return range(1, $difference);
	}

	private function getTournaments()
	{
		return Tournament::getDrawTournaments($this->tournamentId);
	}
	
}