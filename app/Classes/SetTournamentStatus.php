<?php

namespace App\Classes;

use App\Models\Tournament;
use DateTime;

class SetTournamentStatus{

	private $kindDates = [
		1 => 'register_from',
		2 => 'register_to',
		3 => 'date_from',
		4 => 'date_to'
	];
	private $tournaments = [];
	private $activeTournament = [];
	private $activeStatus = [];
	private $tournamentId = null;

	public function index($tournamentId = null)
	{
		$this->tournamentId = $tournamentId;

		$this->getTournaments();

		$this->setTournamentStatus();
	}

	private function setTournamentStatus()
	{
		$date = date("Y-m-d");	

		foreach ($this->tournaments as $key => $tournament) {
			$this->activeTournament = $tournament;

			foreach($this->kindDates as $key => $kind){

				if($tournament[$kind] > $date){
					$currentTime = new DateTime($date);
					$tournamentTime = new DateTime($tournament[$kind]);

					$this->activeStatus = [
						'status'      => $key,
						'status_days' => $currentTime
							->diff($tournamentTime)
							->days
					];

					$this->saveStatusInDatabase();

					break;
				}

				if($key + 1 == count($this->kindDates)){
					$this->activeStatus = [
						'status'      => 5,
						'status_days' => 0
					];

					$this->saveStatusInDatabase();
				}
			}
		}
	}

	private function saveStatusInDatabase()
	{
		Tournament
			::where('id', $this->activeTournament['id'])
			->update($this->activeStatus);
	}

	private function getTournaments()
	{
		$this->tournaments = Tournament
			::getTournamnetListForStatus($this->tournamentId);
	}
	
}