<?php

namespace App\Classes;

class Image{

	public static function saveImage($data, $path)
	{
		$img = base64_decode(preg_replace(
			'#^data:image/\w+;base64,#i',
			'',
			$data
		));

		$imageName = $path . '/' . substr(md5(mt_rand()), 0, 32) . ".png";

		file_put_contents(
			public_path('image/') . $imageName,
			$img
		);

		return $imageName;
	}

	public static function saveWithOptimize($file, $path)
	{
		$filename = $file->getClientOriginalName();
		$fileFormat = array_reverse(explode('.', $filename))[0];
		$filename = substr(md5(mt_rand()), 0, 32) . '.' . $fileFormat;

		$file->move(
			public_path('image/') . $path, 
		 	$filename
		);

		self::compressImage(public_path('image/') . $path . '/' . $filename);

		return $path . '/' . $filename;
	}

	public function optimizeAllPhoto($folder){
		$path = public_path('image/' . $folder);
		$list = scandir($path);

		foreach ($list as $item) {
			$image = $path . '/' . $item;

			if(@exif_imagetype($image))
				$this->compressImage($image);
		}
	}

	private static function compressImage($path)
	{
		$info = getimagesize($path);

        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($path);

        elseif ($info['mime'] == 'image/gif')
            $image = imagecreatefromgif($path);

   		elseif ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($path);

        imagejpeg($image, $path, 30);
	}

}
