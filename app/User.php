<?php

namespace App;

use App\Mail\UserCreated;
use App\Notifications\UserResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use Notifiable;
	CONST VERIFIED_USER = 1;
	CONST UNVERIFIED_USER = 0;

	protected $primaryKey = 'id';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'zip',
		'phone',
		'address',
		'email',
		'password',
		'verified',
		'verification_token',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
		'verification_token',
	];

	/**
	 * Send the password reset notification.
	 *
	 * @param  string $token
	 *
	 * @return void
	 */
	public function sendPasswordResetNotification( $token )
	{
		$this->notify( new UserResetPassword( $token ) );
	}

	public function user_info()
	{
		return $this->belongsTo( 'App\Models\PetsInfo', 'user_id', 'id' );
	}

	public function payments()
	{
		return $this->hasMany( 'App\Models\Payment', 'user_id', 'id' );
	}

	public static function generateVerificationCode()
	{
		return str_random( 40 );
	}

	public function isVerified()
	{
		return $this->verified == USER::VERIFIED_USER;
	}

	public function isAdmin()
	{
		if ( $this->type == 'admin' ) {
			return $this->type = true;
		} else {
			return $this->type = false;
		}

	}

//    protected $dispatchesEvents = [
//        'created' => UserCreated::class,
//    ];
}
