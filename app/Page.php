<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use DB;

class PagePost extends Model
{
    protected $table = "page_entries";
    protected $fillable = ['title','slug', 'content', 'metatitle', 'metadesc'];

}
