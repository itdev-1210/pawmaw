<?php

namespace App\Console\Commands;

use App\Jobs\SendNewPostEmail;
use App\Models\PetsInfo;
use App\Models\ProcessTest;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PostCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'custom:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $process = new ProcessTest();
        $process->start_time = Carbon::now();
        $process->process_name = "postemail";
        $pets  = PetsInfo::with('user')
            ->whereRaw(DB::raw('(thirtyMinuteMailSend = 0  and DATE_ADD(created_at,INTERVAL 30 MINUTE)<Now()) or (thirtyMinuteMailSend = 1 and oneDayMailSend = 0  and DATE_ADD(created_at,INTERVAL 1440 MINUTE)<Now())'))
            ->limit(10)
            ->get();



        if(count($pets)>0){
            foreach ($pets as $pet) {

                if($pet->user!=null){
                    SendNewPostEmail::dispatch($pet);

                    $status =true;
                    if($pet->thirtyMinuteMailSend==0){
                        $pet->thirtyMinuteMailSend=1;
                        $status = false;
                    }

                    if($status){
                        $pet->oneDayMailSend=1;
                    }

                    $pet->save();
                }



            }
        }

        $process->end_time = Carbon::now();

        $process->save();
        $this->info('thanks you');
    }
}
