<?php

namespace App\Console\Commands;

use App\Jobs\SendNewPostEmail;
use App\Mail\DownloadFlyerEmail;
use App\Mail\FoundBlogEmail;
use App\Mail\LostBlogEmail;
use App\Mail\PackageEmail;
use App\Mail\ScamEmail;
use App\Mail\StillMissingEmail;
use App\Models\EmailTime;
use App\Models\PetsInfo;
use App\Models\ProcessTest;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ClearEmailQueueCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:queue:flush';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear email queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $q = EmailTime::where('time', '<=', time())->where('status', 0);
        $time_mails = $q->get();
        $rowsCount = $q->count();

        $this->info('EmailTime rows for deleting: '.$rowsCount);
        $bar = $this->output->createProgressBar($rowsCount);
        $bar->start();
        foreach ($time_mails as $time_mail) {
            $object = json_decode($time_mail->object);
            if ($time_mail->type == 1) {
                // Type 1 : One time Email
                if ($object->model == 'scam') {
                    $time_mail->delete();
                } elseif ($object->model == 'lost') {
                    $time_mail->delete();
                } elseif ($object->model == 'found') {
                    $time_mail->delete();
                } elseif ($object->model == 'flyer') {
                    $time_mail->delete();
                }
            } elseif ($time_mail->type == 2) {
                $petCreateDate = strtotime('+1 month', strtotime($object->created_at));
                // Type 2 : Repetitive
                if ($object->model == 'package' && $petCreateDate > time()) {

                    $checkStatus = \DB::table('pets_info')->select('type')->where('id', '=', $time_mail->pet_id)->first();

                    if($checkStatus && $checkStatus->type != 'reunited') {
                        $time_mail->time += 172800; // time after 2 days
                        $time_mail->update();

                    }
                }elseif ($object->model == 'missing' && $petCreateDate > time()) {

                    $checkStatus = \DB::table('pets_info')->select('type')->where('id', '=', $time_mail->pet_id)->first();

                    if($checkStatus && $checkStatus->type != 'reunited') {
                        $time_mail->time += 172800; // time after 2 days
                        $time_mail->update();
                    }
                }
            }

            $bar->advance();
        }

        $bar->finish();

        $this->info('Left EmailTime rows for emails sending: '.EmailTime::where('time', '<=', time())->where('status', 0)->count());
    }
}
