<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;
use App\Models\EmailTime;
use Illuminate\Support\Facades\Mail;
use App\Mail\ScamEmail;
use App\Mail\PackageEmail;
use App\Mail\LostBlogEmail;
use App\Mail\FoundBlogEmail;
use App\Mail\DownloadFlyerEmail;
use App\Mail\StillMissingEmail;
use Carbon\Carbon;

use function GuzzleHttp\json_decode;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\PostCommand',
        'App\Console\Commands\LostCommand',
        'App\Console\Commands\FoundCommand',
        'App\Console\Commands\ClearEmailQueueCommand'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('custom:command --force')->everyThirtyMinutes();

        $schedule->command('lost:command --force')->everyThirtyMinutes();

        $schedule->command('found:command --force')->everyThirtyMinutes();
        /**
         * TODO: MOVE this into a command. This code is running only on application start...
         *
         * it should be a command!!!
         */
        /**
         * Ask to previous developer: Why did you just block this part of code?
         */
        // return;

        $time_mails = EmailTime::where('time', '<=', time())->where('status', 0)->get();
        \Log::info(count($time_mails));
        $canSendEmailsAt = Carbon::parse('2020-01-25 23:35:35')->addMinutes(50);
        if (Carbon::now()->lt($canSendEmailsAt)) {
            return;
        }

        foreach ($time_mails as $time_mail) {
            $object = json_decode($time_mail->object);
            if ($time_mail->type == 1) {
                // Type 1 : One time Email
                if ($object->model == 'scam') {
                    Mail::to($object->contact_email)->send(new ScamEmail($object));
                    $time_mail->delete();
                } elseif ($object->model == 'lost') {
                    Mail::to($object->contact_email)->send(new LostBlogEmail($object));
                    $time_mail->delete();
                } elseif ($object->model == 'found') {
                    Mail::to($object->contact_email)->send(new FoundBlogEmail($object));
                    $time_mail->delete();
                } elseif ($object->model == 'flyer') {
                    Mail::to($object->contact_email)->send(new DownloadFlyerEmail($object));
                    $time_mail->delete();
                }
            } elseif ($time_mail->type == 2) {
                $petCreateDate = strtotime('+1 month', strtotime($object->created_at));
                // Type 2 : Repetitive
                if ($object->model == 'package' && $petCreateDate > time()) {

                    $checkStatus = \DB::table('pets_info')->select('type')->where('id', '=', $time_mail->pet_id)->first();

                    if($checkStatus && $checkStatus->type != 'reunited') {

                        Mail::to($object->contact_email)->send(new PackageEmail($object));
                        $time_mail->time += 172800; // time after 2 days
                        $time_mail->update();

                    }
                }elseif ($object->model == 'missing' && $petCreateDate > time()) {

                    $checkStatus = \DB::table('pets_info')->select('type')->where('id', '=', $time_mail->pet_id)->first();

                    if($checkStatus && $checkStatus->type != 'reunited') {

                        Mail::to($object->contact_email)->send(new StillMissingEmail($object));
                        $time_mail->time += 172800; // time after 2 days
                        $time_mail->update();

                    }
                }
            }
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
