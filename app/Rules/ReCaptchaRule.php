<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ReCaptchaRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        /**
         * Don't know if all forms have recaptcha
         */
        if(!$value){
            return true;
        }
        $secret_key = env('INVISIBLE_RECAPTCHA_SECRETKEY');
        $verifyCaptchaResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$value);
        $responseCaptchaData = json_decode($verifyCaptchaResponse);
        if($responseCaptchaData->success) {
            return true;
            //proceed with form values
        } else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The reCAPTCHA validation failed';
    }
}
