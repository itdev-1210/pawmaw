<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use DB;

class BlogPost extends Model
{
    use HasSlug;

    protected $table = "blog_entries";
    protected $fillable = ['title','slug', 'content','category_id', 'summary', 'description', 'image', 'excerpt'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['publish_after'];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    protected function getPopularPosts()
    {
        return $this
            ->select(
                'blog_entries.*',
                DB::raw("
                    (
                        SELECT COUNT(*)
                        FROM comments
                        WHERE comments.post_id = blog_entries.id
                        GROUP BY blog_entries.id
                    ) as comment_count
                ")
            )
            ->orderBy('comment_count', 'DESC')
            ->limit(4)
            ->get();
    }
}
