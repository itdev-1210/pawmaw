<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $mailData;

    public function __construct($maildata)
    {
        $this->mailData = $maildata;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		return $this
			->from('tournament-system@golden-dragon.com.ua', 'Golden Dragon')
            ->subject('Завершення регістрації')
            ->view('mails.mail', $this->mailData);
    }
}
