<?php

namespace App\Mail;

use App\Models\PetsEmail;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FoundBlogEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $pet;
    public $email;
    /**
     * Create a new message instance.
     *
     * @param $email

     */
    public function __construct( $pet)
    {
        $this->pet = $pet;
        $this->email = $pet->contact_email;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Step-by-Step Guide What To Do With Your Found Pet";

       
        $emailData = [
            'subject'       => $subject,
            'email'         => $this->email,
        ];

        return $this
        ->subject($subject)
        ->from(env('MAIL_FROM_ADDRESS'), 'PawMaw')
        ->markdown('emails.foundblog_email')
        ->with($emailData)
        ->withSwiftMessage(function($message) {
            $headers = $message->getHeaders();
            $headers->addTextHeader("X-Mailgun-Variables", '{"type": "newsletter_mail"}');
            $headers->addTextHeader("X-Mailgun-Tag", "newsletter_mail");
        });     
    }



}
