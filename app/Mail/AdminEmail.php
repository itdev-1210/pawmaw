<?php

namespace App\Mail;

use App\Models\PetsEmail;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $email;

    /**
     * Create a new message instance.
     *
     * @param $email

     */
    public function __construct( $email)
    {
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Important Notice for Find {$this->email->pet->name} Quickly";

        $emailData = [
            'subject'       => $subject,
            'email'         => $this->email,
        ];

        return $this
        ->subject($subject)
        ->from(env('MAIL_FROM_ADDRESS'), 'PawMaw')
        ->markdown('emails.post_email')
        ->with($emailData)
        ->withSwiftMessage(function($message) {
            $headers = $message->getHeaders();
            $headers->addTextHeader("X-Mailgun-Variables", '{"type": "newsletter_mail"}');
            $headers->addTextHeader("X-Mailgun-Tag", "newsletter_mail");
        });
    }



}
