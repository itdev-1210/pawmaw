<?php

namespace App\Mail;

use App\Models\PetsEmail;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ScamEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $pet;
    public $email;
    /**
     * Create a new message instance.
     *
     * @param $email

     */
    public function __construct( $pet)
    {
        $this->pet = $pet;
        $this->email = $pet->contact_email;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = " How To Identify Lost/Found Pet Scam !";

        return $this
            ->subject($subject)
            ->from(env('MAIL_FROM_ADDRESS'), 'PawMaw')
            ->view('emails.scam_email');
    }



}
