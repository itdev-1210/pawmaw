<?php

namespace App\Mail;

use App\Models\PetsEmail;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PackageEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $pet;
    public $email;
    /**
     * Create a new message instance.
     *
     * @param $email

     */
    public function __construct( $pet)
    {
        $this->pet = $pet;
        $this->email = $pet->contact_email;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->pet->type=='lost') {
            
            $subject = " Activate your PawMaw Alert to increase your chances of finding {$this->pet->name}.";
            
        }else{
            
            $subject = "Activate your PawMaw Alert to increase your chances of finding owner.";
        }
       // return $this->subject($subject)->from(env('MAIL_FROM_ADDRESS'),'PawMaw')->view('emails.package_email');
       
       $emailData = [
            'subject'       => $subject,
            'email'         => $this->email,
        ];

        return $this
        ->subject($subject)
        ->from(env('MAIL_FROM_ADDRESS'), 'PawMaw')
        ->markdown('emails.package_email')
        ->with($emailData)
        ->withSwiftMessage(function($message) {
            $headers = $message->getHeaders();
            $headers->addTextHeader("X-Mailgun-Variables", '{"type": "newsletter_mail"}');
            $headers->addTextHeader("X-Mailgun-Tag", "newsletter_mail");
        });
       
    }



}
