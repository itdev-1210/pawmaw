<?php

namespace App\Mail;

use App\Models\PetsEmail;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FbPostEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $email;

    /**
     * Create a new message instance.
     *
     * @param $email

     */
    public function __construct( $pet)
    {
        $this->pet = $pet;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "View {$this->pet->name}’s Facebook Post";

            
        $emailData = [
            'subject'       => $subject,
            'email'         => $this->email,
        ];

        return $this
        ->subject($subject)
        ->from(env('MAIL_FROM_ADDRESS'), 'PawMaw')
        ->markdown('emails.fbpost_email')
        ->with($emailData)
        ->withSwiftMessage(function($message) {
            $headers = $message->getHeaders();
            $headers->addTextHeader("X-Mailgun-Variables", '{"type": "newsletter_mail"}');
            $headers->addTextHeader("X-Mailgun-Tag", "newsletter_mail");
        });    
    }



}
