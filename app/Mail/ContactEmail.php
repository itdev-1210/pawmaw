<?php

namespace App\Mail;

use App\Models\PetsEmail;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $email;

    /**
     * Create a new message instance.
     *
     * @param $email

     */
    public function __construct( $email)
    {
       // echo'Email<pre>'; print_r($email->email);exit;
        
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Message From Contact Form')
            ->from(env('MAIL_FROM_ADDRESS'), 'PawMaw')
            ->view('emails.contact_email');
    }
}
