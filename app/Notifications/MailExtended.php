<?php
/**
 * Created by PhpStorm.
 * User: tapos
 * Date: 10/28/2018
 * Time: 12:33 AM
 */

namespace App\Notifications;


use Illuminate\Notifications\Messages\MailMessage;

class MailExtended extends MailMessage
{
    /**
     * The notification's data.
     *
     * @var string|null
     */
    public $viewData;

    /**
     * Set the content of the notification.
     *
     * @param string $greeting
     *
     * @return $this
     */
    public function content($content)
    {
        $this->viewData['content'] = $content;

        return $this;
    }

    /**
     * Get the data array for the mail message.
     *
     * @return array
     */
    public function data()
    {
        return array_merge($this->toArray(), $this->viewData);
    }
}
