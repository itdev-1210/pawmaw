<?php
require_once "Mobile_Detect.php";
function encodeString($password, $text)
{
	$salt = openssl_random_pseudo_bytes(256);
	$iv = openssl_random_pseudo_bytes(16);
	$iterations = 999;
	$key = hash_pbkdf2("sha512", $password, $salt, $iterations, 64);

	$encrypted_data = openssl_encrypt(
		$text,
		'aes-256-cbc',
		hex2bin($key),
		OPENSSL_RAW_DATA,
		$iv
	);

    $data = array(
		"ciphertext" => base64_encode($encrypted_data),
		"iv"         => bin2hex($iv),
		"salt"       => bin2hex($salt)
	);

	return json_encode($data);
}

function isMobile() {
    
    $detect = new Mobile_Detect;
    
    if ( $detect->isMobile()) {
         return true;
     } else {
         return false;
     }
}