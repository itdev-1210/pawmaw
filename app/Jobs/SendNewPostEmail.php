<?php

namespace App\Jobs;

use App\Mail\NewPostCronEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendNewPostEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $petWithUser;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($petWithUser)
    {
        //
        $this->petWithUser = $petWithUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->petWithUser->user->email)->send(new NewPostCronEmail($this->petWithUser))->addTextHeader('X-Mailgun-Tag', 'newsletter_mail');;
    }
}
