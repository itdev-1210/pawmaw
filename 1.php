<?php 

$a = '/**/_xdc_._cetzd7 && _xdc_._cetzd7( {
   "html_attributions" : [],
   "result" : {
      "address_components" : [
         {
            "long_name" : "1170",
            "short_name" : "1170",
            "types" : [ "street_number" ]
         },
         {
            "long_name" : "Shakespeare Avenue",
            "short_name" : "Shakespeare Ave",
            "types" : [ "route" ]
         },
         {
            "long_name" : "Highbridge",
            "short_name" : "Highbridge",
            "types" : [ "neighborhood", "political" ]
         },
         {
            "long_name" : "The Bronx",
            "short_name" : "The Bronx",
            "types" : [ "sublocality_level_1", "sublocality", "political" ]
         },
         {
            "long_name" : "Bronx County",
            "short_name" : "Bronx County",
            "types" : [ "administrative_area_level_2", "political" ]
         },
         {
            "long_name" : "New York",
            "short_name" : "NY",
            "types" : [ "administrative_area_level_1", "political" ]
         },
         {
            "long_name" : "United States",
            "short_name" : "US",
            "types" : [ "country", "political" ]
         },
         {
            "long_name" : "10452",
            "short_name" : "10452",
            "types" : [ "postal_code" ]
         }
      ],
      "adr_address" : "\u003cspan class=\"street-address\"\u003e1170 Shakespeare Ave\u003c/span\u003e, \u003cspan class=\"locality\"\u003eThe Bronx\u003c/span\u003e, \u003cspan class=\"region\"\u003eNY\u003c/span\u003e \u003cspan class=\"postal-code\"\u003e10452\u003c/span\u003e, \u003cspan class=\"country-name\"\u003eUSA\u003c/span\u003e",
      "formatted_address" : "1170 Shakespeare Ave, The Bronx, NY 10452, USA",
      "geometry" : {
         "location" : {
            "lat" : 40.8359273,
            "lng" : -73.9233521
         },
         "viewport" : {
            "northeast" : {
               "lat" : 40.8372871802915,
               "lng" : -73.92205601970849
            },
            "southwest" : {
               "lat" : 40.8345892197085,
               "lng" : -73.9247539802915
            }
         }
      },
      "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png",
      "id" : "b806e478f7bbcdb81e85c5e2d557e2ee08a0e431",
      "name" : "1170 Shakespeare Ave",
      "place_id" : "ChIJa-AHpTr0wokRrVHQEPsHIbk",
      "plus_code" : {
         "compound_code" : "R3PG+9M New York, United States",
         "global_code" : "87G8R3PG+9M"
      },
      "reference" : "ChIJa-AHpTr0wokRrVHQEPsHIbk",
      "scope" : "GOOGLE",
      "types" : [ "street_address" ],
      "url" : "https://maps.google.com/?q=1170+Shakespeare+Ave,+The+Bronx,+NY+10452,+USA&ftid=0x89c2f43aa507e06b:0xb92107fb10d051ad",
      "utc_offset" : -300,
      "vicinity" : "The Bronx"
   },
   "status" : "OK"
}
 )';

$a = json_decode($a);
var_dump($a);

?>