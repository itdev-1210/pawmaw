@extends('pawmawFront.layouts.front')

@section('title', 'Dashboard - PawMaw')

@section('content')
    <!-- USER PANEL MAIN SECTION START -->
    <section class="user-panel use-panel_bg mt-5">
        <div class="user-panel_inner" style="background-image: url({{ asset('pawmaw/img/dashboard-bg.png')}});background-position: center; background-size: contain;">
            <div class="container">
                <div class="row">
                    <!-- SIDEBAR START -->
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        @include('user.partials.sidebar')
                    </div>
                    <!-- SIDEBAR END -->

                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <div class="user-panel_inner-content" style="margin-top: 0px;">
                            <!-- JOIN OUR NETWORK SECTION START -->
                            <div class="pet-join-network">
                                <div class="joinournetwork">
                                    <div class="joinournetwork-bg" style="background-image: url( {{ asset('pawmaw/img/common-bg.png') }});">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h1 class="joinournetwork-bg_title others-page_hero-title">JOIN OUR PAWMAW NETWORK</h1>
                                                    <p class="joinournetwork-bg_tag-line">We know how much you care about pets! </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="joinournetwork-content">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="joinournetwork-content_inner">
                                                        <h2 class="joinournetwork-content_inner-title mb-5">OUR PET IS LIKE OUR FAMILY MEMBER!</h2>
                                                        <p class="joinournetwork-content_inner-text mb-5">
                                                            This is why we created PawMaw Pet Network - the growing community of pet lovers, volunteers, pet professionals and other truly wonderful people who understand the devastation of losing a pet and willing to offer their help and support.
                                                        </p>
                                                        <p class="joinournetwork-content_inner-text mb-5">
                                                            Our pet is like our family member. This is why our motive is to help lost/found pets to reunited with their family.
                                                        </p>

                                                        <div class="joinournetwork-content_inner-search mb-3" data-zip-code="{{ @$_GET['zip_code'] }}">
                                                            <img src="{{ asset('pawmaw/img/icons/join.png')}}" alt="Join Icon" class="img-fluid mx-auto d-block mb-5">
                                                            <form action="{{url('user/join_network')}}">  
                                                            
                                                                <div class="form-group has-search mx-auto d-block">
                                                                    <span class="form-control-zip"><i class="fas fa-search-location"></i></span>
                                                                    <input type="search" name="zip_code" id="search-zip-code" class="form-control" placeholder="ZIP Code">
                                                                </div>
                                                                <button type="submit" id="searchButton" class="joinournetwork-content_inner-search-btn zip-code_btn mx-auto d-block">Search by ZIP</button>
                                                               
                                                           </form>
                                                        </div>
                                               				<script>
                                                                    function getUrlVars()
                                                                    {
                                                                        var vars = [], hash;
                                                                        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                                                                        for(var i = 0; i < hashes.length; i++)
                                                                        {
                                                                            hash = hashes[i].split('=');
                                                                            vars.push(hash[0]);
                                                                            vars[hash[0]] = hash[1];
                                                                        }
                                                                        return vars;
                                                                    }
                                                                    var zip=getUrlVars()["zip_code"];
                                                                    $('#search-zip-code').val(zip);
                                                                    $('#search-zip-code').keypress(function (e) {
                                                                    		var key = e.which;
                                                                    		if(key == 13) 
                                                                    		{
                                                                    		$('#searchButton').click();
                                                                    		return false;  
                                                                    		}
                                                                    }); 
                                                             </script>
                                                                    @if(@$_GET['zip_code'])
                                                                        <script>
                                                                            $('html, body').animate({
                                                                                scrollTop: $('#search-zip-code').offset().top - 100
                                                                            }, 1000);
                                                                        </script>
                                                                        
                                                                    @endif
                                                        <div class="joinournetwork-content_inner-social">
                                                            <div class="joinournetwork-content_inner-social_body">
                                                            
                                                                 <div class="joinournetwork-content_inner-social_body-item shadow zip_code_item" style="{{ @$facebook ? '' : 'display: none' }};">
                                    								<a href="{{ @$facebook->link }}">{{ @$facebook->name }}</a>
                                    								<div class="fb-like" data-href="{{ @$facebook->link }}" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
                                								</div>
                                                                <div class="joinournetwork-content_inner-social_body-item shadow">
                                                                    <a href="https://www.facebook.com/PawMawOfficial/">PawMaw</a>
                                                                    <div class="fb-like" data-href="https://www.facebook.com/PawMawOfficial/" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
                                                                </div>
                                                                <div class="joinournetwork-content_inner-social_body-item shadow">
                                                                    <a href="https://www.facebook.com/groups/PawMawOfficial/">Join Our Official Group- PawMaw</a>
                                                                    <a style="background: #4267b2 !important; width: 85px !important; height: 30px !important; color: #fff !important; border-radius: 5px !important; font-size: 14px; display: flex; justify-content: center; align-items: center; white-space: nowrap;" href="https://www.facebook.com/groups/PawMawOfficial/" target="_blank">Join group</a>
                                                                </div>
                                                                <div id="fb-root"></div>
                                                                <script>(function(d, s, id) {
                                                                  var js, fjs = d.getElementsByTagName(s)[0];
                                                                  if (d.getElementById(id)) return;
                                                                  js = d.createElement(s); js.id = id;
                                                                  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2&appId=1863631387098360&autoLogAppEvents=1';
                                                                  fjs.parentNode.insertBefore(js, fjs);
                                                                }(document, 'script', 'facebook-jssdk'));</script>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- JOIN OUR NETWORK SECTION END -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    
    @push('js')
        <script type="text/javascript"></script>
    @endpush

@endsection
