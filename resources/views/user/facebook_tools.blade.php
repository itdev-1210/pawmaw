@extends('pawmawFront.layouts.front')

@section('title', 'Dashboard - PawMaw')

@section('content')
    <link rel="stylesheet" href="{{ asset('pawmaw/css/sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/sweetalert/sweet-alert-animations.min.css') }}">

    <!-- USER PANEL MAIN SECTION START -->
    <section class="user-panel use-panel_bg mt-5">
        <div class="user-panel_inner"
             style="background-image: url({{ asset('pawmaw/img/dashboard-bg.png')}});background-position: center; background-size: contain;">
            <div class="container">
                <div class="row">
                    <!-- SIDEBAR START -->
                @include('user.partials.sidebar_pet')
                <!-- SIDEBAR END -->

                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <div class="user-panel_inner-content">
                            <hr class="d-xl-none py-2">
                            <div class="pet-alert-content">
                                <div class="row">
                                    <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-sm-12 mt-sm-5 mt-xs-5">
                                        @if($pet->package_name)
                                            <div class="active-alert-pack">
                                                <h2 class="active-alert-pack_title">Current
                                                    Package: {{ $pet->package_name }}</h2>
                                            </div>
                                        @else
                                        <div class="active-alert-pack">
                                                <h2 class="active-alert-pack_title">Current
                                                    Package : Free</h2>
                                            </div>
                                        @endif
                                        <div class="pet-alert-content_box mb-5">
                                            <h2 class="pet-alert-content_box-head">
                                                <img src="{{ asset('pawmaw/img/fevi.png') }}" alt="PawMaw"
                                                     class="circle-logo-50">
                                                <span>@if($fbData)
                                                 {{$fbData->name}}
                                                @else
                                                 	Lost & Found Pets
                                                @endif
                                                </span>
                                            </h2>
										<p class="pet-alert-content_box-text py-3">
										@php
											if($pet->name)	{
											 	$petName =$pet->name; 
											 } else { 
											 	$petName='This Pet'; 
											 } 
											if($pet->type=='lost'){
												$dateType='Lost';
												$type='LOST'; 
												$contact="$petName's owner"; 
											} else {
												$dateType='Found'; 
												$type='FOUND'; 
												$contact='the finder'; 
											}
										@endphp
										<h4>Let your nearby people know by SHARE/COMMENT/LIKE {{ $petName }} was {{$pet->type}} on {{ $pet->date }} in {{$pet->last_seen}} </h4>
											<h4>➤ Pet Description: {{ $pet->description }}</h4></br>
											<h4>➤ Information from the Owner:</h4>
											<h4>Area last seen: {{ $pet->last_seen }}</h4>
											<h4>Cross street: {{ $pet->street_address }}</h4>
											<h4>Species: {{ $pet->specie }}</h4>
											<h4>Color: {{ $pet->color }}</h4>
											<h4>Gender: {{ $pet->gender }}</h4>
											<h4>Breed: {{ $pet->breed }}</h4>
											<h4>{{$dateType}} date: {{ $pet->date }}</h4></br>
											<h4>➤ To contact {{$contact}}, click on the link:</li>
											<h4><a>https://pawmaw.com/pet-details/{{ $pet->slug }}</a></h4></br>
											<h4>➤ Lost or found a pet? Please report it here:</h4>
											<h4><a>https://www.pawmaw.com/report/</a></h4>

										</p>

										<div class="pet-alert-content_box-img mx-auto d-block m-0 p-0"
											style="background: #000; border-radius: 5px; border: 0px solid;">

											<div id="petdetails-img-wrap11" class="{{ $pet->type }}">
                        
                         <div class="next-pet_box-img_title" style="border-radius:0px">
                            <h2>
                                <i class="fas fa-bullhorn"></i>
                                PawMaw
                                @if($pet->city)
                                    <span> {{ $pet->city . ', ' . ($pet->country_short == 'US' ? $pet->state : $pet->country_short) }}</span>
                                @endif
                            </h2>
                        </div>
                    

                    @php
                        $size = @getimagesize(asset('uploads/pets_image') . '/' . $pet->photo );
                        $needHeight = 400;
                        if($size){
                            $height = $size[1];
                            $width = $size[0] / ($size[1] / $needHeight);
                        }
                    @endphp

                   {{--   style="background-image: url({{ url('uploads/pets_image') }}/{{$pet->photo}}) , url({{ asset('pawmaw/img/pet-bg.jpg')}});background-size: contain,cover; background-position: center; background-repeat:no-repeat; margin: 0;" --}}

                    <div 
                        class="petdetails-img11 {{ $pet->type }} " 
                        id="image-to-copy"
                        data-check="true" 
                        data-id="{{ $pet->id }}"
                        style="position: relative;margin: 0; background-image: url({{ asset('pawmaw/img/pet-bg.jpg')}}); background-size: cover; background-position: center; background-repeat:no-repeat;  border-radius:0px">
                            
                                <img 
                                style="height: {{ @$needHeight }}px; width: {{ @$width }}px; margin: 0 auto; display: flex; max-width: 100%; object-fit: contain;"
                                src="{{ url('uploads/pets_image') }}/{{$pet->photo}}" alt="">
                           
                       {{--  <div class="next-pet_box-img_title">
                            <h2>
                                <i class="fas fa-bullhorn"></i>
                                PawMaw{{ $pet->city ? ',' : '' }}
                                @if($pet->city)
                                    <span> {{ $pet->city . ', ' . ($pet->country_short == 'US' ? $pet->state : $pet->country_short) }}</span>
                                @endif
                            </h2>
                        </div> --}}
                        
                            <div class="next-pet_box-img_id">

                                <p style="border-radius:0px; background: {{ $pet->type == 'lost' ? '#df1d41' : '#ffc000' }}"><span class="mr-3">ID</span>#{{$pet->id}}</p>
                           
                            </div>
                            <div class="next-pet_box-img_logo">
                                @if($pet->type == 'lost')
                                    <img src="{{ asset('pawmaw/img/fevi.png') }}" alt="Pawmaw" class="img-fluid circle-logo-50">
                                @elseif($pet->type == 'found')
                                    <img src="{{ asset('pawmaw/img/flyer-found-image-icon.png') }}" alt="Pawmaw" class="img-fluid circle-logo-50">
                                @else
                                    <img src="{{ asset('pawmaw/img/reunited-logo.png') }}" alt="Pawmaw" class="img-fluid circle-logo-50">
                                @endif
                            </div>
                      
                      
                    </div>
                    
                </div>
										</div>
									</div>
                                    </div>

                                    <div class="col-12">
                                    @if($facebookShare)
                                        <div class="facebook-tools">
                                            <a href="{{ $facebookShare->viewfacebookpost }}" target="_blank" class="facebook-tools_view-btn">View post <span class="ml-2"><i
                                                            class="far fa-eye"></i></span></a>
                                            <a href="http://www.facebook.com/sharer.php?u={{ $facebookShare->viewfacebookpost }}" target="_blank" class="facebook-tools_share-btn">Share post <span
                                                        class="ml-2"><i class="fas fa-share-alt-square"></i></span></a>
                                        </div>
                                     @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if($pet->package_name && $pet->package_name != 'Alerts Expired!')
                            <h2 class="user-panel_inner-content-msg mt-5 text-center">
                                Please Go to "My Pet Alert" and Make Sure to Comment On the Post
                            </h2>

                            <div class="alert-map">
                                <div class="alert-map_btn btn-success">
                                    <a href="{{ url('/user/alerts/' . $pet->id) }}">Your PawMaw Alert Activated!</a>
                                </div>
                                @if($pet->lat)
                                    <div id="map" style="width: 100%; height: 320px;"></div>
                                @else
                                    <img src="{{ asset('pawmaw/img/map.png')}}" alt="Map"
                                         class="img-fluid img-thumbnail mx-auto d-block alert-map_position">
                                @endif
                                {{-- <img src="{{ asset('pawmaw/img/map.png') }}" alt="Map" class="img-fluid img-thumbnail mx-auto d-block alert-map_position"> --}}
                            </div>
                        @else
                            <h2 class="user-panel_inner-content-msg mt-5 text-center">
                                Please active your pawmaw alert to reach more people in your local area.
                            </h2>

                            <div class="alert-map">
                                <div class="alert-map_overly"></div>
                                <div class="alert-map_icon"><i class="fas fa-lock"></i></div>
                                <div class="alert-map_btn">
                                    <a href="{{ url('/user/alerts/' . $pet->id) }}">Active your pawmaw alert</a>
                                </div>
                                @if($pet->lat)
                                    <div id="map" style="width: 100%; height: 320px;"></div>
                                @else
                                    <img src="{{ asset('pawmaw/img/map.png')}}" alt="Map"
                                         class="img-fluid img-thumbnail mx-auto d-block alert-map_position">
                                @endif
                                {{-- <img src="{{ asset('pawmaw/img/map.png') }}" alt="Map" class="img-fluid img-thumbnail mx-auto d-block alert-map_position"> --}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type='text/javascript' src="{{ asset('pawmaw/js/sweetalert.min.js') }}"></script>

    <script>

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: {{ $pet->lat }}, lng: {{ $pet->lng }} },
                zoom: 15,
            });

            new google.maps.Marker({
                map: map,
                position: {lat: {{ $pet->lat }}, lng: {{ $pet->lng }} },
                animation: google.maps.Animation.DROP
            })
        }

    </script>

    @push('js')
        <script type="text/javascript"></script>
    @endpush
@endsection
