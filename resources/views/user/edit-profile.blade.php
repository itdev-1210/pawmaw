@extends('user.layout.user')

@section('title', 'Edit Profile - PawMaw')

@section('content')
    @include('user.partials.sidebar')
    <div id="acc-body">
        <h1 class="entry-title">
            <span>Edit My Profile</span>
        </h1>


        @if($user->verified==0)


           <h1>Please Verified your email</h1>

            <br>

            @endif



        <form action="{{ url('/user/updateProfile') }}" method="post" id="profile-edit-form">
            {{ csrf_field() }}
            <div class="clear"></div>

            <div class="row">
                <label for="email">Email <span class="required">*</span></label>
                <input type="email" id="email" value="{{$user->email}}" readonly="readonly" disabled="disabled" class="disabled">
            </div>
            <div class="row">
                <label for="fname">First Name <span class="required">*</span></label>
                <input type="text" name="fname" id="fname" placeholder="First Name" value="{{$user->fname}}" required="">
            </div>
            <div class="row">
                <label for="lname">Last Name <span class="required">*</span></label>
                <input type="text" name="lname" id="lname" placeholder="Last Name" value="{{$user->lname}}" required="">
            </div>

            <div class="row">
                <label for="phone">Phone <span class="required">*</span></label>
                <input type="text" name="phone" id="phone" placeholder="Phone" value="{{$user->phone}}" required="">
            </div>
            <div class="row">
                <label for="autocomplete_1">Address <span class="required">*</span></label>
                <input type="text" name="address" id="autocomplete_1" placeholder="Address" value="{{$user->address}}" onfocus="geolocate()" required="" autocomplete="off">
            </div>

            <div class="clear"></div>
            <button class="btn" type="submit">Save Changes</button>

            <div class="clear"></div>
            <div id="fillinaddress1">
                <input type="hidden" name="route" class="route" value="">
                <input type="hidden" name="sublocality_level_1" class="sublocality_level_1" value="">
                <input type="hidden" name="locality" class="locality" value="Elizabeth City">
                <input type="hidden" name="administrative_area_level_3" class="administrative_area_level_3" value="">
                <input type="hidden" name="administrative_area_level_2" class="administrative_area_level_2" value="">
                <input type="hidden" name="administrative_area_level_1" class="administrative_area_level_1" value="">
                <input type="hidden" name="country" class="country" value="">
                <input type="hidden" name="postal_code" class="postal_code" value="">
            </div>
        </form>
    </div><!-- /.acc-body -->

    @push('js')
    @if(Session::has('email_success'))
        @if(Session::get('email_success') == 1)
            <link rel="stylesheet" href="{{ asset('pawmaw/css/sweetalert/sweetalert.css') }}">
            <link rel="stylesheet" href="{{ asset('pawmaw/css/sweetalert/sweet-alert-animations.min.css') }}">
            <script type='text/javascript' src="{{ asset('pawmaw/js/sweetalert.min.js') }}"></script>
            <script type="text/javascript">
                jQuery(document).ready(function(){
                    swal({
                        title: "An activation link has been sent to your email address!",
                        text: "Also please check your spam box if necessarily. If you still didn't received the activation link, please wait for at least 1-2 minutes before checking your emails again.",
                        icon: "success",
                        button: "OK"
                    });
                });
            </script>
        @endif
        @php
            Session::put('email_success',0);
        @endphp
    @endif
    @endpush
@endsection
