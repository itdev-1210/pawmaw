@extends('pawmawFront.layouts.front')

@section('title', 'Dashboard - PawMaw')

@section('content')

    <link rel="stylesheet" href="{{ asset('pawmaw/css/sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/sweetalert/sweet-alert-animations.min.css') }}">

    <!-- USER PANEL MAIN SECTION START -->
    <section class="user-panel use-panel_bg mt-5">
        <div class="user-panel_inner"
             style="background-image: url({{ asset('pawmaw/img/dashboard-bg.png')}});background-position: center; background-size: contain;">
            <div class="container">
                <div class="row">
                    <!-- SIDEBAR START -->
                @include('user.partials.sidebar_pet')

                <!-- SIDEBAR END -->
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <div class="user-panel_inner-content">
                            <div class="pet-update-content">
                                <div class="row">
                                    <div class="col-lg-12 mt-sm-5 mt-xs-5">
                                        <hr class="d-xl-none py-2">
                                        <div class="user-panel_inner-content_title">
                                            <h2>Update Listing</h2>
                                        </div>
                                        <h2 class="pet-update-content-head mb-5">
                                            You can update your listing to get your post to the top again as a featured
                                            post on our website which will help you to reach more website visitor.
                                        </h2>
                                        <div class="pet-update-content_box mb-5 gray-bg">
                                            <div class="row">
                                                <div class="col-lg-5">
                                                    <div class="pet-update-content_box-img">
                                                        <img src="{{ asset('uploads/pets_image') }}/{{$pet->photo}}"
                                                             alt="{{$pet->name}}" class="img-fluid">
                                                    </div>
                                                </div>

                                                @php
                                                    $now = new DateTime("now");
                                                    $lastUpdate = new DateTime($pet->date_list_update);

                                                    $days = $now->diff($lastUpdate)->days;
                                                @endphp

                                                <div class="col-lg-7">
                                                    <div class="pet-update-content_box-info">
                                                        <h2 class="pet-update-content_box-info_name">{{$pet->name}}</h2>
                                                        <p class="pet-update-content_box-info_gender">
                                                            A {{$pet->gender}} {{$pet->specie}}</p>
                                                        <p style="line-height: 1"
                                                           class="pet-update-content_box-info_address mb-5">{{$pet->last_seen}}</p>
                                                        @if($days > 2)
                                                            <a href="javascript:void(0);" onclick="updatePet();"
                                                               class="pet-update-content_box-info_btn">Update</a>
                                                        @else
                                                            <a href="javascript:void(0);"
                                                               style="background: #6c6365; cursor: not-allowed !important;"
                                                               class="pet-update-content_box-info_btn">To next
                                                                update: {{ 2 - $days }} days</a>
                                                        @endif
                                                        <p style="font-size: 16px; margin-top: 30px; display: none;"
                                                           class="update-message">Your pet was successfully updated in
                                                            listing.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if(count($payments) > 0)
                            <h2 class="user-panel_inner-content-msg mt-5 text-center">
                                Please Go to "My Pet Alert" and Make Sure to Comment On the Post
                            </h2>

                            <div class="alert-map">
                                <div class="alert-map_btn btn-success">
                                    <a href="{{ url('/user/alerts/' . $pet->id) }}">Your PawMaw Alert Activated!</a>
                                </div>
                                @if($pet->lat)
                                    <div id="map" style="width: 100%; height: 320px;"></div>
                                @else
                                    <img src="{{ asset('pawmaw/img/map.png')}}" alt="Map"
                                         class="img-fluid img-thumbnail mx-auto d-block alert-map_position">
                                @endif
                                {{-- <img src="{{ asset('pawmaw/img/map.png') }}" alt="Map" class="img-fluid img-thumbnail mx-auto d-block alert-map_position"> --}}
                            </div>
                        @else
                            <h2 class="user-panel_inner-content-msg mt-5 text-center">
                                Please active your pawmaw alert to reach more people in your local area.
                            </h2>

                            <div class="alert-map">
                                <div class="alert-map_overly"></div>
                                <div class="alert-map_icon"><i class="fas fa-lock"></i></div>
                                <div class="alert-map_btn">
                                    <a href="{{ url('/user/alerts/' . $pet->id) }}">Active your pawmaw alert</a>
                                </div>
                                @if($pet->lat)
                                    <div id="map" style="width: 100%; height: 320px;"></div>
                                @else
                                    <img src="{{ asset('pawmaw/img/map.png')}}" alt="Map"
                                         class="img-fluid img-thumbnail mx-auto d-block alert-map_position">
                                @endif
                                {{-- <img src="{{ asset('pawmaw/img/map.png') }}" alt="Map" class="img-fluid img-thumbnail mx-auto d-block alert-map_position"> --}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type='text/javascript' src="{{ asset('pawmaw/js/sweetalert.min.js') }}"></script>

    @push('js')
        <script type="text/javascript"></script>
    @endpush

    <script>
        function updatePet() {
            $.ajax({
                type: 'POST',
                url: '/update-pet-listing',
                data: {
                    _token: $('meta[name=csrf-token]').attr('content'),
                    id: {{ $pet->id }}
                },
                success: function (response) {
                    $('.update-message').fadeIn();

                    setTimeout(function () {
                        $('.update-message').fadeOut();

                        location.reload();
                    }, 3000);
                }
            });
        }

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: {{ $pet->lat }}, lng: {{ $pet->lng }} },
                zoom: 15,
            });

            new google.maps.Marker({
                map: map,
                position: {lat: {{ $pet->lat }}, lng: {{ $pet->lng }} },
                animation: google.maps.Animation.DROP
            })
        }
    </script>

@endsection
