@extends('pawmawFront.layouts.front')

@section('title', 'Dashboard - PawMaw')

@section('content')
    <!-- USER PANEL MAIN SECTION START -->
    <section class="user-panel"
             style="background-image: url({{ asset('pawmaw/img/dashboard-bg.png')}});background-position: center; background-size: contain;">
        <div class="user-panel_sidebar">
            <div class="container">
                <div class="row">
                    <!-- SIDEBAR START -->
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        @include('user.partials.sidebar')
                    </div>
                    <!-- SIDEBAR END -->

                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <div class="user-panel_inner-content">
                            <hr class="d-xl-none py-2">
                            <div class="user-panel_inner-content_title">
                                <h2>Pet Details Edit</h2>
                            </div>

                            {!!Form::model($pet,['method'=>'PETCH', 'enctype'=>'multipart/form-data', 'route'=>['reports_update',$pet->id]])!!}
                            <input type="hidden" name="type" value="{{$pet->type}}">
                            <input type="hidden" name="action" value="lost">
                            <div class="pet-profile-details_info-edit">
                                <div class="pet-profile-details_info-edit-img">
                                    <div class="form-group">
                                        <div class="zoomIn pet-profile-details_info-edit-img_cahnge">
                                            <figure style="background-image: url({{ url('uploads/pets_image') }}/{{$pet->photo}}) ; background-size: contain;background-position: center;background-repeat: no-repeat; width:70%; margin:auto;height:235px;">
                                            <!--
                                                    @if(file_exists( public_path().'/uploads/pets_image/'.$pet->photo ) && $pet->photo!='')
                                                <img class="img-fluid mx-auto d-block w-50" src="{{ url('uploads/pets_image') }}/{{$pet->photo}}" alt="{{$pet->name}}">
                                                    @else
                                                <img class="img-fluid mx-auto d-block w-50" src="{{ url('pawmaw/img/default.png') }}" alt="{{$pet->name}}">
                                                    @endif
                                                    -->
                                            </figure>
                                        </div> <!-- /previwe photo -->

                                        {{-- <div class="file-upload">
                                            <div class="file">
                                                Change Photo
                                                <input type="file" class="form-control-file" name="pet_photo" accept="image/*" required="" id="change_photo"/>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>

                            <div class="pet-profile-details_info-edit-bottom">
                                <div class="form-group row">
                                    <label for="name" class="col-sm-3 col-form-label">Name:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="name" name="name"
                                               value="{{ $pet->name }}" placeholder="Name" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="last_seen" class="col-sm-3 col-form-label">Area Last Seen:</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="address" id="pac-input" tabindex="1"
                                               class="form-control input-ctrl" placeholder="Nearest last seen area"
                                               onfocus="initAutocomplete();" autocomplete="off"
                                               value="{{$pet->last_seen}}" required disabled>
                                        {{-- <input type="text" class="form-control" id="last_seen" name="address"
                                               placeholder="1149 Burdett Way"  value="{{$pet->last_seen}}" onfocus="geolocate()" required="" autocomplete="off"> --}}
                                    </div>
                                    <div id="fillinaddress1">
                                        <input
                                                type="hidden"
                                                name="street_number"
                                                class="route">
                                        <input
                                                type="hidden"
                                                name="route"
                                                class="route">
                                        <input
                                                type="hidden"
                                                name="sublocality_level_1"
                                                class="sublocality_level_1">
                                        <input
                                                type="hidden"
                                                name="locality"
                                                class="locality">
                                        <input
                                                type="hidden"
                                                name="administrative_area_level_3"
                                                class="administrative_area_level_3">
                                        <input
                                                type="hidden"
                                                name="administrative_area_level_2"
                                                class="administrative_area_level_2">
                                        <input
                                                type="hidden"
                                                name="administrative_area_level_1"
                                                class="administrative_area_level_1">
                                        <input
                                                type="hidden"
                                                name="country"
                                                class="country">
                                        <input
                                                type="hidden"
                                                name="country_short"
                                                class="country">
                                        <input
                                                type="hidden"
                                                name="postal_code"
                                                class="postal_code">
                                        <input
                                                type="hidden"
                                                name="lat"
                                                class="postal_code">
                                        <input
                                                type="hidden"
                                                name="lng"
                                                class="postal_code">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="cross_street" class="col-sm-3 col-form-label">Cross Street:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="lost-cross_street"
                                               name="cross_street" value="{{ $pet->street }}" placeholder="Cross Street"
                                               disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="cross_street" class="col-sm-3 col-form-label">Zip code:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="postal_2" name="postal"
                                               value="{{ $pet->zip_code }}" placeholder="Zip code" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="species" class="col-sm-3 col-form-label">Species:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="specie" id="species">
                                            <option value="Dog" @if($pet->specie=='Dog'){{'selected'}}@endif>Dog
                                            </option>
                                            <option value="Cat" @if($pet->specie=='Cat'){{'selected'}}@endif>Cat
                                            </option>
                                            <option value="Bird" @if($pet->specie=='Bird'){{'selected'}}@endif>Bird
                                            </option>
                                            <option value="Horse" @if($pet->specie=='Horse'){{'selected'}}@endif>Horse
                                            </option>
                                            <option value="Rabbit" @if($pet->specie=='Rabbit'){{'selected'}}@endif>
                                                Rabbit
                                            </option>
                                            <option value="Reptile" @if($pet->specie=='Reptile'){{'selected'}}@endif>
                                                Reptile
                                            </option>
                                            <option value="Ferret" @if($pet->specie=='Ferret'){{'selected'}}@endif>
                                                Ferret
                                            </option>
                                            <option value="Other" @if($pet->specie=='Other'){{'selected'}}@endif>Other
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="color" class="col-sm-3 col-form-label">Color:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="color" id="color"
                                               value="{{$pet->color}}" placeholder="Brawn/tan/gray">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="gender" class="col-sm-3 col-form-label">Gender:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="gender" name="sex">
                                            <option
                                                    value="Male"
                                            @if($pet->gender=='Male'){{'selected'}}@endif>
                                                Male
                                            </option>
                                            <option
                                                    value="Female"
                                            @if($pet->gender == 'Female'){{'selected'}}@endif>
                                                Female
                                            </option>
                                        </select>
                                        {{--  <input type="text" class="form-control" name="sex" value="{{$pet->gender }}" id="gender" placeholder="Female"> --}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="breed" class="col-sm-3 col-form-label">Breed:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="breed" name="breed">
                                            <option value="" hidden selected disabled>Breed</option>
                                            <option @if($pet->breed=='Mixed Breed'){{'selected'}} @endif value="Mixed Breed">
                                                Mixed Breed
                                            </option>
                                            <option @if($pet->breed=='Other'){{'selected'}} @endif value="Other">Other
                                            </option>

                                            <option @if($pet->breed=='Unknown'){{'selected'}} @endif value="Unknown">
                                                Unknown
                                            </option>
                                            <option @if($pet->breed=='Abyssinian'){{'selected'}} @endif value="Abyssinian">
                                                Abyssinian
                                            </option>
                                            <option @if($pet->breed=='American Curl'){{'selected'}} @endif value="American Curl">
                                                American Curl
                                            </option>
                                            <option @if($pet->breed=='Affenpinscher'){{'selected'}} @endif value="Affenpinscher">
                                                Affenpinscher
                                            </option>

                                            <option @if($pet->breed=='Afghan Hound'){{'selected'}} @endif value="Afghan Hound">
                                                Afghan Hound
                                            </option>

                                            <option @if($pet->breed=='Airedale Terrier'){{'selected'}} @endif value="Airedale Terrier">
                                                Airedale Terrier
                                            </option>

                                            <option @if($pet->breed=='Akita'){{'selected'}} @endif value="Akita">Akita
                                            </option>

                                            <option @if($pet->breed=='Alaskan Husky'){{'selected'}} @endif value="Alaskan Husky">
                                                Alaskan Husky
                                            </option>

                                            <option @if($pet->breed=='Alaskan Klee Kai'){{'selected'}} @endif value="Alaskan Klee Kai">
                                                Alaskan Klee Kai
                                            </option>

                                            <option @if($pet->breed=='Alaskan Malamute'){{'selected'}} @endif value="Alaskan Malamute">
                                                Alaskan Malamute
                                            </option>

                                            <option @if($pet->breed=='American Bobtail'){{'selected'}} @endif value="American Bobtail">
                                                American Bobtail
                                            </option>
                                            <option @if($pet->breed=='American Bulldog'){{'selected'}} @endif value="American Bulldog">
                                                American Bulldog
                                            </option>
                                            <option @if($pet->breed=='American Shorthair'){{'selected'}} @endif value="American Shorthair">
                                                American Shorthair
                                            </option>
                                            <option @if($pet->breed=='American Wirehair'){{'selected'}} @endif value="American Wirehair">
                                                American Wirehair
                                            </option>

                                            <option @if($pet->breed=='American Bully'){{'selected'}} @endif value="American Bully">
                                                American Bully
                                            </option>

                                            <option @if($pet->breed=='American English Coonhound'){{'selected'}} @endif value="American English Coonhound">
                                                American English Coonhound
                                            </option>

                                            <option @if($pet->breed=='American Eskimo Dog'){{'selected'}} @endif value="American Eskimo Dog">
                                                American Eskimo Dog
                                            </option>

                                            <option @if($pet->breed=='American Foxhound'){{'selected'}} @endif value="American Foxhound">
                                                American Foxhound
                                            </option>

                                            <option @if($pet->breed=='American Hairless Terrier'){{'selected'}} @endif value="American Hairless Terrier">
                                                American Hairless Terrier
                                            </option>

                                            <option @if($pet->breed=='American Leopard Hound'){{'selected'}} @endif value="American Leopard Hound">
                                                American Leopard Hound
                                            </option>

                                            <option @if($pet->breed=='American Staffordshire'){{'selected'}} @endif value="American Staffordshire Terrier">
                                                American Staffordshire Terrier
                                            </option>

                                            <option @if($pet->breed=='American Water Spaniel'){{'selected'}} @endif value="American Water Spaniel">
                                                American Water Spaniel
                                            </option>

                                            <option @if($pet->breed=='Anatolian Shepherd Dog'){{'selected'}} @endif value="Anatolian Shepherd Dog">
                                                Anatolian Shepherd Dog
                                            </option>

                                            <option @if($pet->breed=='Appenzeller Sennenhunde'){{'selected'}} @endif value="Appenzeller Sennenhunde">
                                                Appenzeller Sennenhunde
                                            </option>

                                            <option @if($pet->breed=='Australian Cattle Dog'){{'selected'}} @endif value="Australian Cattle Dog">
                                                Australian Cattle Dog
                                            </option>

                                            <option @if($pet->breed=='Australian Kelpie'){{'selected'}} @endif value="Australian Kelpie">
                                                Australian Kelpie
                                            </option>

                                            <option @if($pet->breed=='Australian Shepherd'){{'selected'}} @endif value="Australian Shepherd">
                                                Australian Shepherd
                                            </option>

                                            <option @if($pet->breed=='Australian Terrier'){{'selected'}} @endif value="Australian Terrier">
                                                Australian Terrier
                                            </option>

                                            <option @if($pet->breed=='Azawakh'){{'selected'}} @endif value="Azawakh">
                                                Azawakh
                                            </option>

                                            <option @if($pet->breed=='Barbet'){{'selected'}} @endif value="Barbet">
                                                Barbet
                                            </option>
                                            <option @if($pet->breed=='Balinese'){{'selected'}} @endif value="Balinese">
                                                Balinese
                                            </option>

                                            <option @if($pet->breed=='Basenji'){{'selected'}} @endif value="Basenji">
                                                Basenji
                                            </option>

                                            <option @if($pet->breed=='Basset Fauve De Bretagne'){{'selected'}} @endif value="Basset Fauve De Bretagne">
                                                Basset Fauve De Bretagne
                                            </option>

                                            <option @if($pet->breed=='Basset Hound'){{'selected'}} @endif value="Basset Hound">
                                                Basset Hound
                                            </option>

                                            <option @if($pet->breed=='Beagle'){{'selected'}} @endif value="Beagle">
                                                Beagle
                                            </option>

                                            <option @if($pet->breed=='Bearded Collie'){{'selected'}} @endif value="Bearded Collie">
                                                Bearded Collie
                                            </option>

                                            <option @if($pet->breed=='Beauceron'){{'selected'}} @endif value="Beauceron">
                                                Beauceron
                                            </option>

                                            <option @if($pet->breed=='Bedlington Terrier'){{'selected'}} @endif value="Bedlington Terrier">
                                                Bedlington Terrier
                                            </option>

                                            <option @if($pet->breed=='Belgian Laekenois'){{'selected'}} @endif value="Belgian Laekenois">
                                                Belgian Laekenois
                                            </option>

                                            <option @if($pet->breed=='Belgian Malinois'){{'selected'}} @endif value="Belgian Malinois">
                                                Belgian Malinois
                                            </option>

                                            <option @if($pet->breed=='Belgian Sheepdog'){{'selected'}} @endif value="Belgian Sheepdog">
                                                Belgian Sheepdog
                                            </option>

                                            <option @if($pet->breed=='Belgian Tervuren'){{'selected'}} @endif value="Belgian Tervuren">
                                                Belgian Tervuren
                                            </option>
                                            <option @if($pet->breed=='Bengal'){{'selected'}} @endif value="Bengal">
                                                Bengal
                                            </option>

                                            <option @if($pet->breed=='Bergamasco'){{'selected'}} @endif value="Bergamasco">
                                                Bergamasco
                                            </option>

                                            <option @if($pet->breed=='Berger Picard'){{'selected'}} @endif value="Berger Picard">
                                                Berger Picard
                                            </option>

                                            <option @if($pet->breed=='Bernese Mountain Dog'){{'selected'}} @endif value="Bernese Mountain Dog">
                                                Bernese Mountain Dog
                                            </option>

                                            <option @if($pet->breed=='Bichon Frise'){{'selected'}} @endif value="Bichon Frise">
                                                Bichon Frise
                                            </option>

                                            <option @if($pet->breed=='Biewer Terrier'){{'selected'}} @endif value="Biewer Terrier">
                                                Biewer Terrier
                                            </option>
                                            <option @if($pet->breed=='Birman'){{'selected'}} @endif value="Birman">
                                                Birman
                                            </option>

                                            <option @if($pet->breed=='Black and Tan Coonhound'){{'selected'}} @endif value="Black and Tan Coonhound">
                                                Black and Tan Coonhound
                                            </option>

                                            <option @if($pet->breed=='Black Russian Terrier'){{'selected'}} @endif value="Black Russian Terrier">
                                                Black Russian Terrier
                                            </option>

                                            <option @if($pet->breed=='Blackmouth Cur'){{'selected'}} @endif value="Blackmouth Cur">
                                                Blackmouth Cur
                                            </option>

                                            <option @if($pet->breed=='Bloodhound'){{'selected'}} @endif value="Bloodhound">
                                                Bloodhound
                                            </option>

                                            <option @if($pet->breed=='Bluetick Coonhound'){{'selected'}} @endif value="Bluetick Coonhound">
                                                Bluetick Coonhound
                                            </option>

                                            <option @if($pet->breed=='Boerboel'){{'selected'}} @endif value="Boerboel">
                                                Boerboel
                                            </option>

                                            <option @if($pet->breed=='Bolognese'){{'selected'}} @endif value="Bolognese">
                                                Bolognese
                                            </option>
                                            <option @if($pet->breed=='Bombay'){{'selected'}} @endif value="Bombay">
                                                Bombay
                                            </option>

                                            <option @if($pet->breed=='Border Collie'){{'selected'}} @endif value="Border Collie">
                                                Border Collie
                                            </option>

                                            <option @if($pet->breed=='Border Terrier'){{'selected'}} @endif value="Border Terrier">
                                                Border Terrier
                                            </option>

                                            <option @if($pet->breed=='Borzoi'){{'selected'}} @endif value="Borzoi">
                                                Borzoi
                                            </option>

                                            <option @if($pet->breed=='Boston Terrier'){{'selected'}} @endif value="Boston Terrier">
                                                Boston Terrier
                                            </option>

                                            <option @if($pet->breed=='Bouvier des Flandres'){{'selected'}} @endif value="Bouvier des Flandres">
                                                Bouvier des Flandres
                                            </option>

                                            <option @if($pet->breed=='Boxer'){{'selected'}} @endif value="Boxer">Boxer
                                            </option>

                                            <option @if($pet->breed=='Boykin Spaniel'){{'selected'}} @endif value="Boykin Spaniel">
                                                Boykin Spaniel
                                            </option>

                                            <option @if($pet->breed=='Bracco Italiano'){{'selected'}} @endif value="Bracco Italiano">
                                                Bracco Italiano
                                            </option>

                                            <option @if($pet->breed=='Braque Du Bourbonnais'){{'selected'}} @endif value="Braque Du Bourbonnais">
                                                Braque Du Bourbonnais
                                            </option>

                                            <option @if($pet->breed=='Briard'){{'selected'}} @endif value="Briard">
                                                Briard
                                            </option>

                                            <option @if($pet->breed=='Brittany Spaniel'){{'selected'}} @endif value="Brittany Spaniel">
                                                Brittany Spaniel
                                            </option>
                                            <option @if($pet->breed=='British Shorthair'){{'selected'}} @endif value="British Shorthair">
                                                British Shorthair
                                            </option>

                                            <option @if($pet->breed=='Broholmer'){{'selected'}} @endif value="Broholmer">
                                                Broholmer
                                            </option>

                                            <option @if($pet->breed=='Brussels Griffon'){{'selected'}} @endif value="Brussels Griffon">
                                                Brussels Griffon
                                            </option>

                                            <option @if($pet->breed=='Bull Terrier'){{'selected'}} @endif value="Bull Terrier">
                                                Bull Terrier
                                            </option>

                                            <option @if($pet->breed=='Bulldog'){{'selected'}} @endif value="Bulldog">
                                                Bulldog
                                            </option>

                                            <option @if($pet->breed=='Bullmastiff'){{'selected'}} @endif value="Bullmastiff">
                                                Bullmastiff
                                            </option>
                                            <option @if($pet->breed=='Burmese'){{'selected'}} @endif value="Burmese">
                                                Burmese
                                            </option>

                                            <option @if($pet->breed=='Cairn Terrier'){{'selected'}} @endif value="Cairn Terrier">
                                                Cairn Terrier
                                            </option>

                                            <option @if($pet->breed=='Canaan Dog'){{'selected'}} @endif value="Canaan Dog">
                                                Canaan Dog
                                            </option>

                                            <option @if($pet->breed=='Cane Corso'){{'selected'}} @endif value="Cane Corso">
                                                Cane Corso
                                            </option>

                                            <option @if($pet->breed=='Cardigan Welsh Corgi'){{'selected'}} @endif value="Cardigan Welsh Corgi">
                                                Cardigan Welsh Corgi
                                            </option>

                                            <option @if($pet->breed=='Catahoula'){{'selected'}} @endif value="Catahoula">
                                                Catahoula
                                            </option>

                                            <option @if($pet->breed=='Caucasian Ovcharka'){{'selected'}} @endif value="Caucasian Ovcharka">
                                                Caucasian Ovcharka
                                            </option>

                                            <option @if($pet->breed=='Cavalier King Charles Spaniel'){{'selected'}} @endif value="Cavalier King Charles Spaniel">
                                                Cavalier King Charles Spaniel
                                            </option>

                                            <option @if($pet->breed=='Central Asian Shepherd Dog'){{'selected'}} @endif value="Central Asian Shepherd Dog">
                                                Central Asian Shepherd Dog
                                            </option>

                                            <option @if($pet->breed=='Cesky Terrier'){{'selected'}} @endif value="Cesky Terrier">
                                                Cesky Terrier
                                            </option>

                                            <option @if($pet->breed=='Chartreux'){{'selected'}} @endif value="Chartreux">
                                                Chartreux
                                            </option>
                                            <option @if($pet->breed=='Chausie'){{'selected'}} @endif value="Chausie">
                                                Chausie
                                            </option>
                                            <option @if($pet->breed=='Chesapeake Bay Retriever'){{'selected'}} @endif value="Chesapeake Bay Retriever">
                                                Chesapeake Bay Retriever
                                            </option>

                                            <option @if($pet->breed=='Chihuahua'){{'selected'}} @endif value="Chihuahua">
                                                Chihuahua
                                            </option>

                                            <option @if($pet->breed=='Chinese Crested'){{'selected'}} @endif value="Chinese Crested">
                                                Chinese Crested
                                            </option>

                                            <option @if($pet->breed=='Chinese Shar-Pei'){{'selected'}} @endif value="Chinese Shar-Pei">
                                                Chinese Shar-Pei
                                            </option>

                                            <option @if($pet->breed=='Chinook'){{'selected'}} @endif value="Chinook">
                                                Chinook
                                            </option>

                                            <option @if($pet->breed=='Chow Chow'){{'selected'}} @endif value="Chow Chow">
                                                Chow Chow
                                            </option>

                                            <option @if($pet->breed=="Cirneco Dell'Etna"){{'selected'}} @endif value="Cirneco Dell'Etna">
                                                Cirneco Dell'Etna
                                            </option>

                                            <option @if($pet->breed=='Clumber Spaniel'){{'selected'}} @endif value="Clumber Spaniel">
                                                Clumber Spaniel
                                            </option>

                                            <option @if($pet->breed=='Cocker Spaniel'){{'selected'}} @endif value="Cocker Spaniel">
                                                Cocker Spaniel
                                            </option>

                                            <option @if($pet->breed=='Collie'){{'selected'}} @endif value="Collie">
                                                Collie
                                            </option>

                                            <option @if($pet->breed=='Coton de Tulear'){{'selected'}} @endif value="Coton de Tulear">
                                                Coton de Tulear
                                            </option>
                                            <option @if($pet->breed=='Cornish Rex'){{'selected'}} @endif value="Cornish Rex">
                                                Cornish Rex
                                            </option>

                                            <option @if($pet->breed=='Curly-Coated Retriever'){{'selected'}} @endif value="Curly-Coated Retriever">
                                                Curly-Coated Retriever
                                            </option>

                                            <option @if($pet->breed=='Czechoslovakian Vlcak'){{'selected'}} @endif value="Czechoslovakian Vlcak">
                                                Czechoslovakian Vlcak
                                            </option>


                                            <option @if($pet->breed=='Dachshund'){{'selected'}} @endif value="Dachshund">
                                                Dachshund
                                            </option>

                                            <option @if($pet->breed=='Dalmatian'){{'selected'}} @endif value="Dalmatian">
                                                Dalmatian
                                            </option>

                                            <option @if($pet->breed=='Dandie Dinmont Terrier'){{'selected'}} @endif value="Dandie Dinmont Terrier">
                                                Dandie Dinmont Terrier
                                            </option>

                                            <option @if($pet->breed=='Danish-Swedish Farmdog'){{'selected'}} @endif value="Danish-Swedish Farmdog">
                                                Danish-Swedish Farmdog
                                            </option>

                                            <option @if($pet->breed=='Deutscher Wachtelhund'){{'selected'}} @endif value="Deutscher Wachtelhund">
                                                Deutscher Wachtelhund
                                            </option>

                                            <option @if($pet->breed=='Doberman Pinscher'){{'selected'}} @endif value="Doberman Pinscher">
                                                Doberman Pinscher
                                            </option>

                                            <option @if($pet->breed=='Dogo Argentino'){{'selected'}} @endif value="Dogo Argentino">
                                                Dogo Argentino
                                            </option>

                                            <option @if($pet->breed=='Dogue de Bordeaux'){{'selected'}} @endif value="Dogue de Bordeaux">
                                                Dogue de Bordeaux
                                            </option>
                                            <option @if($pet->breed=='Donskoy'){{'selected'}} @endif value="Donskoy">
                                                Donskoy
                                            </option>
                                            <option @if($pet->breed=='Devon Rex'){{'selected'}} @endif value="Devon Rex">
                                                Devon Rex
                                            </option>

                                            <option @if($pet->breed=='Drentsche Patrijshond'){{'selected'}} @endif value="Drentsche Patrijshond">
                                                Drentsche Patrijshond
                                            </option>

                                            <option @if($pet->breed=='Drever'){{'selected'}} @endif value="Drever">
                                                Drever
                                            </option>

                                            <option @if($pet->breed=='Dutch Shepherd'){{'selected'}} @endif value="Dutch Shepherd">
                                                Dutch Shepherd
                                            </option>


                                            <option @if($pet->breed=='Egyptian Mau'){{'selected'}} @endif value="Egyptian Mau">
                                                Egyptian Mau
                                            </option>
                                            <option @if($pet->breed=='English Bulldog'){{'selected'}} @endif value="English Bulldog">
                                                English Bulldog
                                            </option>

                                            <option @if($pet->breed=='English Cocker Spaniel'){{'selected'}} @endif value="English Cocker Spaniel">
                                                English Cocker Spaniel
                                            </option>

                                            <option @if($pet->breed=='English Foxhound'){{'selected'}} @endif value="English Foxhound">
                                                English Foxhound
                                            </option>

                                            <option @if($pet->breed=='English Pointer'){{'selected'}} @endif value="English Pointer">
                                                English Pointer
                                            </option>

                                            <option @if($pet->breed=='English Setter'){{'selected'}} @endif value="English Setter">
                                                English Setter
                                            </option>

                                            <option @if($pet->breed=='English Shepherd'){{'selected'}} @endif value="English Shepherd">
                                                English Shepherd
                                            </option>

                                            <option @if($pet->breed=='English Springer Spaniel'){{'selected'}} @endif value="English Springer Spaniel">
                                                English Springer Spaniel
                                            </option>

                                            <option @if($pet->breed=='English Toy Spaniel'){{'selected'}} @endif value="English Toy Spaniel">
                                                English Toy Spaniel
                                            </option>

                                            <option @if($pet->breed=='Entlebucher Mountain Dog'){{'selected'}} @endif value="Entlebucher Mountain Dog">
                                                Entlebucher Mountain Dog
                                            </option>

                                            <option @if($pet->breed=='Estrela Mountain Dog'){{'selected'}} @endif value="Estrela Mountain Dog">
                                                Estrela Mountain Dog
                                            </option>

                                            <option @if($pet->breed=='Eurasier'){{'selected'}} @endif value="Eurasier">
                                                Eurasier
                                            </option>
                                            <option @if($pet->breed=='Exotic Shorthair'){{'selected'}} @endif value="Exotic Shorthair">
                                                Exotic Shorthair
                                            </option>

                                            <option @if($pet->breed=='Field Spaniel'){{'selected'}} @endif value="Field Spaniel">
                                                Field Spaniel
                                            </option>

                                            <option @if($pet->breed=='Finnish Lapphund'){{'selected'}} @endif value="Finnish Lapphund">
                                                Finnish Lapphund
                                            </option>

                                            <option @if($pet->breed=='Finnish Spitz'){{'selected'}} @endif value="Finnish Spitz">
                                                Finnish Spitz
                                            </option>

                                            <option @if($pet->breed=='Flat-Coated Retriever'){{'selected'}} @endif value="Flat-Coated Retriever">
                                                Flat-Coated Retriever
                                            </option>

                                            <option @if($pet->breed=='French Bulldog'){{'selected'}} @endif value="French Bulldog">
                                                French Bulldog
                                            </option>

                                            <option @if($pet->breed=='French Spaniel'){{'selected'}} @endif value="French Spaniel">
                                                French Spaniel
                                            </option>

                                            <option @if($pet->breed=='German Longhaired Pointer'){{'selected'}} @endif value="German Longhaired Pointer">
                                                German Longhaired Pointer
                                            </option>

                                            <option @if($pet->breed=='German Pinscher'){{'selected'}} @endif value="German Pinscher">
                                                German Pinscher
                                            </option>

                                            <option @if($pet->breed=='German Shepherd'){{'selected'}} @endif value="German Shepherd">
                                                German Shepherd
                                            </option>

                                            <option @if($pet->breed=='German Shorthaired Pointer'){{'selected'}} @endif value="German Shorthaired Pointer">
                                                German Shorthaired Pointer
                                            </option>

                                            <option @if($pet->breed=='German Spitz'){{'selected'}} @endif value="German Spitz">
                                                German Spitz
                                            </option>

                                            <option @if($pet->breed=='German Wirehaired Pointer'){{'selected'}} @endif value="German Wirehaired Pointer">
                                                German Wirehaired Pointer
                                            </option>

                                            <option @if($pet->breed=='Giant Schnauzer'){{'selected'}} @endif value="Giant Schnauzer">
                                                Giant Schnauzer
                                            </option>

                                            <option @if($pet->breed=='Glen of Imaal Terrier'){{'selected'}} @endif value="Glen of Imaal Terrier">
                                                Glen of Imaal Terrier
                                            </option>

                                            <option @if($pet->breed=='Golden Retriever'){{'selected'}} @endif value="Golden Retriever">
                                                Golden Retriever
                                            </option>

                                            <option @if($pet->breed=='Gordon Setter'){{'selected'}} @endif value="Gordon Setter">
                                                Gordon Setter
                                            </option>

                                            <option @if($pet->breed=='Grand Basset Griffon Vendeen'){{'selected'}} @endif value="Grand Basset Griffon Vendeen">
                                                Grand Basset Griffon Vendeen
                                            </option>

                                            <option @if($pet->breed=='Great Dane'){{'selected'}} @endif value="Great Dane">
                                                Great Dane
                                            </option>

                                            <option @if($pet->breed=='Great Pyrenees'){{'selected'}} @endif value="Great Pyrenees">
                                                Great Pyrenees
                                            </option>

                                            <option @if($pet->breed=='Greater Swiss Mountain Dog'){{'selected'}} @endif value="Greater Swiss Mountain Dog">
                                                Greater Swiss Mountain Dog
                                            </option>

                                            <option @if($pet->breed=='Greyhound'){{'selected'}} @endif value="Greyhound">
                                                Greyhound
                                            </option>
                                            <option @if($pet->breed=='Hamiltonstovare'){{'selected'}} @endif value="Hamiltonstovare">
                                                Hamiltonstovare
                                            </option>

                                            <option @if($pet->breed=='Harrier'){{'selected'}} @endif value="Harrier">
                                                Harrier
                                            </option>

                                            <option @if($pet->breed=='Havanese'){{'selected'}} @endif value="Havanese">
                                                Havanese
                                            </option>
                                            <option @if($pet->breed=='Havana'){{'selected'}} @endif value="Havana">
                                                Havana
                                            </option>

                                            <option @if($pet->breed=='Highlander'){{'selected'}} @endif value="Highlander">
                                                Highlander
                                            </option>
                                            <option @if($pet->breed=='Himalayan'){{'selected'}} @endif value="Himalayan">
                                                Himalayan
                                            </option>
                                            <option @if($pet->breed=='Hound'){{'selected'}} @endif value="Hound">Hound
                                            </option>
                                            <option @if($pet->breed=='Household Pet Cat'){{'selected'}} @endif value="Household Pet Cat">
                                                Household Pet Cat
                                            </option>
                                            <option @if($pet->breed=='Household Pet Kitten'){{'selected'}} @endif value="Household Pet Kitten">
                                                Household Pet Kitten
                                            </option>

                                            <option @if($pet->breed=='Hovawart'){{'selected'}} @endif value="Hovawart">
                                                Hovawart
                                            </option>

                                            <option @if($pet->breed=='Ibizan Hound'){{'selected'}} @endif value="Ibizan Hound">
                                                Ibizan Hound
                                            </option>

                                            <option @if($pet->breed=='Icelandic Sheepdog'){{'selected'}} @endif value="Icelandic Sheepdog">
                                                Icelandic Sheepdog
                                            </option>

                                            <option @if($pet->breed=='Irish Red and White Setter'){{'selected'}} @endif value="Irish Red and White Setter">
                                                Irish Red and White Setter
                                            </option>

                                            <option @if($pet->breed=='Irish Setter'){{'selected'}} @endif value="Irish Setter">
                                                Irish Setter
                                            </option>

                                            <option @if($pet->breed=='Irish Terrier'){{'selected'}} @endif value="Irish Terrier">
                                                Irish Terrier
                                            </option>

                                            <option @if($pet->breed=='Irish Water Spaniel'){{'selected'}} @endif value="Irish Water Spaniel">
                                                Irish Water Spaniel
                                            </option>

                                            <option @if($pet->breed=='Irish Wolfhound'){{'selected'}} @endif value="Irish Wolfhound">
                                                Irish Wolfhound
                                            </option>

                                            <option @if($pet->breed=='Italian Greyhound'){{'selected'}} @endif value="Italian Greyhound">
                                                Italian Greyhound
                                            </option>

                                            <option @if($pet->breed=='Jack Russell Terrier'){{'selected'}} @endif value="Jack Russell Terrier">
                                                Jack Russell Terrier
                                            </option>

                                            <option @if($pet->breed=='Jagdterrier'){{'selected'}} @endif value="Jagdterrier">
                                                Jagdterrier
                                            </option>

                                            <option @if($pet->breed=='Japanese Bobtail'){{'selected'}} @endif value="Japanese Bobtail">
                                                Japanese Bobtail
                                            </option>
                                            <option @if($pet->breed=='Japanese Chin'){{'selected'}} @endif value="Japanese Chin">
                                                Japanese Chin
                                            </option>


                                            <option @if($pet->breed=='Kai Ken'){{'selected'}} @endif value="Kai Ken">Kai
                                                Ken
                                            </option>

                                            <option @if($pet->breed=='Keeshond'){{'selected'}} @endif value="Keeshond">
                                                Keeshond
                                            </option>

                                            <option @if($pet->breed=='Kerry Blue Terrier'){{'selected'}} @endif value="Kerry Blue Terrier">
                                                Kerry Blue Terrier
                                            </option>

                                            <option @if($pet->breed=='Kishu Ken'){{'selected'}} @endif value="Kishu Ken">
                                                Kishu Ken
                                            </option>

                                            <option @if($pet->breed=='Komondor'){{'selected'}} @endif value="Komondor">
                                                Komondor
                                            </option>

                                            <option @if($pet->breed=='Korat'){{'selected'}} @endif value="Korat">Korat
                                            </option>
                                            <option @if($pet->breed=='Korean Jindo'){{'selected'}} @endif value="Korean Jindo">
                                                Korean Jindo
                                            </option>

                                            <option @if($pet->breed=='Kromfohrlander'){{'selected'}} @endif value="Kromfohrlander">
                                                Kromfohrlander
                                            </option>

                                            <option @if($pet->breed=='Kurilian Bobtail'){{'selected'}} @endif value="Kurilian Bobtail">
                                                Kurilian Bobtail
                                            </option>
                                            <option @if($pet->breed=='Kuvasz'){{'selected'}} @endif value="Kuvasz">
                                                Kuvasz
                                            </option>

                                            <option @if($pet->breed=='Labrador Retriever'){{'selected'}} @endif value="Labrador Retriever">
                                                Labrador Retriever
                                            </option>

                                            <option @if($pet->breed=='Lagotto Romagnolo'){{'selected'}} @endif value="Lagotto Romagnolo">
                                                Lagotto Romagnolo
                                            </option>

                                            <option @if($pet->breed=='Lakeland Terrier'){{'selected'}} @endif value="Lakeland Terrier">
                                                Lakeland Terrier
                                            </option>

                                            <option @if($pet->breed=='Lancashire Heeler'){{'selected'}} @endif value="Lancashire Heeler">
                                                Lancashire Heeler
                                            </option>
                                            <option @if($pet->breed=='LaPerm'){{'selected'}} @endif value="LaPerm">
                                                LaPerm
                                            </option>

                                            <option @if($pet->breed=='Leonberger'){{'selected'}} @endif value="Leonberger">
                                                Leonberger
                                            </option>

                                            <option @if($pet->breed=='Lhasa Apso'){{'selected'}} @endif value="Lhasa Apso">
                                                Lhasa Apso
                                            </option>

                                            <option @if($pet->breed=='Löwchen'){{'selected'}} @endif value="Löwchen">
                                                Löwchen
                                            </option>


                                            <option @if($pet->breed=='Maltese'){{'selected'}} @endif value="Maltese">
                                                Maltese
                                            </option>
                                            <option @if($pet->breed=='Maine Coon'){{'selected'}} @endif value="Maine Coon">
                                                Maine Coon
                                            </option>

                                            <option @if($pet->breed=='Manchester Terrier'){{'selected'}} @endif value="Manchester Terrier">
                                                Manchester Terrier
                                            </option>
                                            <option @if($pet->breed=='Manx'){{'selected'}} @endif value="Manx">Manx
                                            </option>

                                            <option @if($pet->breed=='Mastiff'){{'selected'}} @endif value="Mastiff">
                                                Mastiff
                                            </option>

                                            <option @if($pet->breed=='Miniature American Shepherd'){{'selected'}} @endif value="Miniature American Shepherd">
                                                Miniature American Shepherd
                                            </option>

                                            <option @if($pet->breed=='Miniature Bull Terrier'){{'selected'}} @endif value="Miniature Bull Terrier">
                                                Miniature Bull Terrier
                                            </option>

                                            <option @if($pet->breed=='Miniature Pinscher'){{'selected'}} @endif value="Miniature Pinscher">
                                                Miniature Pinscher
                                            </option>

                                            <option @if($pet->breed=='Miniature Schnauzer'){{'selected'}} @endif value="Miniature Schnauzer">
                                                Miniature Schnauzer
                                            </option>
                                            <option @if($pet->breed=='Minskin'){{'selected'}} @endif value="Minskin">
                                                Minskin
                                            </option>

                                            <option @if($pet->breed=='Mudi'){{'selected'}} @endif value="Mudi">Mudi
                                            </option>
                                            <option @if($pet->breed=='Munchkin'){{'selected'}} @endif value="Munchkin">
                                                Munchkin
                                            </option>


                                            <option @if($pet->breed=='Neapolitan Mastiff'){{'selected'}} @endif value="Neapolitan Mastiff">
                                                Neapolitan Mastiff
                                            </option>

                                            <option @if($pet->breed=='Nebelung'){{'selected'}} @endif value="Nebelung">
                                                Nebelung
                                            </option>
                                            <option @if($pet->breed=='Nederlandse Kooikerhondje'){{'selected'}} @endif value="Nederlandse Kooikerhondje">
                                                Nederlandse Kooikerhondje
                                            </option>

                                            <option @if($pet->breed=='Newfoundland'){{'selected'}} @endif value="Newfoundland">
                                                Newfoundland
                                            </option>

                                            <option @if($pet->breed=='Norfolk Terrier'){{'selected'}} @endif value="Norfolk Terrier">
                                                Norfolk Terrier
                                            </option>

                                            <option @if($pet->breed=='Norrbottenspets'){{'selected'}} @endif value="Norrbottenspets">
                                                Norrbottenspets
                                            </option>

                                            <option @if($pet->breed=='Norwegian Buhund'){{'selected'}} @endif value="Norwegian Buhund">
                                                Norwegian Buhund
                                            </option>

                                            <option @if($pet->breed=='Norwegian Elkhound'){{'selected'}} @endif value="Norwegian Elkhound">
                                                Norwegian Elkhound
                                            </option>
                                            <option @if($pet->breed=='Norwegian Forest Cat'){{'selected'}} @endif value="Norwegian Forest Cat">
                                                Norwegian Forest Cat
                                            </option>

                                            <option @if($pet->breed=='Norwegian Lundehund'){{'selected'}} @endif value="Norwegian Lundehund">
                                                Norwegian Lundehund
                                            </option>

                                            <option @if($pet->breed=='Norwich Terrier'){{'selected'}} @endif value="Norwich Terrier">
                                                Norwich Terrier
                                            </option>

                                            <option @if($pet->breed=='Nova Scotia Duck Tolling Retriever'){{'selected'}} @endif value="Nova Scotia Duck Tolling Retriever">
                                                Nova Scotia Duck Tolling
                                                Retriever
                                            </option>


                                            <option @if($pet->breed=='Old English Sheepdog'){{'selected'}} @endif value="Old English Sheepdog">
                                                Old English Sheepdog
                                            </option>

                                            <option @if($pet->breed=='Other'){{'selected'}} @endif value="Other">Other
                                            </option>
                                            <option @if($pet->breed=='Ocicat'){{'selected'}} @endif value="Ocicat">
                                                Ocicat
                                            </option>
                                            <option @if($pet->breed=='Ojos Azules'){{'selected'}} @endif value="Ojos Azules">
                                                Ojos Azules
                                            </option>

                                            <option @if($pet->breed=='Oriental'){{'selected'}} @endif value="Oriental">
                                                Oriental
                                            </option>
                                            <option @if($pet->breed=='Otterhound'){{'selected'}} @endif value="Otterhound">
                                                Otterhound
                                            </option>


                                            <option @if($pet->breed=='Papillon'){{'selected'}} @endif value="Papillon">
                                                Papillon
                                            </option>

                                            <option @if($pet->breed=='Parson Russell Terrier'){{'selected'}} @endif value="Parson Russell Terrier">
                                                Parson Russell Terrier
                                            </option>

                                            <option @if($pet->breed=='Patterdale Terrier'){{'selected'}} @endif value="Patterdale Terrier">
                                                Patterdale Terrier
                                            </option>

                                            <option @if($pet->breed=='Pekingese'){{'selected'}} @endif value="Pekingese">
                                                Pekingese
                                            </option>

                                            <option @if($pet->breed=='Pembroke Welsh Corgi'){{'selected'}} @endif value="Pembroke Welsh Corgi">
                                                Pembroke Welsh Corgi
                                            </option>

                                            <option @if($pet->breed=='Perro De Presa Canario'){{'selected'}} @endif value="Perro De Presa Canario">
                                                Perro De Presa Canario
                                            </option>
                                            <option @if($pet->breed=='Persian'){{'selected'}} @endif value="Persian">
                                                Persian
                                            </option>

                                            <option @if($pet->breed=='Peruvian Inca Orchid'){{'selected'}} @endif value="Peruvian Inca Orchid">
                                                Peruvian Inca Orchid
                                            </option>

                                            <option @if($pet->breed=='Peterbald'){{'selected'}} @endif value="Peterbald">
                                                Peterbald
                                            </option>
                                            <option @if($pet->breed=='Petit Basset Griffon Vendéen'){{'selected'}} @endif value="Petit Basset Griffon Vendéen">
                                                Petit Basset Griffon Vendéen
                                            </option>

                                            <option @if($pet->breed=='Pharaoh Hound'){{'selected'}} @endif value="Pharaoh Hound">
                                                Pharaoh Hound
                                            </option>

                                            <option @if($pet->breed=='Pit Bull'){{'selected'}} @endif value="Pit Bull">
                                                Pit Bull
                                            </option>
                                            <option @if($pet->breed=='Pixiebob'){{'selected'}} @endif value="Pixiebob">
                                                Pixiebob
                                            </option>

                                            <option @if($pet->breed=='Plott'){{'selected'}} @endif value="Plott">Plott
                                            </option>

                                            <option @if($pet->breed=='Pointer'){{'selected'}} @endif value="Pointer">
                                                Pointer
                                            </option>

                                            <option @if($pet->breed=='Polish Lowland Sheepdog'){{'selected'}} @endif value="Polish Lowland Sheepdog">
                                                Polish Lowland Sheepdog
                                            </option>

                                            <option @if($pet->breed=='Pomeranian'){{'selected'}} @endif value="Pomeranian">
                                                Pomeranian
                                            </option>

                                            <option @if($pet->breed=='Poodle'){{'selected'}} @endif value="Poodle">
                                                Poodle
                                            </option>

                                            <option @if($pet->breed=='Portuguese Podengo'){{'selected'}} @endif value="Portuguese Podengo">
                                                Portuguese Podengo
                                            </option>

                                            <option @if($pet->breed=='Portuguese Podengo Pequeno'){{'selected'}} @endif value="Portuguese Podengo Pequeno">
                                                Portuguese Podengo Pequeno
                                            </option>

                                            <option @if($pet->breed=='Portuguese Pointer'){{'selected'}} @endif value="Portuguese Pointer">
                                                Portuguese Pointer
                                            </option>

                                            <option @if($pet->breed=='Portuguese Sheepdog'){{'selected'}} @endif value="Portuguese Sheepdog">
                                                Portuguese Sheepdog
                                            </option>

                                            <option @if($pet->breed=='Portuguese Water Dog'){{'selected'}} @endif value="Portuguese Water Dog">
                                                Portuguese Water Dog
                                            </option>

                                            <option @if($pet->breed=='Pug'){{'selected'}} @endif value="Pug">Pug
                                            </option>

                                            <option @if($pet->breed=='Puli'){{'selected'}} @endif value="Puli">Puli
                                            </option>

                                            <option @if($pet->breed=='Pumi'){{'selected'}} @endif value="Pumi">Pumi
                                            </option>

                                            <option @if($pet->breed=='Pyrenean Mastiff'){{'selected'}} @endif value="Pyrenean Mastiff">
                                                Pyrenean Mastiff
                                            </option>

                                            <option @if($pet->breed=='Pyrenean Shepherd'){{'selected'}} @endif value="Pyrenean Shepherd">
                                                Pyrenean Shepherd
                                            </option>

                                            <option @if($pet->breed==''){{'selected'}} @endif value="Queensland Heeler">
                                                Queensland Heeler
                                            </option>


                                            <option @if($pet->breed=='Rafeiro Do Alentejo'){{'selected'}} @endif value="Rafeiro Do Alentejo">
                                                Rafeiro Do Alentejo
                                            </option>
                                            <option @if($pet->breed=='Ragdoll'){{'selected'}} @endif value="Ragdoll">
                                                Ragdoll
                                            </option>

                                            <option @if($pet->breed=='Rat Terrier'){{'selected'}} @endif value="Rat Terrier">
                                                Rat Terrier
                                            </option>

                                            <option @if($pet->breed=='Redbone Coonhound'){{'selected'}} @endif value="Redbone Coonhound">
                                                Redbone Coonhound
                                            </option>

                                            <option @if($pet->breed=='Rhodesian Ridgeback'){{'selected'}} @endif value="Rhodesian Ridgeback">
                                                Rhodesian Ridgeback
                                            </option>

                                            <option @if($pet->breed=='Rottweiler'){{'selected'}} @endif value="Rottweiler">
                                                Rottweiler
                                            </option>

                                            <option @if($pet->breed=='Running Walker Foxhound'){{'selected'}} @endif value="Running Walker Foxhound">
                                                Running Walker Foxhound
                                            </option>

                                            <option @if($pet->breed=='Russian Blue'){{'selected'}} @endif value="Russian Blue">
                                                Russian Blue
                                            </option>
                                            <option @if($pet->breed=='Russian Toy'){{'selected'}} @endif value="Russian Toy">
                                                Russian Toy
                                            </option>

                                            <option @if($pet->breed=='Russian Tsvetnaya Bolonka'){{'selected'}} @endif value="Russian Tsvetnaya Bolonka">
                                                Russian Tsvetnaya Bolonka
                                            </option>
                                            <option @if($pet->breed=='Saint Bernard'){{'selected'}} @endif value="Saint Bernard">
                                                Saint Bernard
                                            </option>

                                            <option @if($pet->breed=='Saluki'){{'selected'}} @endif value="Saluki">
                                                Saluki
                                            </option>

                                            <option @if($pet->breed=='Samoyed'){{'selected'}} @endif value="Samoyed">
                                                Samoyed
                                            </option>
                                            <option @if($pet->breed=='Savannah'){{'selected'}} @endif value="Savannah">
                                                Savannah
                                            </option>
                                            <option @if($pet->breed=='Serengeti'){{'selected'}} @endif value="Serengeti">
                                                Serengeti
                                            </option>

                                            <option @if($pet->breed=='Schapendoes'){{'selected'}} @endif value="Schapendoes">
                                                Schapendoes
                                            </option>

                                            <option @if($pet->breed=='Schipperke'){{'selected'}} @endif value="Schipperke">
                                                Schipperke
                                            </option>

                                            <option @if($pet->breed=='Schnauzer'){{'selected'}} @endif value="Schnauzer">
                                                Schnauzer
                                            </option>

                                            <option @if($pet->breed=='Scottish Deerhound'){{'selected'}} @endif value="Scottish Deerhound">
                                                Scottish Deerhound
                                            </option>
                                            <option @if($pet->breed=='Scottish Fold'){{'selected'}} @endif value="Scottish Fold">
                                                Scottish Fold
                                            </option>

                                            <option @if($pet->breed=='Scottish Terrier'){{'selected'}} @endif value="Scottish Terrier">
                                                Scottish Terrier
                                            </option>

                                            <option @if($pet->breed=='Sealyham Terrier'){{'selected'}} @endif value="Sealyham Terrier">
                                                Sealyham Terrier
                                            </option>
                                            <option @if($pet->breed=='Selkirk Rex'){{'selected'}} @endif value="Selkirk Rex">
                                                Selkirk Rex
                                            </option>

                                            <option @if($pet->breed=='Shar-Pei'){{'selected'}} @endif value="Shar-Pei">
                                                Shar-Pei
                                            </option>

                                            <option @if($pet->breed=='Shetland Sheepdog'){{'selected'}} @endif value="Shetland Sheepdog">
                                                Shetland Sheepdog
                                            </option>

                                            <option @if($pet->breed=='Siberian'){{'selected'}} @endif value="Siberian">
                                                Siberian
                                            </option>
                                            <option @if($pet->breed=='Shiba Inu'){{'selected'}} @endif value="Shiba Inu">
                                                Shiba Inu
                                            </option>

                                            <option @if($pet->breed=='Shih Tzu'){{'selected'}} @endif value="Shih Tzu">
                                                Shih Tzu
                                            </option>

                                            <option @if($pet->breed=='Shikoku'){{'selected'}} @endif value="Shikoku">
                                                Shikoku
                                            </option>

                                            <option @if($pet->breed=='Siamese'){{'selected'}} @endif value="Siamese">
                                                Siamese
                                            </option>
                                            <option @if($pet->breed=='Siberian Husky'){{'selected'}} @endif value="Siberian Husky">
                                                Siberian Husky
                                            </option>

                                            <option @if($pet->breed=='Silky Terrier'){{'selected'}} @endif value="Silky Terrie">
                                                Silky Terrier
                                            </option>
                                            <option @if($pet->breed=='Singapura'){{'selected'}} @endif value="Singapura">
                                                Singapura
                                            </option>

                                            <option @if($pet->breed=='Skye Terrier'){{'selected'}} @endif value="Skye Terrier">
                                                Skye Terrier
                                            </option>

                                            <option @if($pet->breed=='Sloughi'){{'selected'}} @endif value="Sloughi">
                                                Sloughi
                                            </option>

                                            <option @if($pet->breed=='Slovensky Cuvac'){{'selected'}} @endif value="Slovensky Cuvac">
                                                Slovensky Cuvac
                                            </option>

                                            <option @if($pet->breed=='Slovensky Kopov'){{'selected'}} @endif value="Slovensky Kopov">
                                                Slovensky Kopov
                                            </option>

                                            <option @if($pet->breed=='Small Munsterlander Pointer'){{'selected'}} @endif value="Small Munsterlander Pointer">
                                                Small Munsterlander Pointer
                                            </option>

                                            <option @if($pet->breed=='Smooth Fox Terrier'){{'selected'}} @endif value="Smooth Fox Terrier">
                                                Smooth Fox Terrier
                                            </option>
                                            <option @if($pet->breed=='Somali'){{'selected'}} @endif value="Somali">
                                                Somali
                                            </option>
                                            <option @if($pet->breed=='Snowshoe'){{'selected'}} @endif value="Snowshoe">
                                                Snowshoe
                                            </option>

                                            <option @if($pet->breed=='Soft Coated Wheaten Terrier'){{'selected'}} @endif value="Soft Coated Wheaten Terrier">
                                                Soft Coated Wheaten Terrier
                                            </option>
                                            <option @if($pet->breed=='Sokoke'){{'selected'}} @endif value="Sokoke">
                                                Sokoke
                                            </option>

                                            <option @if($pet->breed=='Spanish Mastiff'){{'selected'}} @endif value="Spanish Mastiff">
                                                Spanish Mastiff
                                            </option>

                                            <option @if($pet->breed=='Spanish Water Dog'){{'selected'}} @endif value="Spanish Water Dog">
                                                Spanish Water Dog
                                            </option>

                                            <option @if($pet->breed=='Sphynx'){{'selected'}} @endif value="Sphynx">
                                                Sphynx
                                            </option>
                                            <option @if($pet->breed=='Spinone Italiano'){{'selected'}} @endif value="Spinone Italiano">
                                                Spinone Italiano
                                            </option>

                                            <option @if($pet->breed=='Stabyhoun'){{'selected'}} @endif value="Stabyhoun">
                                                Stabyhoun
                                            </option>

                                            <option @if($pet->breed=='Staffordshire Bull Terrier'){{'selected'}} @endif value="Staffordshire Bull Terrier">
                                                Staffordshire Bull Terrier
                                            </option>

                                            <option @if($pet->breed=='Sussex Spaniel'){{'selected'}} @endif value="Sussex Spaniel">
                                                Sussex Spaniel
                                            </option>

                                            <option @if($pet->breed=='Swedish Lapphund'){{'selected'}} @endif value="Swedish Lapphund">
                                                Swedish Lapphund
                                            </option>

                                            <option @if($pet->breed=='Swedish Vallhund'){{'selected'}} @endif value="Swedish Vallhund">
                                                Swedish Vallhund
                                            </option>


                                            <option @if($pet->breed=='Tamaskan'){{'selected'}} @endif value="Tamaskan">
                                                Tamaskan
                                            </option>

                                            <option @if($pet->breed=='Terrier Mix'){{'selected'}} @endif value="Terrier Mix">
                                                Terrier Mix
                                            </option>

                                            <option @if($pet->breed=='Thai'){{'selected'}} @endif value="Thai">Thai
                                            </option>
                                            <option @if($pet->breed=='Thai Ridgeback'){{'selected'}} @endif value="Thai Ridgeback">
                                                Thai Ridgeback
                                            </option>

                                            <option @if($pet->breed=='Tibetan Mastiff'){{'selected'}} @endif value="Tibetan Mastiff">
                                                Tibetan Mastiff
                                            </option>

                                            <option @if($pet->breed=='Tibetan Spaniel'){{'selected'}} @endif value="Tibetan Spaniel">
                                                Tibetan Spaniel
                                            </option>

                                            <option @if($pet->breed=='Tibetan Terrier'){{'selected'}} @endif value="Tibetan Terrier">
                                                Tibetan Terrier
                                            </option>

                                            <option @if($pet->breed=='Tonkinese'){{'selected'}} @endif value="Tonkinese">
                                                Tonkinese
                                            </option>
                                            <option @if($pet->breed=='Tornjak'){{'selected'}} @endif value="Tornjak">
                                                Tornjak
                                            </option>

                                            <option @if($pet->breed=='Tosa'){{'selected'}} @endif value="Tosa">Tosa
                                            </option>

                                            <option @if($pet->breed=='Toyger'){{'selected'}} @endif value="Toyger">
                                                Toyger
                                            </option>
                                            <option @if($pet->breed=='Toy Fox Terrier'){{'selected'}} @endif value="Toy Fox Terrier">
                                                Toy Fox Terrier
                                            </option>

                                            <option @if($pet->breed=='Transylvanian Hound'){{'selected'}} @endif value="Transylvanian Hound">
                                                Transylvanian Hound
                                            </option>

                                            <option @if($pet->breed=='Treeing Walker Brindle'){{'selected'}} @endif value="Treeing Walker Brindle">
                                                Treeing Walker Brindle
                                            </option>

                                            <option @if($pet->breed=='Treeing Walker Coonhound'){{'selected'}} @endif value="Treeing Walker Coonhound">
                                                Treeing Walker Coonhound
                                            </option>
                                            <option @if($pet->breed=='Turkish Angora'){{'selected'}} @endif value="Turkish Angora">
                                                Turkish Angora
                                            </option>
                                            <option @if($pet->breed=='Turkish Van'){{'selected'}} @endif value="Turkish Van">
                                                Turkish Van
                                            </option>

                                            <option @if($pet->breed=='Unknown'){{'selected'}} @endif value="Unknown">
                                                Unknown
                                            </option>

                                            <option @if($pet->breed=='Vizsla'){{'selected'}} @endif value="Vizsla">
                                                Vizsla
                                            </option>

                                            <option @if($pet->breed=='Weimaraner'){{'selected'}} @endif value="Weimaraner">
                                                Weimaraner
                                            </option>

                                            <option @if($pet->breed=='Welsh Springer Spaniel'){{'selected'}} @endif value="Welsh Springer Spaniel">
                                                Welsh Springer Spaniel
                                            </option>

                                            <option @if($pet->breed=='Welsh Terrier'){{'selected'}} @endif value="Welsh Terrier">
                                                Welsh Terrier
                                            </option>

                                            <option @if($pet->breed=='West Highland White Terrier'){{'selected'}} @endif value="West Highland White Terrier">
                                                West Highland White Terrier
                                            </option>

                                            <option @if($pet->breed=='Whippet'){{'selected'}} @endif value="Whippet">
                                                Whippet
                                            </option>

                                            <option @if($pet->breed=='Wire Fox Terrier'){{'selected'}} @endif value="Wire Fox Terrier">
                                                Wire Fox Terrier
                                            </option>

                                            <option @if($pet->breed=='Wirehaired Pointing Griffon'){{'selected'}} @endif value="Wirehaired Pointing Griffon">
                                                Wirehaired Pointing Griffon
                                            </option>

                                            <option @if($pet->breed=='Wolfdog'){{'selected'}} @endif value="Wolfdog">
                                                Wolfdog
                                            </option>

                                            <option @if($pet->breed=='Working Kelpie'){{'selected'}} @endif value="Working Kelpie">
                                                Working Kelpie
                                            </option>

                                            <option @if($pet->breed=='Xoloitzcuintli'){{'selected'}} @endif value="Xoloitzcuintli">
                                                Xoloitzcuintli
                                            </option>

                                            <option @if($pet->breed=='Yorkshire Terrier'){{'selected'}} @endif value="Yorkshire Terrier">
                                                Yorkshire Terrier
                                            </option>
                                        </select>
                                        {{-- <select  class="form-control" id="breed" name="breed">
                                            <option value="" @if($pet->breed==''){{'selected'}}@endif>Mixed Breed</option>

                                            <option value="Unknown" @if($pet->breed=='Unknown'){{'selected'}}@endif>Unknown</option>

                                            <option value="Other" @if($pet->breed=='Other'){{'selected'}}@endif>Other</option>

                                            <option value="Affenpinscher" @if($pet->breed=='Affenpinscher'){{'selected'}}@endif>Affenpinscher</option>

                                            <option value="Afghan Hound" @if($pet->breed=='Afghan Hound'){{'selected'}}@endif>Afghan Hound</option>

                                            <option value="Airedale Terrier" @if($pet->breed=='Airedale Terrier'){{'selected'}}@endif>Airedale Terrier</option>

                                            <option value="Akita" @if($pet->breed=='Akita'){{'selected'}}@endif>Akita</option>

                                            <option value="Alaskan Husky" @if($pet->breed=='Alaskan Husky'){{'selected'}}@endif>Alaskan Husky</option>

                                            <option value="Alaskan Klee Kai" @if($pet->breed=='Alaskan Klee Kai'){{'selected'}}@endif>Alaskan Klee Kai</option>

                                            <option value="Alaskan Malamute" @if($pet->breed=='Alaskan Malamute'){{'selected'}}@endif>Alaskan Malamute</option>

                                            <option value="American Bulldog" @if($pet->breed=='American Bulldog'){{'selected'}}@endif>American Bulldog</option>

                                            <option value="American Bully" @if($pet->breed=='American Bully'){{'selected'}}@endif>American Bully</option>

                                            <option value="American English Coonhound" @if($pet->breed=='American English Coonhound'){{'selected'}}@endif>American English Coonhound</option>

                                            <option value="American Eskimo Dog" @if($pet->breed=='American Eskimo Dog'){{'selected'}}@endif>American Eskimo Dog</option>

                                            <option value="American Foxhound" @if($pet->breed=='American Foxhound'){{'selected'}}@endif>American Foxhound</option>

                                            <option value="American Hairless Terrier" @if($pet->breed=='American Hairless Terrier'){{'selected'}}@endif>American Hairless Terrier</option>

                                            <option value="American Leopard Hound" @if($pet->breed=='American Leopard Hound'){{'selected'}}@endif>American Leopard Hound</option>

                                            <option value="American Staffordshire Terrier" @if($pet->breed=='American Staffordshire Terrier'){{'selected'}}@endif>American Staffordshire Terrier</option>

                                            <option value="American Water Spaniel" @if($pet->breed=='American Water Spaniel'){{'selected'}}@endif>American Water Spaniel</option>

                                            <option value="Anatolian Shepherd Dog" @if($pet->breed=='Anatolian Shepherd Dog'){{'selected'}}@endif>Anatolian Shepherd Dog</option>

                                            <option value="Appenzeller Sennenhunde" @if($pet->breed=='Appenzeller Sennenhunde'){{'selected'}}@endif>Appenzeller Sennenhunde</option>

                                            <option value="Australian Cattle Dog" @if($pet->breed=='Australian Cattle Dog'){{'selected'}}@endif>Australian Cattle Dog</option>

                                            <option value="Australian Kelpie" @if($pet->breed=='Australian Kelpie'){{'selected'}}@endif>Australian Kelpie</option>

                                            <option value="Australian Shepherd" @if($pet->breed=='Australian Shepherd'){{'selected'}}@endif>Australian Shepherd</option>

                                            <option value="Australian Terrier" @if($pet->breed=='Australian Terrier'){{'selected'}}@endif>Australian Terrier</option>

                                            <option value="Azawakh" @if($pet->breed=='Azawakh'){{'selected'}}@endif>Azawakh</option>

                                            <option value="Barbet" @if($pet->breed=='Barbet'){{'selected'}}@endif>Barbet</option>

                                            <option value="Basenji" @if($pet->breed=='Basenji'){{'selected'}}@endif>Basenji</option>

                                            <option value="Basset Fauve De Bretagne" @if($pet->breed=='Basset Fauve De Bretagne'){{'selected'}}@endif>Basset Fauve De Bretagne</option>

                                            <option value="Basset Hound" @if($pet->breed=='Basset Hound'){{'selected'}}@endif>Basset Hound</option>

                                            <option value="Beagle" @if($pet->breed=='Beagle'){{'selected'}}@endif>Beagle</option>

                                            <option value="Bearded Collie" @if($pet->breed=='Bearded Collie'){{'selected'}}@endif>Bearded Collie</option>

                                            <option value="Beauceron" @if($pet->breed=='Beauceron'){{'selected'}}@endif>Beauceron</option>

                                            <option value="Bedlington Terrier" @if($pet->breed=='Bedlington Terrier'){{'selected'}}@endif>Bedlington Terrier</option>

                                            <option value="Belgian Laekenois" @if($pet->breed=='Belgian Laekenois'){{'selected'}}@endif>Belgian Laekenois</option>

                                            <option value="Belgian Malinois" @if($pet->breed=='Belgian Malinois'){{'selected'}}@endif>Belgian Malinois</option>

                                            <option value="Belgian Sheepdog" @if($pet->breed=='Belgian Sheepdog'){{'selected'}}@endif>Belgian Sheepdog</option>

                                            <option value="Belgian Tervuren" @if($pet->breed=='Belgian Tervuren'){{'selected'}}@endif>Belgian Tervuren</option>

                                            <option value="Bergamasco" @if($pet->breed=='Bergamasco'){{'selected'}}@endif>Bergamasco</option>

                                            <option value="Berger Picard" @if($pet->breed=='Berger Picard'){{'selected'}}@endif>Berger Picard</option>

                                            <option value="Bernese Mountain Dog" @if($pet->breed=='Bernese Mountain Dog'){{'selected'}}@endif>Bernese Mountain Dog</option>

                                            <option value="Bichon Frise" @if($pet->breed=='Bichon Frise'){{'selected'}}@endif>Bichon Frise</option>

                                            <option value="Biewer Terrier" @if($pet->breed=='Biewer Terrier'){{'selected'}}@endif>Biewer Terrier</option>

                                            <option value="Black and Tan Coonhound" @if($pet->breed=='Black and Tan Coonhound'){{'selected'}}@endif>Black and Tan Coonhound</option>

                                            <option value="Black Russian Terrier" @if($pet->breed=='Black Russian Terrier'){{'selected'}}@endif>Black Russian Terrier</option>

                                            <option value="Blackmouth Cur" @if($pet->breed=='Blackmouth Cur'){{'selected'}}@endif>Blackmouth Cur</option>

                                            <option value="Bloodhound" @if($pet->breed=='Bloodhound'){{'selected'}}@endif>Bloodhound</option>

                                            <option value="Bluetick Coonhound" @if($pet->breed=='Bluetick Coonhound'){{'selected'}}@endif>Bluetick Coonhound</option>

                                            <option value="Boerboel" @if($pet->breed=='Boerboel'){{'selected'}}@endif>Boerboel</option>

                                            <option value="Bolognese" @if($pet->breed=='Bolognese'){{'selected'}}@endif>Bolognese</option>

                                            <option value="Border Collie" @if($pet->breed=='Border Collie'){{'selected'}}@endif>Border Collie</option>

                                            <option value="Border Terrier" @if($pet->breed=='Border Terrier'){{'selected'}}@endif>Border Terrier</option>

                                            <option value="Borzoi" @if($pet->breed=='Borzoi'){{'selected'}}@endif>Borzoi</option>

                                            <option value="Boston Terrier" @if($pet->breed=='Boston Terrier'){{'selected'}}@endif>Boston Terrier</option>

                                            <option value="Bouvier des Flandres" @if($pet->breed=='Bouvier des Flandres'){{'selected'}}@endif>Bouvier des Flandres</option>

                                            <option value="Boxer" @if($pet->breed=='Boxer'){{'selected'}}@endif>Boxer</option>

                                            <option value="Boykin Spaniel" @if($pet->breed=='Boykin Spaniel'){{'selected'}}@endif>Boykin Spaniel</option>

                                            <option value="Bracco Italiano" @if($pet->breed=='Bracco Italiano'){{'selected'}}@endif>Bracco Italiano</option>

                                            <option value="Braque Du Bourbonnais" @if($pet->breed=='Braque Du Bourbonnais'){{'selected'}}@endif>Braque Du Bourbonnais</option>

                                            <option value="Briard" @if($pet->breed=='Briard'){{'selected'}}@endif>Briard</option>

                                            <option value="Brittany Spaniel" @if($pet->breed=='Brittany Spaniel'){{'selected'}}@endif>Brittany Spaniel</option>

                                            <option value="Broholmer" @if($pet->breed=='Broholmer'){{'selected'}}@endif>Broholmer</option>

                                            <option value="Brussels Griffon" @if($pet->breed=='Brussels Griffon'){{'selected'}}@endif>Brussels Griffon</option>

                                            <option value="Bull Terrier" @if($pet->breed=='Bull Terrier'){{'selected'}}@endif>Bull Terrier</option>

                                            <option value="Bulldog" @if($pet->breed=='Bulldog'){{'selected'}}@endif>Bulldog</option>

                                            <option value="Bullmastiff" @if($pet->breed=='Bullmastiff'){{'selected'}}@endif>Bullmastiff</option>

                                            <option value="Cairn Terrier" @if($pet->breed=='Cairn Terrier'){{'selected'}}@endif>Cairn Terrier</option>

                                            <option value="Canaan Dog" @if($pet->breed=='Canaan Dog'){{'selected'}}@endif>Canaan Dog</option>

                                            <option value="Cane Corso" @if($pet->breed=='Cane Corso'){{'selected'}}@endif>Cane Corso</option>

                                            <option value="Cardigan Welsh Corgi" @if($pet->breed=='Cardigan Welsh Corgi'){{'selected'}}@endif>Cardigan Welsh Corgi</option>

                                            <option value="Catahoula" @if($pet->breed=='Catahoula'){{'selected'}}@endif>Catahoula</option>

                                            <option value="Caucasian Ovcharka" @if($pet->breed=='Caucasian Ovcharka'){{'selected'}}@endif>Caucasian Ovcharka</option>

                                            <option value="Cavalier King Charles Spaniel" @if($pet->breed=='Cavalier King Charles Spaniel'){{'selected'}}@endif>Cavalier King Charles Spaniel</option>

                                            <option value="Central Asian Shepherd Dog" @if($pet->breed=='Central Asian Shepherd Dog'){{'selected'}}@endif>Central Asian Shepherd Dog</option>

                                            <option value="Cesky Terrier" @if($pet->breed=='Cesky Terrier'){{'selected'}}@endif>Cesky Terrier</option>

                                            <option value="Chesapeake Bay Retriever" @if($pet->breed=='Chesapeake Bay Retriever'){{'selected'}}@endif>Chesapeake Bay Retriever</option>

                                            <option value="Chihuahua" @if($pet->breed=='Chihuahua'){{'selected'}}@endif>Chihuahua</option>

                                            <option value="Chinese Crested" @if($pet->breed=='Chinese Crested'){{'selected'}}@endif>Chinese Crested</option>

                                            <option value="Chinese Shar-Pei" @if($pet->breed=='Chinese Shar-Pei'){{'selected'}}@endif>Chinese Shar-Pei</option>

                                            <option value="Chinook" @if($pet->breed=='Chinook'){{'selected'}}@endif>Chinook</option>

                                            <option value="Chow Chow" @if($pet->breed=='Chow Chow'){{'selected'}}@endif>Chow Chow</option>

                                            <option value="Cirneco Dell'Etna" @if($pet->breed=="Cirneco Dell'Etna"){{'selected'}}@endif>Cirneco Dell'Etna</option>

                                            <option value="Clumber Spaniel" @if($pet->breed=='Clumber Spaniel'){{'selected'}}@endif>Clumber Spaniel</option>

                                            <option value="Cocker Spaniel" @if($pet->breed=='Cocker Spaniel'){{'selected'}}@endif>Cocker Spaniel</option>

                                            <option value="Collie" @if($pet->breed=='Collie'){{'selected'}}@endif>Collie</option>

                                            <option value="Coton de Tulear" @if($pet->breed=='Coton de Tulear'){{'selected'}}@endif>Coton de Tulear</option>

                                            <option value="Curly-Coated Retriever" @if($pet->breed=='Curly-Coated Retriever'){{'selected'}}@endif>Curly-Coated Retriever</option>

                                            <option value="Czechoslovakian Vlcak" @if($pet->breed=='Czechoslovakian Vlcak'){{'selected'}}@endif>Czechoslovakian Vlcak</option>

                                            <option value="Dachshund" @if($pet->breed=='Dachshund'){{'selected'}}@endif>Dachshund</option>

                                            <option value="Dalmatian" @if($pet->breed=='Dalmatian'){{'selected'}}@endif>Dalmatian</option>

                                            <option value="Dandie Dinmont Terrier" @if($pet->breed=='Dandie Dinmont Terrier'){{'selected'}}@endif>Dandie Dinmont Terrier</option>

                                            <option value="Danish-Swedish Farmdog" @if($pet->breed=='Danish-Swedish Farmdog'){{'selected'}}@endif>Danish-Swedish Farmdog</option>

                                            <option value="Deutscher Wachtelhund" @if($pet->breed=='Deutscher Wachtelhund'){{'selected'}}@endif>Deutscher Wachtelhund</option>

                                            <option value="Doberman Pinscher" @if($pet->breed=='Doberman Pinscher'){{'selected'}}@endif>Doberman Pinscher</option>

                                            <option value="Dogo Argentino" @if($pet->breed=='Dogo Argentino'){{'selected'}}@endif>Dogo Argentino</option>

                                            <option value="Dogue de Bordeaux" @if($pet->breed=='Dogue de Bordeaux'){{'selected'}}@endif>Dogue de Bordeaux</option>

                                            <option value="Drentsche Patrijshond" @if($pet->breed=='Drentsche Patrijshond'){{'selected'}}@endif>Drentsche Patrijshond</option>

                                            <option value="Drever" @if($pet->breed=='Drever'){{'selected'}}@endif>Drever</option>

                                            <option value="Dutch Shepherd" @if($pet->breed=='Dutch Shepherd'){{'selected'}}@endif>Dutch Shepherd</option>

                                            <option value="English Bulldog" @if($pet->breed=='English Bulldog'){{'selected'}}@endif>English Bulldog</option>

                                            <option value="English Cocker Spaniel" @if($pet->breed=='English Cocker Spaniel'){{'selected'}}@endif>English Cocker Spaniel</option>

                                            <option value="English Foxhound" @if($pet->breed=='English Foxhound'){{'selected'}}@endif>English Foxhound</option>

                                            <option value="English Pointer" @if($pet->breed=='English Pointer'){{'selected'}}@endif>English Pointer</option>

                                            <option value="English Setter" @if($pet->breed=='English Setter'){{'selected'}}@endif>English Setter</option>

                                            <option value="English Shepherd" @if($pet->breed=='English Shepherd'){{'selected'}}@endif>English Shepherd</option>

                                            <option value="English Springer Spaniel" @if($pet->breed=='English Springer Spaniel'){{'selected'}}@endif>English Springer Spaniel</option>

                                            <option value="English Toy Spaniel" @if($pet->breed=='English Toy Spaniel'){{'selected'}}@endif>English Toy Spaniel</option>

                                            <option value="Entlebucher Mountain Dog" @if($pet->breed=='Entlebucher Mountain Dog'){{'selected'}}@endif>Entlebucher Mountain Dog</option>

                                            <option value="Estrela Mountain Dog" @if($pet->breed=='Estrela Mountain Dog'){{'selected'}}@endif>Estrela Mountain Dog</option>

                                            <option value="Eurasier" @if($pet->breed=='Eurasier'){{'selected'}}@endif>Eurasier</option>

                                            <option value="Field Spaniel" @if($pet->breed=='Field Spaniel'){{'selected'}}@endif>Field Spaniel</option>

                                            <option value="Finnish Lapphund" @if($pet->breed=='Finnish Lapphund'){{'selected'}}@endif>Finnish Lapphund</option>

                                            <option value="Finnish Spitz" @if($pet->breed=='Finnish Spitz'){{'selected'}}@endif>Finnish Spitz</option>

                                            <option value="Flat-Coated Retriever" @if($pet->breed=='Flat-Coated Retriever'){{'selected'}}@endif>Flat-Coated Retriever</option>

                                            <option value="French Bulldog" @if($pet->breed=='French Bulldog'){{'selected'}}@endif>French Bulldog</option>

                                            <option value="French Spaniel" @if($pet->breed=='French Spaniel'){{'selected'}}@endif>French Spaniel</option>

                                            <option value="German Longhaired Pointer" @if($pet->breed=='German Longhaired Pointer'){{'selected'}}@endif>German Longhaired Pointer</option>

                                            <option value="German Pinscher" @if($pet->breed=='German Pinscher'){{'selected'}}@endif>German Pinscher</option>

                                            <option value="German Shepherd" @if($pet->breed=='German Shepherd'){{'selected'}}@endif>German Shepherd</option>

                                            <option value="German Shorthaired Pointer" @if($pet->breed=='German Shorthaired Pointer'){{'selected'}}@endif>German Shorthaired Pointer</option>

                                            <option value="German Spitz" @if($pet->breed=='German Spitz'){{'selected'}}@endif>German Spitz</option>

                                            <option value="German Wirehaired Pointer" @if($pet->breed=='German Wirehaired Pointer'){{'selected'}}@endif>German Wirehaired Pointer</option>

                                            <option value="Giant Schnauzer" @if($pet->breed=='Giant Schnauzer'){{'selected'}}@endif>Giant Schnauzer</option>

                                            <option value="Glen of Imaal Terrier" @if($pet->breed=='Glen of Imaal Terrier'){{'selected'}}@endif>Glen of Imaal Terrier</option>

                                            <option value="Golden Retriever" @if($pet->breed=='Golden Retriever'){{'selected'}}@endif>Golden Retriever</option>

                                            <option value="Gordon Setter" @if($pet->breed=='Gordon Setter'){{'selected'}}@endif>Gordon Setter</option>

                                            <option value="Grand Basset Griffon Vendeen" @if($pet->breed=='Grand Basset Griffon Vendeen'){{'selected'}}@endif>Grand Basset Griffon Vendeen</option>

                                            <option value="Great Dane" @if($pet->breed=='Great Dane'){{'selected'}}@endif>Great Dane</option>

                                            <option value="Great Pyrenees" @if($pet->breed=='Great Pyrenees'){{'selected'}}@endif>Great Pyrenees</option>

                                            <option value="Greater Swiss Mountain Dog" @if($pet->breed=='Greater Swiss Mountain Dog'){{'selected'}}@endif>Greater Swiss Mountain Dog</option>

                                            <option value="Greyhound" @if($pet->breed=='Greyhound'){{'selected'}}@endif>Greyhound</option>

                                            <option value="Hamiltonstovare" @if($pet->breed=='Hamiltonstovare'){{'selected'}}@endif>Hamiltonstovare</option>

                                            <option value="Harrier" @if($pet->breed=='Harrier'){{'selected'}}@endif>Harrier</option>

                                            <option value="Havanese" @if($pet->breed=='Havanese'){{'selected'}}@endif>Havanese</option>

                                            <option value="Hound" @if($pet->breed=='Hound'){{'selected'}}@endif>Hound</option>

                                            <option value="Hovawart" @if($pet->breed=='Hovawart'){{'selected'}}@endif>Hovawart</option>

                                            <option value="Ibizan Hound" @if($pet->breed=='Ibizan Hound'){{'selected'}}@endif>Ibizan Hound</option>

                                            <option value="Icelandic Sheepdog" @if($pet->breed=='Icelandic Sheepdog'){{'selected'}}@endif>Icelandic Sheepdog</option>

                                            <option value="Irish Red and White Setter" @if($pet->breed=='Irish Red and White Setter'){{'selected'}}@endif>Irish Red and White Setter</option>

                                            <option value="Irish Setter" @if($pet->breed=='Irish Setter'){{'selected'}}@endif>Irish Setter</option>

                                            <option value="Irish Terrier" @if($pet->breed=='Irish Terrier'){{'selected'}}@endif>Irish Terrier</option>

                                            <option value="Irish Water Spaniel" @if($pet->breed=='Irish Water Spaniel'){{'selected'}}@endif>Irish Water Spaniel</option>

                                            <option value="Irish Wolfhound" @if($pet->breed=='Irish Wolfhound'){{'selected'}}@endif>Irish Wolfhound</option>

                                            <option value="Italian Greyhound" @if($pet->breed=='Italian Greyhound'){{'selected'}}@endif>Italian Greyhound</option>

                                            <option value="Jack Russell Terrier" @if($pet->breed=='Jack Russell Terrier'){{'selected'}}@endif>Jack Russell Terrier</option>

                                            <option value="Jagdterrier" @if($pet->breed=='Jagdterrier'){{'selected'}}@endif>Jagdterrier</option>

                                            <option value="Japanese Chin" @if($pet->breed=='Japanese Chin'){{'selected'}}@endif>Japanese Chin</option>

                                            <option value="Kai Ken" @if($pet->breed=='Kai Ken'){{'selected'}}@endif>Kai Ken</option>

                                            <option value="Keeshond" @if($pet->breed=='Keeshond'){{'selected'}}@endif>Keeshond</option>

                                            <option value="Kerry Blue Terrier" @if($pet->breed=='Kerry Blue Terrier'){{'selected'}}@endif>Kerry Blue Terrier</option>

                                            <option value="Kishu Ken" @if($pet->breed=='Kishu Ken'){{'selected'}}@endif>Kishu Ken</option>

                                            <option value="Komondor" @if($pet->breed=='Komondor'){{'selected'}}@endif>Komondor</option>

                                            <option value="Korean Jindo" @if($pet->breed=='Korean Jindo'){{'selected'}}@endif>Korean Jindo</option>

                                            <option value="Kromfohrlander" @if($pet->breed=='Kromfohrlander'){{'selected'}}@endif>Kromfohrlander</option>

                                            <option value="Kuvasz" @if($pet->breed=='Kuvasz'){{'selected'}}@endif>Kuvasz</option>

                                            <option value="Labrador Retriever" @if($pet->breed=='Labrador Retriever'){{'selected'}}@endif>Labrador Retriever</option>

                                            <option value="Lagotto Romagnolo" @if($pet->breed=='Lagotto Romagnolo'){{'selected'}}@endif>Lagotto Romagnolo</option>

                                            <option value="Lakeland Terrier" @if($pet->breed=='Lakeland Terrier'){{'selected'}}@endif>Lakeland Terrier</option>

                                            <option value="Lancashire Heeler" @if($pet->breed=='Lancashire Heeler'){{'selected'}}@endif>Lancashire Heeler</option>

                                            <option value="Leonberger" @if($pet->breed=='Leonberger'){{'selected'}}@endif>Leonberger</option>

                                            <option value="Lhasa Apso" @if($pet->breed=='Lhasa Apso'){{'selected'}}@endif>Lhasa Apso</option>

                                            <option value="Löwchen" @if($pet->breed=='Löwchen'){{'selected'}}@endif>Löwchen</option>

                                            <option value="Maltese" @if($pet->breed=='Maltese'){{'selected'}}@endif>Maltese</option>

                                            <option value="Manchester Terrier" @if($pet->breed=='Manchester Terrier'){{'selected'}}@endif>Manchester Terrier</option>

                                            <option value="Mastiff" @if($pet->breed=='Mastiff'){{'selected'}}@endif>Mastiff</option>

                                            <option value="Miniature American Shepherd" @if($pet->breed=='Miniature American Shepherd'){{'selected'}}@endif>Miniature American Shepherd</option>

                                            <option value="Miniature Bull Terrier" @if($pet->breed=='Miniature Bull Terrier'){{'selected'}}@endif>Miniature Bull Terrier</option>

                                            <option value="Miniature Pinscher" @if($pet->breed=='Miniature Pinscher'){{'selected'}}@endif>Miniature Pinscher</option>

                                            <option value="Miniature Schnauzer" @if($pet->breed=='Miniature Schnauzer'){{'selected'}}@endif>Miniature Schnauzer</option>

                                            <option value="Mixed Breed" @if($pet->breed=='Mixed Breed'){{'selected'}}@endif>Mixed Breed</option>

                                            <option value="Mudi" @if($pet->breed=='Mudi'){{'selected'}}@endif>Mudi</option>

                                            <option value="Neapolitan Mastiff" @if($pet->breed=='Neapolitan Mastiff'){{'selected'}}@endif>Neapolitan Mastiff</option>

                                            <option value="Nederlandse Kooikerhondje" @if($pet->breed=='Nederlandse Kooikerhondje'){{'selected'}}@endif>Nederlandse Kooikerhondje</option>

                                            <option value="Newfoundland" @if($pet->breed=='Newfoundland'){{'selected'}}@endif>Newfoundland</option>

                                            <option value="Norfolk Terrier" @if($pet->breed=='Norfolk Terrier'){{'selected'}}@endif>Norfolk Terrier</option>

                                            <option value="Norrbottenspets" @if($pet->breed=='Norrbottenspets'){{'selected'}}@endif>Norrbottenspets</option>

                                            <option value="Norwegian Buhund" @if($pet->breed=='Norwegian Buhund'){{'selected'}}@endif>Norwegian Buhund</option>

                                            <option value="Norwegian Elkhound" @if($pet->breed=='Norwegian Elkhound'){{'selected'}}@endif>Norwegian Elkhound</option>

                                            <option value="Norwegian Lundehund" @if($pet->breed=='Norwegian Lundehund'){{'selected'}}@endif>Norwegian Lundehund</option>

                                            <option value="Norwich Terrier" @if($pet->breed=='Norwich Terrier'){{'selected'}}@endif>Norwich Terrier</option>

                                            <option value="Nova Scotia Duck Tolling Retriever" @if($pet->breed=='Nova Scotia Duck Tolling Retriever'){{'selected'}}@endif>Nova Scotia Duck Tolling Retriever</option>

                                            <option value="Old English Sheepdog" @if($pet->breed=='Old English Sheepdog'){{'selected'}}@endif>Old English Sheepdog</option>

                                            <option value="Otterhound" @if($pet->breed=='Otterhound'){{'selected'}}@endif>Otterhound</option>

                                            <option value="Papillon" @if($pet->breed=='Papillon'){{'selected'}}@endif>Papillon</option>

                                            <option value="Parson Russell Terrier" @if($pet->breed=='Parson Russell Terrier'){{'selected'}}@endif>Parson Russell Terrier</option>

                                            <option value="Patterdale Terrier" @if($pet->breed=='Patterdale Terrier'){{'selected'}}@endif>Patterdale Terrier</option>

                                            <option value="Pekingese" @if($pet->breed=='Pekingese'){{'selected'}}@endif>Pekingese</option>

                                            <option value="Pembroke Welsh Corgi" @if($pet->breed=='Pembroke Welsh Corgi'){{'selected'}}@endif>Pembroke Welsh Corgi</option>

                                            <option value="Perro De Presa Canario" @if($pet->breed=='Perro De Presa Canario'){{'selected'}}@endif>Perro De Presa Canario</option>

                                            <option value="Peruvian Inca Orchid" @if($pet->breed=='Peruvian Inca Orchid'){{'selected'}}@endif>Peruvian Inca Orchid</option>

                                            <option value="Petit Basset Griffon Vendéen" @if($pet->breed=='Petit Basset Griffon Vendéen'){{'selected'}}@endif>Petit Basset Griffon Vendéen</option>

                                            <option value="Pharaoh Hound" @if($pet->breed=='Pharaoh Hound'){{'selected'}}@endif>Pharaoh Hound</option>

                                            <option value="Pit Bull" @if($pet->breed=='Pit Bull'){{'selected'}}@endif>Pit Bull</option>

                                            <option value="Plott" @if($pet->breed=='Plott'){{'selected'}}@endif>Plott</option>

                                            <option value="Plott" @if($pet->breed=='Plott'){{'selected'}}@endif>Plott</option>

                                            <option value="Pointer" @if($pet->breed=='Pointer'){{'selected'}}@endif>Pointer</option>

                                            <option value="Polish Lowland Sheepdog" @if($pet->breed=='Polish Lowland Sheepdog'){{'selected'}}@endif>Polish Lowland Sheepdog</option>

                                            <option value="Pomeranian" @if($pet->breed=='Pomeranian'){{'selected'}}@endif>Pomeranian</option>

                                            <option value="Poodle" @if($pet->breed=='Poodle'){{'selected'}}@endif>Poodle</option>

                                            <option value="Portuguese Podengo" @if($pet->breed=='Portuguese Podengo'){{'selected'}}@endif>Portuguese Podengo</option>

                                            <option value="Portuguese Podengo Pequeno" @if($pet->breed=='Portuguese Podengo Pequeno'){{'selected'}}@endif>Portuguese Podengo Pequeno</option>

                                            <option value="Portuguese Pointer" @if($pet->breed=='Portuguese Pointer'){{'selected'}}@endif>Portuguese Pointer</option>

                                            <option value="Portuguese Sheepdog" @if($pet->breed=='Portuguese Sheepdog'){{'selected'}}@endif>Portuguese Sheepdog</option>

                                            <option value="Portuguese Water Dog" @if($pet->breed=='Portuguese Water Dog'){{'selected'}}@endif>Portuguese Water Dog</option>

                                            <option value="Pug" @if($pet->breed=='Pug'){{'selected'}}@endif>Pug</option>

                                            <option value="Puli" @if($pet->breed=='Puli'){{'selected'}}@endif>Puli</option>

                                            <option value="Puli" @if($pet->breed=='Puli'){{'selected'}}@endif>Puli</option>

                                            <option value="Pumi" @if($pet->breed=='Pumi'){{'selected'}}@endif>Pumi</option>

                                            <option value="Pyrenean Mastiff" @if($pet->breed=='Pyrenean Mastiff'){{'selected'}}@endif>Pyrenean Mastiff</option>

                                            <option value="Pyrenean Shepherd" @if($pet->breed=='Pyrenean Shepherd'){{'selected'}}@endif>Pyrenean Shepherd</option>

                                            <option value="Queensland Heeler" @if($pet->breed=='Queensland Heeler'){{'selected'}}@endif>Queensland Heeler</option>

                                            <option value="Rafeiro Do Alentejo" @if($pet->breed=='Rafeiro Do Alentejo'){{'selected'}}@endif>Rafeiro Do Alentejo</option>

                                            <option value="Rat Terrier" @if($pet->breed=='Rat Terrier'){{'selected'}}@endif>Rat Terrier</option>

                                            <option value="Redbone Coonhound" @if($pet->breed=='Redbone Coonhound'){{'selected'}}@endif>Redbone Coonhound</option>

                                            <option value="Rhodesian Ridgeback" @if($pet->breed=='Rhodesian Ridgeback'){{'selected'}}@endif>Rhodesian Ridgeback</option>

                                            <option value="Rottweiler" @if($pet->breed=='Rottweiler'){{'selected'}}@endif>Rottweiler</option>

                                            <option value="Running Walker Foxhound" @if($pet->breed=='Running Walker Foxhound'){{'selected'}}@endif>Running Walker Foxhound</option>

                                            <option value="Russian Toy" @if($pet->breed=='Russian Toy'){{'selected'}}@endif>Russian Toy</option>

                                            <option value="Russian Tsvetnaya Bolonka" @if($pet->breed=='Russian Tsvetnaya Bolonka'){{'selected'}}@endif>Russian Tsvetnaya Bolonka</option>

                                            <option value="Saint Bernard" @if($pet->breed=='Saint Bernard'){{'selected'}}@endif>Saint Bernard</option>

                                            <option value="Saluki" @if($pet->breed=='Saluki'){{'selected'}}@endif>Saluki</option>

                                            <option value="Samoyed" @if($pet->breed=='Samoyed'){{'selected'}}@endif>Samoyed</option>

                                            <option value="Schapendoes" @if($pet->breed=='Schapendoes'){{'selected'}}@endif>Schapendoes</option>

                                            <option value="Schipperke" @if($pet->breed=='Schipperke'){{'selected'}}@endif>Schipperke</option>

                                            <option value="Schnauzer" @if($pet->breed=='Schnauzer'){{'selected'}}@endif>Schnauzer</option>

                                            <option value="Scottish Deerhound" @if($pet->breed=='Scottish Deerhound'){{'selected'}}@endif>Scottish Deerhound</option>

                                            <option value="Scottish Terrier" @if($pet->breed=='Scottish Terrier'){{'selected'}}@endif>Scottish Terrier</option>

                                            <option value="Sealyham Terrier" @if($pet->breed=='Sealyham Terrier'){{'selected'}}@endif>Sealyham Terrier</option>

                                            <option value="Shar-Pei" @if($pet->breed=='Shar-Pei'){{'selected'}}@endif>Shar-Pei</option>

                                            <option value="Shetland Sheepdog" @if($pet->breed=='Shetland Sheepdog'){{'selected'}}@endif>Shetland Sheepdog</option>

                                            <option value="Shiba Inu" @if($pet->breed=='Shiba Inu'){{'selected'}}@endif>Shiba Inu</option>

                                            <option value="Shih Tzu" @if($pet->breed=='Shih Tzu'){{'selected'}}@endif>Shih Tzu</option>

                                            <option value="Shikoku" @if($pet->breed=='Shikoku'){{'selected'}}@endif>Shikoku</option>

                                            <option value="Siberian Husky" @if($pet->breed=='Siberian Husky'){{'selected'}}@endif>Siberian Husky</option>

                                            <option value="Silky Terrie" @if($pet->breed=='Silky Terrie'){{'selected'}}@endif>Silky Terrier</option>

                                            <option value="Skye Terrier" @if($pet->breed=='Skye Terrier'){{'selected'}}@endif>Skye Terrier</option>

                                            <option value="Sloughi" @if($pet->breed=='Sloughi'){{'selected'}}@endif>Sloughi</option>

                                            <option value="Slovensky Cuvac" @if($pet->breed=='Slovensky Cuvac'){{'selected'}}@endif>Slovensky Cuvac</option>

                                            <option value="Slovensky Kopov" @if($pet->breed=='Slovensky Kopov'){{'selected'}}@endif>Slovensky Kopov</option>

                                            <option value="Small Munsterlander Pointer" @if($pet->breed=='Small Munsterlander Pointer'){{'selected'}}@endif>Small Munsterlander Pointer</option>

                                            <option value="Smooth Fox Terrier" @if($pet->breed=='Smooth Fox Terrier'){{'selected'}}@endif>Smooth Fox Terrier</option>

                                            <option value="Soft Coated Wheaten Terrier" @if($pet->breed=='Soft Coated Wheaten Terrier'){{'selected'}}@endif>Soft Coated Wheaten Terrier</option>

                                            <option value="Spanish Mastiff" @if($pet->breed=='Spanish Mastiff'){{'selected'}}@endif>Spanish Mastiff</option>

                                            <option value="Spanish Water Dog" @if($pet->breed=='Spanish Water Dog'){{'selected'}}@endif>Spanish Water Dog</option>

                                            <option value="Spinone Italiano" @if($pet->breed=='Spinone Italiano'){{'selected'}}@endif>Spinone Italiano</option>

                                            <option value="Stabyhoun" @if($pet->breed=='Stabyhoun'){{'selected'}}@endif>Stabyhoun</option>

                                            <option value="Staffordshire Bull Terrier" @if($pet->breed=='Staffordshire Bull Terrier'){{'selected'}}@endif>Staffordshire Bull Terrier</option>

                                            <option value="Sussex Spaniel" @if($pet->breed=='Sussex Spaniel'){{'selected'}}@endif>Sussex Spaniel</option>

                                            <option value="Swedish Lapphund" @if($pet->breed=='Swedish Lapphund'){{'selected'}}@endif>Swedish Lapphund</option>

                                            <option value="Swedish Vallhund" @if($pet->breed=='Swedish Vallhund'){{'selected'}}@endif>Swedish Vallhund</option>

                                            <option value="Tamaskan" @if($pet->breed=='Tamaskan'){{'selected'}}@endif>Tamaskan</option>

                                            <option value="Terrier Mix" @if($pet->breed=='Terrier Mix'){{'selected'}}@endif>Terrier Mix</option>

                                            <option value="Thai Ridgeback" @if($pet->breed=='Thai Ridgeback'){{'selected'}}@endif>Thai Ridgeback</option>

                                            <option value="Tibetan Mastiff" @if($pet->breed=='Tibetan Mastiff'){{'selected'}}@endif>Tibetan Mastiff</option>

                                            <option value="Tibetan Spaniel" @if($pet->breed=='Tibetan Spaniel'){{'selected'}}@endif>Tibetan Spaniel</option>

                                            <option value="Tibetan Terrier" @if($pet->breed=='Tibetan Terrier'){{'selected'}}@endif>Tibetan Terrier</option>

                                            <option value="Tornjak" @if($pet->breed=='Tornjak'){{'selected'}}@endif>Tornjak</option>

                                            <option value="Tosa" @if($pet->breed=='Tosa'){{'selected'}}@endif>Tosa</option>

                                            <option value="Toy Fox Terrier" @if($pet->breed=='Toy Fox Terrier'){{'selected'}}@endif>Toy Fox Terrier</option>

                                            <option value="Transylvanian Hound" @if($pet->breed=='Transylvanian Hound'){{'selected'}}@endif>Transylvanian Hound</option>

                                            <option value="Treeing Walker Brindle" @if($pet->breed=='Treeing Walker Brindle'){{'selected'}}@endif>Treeing Walker Brindle</option>

                                            <option value="Treeing Walker Coonhound" @if($pet->breed=='Treeing Walker Coonhound'){{'selected'}}@endif>Treeing Walker Coonhound</option>

                                            <option value="Unknown" @if($pet->breed=='Unknown'){{'selected'}}@endif>Unknown</option>

                                            <option value="Vizsla" @if($pet->breed=='Vizsla'){{'selected'}}@endif>Vizsla</option>

                                            <option value="Weimaraner" @if($pet->breed=='Weimaraner'){{'selected'}}@endif>Weimaraner</option>

                                            <option value="Welsh Springer Spaniel" @if($pet->breed=='Welsh Springer Spaniel'){{'selected'}}@endif>Welsh Springer Spaniel</option>

                                            <option value="Welsh Terrier" @if($pet->breed=='Welsh Terrier'){{'selected'}}@endif>Welsh Terrier</option>

                                            <option value="West Highland White Terrier" @if($pet->breed=='West Highland White Terrier'){{'selected'}}@endif>West Highland White Terrier</option>

                                            <option value="Whippet" @if($pet->breed=='Whippet'){{'selected'}}@endif>Whippet</option>

                                            <option value="Wire Fox Terrier" @if($pet->breed=='Wire Fox Terrier'){{'selected'}}@endif>Wire Fox Terrier</option>

                                            <option value="Wirehaired Pointing Griffon" @if($pet->breed=='Wirehaired Pointing Griffon'){{'selected'}}@endif>Wirehaired Pointing Griffon</option>

                                            <option value="Wolfdog" @if($pet->breed=='Wolfdog'){{'selected'}}@endif>Wolfdog</option>

                                            <option value="Working Kelpie" @if($pet->breed=='Working Kelpie'){{'selected'}}@endif>Working Kelpie</option>

                                            <option value="Xoloitzcuintli" @if($pet->breed=='Xoloitzcuintli'){{'selected'}}@endif>Xoloitzcuintli</option>

                                            <option value="Yorkshire Terrier" @if($pet->breed=='Yorkshire Terrier'){{'selected'}}@endif>Yorkshire Terrier</option>
                                        </select> --}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="datepicker" class="col-sm-3 col-form-label">Lost Date:</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="text" id="date" name="lost_found_date" onkeydown="return false"
                                               placeholder="Lost date*" value="{{$pet->date}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="phone_number" class="col-sm-3 col-form-label">Description:</label>
                                    <div class="col-12">
                                            <textarea class="form-control" rows="5" name="description"
                                                      placeholder="She is four pounds, very short tail and has a hump on her back. She has a very soft bark. Not fixed. Skittish and afraid of people. Orange collar with name and license tags.Not fixed and !!!!afraid of people. Orange collar with name and license tags.">{{$pet->description}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group d-flex flex-row justify-content-end">
                                    <button type="submit" class="mt-3 pet-profile-details_info-edit-bottom_btn">Update
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}

                        <!-- /.form tag end -->
                            @if(count($payments) > 0)
                                <h2 class="user-panel_inner-content-msg mt-5 text-center">
                                    Please Go to "My Pet Alert" and Make Sure to Comment On the Post
                                </h2>

                                <div class="alert-map">
                                    <div class="alert-map_btn btn-success">
                                        <a href="{{ url('/user/alerts/' . $pet->id) }}">Your PawMaw Alert Activated!</a>
                                    </div>
                                    @if($pet->lat)
                                        <div id="map" style="width: 100%; height: 320px;"></div>
                                    @else
                                        <img src="{{ asset('pawmaw/img/map.png')}}" alt="Map"
                                             class="img-fluid img-thumbnail mx-auto d-block alert-map_position">
                                    @endif
                                    {{-- <img src="{{ asset('pawmaw/img/map.png') }}" alt="Map" class="img-fluid img-thumbnail mx-auto d-block alert-map_position"> --}}
                                </div>
                            @else
                                <h2 class="user-panel_inner-content-msg mt-5 text-center">
                                    Please active your pawmaw alert to reach more people in your local area.
                                </h2>

                                <div class="alert-map">
                                    <div class="alert-map_overly"></div>
                                    <div class="alert-map_icon"><i class="fas fa-lock"></i></div>
                                    <div class="alert-map_btn">
                                        <a style="font-size:1.2rem!important;padding: 6px 7px!important" href="#">Active your pawmaw alert</a>
                                    </div>
                                    @if($pet->lat)
                                        <div id="map" style="width: 100%; height: 230px;"></div>
                                    @else
                                        <img src="{{ asset('pawmaw/img/map.png')}}" alt="Map"
                                             class="img-fluid img-thumbnail mx-auto d-block alert-map_position">
                                    @endif
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @push('js')
        <script type="text/javascript">

        </script>
    @endpush

    <!-- Include Date Picker -->
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

    <script>
        $(document).ready(function () {
            var date_input = $('input[name="lost_found_date"]');
            var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
            date_input.datepicker({
                format: 'mm/dd/yyyy',
                container: container,
                todayHighlight: true,
                autoclose: true,
                language: 'da',
                enableOnReadonly: false,
                startDate: '-1830d',
                endDate: '+0d'
            });
        });

    </script>

    <script>
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: {{ $pet->lat }}, lng: {{ $pet->lng }} },
                zoom: 15,
            });

            new google.maps.Marker({
                map: map,
                position: {lat: {{ $pet->lat }}, lng: {{ $pet->lng }} },
                animation: google.maps.Animation.DROP
            })
        }
    </script>
@endsection
