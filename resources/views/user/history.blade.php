@extends('user.layout.user')
@section('title', 'Promotion - PawMaw')

@section('content')
    @include('user.partials.sidebar')

    <div id="acc-body">
        <h1 class="entry-title"><span>My Payment History</span></h1>



        @if($user->verified==0)


            <h1>Please Verified your email</h1>

            <br>

        @endif

        {{--<div id="unpaid-list" class="order-list">--}}
            {{--<h4>Unpaid</h4>--}}
            {{--<ul>--}}
                {{--<li class="title">--}}
                    {{--<strong>Order#</strong> <em>Date/Time</em>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#">--}}
                        {{--<strong>Order#12043761537815653</strong> <em>24th September, 2018 [19:00 PM]</em>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#">--}}
                        {{--<strong>Order#12043761537614502</strong> <em>22nd September, 2018 [11:08 AM]</em>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#">--}}
                        {{--<strong>Order#12043761537610446</strong> <em>22nd September, 2018 [10:00 AM]</em>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#">--}}
                        {{--<strong>Order#12043761537609807</strong> <em>22nd September, 2018 [09:50 AM]</em>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#">--}}
                        {{--<strong>Order#12043761537597266</strong> <em>22nd September, 2018 [06:21 AM]</em>--}}
                    {{--</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</div>--}}

        <div id="paid-list" class="order-list">
            <h4>Paid</h4>
            <ul>
                <li class="title">
                    <strong>Invoice#</strong> <em>Order#</em>
                </li>



                @foreach( $payment as $key => $row)

                <li>
                    <a href="#">
                        <strong>Invoice# {{$row->invoice_id}}</strong> <em>{{$row->order_number}}</em>
                    </a>
                </li>


                @endforeach

            </ul>
        </div>

        {{--<div id="archived-list" class="order-list">--}}
            {{--<h4>Archived</h4>--}}

            {{--<ul>--}}
                {{--<li class="title">--}}
                    {{--<strong>Order#</strong> <em>Date/Time</em>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#">--}}
                        {{--<strong>Order#11063761537520173</strong> <em>21st September, 2018 [08:56 AM]</em>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#">--}}
                        {{--<strong>Order#11063761537517674</strong> <em>21st September, 2018 [08:14 AM]</em>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#">--}}
                        {{--<strong>Order#11063761537497261</strong> <em>21st September, 2018 [02:34 AM]</em>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#">--}}
                        {{--<strong>Order#11063761537495568</strong> <em>21st September, 2018 [02:06 AM]</em>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#">--}}
                        {{--<strong>Order#11063761537466428</strong> <em>20th September, 2018 [18:00 PM]</em>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#">--}}
                        {{--<strong>Order#11063761537462817</strong> <em>20th September, 2018 [17:00 PM]</em>--}}
                    {{--</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</div>--}}
        <div class="clear"></div>
    </div><!-- /.acc-body -->
@endsection
