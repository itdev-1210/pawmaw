<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('pawmaw/img/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('pawmaw/img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('pawmaw/img/favicon/android-chrome-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('pawmaw/img/favicon/favicon-16x16.png') }}">
    <link rel="mask-icon" href="{{ asset('pawmaw/img/favicon/safari-pinned-tab.svg') }}" color="#ffffff">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{ asset('pawmaw/css/genericons.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/fancybox.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/mixitup.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/jquery-ui-1.12.icon-font.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/mobile.css') }}">


	<!-- Global site tag (gtag.js) - Google Ads: 908508792 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-908508792"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'AW-908508792');
	</script>
	
	
	@yield('pageHead')

</head>

<body class="page-template page-template-page-templates page-template-my-account-page page-template-page-templatesmy-account-page-php logged-in">

@include('pawmawFront.global.page_loader')

<div id="page">

    @include('user.partials.header')

    <div class="site-content">
        <div class="warper main-acc-body">
            <div class="clear"></div>

            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            @yield('content')

            <div class="clear"></div>
        </div><!-- /.main-acc-body -->
        <div class="clear"></div>
        <div class="clear"></div>
    </div><!-- /.site-content -->

    @include('user.partials.blog_item')

    @include('user.partials.footer')

</div><!-- /#page -->

<!-- jQuery -->
{{--<script type='text/javascript' src="{{ asset('pawmaw/js/jquery-migrate.min.js') }}"></script>--}}
<script type='text/javascript' src="{{ asset('pawmaw/js/jquery.js') }}"></script>
<script type='text/javascript' src="{{ asset('pawmaw/js/fancybox.js') }}"></script>
<script type='text/javascript' src="{{ asset('pawmaw/js/mixitup.js') }}"></script>
<script type='text/javascript' src="{{ asset('pawmaw/js/viewportchecker.js') }}"></script>
<script type='text/javascript' src="{{ asset('pawmaw/js/owl.carousel.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('pawmaw/js/slider.js') }}"></script>
<script type='text/javascript' src="{{ asset('pawmaw/js/jquery-ui.js') }}"></script>
<script type='text/javascript' src="{{ asset('pawmaw/js/html5.js') }}"></script>
<script type='text/javascript' src="{{ asset('pawmaw/js/functions.js') }}"></script>

<!-- Hero Form -->


<script type='text/javascript' src='{{ asset('pawmaw/js/hero-form.js') }}'></script>

<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5bd01bb15bbbef0011bd59ab&product=inline-share-buttons' async='async'></script>

<!-- MAIN SCRIPTS -->
<script type='text/javascript' src='{{ asset('pawmaw/js/main.js') }}'></script>

<script>

    // This example adds a search box to a map, using the Google Place Autocomplete
    // feature. People can enter geographical searches. The search box will return a
    // pick list containing a mix of places and predicted search terms.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
    var map, infowindow, geocoder;

    function initAutocomplete() {
        geocoder = new google.maps.Geocoder();

        infowindow = new google.maps.InfoWindow();

        var input3 = document.getElementById('reportlostaddress');
        var input4 = document.getElementById('foundaddress');
        var searchBox3 = new google.maps.places.SearchBox(input3);
        var searchBox4 = new google.maps.places.SearchBox(input4);
        //  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.

        searchBox3.addListener('places_changed', function () {

            var places = searchBox3.getPlaces();


            if (places.length == 0) {
                return;
            }
            var myAddress = places[0]["address_components"];
            for (let i = 0; i <myAddress.length ; i++) {
                var aData = myAddress[i]['types'][0];
                if(aData=="postal_code"){
                    jQuery("#postal_2").val(myAddress[i]["long_name"]);
                    break;
                }
            }



        });

        searchBox4.addListener('places_changed', function () {

            var places = searchBox4.getPlaces();


            if (places.length == 0) {
                return;
            }
            var myAddress = places[0]["address_components"];
            for (let i = 0; i <myAddress.length ; i++) {
                var aData = myAddress[i]['types'][0];
                if(aData=="postal_code"){
                    jQuery("#postal_3").val(myAddress[i]["long_name"]);
                    break;
                }
            }



        });
    }

    function toggleBounce(marker) {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

</script>

<script type="text/javascript" async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBK0zK90-BoiWGflg9pXhBWF6DMwOzyTaU&amp;libraries=places&amp;callback=initAutocomplete"></script>

@stack('js')
</body>
</html>
