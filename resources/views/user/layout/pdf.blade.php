<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    @php

        $color = '#ff0019';

        if($petinfo->type == 'found'){
            $color = '#ffa500';
        } elseif($petinfo->type == 'reunited'){
            $color = '#0ab819';
        }

    @endphp

    <style>

        @page { margin: 0 !important; padding: 0 !important; }

        html{
            margin: 0 !important;
            padding: 0 !important;
        }

        body{
            margin: 0 !important;
            padding: 0 !important;
            background: url({{ Request::root() }}/pawmaw/img/Flyer-img/flyer-found.png);
            border: 3px solid {{ $color }};
            width: 100%;
            height: 100%;
        }

        table{
            width: 100%;
        }

        td.center{
            text-align: center;
        }

        .logo{
            margin: 10px 0 0;
            height: auto;
            max-width: 150px;
        }

        .main-title{
            font-weight: 700;
            font-size: 50px;
            color: {{ $color }};
            margin-top: 5px;
            margin-bottom: 10px;
        }

        .pet-image-wrap{
            width: 85%;
            border: 3px solid {{ $color }};
            border-radius: 7px;
            padding: 10px 0;
            margin: 0 auto
        }

        .pet-image{
            position: relative;
            margin: 0 auto;
            border-radius: 5px;
            overflow: hidden;
        }

        .pet-image .id{
            position: absolute;
            left: 0;
        }

        .pet-image .id p{
            color: red;
            font-weight: 700;
            font-size: 14px;
            background: {{ $color }};
            color: #fff;
            display: inline-block;
            margin: 0;
            text-align: left; 
            border-radius: 0 5px 0 0;
            padding: 2px 8px;
        }

        .header-title{
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            background: {{ $color }};
            height: 35px;
        }

        .header-title__image{
            width: 24px;
            height: 24px;
            border: 1px solid #fff;
            border-radius: 12px;
            float: left;
            margin-top: 5px;
            margin-left: 5px;
        }

        .header-title__image img{
            width: 18px;
            height: 18px;
            display: inline;
            margin-top: 4px;
        }

        .header-title__text p{
            margin: 0;
            white-space: nowrap;
        }

        .header-title__text{
            position: absolute;
            left: 10px;
            top: 7px;
        }

        .header-title__text p{
            color: #fff;
            text-align: left;
            font-weight: 700
        }

        .small-logo{
            position: absolute;
            right: 7px;
            top: 0;
            width: 25px;
        }

        .seen-me-text{
            font-weight: 700;
            font-size: 25px;
            margin: 10px 0;
        }

        .pet-name{
            font-weight: 700;
            font-size: 30px;
            margin: 5px 0;
        }

        .pet-name span{
            color: {{ $color }};
        }

        .pet-description{
            padding: 0 20px;
        }

        .pet-description .title p{
            font-weight: 700;
            font-size: 18px;
            color: {{ $color }};
            margin: 10px 0 5px;
        }

        .pet-description .description{
            height: 68px;
            overflow: hidden;
        }

        .pet-description .description p{
            font-weight: 700;
            font-size: 18px;
            margin: 0px;
        }

        .pet-last-seen p{
            font-weight: 700;
            font-size: 20px;
            color: {{ $color }};
            margin: 10px 0;
        }

        .pet-last-seen span{
            color: #000;
        }

        .some-details{
            margin: 0 auto 10px;
            text-align: center;
        }

        .some-details span:nth-child(3), .some-details span:first-child{
            font-size: 20px;
            color: {{ $color }};
            font-weight: 700; 
        }

        .some-details span:nth-child(2), .some-details span:nth-child(4){
            font-size: 20px;
            font-weight: 700; 
        }

        .some-details span:nth-child(2){
            margin-right: 35px;
        }
    
        .contact-title{
            margin: 35px 0 10px;
            font-size: 20px;
        }

        .some-details .gender span{
            font-size: 20px;
            color: {{ $color }};
            font-weight: 700; 
        }

        .small-logo img{
            width: 100%;
        }

        .contact-block{
            border: 3px solid {{ $color }};
            border-radius: 7px;
            padding: 7px 30px;
            margin: 0 auto;
            width: 60%;
        }

        .contact-block p{
            font-size: 40px;
            font-weight: 700;
            color: {{ $color }};
            margin: 0;
        }

        .contact-block span{
            font-size: 22px;
            font-weight: 700;
            margin: 0;
        }

        .author{
            font-size: 18px;
            font-weight: 700; 
        }

        .author span{
            color: {{ $color }};
        }

    </style>

</head>

<body>
    <div>
        <table>
            <tr>
                <td class="center">
                    @if($petinfo->type == 'found')
                        <img 
                            src="{{ asset('pawmaw/img/flyer-logo-found.png') }}" 
                            class="logo" 
                            alt="">
                    @else
                        <img 
                            src="{{ asset('pawmaw/img/Flyer-img/logo-found.png') }}" 
                            class="logo" 
                            alt="">
                    @endif
                </td>
            </tr>
            <tr>
                <td class="center">
                    <h1 class="main-title">“{{ ucfirst($petinfo->type) }} {{ $petinfo->specie }}”</h1>
                </td>
            </tr>
            <tr>
                <td class="center">
                    @php
                        $size = getimagesize(asset('uploads/pets_image') . '/' . $petinfo->photo );
                        $needHeight = 350;
                        $height = $size[1];
                        $width = $size[0] / ($size[1] / $needHeight);
                    @endphp
                    <div class="pet-image-wrap">
                        <div 
                            class="pet-image"
                            style="height: {{ $needHeight }}px; width: {{ $width }}px">
                            <img 
                                style="height: {{ $needHeight }}px; width: {{ $width }}px"
                                src="{{ asset('uploads/pets_image') }}/{{ $petinfo->photo }}">
                            <div class="header-title">
                                <div class="header-title__image">
                                    <img src="{{ asset('pawmaw/img/horn-2.png') }}" alt="">
                                </div>
                                <div class="header-title__text">

                                    @php

                                        $address = $petinfo->city
                                            ? $petinfo->city 
                                                . ', ' 
                                                . ($petinfo->country_short == 'US' ? $petinfo->state : $petinfo->country_short)
                                            : '';
                                        $addressLength = strlen($address);

                                        if($addressLength > 16){
                                            $address = substr($address, 0, 16) . '...';
                                        }

                                    @endphp

                                    <p>
                                        PawMaw{{ $address ? ', ' . $address : '' }}
                                    </p>

                                </div>
                            </div>
                            <div 
                                class="id"
                                style="top: {{ $needHeight - 19}}px">
                                <p>ID<span> #{{$petinfo->id}}</p>
                            </div>
                                <div 
                                    class="small-logo"
                                    style="top: {{ $needHeight - 34}}px">
                                    @if($petinfo->type == 'found')
                                        <img src="{{ asset('pawmaw/img/flyer-found-image-icon.png') }}">
                                    @elseif($petinfo->type == 'lost')
                                        <img src="{{ asset('pawmaw/img/fevi.png') }}">
                                    @else
                                        <img src="{{ asset('pawmaw/img/reunited-logo.png') }}" alt="Pawmaw" class="img-fluid circle-logo-50">
                                    @endif
                                </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="center">
                    <p class="seen-me-text">Have you seen me?</p>
                </td>
            </tr>
            <tr>
                <td class="center">
                    <p class="pet-name">Name: <span>{{ $petinfo->name }}</span></p>
                </td>
            </tr>
            <tr>
                <td class="center">
                    <div class="pet-description">
                        <div class="title">
                            <p>Description:</p>
                        </div>
                        <div class="description">
                            <p>{{ $petinfo->description }}</p>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="center">
                    <div class="pet-last-seen">
                        <p>Area Last Seen: <span>{{ $petinfo->last_seen }}</span></p>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="center">
                    <div class="some-details">
                        <span>{{ $petinfo->type == 'found' ? 'Found date:' : 'Lost Date:' }}</span>
                        <span>{{ date('m/d/Y', strtotime($petinfo->date)) }}</span>
                        <span>Gender:</span>
                        <span>{{ $petinfo->gender }}</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="center">
                    <p class="contact-title">If You Have Any Information Please Contact:</p>
                </td>
            </tr>
            <tr>
                <td class="center">
                    <div class="contact-block">
                        <p>{{ $petinfo->user->phone }}</p>
                        <span>{{ $petinfo->user->email }}</span>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="center">
                    <p class="author">Free Flyer Made By: <span>PawMaw.com</span></p>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
