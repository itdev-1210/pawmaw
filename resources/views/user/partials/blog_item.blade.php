<div class="warper">
    <h2 class="secondary-title">You may take these folowing action(s)</h2>
    <ul id="rel-srv">
        <li>
            <a href="#">
                <img src="{{ asset('pawmaw/img/icon/search-icon.png') }}" height="120" width="120">
                <strong>Search Lost &amp; Found Pet</strong>
                <p>If you lost your pet, you can search in our #1 largest database to find the pet. Get your pet back home sooner!</p>
                <div class="clear"></div>
            </a>
        </li>
        <li>
            <a href="#">
                <img src="{{ asset('pawmaw/img/icon/lost-icon.png') }}" height="120" width="120">
                <strong>Report Lost Pet</strong>
                <p>Post your lost pet info to PetAlertNow &amp; get back your family member.  We will send alert the local community.</p>
                <div class="clear"></div>
            </a>
        </li>
        <li>
            <a href="#">
                <img src="{{ asset('pawmaw/img/icon/found-icon.png') }}" height="120" width="120">
                <strong>Report Found Pet</strong>
                <p>Found a Pet! Report a found pet to make their way to home. And help the family to get their pet back quickly.</p>
                <div class="clear"></div>
            </a>
        </li>
    </ul>

    <div class="clear"></div>
</div><!-- User Dashboard Footer Top Blog Item -->