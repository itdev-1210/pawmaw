<div class="user-panel_inner-side">
    <div class="user-panel_inner-side_nav user-panel_home">
        <h2><i class="fas fa-poll-h"></i> My Post Panel</h2>
        <hr>
        <!-- user panel nav -->
        <ul class="user_nav">
            <li class="user_nav-item">
                <a href="{{ url('user/join_network/') }}" class="user_nav-link @if(Request::segment(2)=='join_network'){{'active'}}@endif"><i class="fas fa-network-wired"></i> <span>Join our network</span></a>
            </li>
            <li class="user_nav-item">
                <a href="{{ url('/user/my-account') }}" class="user_nav-link @if(Request::segment(2)=='my-account'){{'active'}}@endif"><i class="fas fa-users-cog"></i> <span>My posted entry</span></a>
            </li>
            <li class="user_nav-item">
                <a href="{{ url('/user/report') }}" class="user_nav-link @if(Request::segment(2)=='report'){{'active'}}@endif"><i class="fas fa-user-plus"></i> <span>Report another pet</span></a>
            </li>
            <li class="user_nav-item">
                <a href="{{ url('/user/changePassword') }}" class="user_nav-link @if(Request::segment(2)=='changePassword'){{'active'}}@endif"><i class="fas fa-unlock"></i> <span>Change password</span></a>
            </li>
            <li class="user_nav-item">
                <a  href="{{ url('/user/logout') }}"
                   onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();" class="user_nav-link"><i class="fas fa-sign-out-alt"></i> <span>Logout</span></a>

                <form id="logout-form" action="{{ url('/user/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
        <!-- user panel nav -->
    </div>
</div>