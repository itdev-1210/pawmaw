
<div class="col-lg-4 col-md-4 col-sm-12">
    <div class="user-panel_inner-side">
        <!-- user panel profile -->
        <div class="user-panel_inner-side_pet-box">
            <div class="edit-icon">
                <a href="{{ url('/user/report/edit/') }}/{{ @$pet->id }}">
                    <i class="far fa-edit"></i>
                </a>
            </div>
            <div class="user-pet_photo" style="background-image: url({{ url('uploads/pets_image') }}/{{@$pet->photo}}) ;background-position: center;background-size: cover;margin-left: 20%;">
<!--                <img src="{{ asset('uploads/pets_image') }}/{{@$pet->photo}}" alt="{{@$pet->name}}" class="img-fluid img-thumbnail">-->
            </div>
            <h2>{{@$pet->name}}</h2>            
            <div class="gender">
                <p>A {{@$pet->gender}} {{@$pet->specie}}  <span class="tag-{{@$pet->type}}">{{ucfirst(@$pet->type)}}</span></p>
            </div>
            @if(@$pet->type != 'reunited')
            <div class="marked">
                <a href="javascript:void(0);" class="tag-reunited form-delete">Mark as reunited</a>
            </div>
            @endif
        </div>

        <!-- user panel profile -->

        <div class="user-panel_inner-side_nav">
            <!-- user panel nav -->
            <ul class="user_nav">
                <li class="user_nav-item">
                    <a href="{{ url('/user/my-account') }}" class="user_nav-link @if(Request::segment(2)=='my-account'){{'active'}}@endif"><i class="fas fa-users-cog"></i> &nbsp; <span>My posted entry</span></a>
                </li>
                <li class="user_nav-item">
                    <a href="{{ url('/user/download-flyer') }}/{{@$pet->id}}" class="user_nav-link @if(Request::segment(2)=='download-flyer'){{'active'}}@endif"><i class="fas fa-folder-plus"></i> &nbsp; <span>Download flyer</span></a>
                </li>
                <li class="user_nav-item">
                    <a href="{{ url('user/pet_alert/') }}/{{ @$pet->id }}" class="user_nav-link @if(Request::segment(2)=='pet_alert'){{'active'}}@endif"><i class="fab fa-facebook"></i> &nbsp; <span>My pet alert</span></a>
                </li>
                <li class="user_nav-item">
                    <a href="{{ url('user/alerts/') }}/{{ @$pet->id }}" class="user_nav-link @if(Request::segment(2)=='alerts'){{'active'}}@endif"><i class="far fa-bell"></i> &nbsp; <span>Active alert</span></a>
                </li>
                <li class="user_nav-item">
                    <a href="{{ url('user/join_network/') }}/{{ @$pet->id }}" class="user_nav-link @if(Request::segment(2)=='join_network'){{'active'}}@endif"><i class="fas fa-network-wired"></i> &nbsp; <span>Join our network</span></a>
                </li>
                <hr>
                <li class="user_nav-item">
                    <a href="{{ url('user/pet-details/') }}/{{ @$pet->slug }}" class="user_nav-link @if(Request::segment(2)=='pet-details'){{'active'}}@endif"><i class="fas fa-file"></i> &nbsp; <span>Pet Details</span></a>
                </li>
                <li class="user_nav-item">
                    <a href="{{ url('user/update_listing/') }}/{{ @$pet->id }}" class="user_nav-link @if(Request::segment(2)=='update_listing'){{'active'}}@endif"><i class="fas fa-angle-double-up"></i> &nbsp; <span>Update Listing</span></a>
                </li>
                <li class="user_nav-item">
                    <a href="{{ url('user/faqs/') }}/{{ @$pet->id }}" class="user_nav-link @if(Request::segment(2)=='faqs'){{'active'}}@endif"><i class="fas fa-bell"></i> &nbsp; <span>FAQ</span></a>
                </li>
            </ul>
            <!-- user panel nav -->

            <div class="action-btn mt-4">
                {!! Form::model($pet, ['method' => 'delete', 'route' => ['reports_delete', @$pet->id]]) !!}

                <span class="custom-btn confirm-delete"><span class="mr-3"><i class="fas fa-trash-alt"></i></span> Remove pet</span>

                {!! Form::hidden('id', @$pet->id) !!}

                {!! Form::close() !!}
            </div>
        </div>

    </div>

    <script type="text/javascript">
        $('.form-delete').click(function(e){
            e.preventDefault();
            $form = $(this);
            swal({
                title: "Is {{ $pet->name ? '' . ucfirst($pet->name) . '' : '' }} Reunited?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, Reunited!",
                cancelButtonText: "No, Cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            });
            $('button.confirm').on('click',function(){
                location.href = '{{ url('user/report/reunited/' . @$pet->id) }}';
            });
        });
    </script>

</div><!-- user panel nav -->