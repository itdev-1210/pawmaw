<header id="site-header">
    <div class="warper">
        <div class="site-branding">
            <a href="{{ url('/') }}">
                <img src="{{ asset('pawmaw/img/logo.png') }}">
            </a>
        </div>

        <span id="navopen"><span class="genericon genericon-menu"></span></span>

        <nav id="main-navigation">
            <div id="nav-src-con"> <span id="navclose" class="genericon genericon-close-alt"></span> </div>
            <ul id="menu-menu-1" class="nav-menu">
                <li id="lostfoundlink" class="searchformlink">
                    <a href="{{ url('/') }}">
                        <span class="genericon genericon-search"></span> Lost &amp; Found
                    </a>
                </li>
                <li>
                    <a href="{{ url('/reunited') }}">
                        <span class="genericon genericon-heart"></span> reunited
                    </a>
                </li>
                <li id="my-account">
                    <a href="{{ url('/user/my-account') }}">
                        <span class="genericon genericon-user"></span> Dashboard
                    </a>
                </li>
                {{--<li id="report-pet" class="lostformlink">--}}
                    {{--<a href="{{ url('/report') }}">--}}
                        {{--<span class="genericon genericon-edit"></span> Report Another Pet--}}
                    {{--</a>--}}
                {{--</li>--}}
            </ul>
        </nav><!-- /#main-navigation-->

        <div class="clear"></div>
    </div>
</header><!-- /#site-header -->