<div id="footer-widget-area">
    <div class="warper">
        <aside id="nav_menu-2" class="widget widget_nav_menu">
            <h2 class="widget-title">
                <span>Navigation</span>
            </h2>

            <div class="menu-footer-nav-container">
                <ul id="menu-footer-nav" class="menu">
                    <li id="menu-item-1195" class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-1195">
                        <a href="{{ url('blog') }}">Blog</a>
                    </li>
                    <li id="menu-item-930" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-930">
                        <a href="{{ url('privacy-policy') }}">Privacy Policy</a>
                    </li>
                    <li id="menu-item-931" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-931">
                        <a href="{{ url('refund-policy') }}">Refund Policy</a>
                    </li>
                    <li id="menu-item-1018" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1018">
                        <a href="{{ url('contact') }}">Contact Us</a>
                    </li>
                </ul>
            </div>
        </aside><!-- /.widget_nav_menu-->

        <aside id="contact_info-2" class="widget widget_contact_info">
            <h2 class="widget-title">
                <span>Get in Touch</span>
            </h2>

            <div id="contact_info_2">
                <div class="contact_info">
                    <div class="ln">
                        <span class="genericon genericon-location"></span>
                        Los Angeles County, California, USA
                    </div>

                    <div class="ln">
                        <span class="genericon genericon-handset"></span>
                        <a href="tel:+1 909 764 0024">+1 909 764 0024</a>
                    </div>

                    <div class="ln">
                        <span class="genericon genericon-mail"></span>
                        <a href="mailto:info@pawmaw.com">info@pawmaw.com</a>
                    </div>

                    <div class="ln">
                        <span class="genericon genericon-mail"></span>
                        <a href="mailto:sales@pawmaw.com">sales@pawmaw.com</a>
                    </div>
                </div>
            </div>
        </aside><!-- /.widget_contact_info -->

        <aside id="social-3" class="widget widget_social">
            <h2 class="widget-title">
                <span>Follow Us</span>
            </h2>

            <div id="social_3">
                <div class="socials">
                    <a href="https://www.facebook.com/PawMawOfficial" target="_blank">
                        <img src="{{ asset('pawmaw/img/social/facebook.png') }}">
                    </a>
                    <a href="https://twitter.com/PawMawOfficial" target="_blank">
                        <img src="{{ asset('pawmaw/img/social/twitter.png') }}">
                    </a>
                    <a href="https://www.youtube.com/channel/UCnIlAOqCFQA6AMcWliy_hkw?view_as=subscriber" target="_blank">
                        <img src="{{ asset('pawmaw/img/social/you-tube.png') }}">
                    </a>
                    <a href="https://plus.google.com/u/4/100444258065157104492" target="_blank">
                        <img src="{{ asset('pawmaw/img/social/google_plus.png') }}">
                    </a>
                    <div class="clear"></div>
                </div>
            </div>
        </aside>
    </div>
</div><!-- /.footer-widget-area -->

<div id="site-credits">
    <div id="site-credits-in">
        <div class="warper">
            <div id="copyright" class="left">
                Copyright &copy; <script>document.write(new Date().getFullYear());</script>
                <a href="{{ url('/') }}">PawMaw</a>.
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div><!-- /.site-credits-->