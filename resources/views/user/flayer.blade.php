@extends('pawmawFront.layouts.front')

@section('title', 'Dashboard - PawMaw')

@section('content')
    <link rel="stylesheet" href="{{ asset('pawmaw/css/sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/sweetalert/sweet-alert-animations.min.css') }}">
    <!-- USER PANEL MAIN SECTION START -->
    <section class="user-panel use-panel_bg mt-5">
        <div class="user-panel_inner"
             style="background-image: url({{ asset('pawmaw/img/dashboard-bg.png')}});background-position: center; background-size: contain;">
            <div class="container">
                <div class="row">
                    <!-- SIDEBAR START -->
                @include('user.partials.sidebar_pet')
                <!-- SIDEBAR END -->

                    <!-- CONTENT START -->
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <div class="user-panel_inner-content">
                            <hr class="d-xl-none py-2">
                            <div class="download-flyer">
                                <h2 class="download-flyer_title">Download a Printable Lost & Found Flyer of
                                    {{$pet->name}}.</h2>
                                <a href="{{ url('/user/pdf-report') }}/{{ $pet->id }}" target="_blank"
                                   class="download-flyer_btn mx-auto d-block">Download Flyer</a>
                            </div>

                            @if(count($payments) > 0)
                                <h2 class="user-panel_inner-content-msg mt-5 text-center">
                                    Please Go to "My Pet Alert" and Make Sure to Comment On the Post
                                </h2>

                                <div class="alert-map">
                                    <div class="alert-map_btn btn-success">
                                        <a href="{{ url('/user/alerts/' . $pet->id) }}">Your PawMaw Alert Activated!</a>
                                    </div>
                                    @if($pet->lat)
                                        <div id="map" style="width: 100%; height: 320px;"></div>
                                    @else
                                        <img src="{{ asset('pawmaw/img/map.png')}}" alt="Map"
                                             class="img-fluid img-thumbnail mx-auto d-block alert-map_position">
                                    @endif
                                    {{-- <img src="{{ asset('pawmaw/img/map.png') }}" alt="Map" class="img-fluid img-thumbnail mx-auto d-block alert-map_position"> --}}
                                </div>
                            @else
                                <h2 class="user-panel_inner-content-msg mt-5 text-center">
                                    Please active your pawmaw alert to reach more people in your local area.
                                </h2>

                                <div class="alert-map">
                                    <div class="alert-map_overly"></div>
                                    <div class="alert-map_icon"><i class="fas fa-lock"></i></div>
                                    <div class="alert-map_btn">
                                        <a href="{{ url('/user/alerts/' . $pet->id) }}">Active your pawmaw alert</a>
                                    </div>
                                    @if($pet->lat)
                                        <div id="map" style="width: 100%; height: 320px;"></div>
                                    @else
                                        <img src="{{ asset('pawmaw/img/map.png')}}" alt="Map"
                                             class="img-fluid img-thumbnail mx-auto d-block alert-map_position">
                                    @endif
                                    {{--  <img src="{{ asset('pawmaw/img/map.png')}}" alt="Map" class="img-fluid img-thumbnail mx-auto d-block alert-map_position"> --}}
                                </div>
                            @endif
                        </div>
                    </div>
                    <!-- CONTENT END -->
                </div>
            </div>
        </div>
    </section>

    @push('js')
        <script type='text/javascript' src="{{ asset('pawmaw/js/sweetalert.min.js') }}"></script>
        <script type="text/javascript">
            jQuery('.confirm-delete').on('click', function (e) {
                e.preventDefault();
                $this = jQuery(this);
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, delete!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                });
                jQuery('button.confirm').on('click', function () {
                    $this.closest('form').submit();
                });
            });

            jQuery('.confirm-reunited').on('click', function (e) {
                e.preventDefault();
                $this = jQuery(this);
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-success",
                    confirmButtonText: "Yes, reunited!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                });
                jQuery('button.confirm').on('click', function () {
                    $this.closest('form').submit();
                });
            });
        </script>
    @endpush

    <script>
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: {{ $pet->lat }}, lng: {{ $pet->lng }} },
                zoom: 15,
            });

            new google.maps.Marker({
                map: map,
                position: {lat: {{ $pet->lat }}, lng: {{ $pet->lng }} },
                animation: google.maps.Animation.DROP
            })
        }

    </script>
@endsection
