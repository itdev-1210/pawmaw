@extends('pawmawFront.layouts.front') @section('title', 'Dashboard - PawMaw') @section('content')
<div id="loader" class="loader-container" style="display: <?php echo isset($_GET['test'])?'flex':'none'?>">
	<img width="100" height="100" src="/assets/img/loader.svg" /><br/>
@if (!$isMobile)
<b style="font-size:25px;color:white">We Are Analyzing And Processing Your Image...</b>
@endif
</div>
<!-- USER PANEL MAIN SECTION START -->
<section class="user-panel" style="background-image: url({{ asset('pawmaw/img/dashboard-bg.png')}});background-position: center; background-size: contain;">
    <div class="user-panel_sidebar">
        <div class="container">
            <div class="row">
                <!-- SIDEBAR START -->
                <div class="col-lg-4 col-md-4 col-sm-12">
                    @include('user.partials.sidebar')
                </div>
                <!-- SIDEBAR END -->

                <div class="col-lg-8 col-md-8 col-sm-12">
                    
                    <div class="user-panel_inner-content">
                        <hr class="d-xl-none py-2">
                        <div class="row">
                            <div class="col-8 offset-2 py-5">
                                <h2 class="about-title_sub pet-posted_content-title mb-5">
                                    <span class="text-uppercase">Report Another Pet</span>
                                </h2>
                            </div>
                        </div>

                    @if (count($errors) > 0)
                    <div class="alert alert-danger alert-no-border alert-txt-colored alert-close alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        <strong>Whoops!</strong> There were some problems with your input.
                        <ul>
                            @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                        <div class="panel user-another-report_form">
                            <div class="panel-heading">
                                <div class="row no-gutters">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <a href="#" class="active float-left" id="lost-report-form-link">Report Lost
                                                pet</a>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <a href="#" class="float-right" id="found-report-form-link">Report Found
                                                pet</a>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body py-5">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form id="lost-report-form" onSubmit="return validation();" role="form" enctype="multipart/form-data" action="{{ url('/user/report-store') }}" method="post" style="display: block;">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="action" value="lost">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-btn imgInp-lostbx">
                                                            <span class="btn btn-default btn-file report_upload-title">
                                                                choose photo <input type="file" name="pet_photo" id="imgInp-lost" required="">
                                                            </span>
                                                    </span>
                                                    <input type="text" class="form-control report_upload-bg report_upload-bg-lost report-another_lost-tag" readonly>
                                                    <img id='img-upload-lost' />
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <input type="text" name="name" tabindex="2" class="form-control report_another-input-ctrl report-another_lost-tag" placeholder="Pet Name" required>
                                            </div>


                                            <div class="form-group">
                                                <input type="text" name="address" id="pac-input" tabindex="1" class="form-control report_another-input-ctrl report-another_lost-tag" placeholder="Nearest last seen area" onfocus="initAutocomplete();" autocomplete="off" required>
                                            </div>
                                            <div id="fillinaddress1">
                                                <input 
                                                    type="hidden" 
                                                    name="street_number" 
                                                    class="route">
                                                <input 
                                                    type="hidden" 
                                                    name="route" 
                                                    class="route">
                                                <input 
                                                    type="hidden" 
                                                    name="sublocality_level_1" 
                                                    class="sublocality_level_1">
                                                <input 
                                                    type="hidden" 
                                                    name="locality" 
                                                    class="locality">
                                                <input 
                                                    type="hidden" 
                                                    name="administrative_area_level_3" 
                                                    class="administrative_area_level_3">
                                                <input 
                                                    type="hidden" 
                                                    name="administrative_area_level_2" 
                                                    class="administrative_area_level_2">
                                                <input 
                                                    type="hidden" 
                                                    name="administrative_area_level_1" 
                                                    class="administrative_area_level_1">
                                                <input 
                                                    type="hidden" 
                                                    name="country" 
                                                    class="country">
                                                <input 
                                                    type="hidden" 
                                                    name="country_short" 
                                                    class="country">
                                                <input 
                                                    type="hidden" 
                                                    name="postal_code" 
                                                    class="postal_code">
                                                <input 
                                                    type="hidden" 
                                                    name="lat" 
                                                    class="postal_code">
                                                <input 
                                                    type="hidden" 
                                                    name="lng" 
                                                    class="postal_code">
                                            </div>
                                            <div class="form-group">
                                                {{-- <div id="map" class="report_another-nearest_map report-another_lost-tag nearest-map">

                                                </div> --}}
                                                <div class="nearest-map" id="map" style="display: none; height: 220px;"><span>Map</span></div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-7">
                                                        <div class="form-group">
                                                            <input type="text" name="street" id="lost-cross_street" tabindex="2" class="form-control report_another-input-ctrl report-another_lost-tag" placeholder="Cross street" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-5">
                                                        <div class="form-group">
                                                            <input type="text" name="postal" id="postal_3" tabindex="3" class="form-control report_another-input-ctrl report-another_lost-tag" placeholder="Zip" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="report_another-next-btn float-right text-uppercase">Next</button>
                                            </div>
                                        </form>
                                        <form id="found-report-form" onSubmit="return validation1();" role="form" enctype="multipart/form-data" action="{{ url('/user/report-store') }}" method="post" style="display: none;">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="action" value="found">


                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-btn imgInp-lostbx1">
                                                            <span class="btn btn-default btn-file report_upload-title" style="background-color:#ffc000;">
                                                                choose photo <input type="file" name="pet_photo" id="imgInp-found" required="">
                                                            </span>
                                                    </span>
                                                    <input type="text" class="form-control report_upload-bg report_upload-bg-found report-another_found-tag" readonly>
                                                    <img id='img-upload-found' />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <input type="text" name="name" tabindex="2" class="form-control report_another-input-ctrl report-another_found-tag" placeholder="Pet Name (If Known)">
                                            </div>

                                            <div class="form-group">
                                                <input type="text" name="address" id="pac-input2" tabindex="1" class="form-control report_another-input-ctrl report-another_found-tag" placeholder="Nearest last seen area" onfocus="initAutocomplete();" autocomplete="off" required>
                                            </div>
                                            <div id="fillinaddress1">
                                                <input 
                                                    type="hidden" 
                                                    name="street_number" 
                                                    class="route found-pet">
                                                <input 
                                                    type="hidden" 
                                                    name="route" 
                                                    class="route found-pet">
                                                <input 
                                                    type="hidden" 
                                                    name="sublocality_level_1" 
                                                    class="sublocality_level_1 found-pet">
                                                <input 
                                                    type="hidden" 
                                                    name="locality" 
                                                    class="locality found-pet">
                                                <input 
                                                    type="hidden" 
                                                    name="administrative_area_level_3" 
                                                    class="administrative_area_level_3 found-pet">
                                                <input 
                                                    type="hidden" 
                                                    name="administrative_area_level_2" 
                                                    class="administrative_area_level_2 found-pet">
                                                <input 
                                                    type="hidden" 
                                                    name="administrative_area_level_1" 
                                                    class="administrative_area_level_1 found-pet">
                                                <input 
                                                    type="hidden" 
                                                    name="country" 
                                                    class="country found-pet">
                                                <input 
                                                    type="hidden" 
                                                    name="country_short" 
                                                    class="country found-pet">
                                                <input 
                                                    type="hidden" 
                                                    name="postal_code" 
                                                    class="postal_code found-pet">
                                                <input 
                                                    type="hidden" 
                                                    name="lat" 
                                                    class="postal_code found-pet">
                                                <input 
                                                    type="hidden" 
                                                    name="lng" 
                                                    class="postal_code found-pet">
                                            </div>
                                            <div class="form-group">
                                                {{-- <div id="map99" class="report_another-nearest_map report-another_found-tag">

                                                </div> --}}
                                                <div class="nearest-map report-pet-map" id="map2" style="height: 220px"></div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-7">
                                                        <div class="form-group">
                                                           <input type="text" name="street" id="cross_street" tabindex="2" class="form-control input-ctrl found-pet found_cross_street report-another_found-tag report_another-input-ctrl" placeholder="Cross street" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-5">
                                                        <div class="form-group">
                                                            <input type="text" name="postal" id="zip" tabindex="3" class="form-control input-ctrl found-pet zip-found report-another_found-tag report_another-input-ctrl" placeholder="Zip" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <button type="submit" class="report_another-next-btn-found float-right text-uppercase">Next</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@push('js')
<script type="text/javascript">

 function validation() {
    var file_size = $('#imgInp-lost')[0].files[0].size;
    if (file_size > 5242880) {
        $('.error-image').remove();
        $("<span class='error-image' style='margin-top: -19px;z-index: 9999;bottom:-14px;'>File size is greater than 5MB</span>").insertAfter($(".imgInp-lostbx"));
        return false;
    }
    $('.error-image').remove();
    $('body').css('opacity', 0.4);
    var $loader = jQuery("div#loader");	
    /*
	@if (!$isMobile)
	$loader.css("background-color", '#df1d41');
	@endif
	*/
	$loader.show();
	setTimeout(function(){
		$('body').css('opacity', '1');
		$loader.hide();
	}, 5000);
    return true;
}
function validation1() {
   
    var file_size = $('#imgInp-found')[0].files[0].size;
    if (file_size > 5242880) {
        $('.error-image').remove();
        $("<span class='error-image' style='margin-top: -19px;z-index: 9999;bottom:-14px;'>File size is greater than 5MB</span>").insertAfter($(".imgInp-lostbx1"));
        return false;
    }
    $('.error-image').remove();
    var $loader = jQuery("div#loader");	

	$loader.show();
    return true;
}   

</script>
@endpush @endsection
