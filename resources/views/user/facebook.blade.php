<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
</head>
<body>
	<script>
		// initialize and setup facebook js sdk
		window.fbAsyncInit = function() {
		    FB.init({
		      appId      : '404710543602196',
		      xfbml      : true,
		      version    : 'v3.2'
		    });
		    FB.getLoginStatus(function(response) {
		    	if (response.status === 'connected') {
		    		document.getElementById('status').innerHTML = 'We are connected.';
		    		document.getElementById('login').style.visibility = 'hidden';
		    	} else if (response.status === 'not_authorized') {
		    		document.getElementById('status').innerHTML = 'We are not logged in.'
		    	} else {
		    		document.getElementById('status').innerHTML = 'You are not logged into Facebook.';
		    	}
		    });
		};
		(function(d, s, id){
		    var js, fjs = d.getElementsByTagName(s)[0];
		    if (d.getElementById(id)) {return;}
		    js = d.createElement(s); js.id = id;
		    js.src = "//connect.facebook.net/en_US/sdk.js";
		    fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		
		function login() {
			FB.login(function(response) {
				if (response.status === 'connected') {
		    		document.getElementById('status').innerHTML = 'We are connected.';
		    		document.getElementById('login').style.visibility = 'hidden';
		    	} else if (response.status === 'not_authorized') {
		    		document.getElementById('status').innerHTML = 'We are not logged in.'
		    	} else {
		    		document.getElementById('status').innerHTML = 'You are not logged into Facebook.';
		    	}
			});
		}
		// getting basic user info
		function getInfo() {
			FB.api('/me', 'GET', {fields: 'first_name,last_name,name,id'}, function(response) {
				document.getElementById('status').innerHTML = response.id;
			});
		}
		// posting on user timeline
		/* function postonwall() {
	  var access_token1 = FB.getAuthResponse()['accessToken'];
	  var msg = 'Hello World';
	  var page_id = 'xxxxxxxxxxxxxxx';
	  FB.api('/' + page_id + '/feed', 'post', { message: msg, access_token: access_token1 }, function (response) {
	  if (!response || response.error) {
	  console.log("Error in Feed posting." + response.error.message);
	  } else {
	  console.log("Feed posted successfully.");
	  }
	  });

	  }  */
		function post() {
			//alert(FB.getAuthResponse()['accessToken']);
			  //var access_token = FB.getAuthResponse()['accessToken'];
			 FB.api('/me/feed', 'post', {message: 'my first status...',access_token: 'EAAgcvhI1iSEBAFZCX6SHeWZCWqpYhhWTY8r4AuZAEqp94n4aG2IndfqALcytWj4vl86aZAGtX14ZBKsosavrATX6DZCERvJObGaQbNiMvwE7zQKfa4ZBGyRhf7hWw46GHzwOqDLm1l87OqpnZBAZA8Y0ljXILqV93jMvR6g7ljW9hVFoGYGFiuY3obAWZCZAsZAvlZBAfU2XUQgEOYQZDZD'}, function(response) {
				document.getElementById('status').innerHTML = response;
			}); 
		}
	</script>

	<div id="status"></div>
	<button onclick="getInfo()">Get Info</button>
	<button onclick="post()">Post</button>
	<button onclick="login()" id="login">Login</button>
</body>
</html>