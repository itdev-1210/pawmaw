
    <style type="text/css">
        html {position: relative; min-height: 100%; font-size: 62.5%;}
        body{ margin: 0; padding: 0; min-width: 100%; width: 100% !important; height: 100% !important; box-sizing: border-box;}
        h1,h2,h3, p, a {margin: 0; padding: 0;}

        .pdf-wrapper {
            width: 600px;
            height: 800px;
            border: 4px solid #c71a16;
            margin: 20px auto;
            padding: 5px 3px;
            box-sizing: border-box;
        }
        .pdf-inner-wrapper {
            width: 566px;
            height: 780px;
            padding: 10px 15px;
            box-sizing: border-box;
            border: 1px solid #ccc;
            margin-left: auto;
            margin-right: auto;
        }
        .pdf-title {
            font-size: 48px;
            font-weight: 400;
            text-align: center;
            color: #000000;
        }
        .pdf-pet_title {
            font-size: 58px;
            font-weight: 700;
            text-align: center;
            text-transform: uppercase;
            color: #000000;
        }
        .pdf-find-date {
            font-size: 28px;
            font-weight: 700;
            text-align: center;
            text-transform: capitalize;
            font-style: italic;
            color: #000;
            margin-bottom: 20px;
        }

        .pdf-details span {
            font-size: 16px;
            font-weight: 400;
            color: #010101;
        }

        .pdf-details b {
            font-size: 18px !important;
            font-weight: 700;
            color: #010101;
            cursor: pointer;
        }

        .sex-align {
            text-align: right;
            margin-left: 150px;
        }

        .pdf-info {
            font-size: 18px;
            font-weight: 400;
            color: #010101;
            text-align: center;
            text-decoration: underline;
        }

        .pdf-contact_num {
            font-size: 28px;
            line-height: 1;
            font-weight: 700;
            color: #010101;
            text-transform: uppercase;
            text-align: center;
        }

        .pdf-email {
            font-size: 18px;
            font-weight: 700;
            color: #010101;
            text-transform: none !important;
            text-align: center;
        }

        .pdf-pet_id {
            text-align: right;
            font-size: 14px;
            font-weight: 700;
            line-height: 1;
        }
        .pdf-pet_id span {
            margin-right: 5px;
        }
    </style>

<div class="pdf-wrapper">
    <div class="pdf-inner-wrapper">
        <h1 class="pdf-title">www.PawMaw.com</h1>
        <h2 class="pdf-pet_title">{{ucfirst($petinfo->type)}} {{$petinfo->specie}}</h2>
        <p class="pdf-find-date">"{{$petinfo->name}}" {{ucfirst($petinfo->type)}} {{date('d/m/Y', strtotime($petinfo->date))}}</p>
        <div>
            <img width="566" height="366" src="{{ asset('uploads/pets_image') }}/{{$petinfo->photo}}" alt="{{$petinfo->specie}}">
        </div>
        <div class="pdf-details">
            <p>
                <b>Breed: </b><span>{{$petinfo->breed}}</span>
                <span class="sex-align">
                    <b>Sex: </b><span>{{$petinfo->gender}}</span>
                </span>
            </p>
            <p><b>Color: </b><span>{{$petinfo->color}}</span></p>
            <p>
                <b>Details: </b>
                <span>{{$petinfo->description}}</span>
            </p>
            <p><b>Last Seen: </b><span>{{$petinfo->last_seen}}</span></p>
        </div>
        <br>
        <br>
        <div>
            <p class="pdf-info">If you have any Information please contact:</p>
       <br/>     <p class="pdf-contact_num">
                <b>{{$petinfo->user->phone}}</b><br>
                <a href="mailto:{{$petinfo->user->email}}" class="pdf-email"><b>{{$petinfo->user->email}}</b></a>
            </p>
            <p class="pdf-pet_id">
                Pet Id:
                <br>
                <span># {{$petinfo->id}}</span>
            </p>
        </div>
    </div>
</div>
