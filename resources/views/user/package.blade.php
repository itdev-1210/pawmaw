@extends('pawmawFront.layouts.front')

@section('title', 'Edit Profile - PawMaw')

@section('content')
    <style type="text/css">

    </style>
    <!-- PACKAGE WRAPPER SECTION START -->
    <section class="package">
        <!-- PACKAGE TITLE START -->
        <div class="package-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1 class="package-header_title">Choose your paw alert</h1>
                        <h2 class="package-header_sub">OVER 50,000 PETS FOUND & COUNTING</h2>
                    </div>
                </div>
            </div>
        </div>
        <!-- PACKAGE TITLE END -->

        <!-- PACKAGE HEADER IMAGE START -->
        <div class="package-img">&nbsp;</div>
        <!-- PACKAGE HEADER IMAGE END -->

        <!-- PACKAGE WRAP START -->
        <div class="package-wrap">
            <div class="container">
                <div class="package-wrap_skip-sm d-none">
                    <a href="{{ url('/user/my-account') }}" class="package-wrap_skip-link">Skip and go to Dashboard</a>
                </div>
                <div class="row package-wrap_top">
                    <div class="col-lg-4 col-md-4 col-sm-12 mb-sm-5 mb-xs-5">
                        <div class="package-title_img py-5">
                            <img src="{{ asset('pawmaw/img/package/basic.png') }}" alt="Basic"
                                 class="img-fluid mx-auto d-block">
                        </div>

                        <div class="card package-wrap_box">
                            <div class="package-wrap_box-head">
                                <i class="fas fa-circle"></i>
                                <h2 class="package-wrap_box-head_title">Basic</h2>
                                <div class="package-wrap_box-head_price basic">
                                    <span><i class="fas fa-dollar-sign"></i> 24.</span><span>99</span>
                                </div>
                            </div>
                            <div class="package-wrap_box-body">
                                <ul>
                                    <li>
                                        <i class="fas fa-paper-plane"></i>
                                        <strong>Send 1,000 Paw Alert Nearest Pet Lover</strong> in your area within 10
                                        miles
                                    </li>
                                    <li>
                                        <i class="fas fa-calendar-check"></i> 3 Days Campaign
                                    </li>
                                    <li>
                                        <i class="fas fa-file"></i> Custom Poster Design
                                    </li>
                                    <li>
                                        <i class="fas fa-bookmark"></i> Feature Post On our website
                                    </li>
                                    <li>
                                        <i class="fas fa-bell"></i> Real Time Notifications
                                    </li>
                                    <li>
                                        <i class="fas fa-clock"></i> 24/7 Customer Support
                                    </li>
                                </ul>
                            </div>
                            <div class="package-wrap_box-footer">
                                <button type="button" class="pb-btn" data-package-code="package-basic"><i
                                            class="fas fa-angle-double-right"></i> <span>Place Order</span>
                                    <i class="fas fa-angle-double-right"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 mb-sm-5 mb-xs-5">
                        <div class="package-title_img py-5">
                            <img src="{{ asset('pawmaw/img/package/standard.png') }}" alt="Basic"
                                 class="img-fluid mx-auto d-block">
                        </div>

                        <div class="card package-wrap_box">
                            <div class="package-wrap_box-head">
                                <i class="fas fa-circle"></i>
                                <h2 class="package-wrap_box-head_title">Standard</h2>
                                <div class="package-wrap_box-head_price standard">
                                    <span><i class="fas fa-dollar-sign"></i> 49.</span><span>99</span>
                                </div>
                                <div class="ribbon ribbon-top-right"><span>Most Popular</span></div>
                            </div>
                            <div class="package-wrap_box-body">
                                <ul>
                                    <li>
                                        <i class="fas fa-paper-plane"></i>
                                        <strong>Send 5,000 Paw Alert Nearest Pet Lover</strong> in your area within 10
                                        miles
                                    </li>
                                    <li>
                                        <i class="fas fa-calendar-check"></i> 7 Days Campaign
                                    </li>
                                    <li>
                                        <i class="fas fa-file"></i> Custom Poster Design
                                    </li>
                                    <li>
                                        <i class="fas fa-bookmark"></i> Feature Post On our website
                                    </li>
                                    <li>
                                        <i class="fas fa-bell"></i> Real Time Notifications
                                    </li>
                                    <li>
                                        <i class="fas fa-clock"></i> 24/7 Customer Support
                                    </li>
                                </ul>
                            </div>
                            <div class="package-wrap_box-footer">
                                <button type="button" class="ps-btn" data-package-code="package-standard"><i
                                            class="fas fa-angle-double-right"></i> <span>Place Order</span>
                                    <i class="fas fa-angle-double-right"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 mb-sm-5 mb-xs-5">
                        <div class="package-title_img py-5">
                            <img src="{{ asset('pawmaw/img/package/premium.png') }}" alt="Basic"
                                 class="img-fluid mx-auto d-block">
                        </div>

                        <div class="card package-wrap_box">
                            <div class="package-wrap_box-head">
                                <i class="fas fa-circle"></i>
                                <h2 class="package-wrap_box-head_title">Premium</h2>
                                <div class="package-wrap_box-head_price premium">
                                    <span><i class="fas fa-dollar-sign"></i> 89.</span><span>99</span>
                                </div>
                            </div>
                            <div class="package-wrap_box-body">
                                <ul>
                                    <li>
                                        <i class="fas fa-paper-plane"></i>
                                        <strong>Send 10,000 Paw Alert Nearest Pet Lover</strong> in your area within 10
                                        miles
                                    </li>
                                    <li>
                                        <i class="fas fa-calendar-check"></i> 10 Days Campaign
                                    </li>
                                    <li>
                                        <i class="fas fa-file"></i> Custom Poster Design
                                    </li>
                                    <li>
                                        <i class="fas fa-bookmark"></i> Feature Post On our website
                                    </li>
                                    <li>
                                        <i class="fas fa-bell"></i> Real Time Notifications
                                    </li>
                                    <li>
                                        <i class="fas fa-clock"></i> 24/7 Customer Support
                                    </li>
                                </ul>
                            </div>
                            <div class="package-wrap_box-footer">
                                <button type="button" class="pp-btn" data-package-code="package-premium"><i
                                            class="fas fa-angle-double-right"></i> <span>Place Order</span>
                                    <i class="fas fa-angle-double-right"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="package-wrap_skip">
                    <a href="{{ url('/user/my-account') }}" class="package-wrap_skip-link package-wrap_skip_fix"
                       style=" font-size:16px;padding:8px 24px;position:fixed;bottom:45px;right:95px; box-shadow:0px 0px 10px rgba(0,0,0,0.6); z-index:999;">Skip
                        and go to Dashboard</a>
                </div>

            </div>
        </div>
        <!-- PACKAGE WRAP END -->
    </section>
    <!-- PACKAGE WRAPPER SECTION END -->
    <script>
        (function (document, src, libName, config) {
            var script = document.createElement('script');
            script.src = src;
            script.async = true;
            var firstScriptElement = document.getElementsByTagName('script')[0];
            script.onload = function () {
                for (var namespace in config) {
                    if (config.hasOwnProperty(namespace)) {
                        window[libName].setup.setConfig(namespace, config[namespace]);
                    }
                }
                window[libName].register();
            };

            firstScriptElement.parentNode.insertBefore(script, firstScriptElement);
        })(document, 'https://secure.avangate.com/checkout/client/twoCoInlineCart.js', 'TwoCoInlineCart', {
            "app": {"merchant": "{{env('TWOCHECKOUT_MERCHANT')}}"},
            "cart": {"host": "https:\/\/secure.2checkout.com"}
        });
    </script>
    <script type="text/javascript">
		<?php
		$ref = [
			'user_id' => $user->id,
			'pet_id'  => $pet->id
		];
		?>
        $(document).ready(function () {
            //$('#lost_date').datepicker();

            $('.package-wrap_box button').on('click', function (e) {
                e.preventDefault();

                var package_code = $(this).data('package-code');

                // TwoCoInlineCart.billing.setCountry('US'); // customer billing country
                // TwoCoInlineCart.billing.setCity('Townsville'); // customer billing country
                // TwoCoInlineCart.billing.setState('Ohio'); // customer billing country
                // TwoCoInlineCart.billing.setZip('43206'); // customer billing country
                // TwoCoInlineCart.billing.setName('John s'); // customer billing Name
                TwoCoInlineCart.billing.setEmail('{{$user->email}}');

                TwoCoInlineCart.products.removeAll();
                TwoCoInlineCart.products.add({
                    code: package_code,
                    quantity: 1
                });

                TwoCoInlineCart.cart.setTest({{env('TWOCHECKOUT_SANDBOX')}});
                TwoCoInlineCart.cart.setExternalCustomerReference('<?= json_encode( $ref ) ?>');
                TwoCoInlineCart.cart.setCurrency('{{env('TWOCHECKOUT_CURRENCY')}}');
                TwoCoInlineCart.cart.checkout();
            });
        });
    </script>
@endsection
