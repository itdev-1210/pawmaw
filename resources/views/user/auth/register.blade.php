@extends('pawmawFront.layouts.front')

@section('title', 'Register - PawMaw')

@section('content')
<script src="https://forms.thechecker.co/5caf297e0bd2b4473742746c.js"></script>
<section class="login-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 col-md-8 offset-md-2 col-sm-12">
                <div class="login-box mx-auto d-block">
                    <div class="panel panel-login login-box_content">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <a href="{{ url('/user/login') }}" class="float-left login-box_content-title_in">Login</a>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <a href="{{ url('/user/register') }}" class="active float-right login-box_content-title_up">Join Us</a>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body py-5">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger alert-no-border alert-txt-colored alert-close alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                <strong>Whoops!</strong> There were some problems with your input.
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            @if(isset($_GET['invalidemail']))
                            <div class="alert alert-danger alert-no-border alert-txt-colored alert-close alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                <strong>Whoops!</strong> There were some problems with your input.
                                <ul>
                                    
                                    <li>Please Enter valid email address.</li>
                                    
                                </ul>
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-lg-12">
                              
                                    <form id="signup-form" role="form" method="POST" action="{{ url('/user/register') }}" >
                                        {{ csrf_field() }}
                                        <input type="hidden" name="request" value="0">
                                        <div class="form-group">
                                            <span class="log-icon"><i class="fas fa-user"></i></span>
                                            <input type="text" name="name" tabindex="1" class="form-control" placeholder="Name*" required>
                                        </div>
                                        <div class="form-group">
                                            <span class="log-icon"><i class="fas fa-envelope"></i></span>
                                            <input type="email" name="email" tabindex="2" class="form-control" placeholder="Email*" required>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <span class="log-icon"><i class="fas fa-home"></i></span>
                                                    <input type="text" name="postal" tabindex="3" class="form-control" placeholder="Zip Code*" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="form-group">
                                                    <span class="log-icon"><i class="fas fa-phone"></i></span>
                                                    <input type="text" name="phone" tabindex="4" class="form-control" placeholder="Phone*" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <span class="log-icon"><i class="fas fa-lock"></i></span>
                                            <input type="password" name="password" tabindex="5" class="form-control" placeholder="Password*" required>
                                        </div>
                                        <div class="form-group">
                                            <span class="log-icon"><i class="fas fa-lock"></i></span>
                                            <input type="password" name="password_confirmation" tabindex="6" class="form-control" placeholder="Confirm Password*">
                                        </div>
                                        <div class="form-group">
                                            <button 
												type="button"
												class="join_btn signup-btn g-recaptcha"
												data-sitekey="{{env('INVISIBLE_RECAPTCHA_SITEKEY')}}"
												data-callback="onSubmit"
												data-badge="inline"
											>
												Get Started
											</button>
                                        </div>
                                        <div class="form-group mt-4 mx-auto d-block">
                                            <div class="form-check form-check-inline mx-auto d-block">
                                                <label class="custom-control overflow-checkbox">
                                                    <input type="checkbox" class="overflow-control-input form-check-input" checked>
                                                    <span class="overflow-control-indicator"></span>
                                                    <p class="overflow-control-description signupCheck"><a href="{{url('/terms')}}"  style="color:#b1b1b1;" target="_blank">I agree all statement in <span>Terms of Our Service.</span></a></p>
                                                </label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- LOGIN HERO SECTION START -->

<!-- GET YOUR PET BACK HOME SECTION START -->
<section class="getyourpet mt-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="getyourpet-title wow swing" data-wow-duration="1s" data-wow-delay=".3s">Get your pet back home</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <a href="{{ url('/?active-form=1') }}" class="getyourpet-btn mx-auto d-block wow bounceIn" data-wow-duration="1s" data-wow-delay=".3s">Report lost pet</a>
            </div>
        </div>
    </div>
</section>
<!-- GET YOUR PET BACK HOME SECTION END -->
</div>
    <!-- Catpcha Modal 
        <div class="modal fade" id="captchaModal" tabindex="-1" role="dialog" aria-labelledby="captchaModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="padding:2rem; border-radius:10px;">
                    <div class="modal-header">
                        <h5 class="modal-title email-finder-title" id="captchaLabel">Join Us</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body captcha-body">
						<div class="form-group">
                        	<div class="g-recaptcha" data-sitekey="6LdjAsgUAAAAAI21gKWK3Lkm2AtSmO3vYjXs5zbS">
							</div>
						<span id="captchaMsg" style="color:red"></span>
						<input type="hidden" value=1 id="captchaType"/>
							
						</div>

                        <button id="captchaButton" class="email-finder-btn">Submit</button>
                    </div>
                </div>
            </div>
        </div>
	-->
<script>
function onSubmit(token) {
	$("#signup-form").submit();
}
/*
 $(".join_btn").click(function(e){
    //$("#signup-form").preventDefault();
    $('#captchaModal').modal('show'); 
});



 $("#captchaButton").click(function() {
	var v = grecaptcha.getResponse();
	if(v.length == 0)
    {
        document.getElementById('captchaMsg').innerHTML="You can't leave Captcha Code empty";
        return false;
    }
    else
    {
        $("#signup-form").submit();
    }

});
*/
</script>

@endsection
