@extends('pawmawFront.layouts.front')

@section('title', 'Login - PawMaw')

@section('content')
<section class="login-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 col-md-8 offset-md-2 col-sm-12">
                <div class="login-box mx-auto d-block">
                    <div class="panel panel-login login-box_content">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <a href="{{ url('/user/login') }}" class="active float-left login-box_content-title_in">Login</a>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <a href="{{ url('/user/register') }}" class="float-right login-box_content-title_up">Join Us</a>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body py-5">
                            <div class="row">
                                <div class="col-lg-12">
                                    @if (isset($_GET['reset']))
                                        <div class="alert alert-success">
                                            Password has been reset login now.
                                        </div>
                                    @endif
                                    <form id="login-form" role="form" method="POST" action="{{ url('/user/login') }}" style="display: block;">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <span class="log-icon"><i class="fas fa-envelope"></i></span>
                                            <input type="email" name="email" tabindex="1" class="form-control" placeholder="Email">
                                            @if(isset($_GET['redirecturl']))
                                              <input type="hidden" name="redirectulr" value="{{$_GET['redirecturl']}}">
                                            @endif
                                            
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <span class="log-icon"><i class="fas fa-lock"></i></span>
                                            <input type="password" name="password" tabindex="2" class="form-control" placeholder="password">
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="message">
                                            @if(@session('message'))
                                                <p id="incorrect-message" style="color: red; font-size: 16px; margin-bottom: 20px;">{{ session('message') }}</p>
                                            
                                                <script>
                                                    setTimeout(function(){
                                                        $('#incorrect-message').fadeOut();
                                                    }, 5000);
                                                </script>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <button class="login-btn">Login</button>
                                        </div>
                                        <div class="form-group mt-4">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-check form-check-inline">
                                                        <label class="custom-control overflow-checkbox">
                                                            <input type="checkbox" class="overflow-control-input form-check-input">
                                                            <span class="overflow-control-indicator"></span>
                                                            <span class="overflow-control-description loginCheck">Remember Me</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <a href="{{ url('/user/password/reset') }}" class="login-forgot">Forgot Password?</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- LOGIN HERO SECTION START -->

<!-- GET YOUR PET BACK HOME SECTION START -->
<section class="getyourpet mt-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="getyourpet-title wow swing" data-wow-duration="1s" data-wow-delay=".3s">Get your pet back home</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <a href="{{ url('/?active-form=1') }}" class="getyourpet-btn mx-auto d-block wow bounceIn" data-wow-duration="1s" data-wow-delay=".3s">Report lost pet</a>
            </div>
        </div>
    </div>
</section>
<!-- GET YOUR PET BACK HOME SECTION END -->
</div>
@endsection
