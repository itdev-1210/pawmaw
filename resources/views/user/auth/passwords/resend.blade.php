@extends('pawmawFront.layouts.front')

@section('title', 'Email - PawMaw')

<!-- Main Content -->
@section('content')
    <div class="site-content pass-reset-email-page" style="background: #fff;">
        <div class="warper">
            <h1 class="entry-title">
                <span>Resend Activation Code</span>
            </h1>
            <div class="clear"></div>

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form id="join-form" role="form" method="POST" action="{{ route('resendactivationPost') }}">
                {{ csrf_field() }}
                <div class="clear"></div>

                <div class="row{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">E-Mail Address <span class="required">*</span></label>
                    <input id="email" type="email" name="email" value="{{ old('email') }}" placeholder="E-mail"  required="">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="clear"></div>
                <button type="submit" class="btn">Resend Activation Email</button>
                <div class="clear"></div>
            </form>
        </div>
    </div>
@endsection
