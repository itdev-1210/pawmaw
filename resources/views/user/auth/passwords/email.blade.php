@extends('pawmawFront.layouts.front')

@section('title', 'Email - PawMaw')

<!-- Main Content -->
@section('content')
    <section class="login-bg h-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-sm-12">
                    <div class="reset-login-box mx-auto d-block">
                        <div class="reset-login-box-content pass-reset-email-page">
                            <div class="warper">
                                <h2 class="reset-login-title">Reset Your Password</h2>
                                <p class="reset-login-tag-line">You can reset your password by providing your email address</p>


                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <form class="reset-login-form" role="form" method="POST" action="{{ url('/user/password/email') }}">
                                    {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <span class="log-icon"><i class="fas fa-envelope"></i></span>
                                        <input id="email" class="form-control" type="email" name="email" value="{{ old('email') }}" placeholder="Email"  required="">

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    {{--<div class="row{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                                        {{--<label for="email">E-Mail Address <span class="required">*</span></label>--}}
                                        {{--<input id="email" type="email" name="email" value="{{ old('email') }}" placeholder="E-mail"  required="">--}}
                                        {{--@if ($errors->has('email'))--}}
                                            {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('email') }}</strong>--}}
                            {{--</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}

                                    <button type="submit" class="reset-login-btn text-center mx-auto d-block">Submit</button>
                                    <a href="{{ url('/user/login') }}" class="reset-return-login">Return To Login</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- GET YOUR PET BACK HOME SECTION START -->
    <section class="getyourpet mt-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="getyourpet-title wow swing" data-wow-duration="1s" data-wow-delay=".3s">Get your pet back home</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <a href="{{ url('/?active-form=1') }}" class="getyourpet-btn mx-auto d-block wow bounceIn" data-wow-duration="1s" data-wow-delay=".3s">Report lost pet</a>
                </div>
            </div>
        </div>
    </section>
    <!-- GET YOUR PET BACK HOME SECTION END -->
@endsection
