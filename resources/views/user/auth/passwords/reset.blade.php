@extends('pawmawFront.layouts.front')

@section('title', 'Reset - PawMaw')

@section('content')
    <section class="login-bg h-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-sm-12">
                    <div class="reset-login-box mx-auto d-block">
                        <div class="reset-login-box-content pass-reset-pass-page">
                            <div class="warper">
                                <span class="reset-log-title-icon text-center"><i class="fas fa-lock"></i></span>
                                <h2 class="reset-login-title">Reset Your Password</h2>

                                <form class="reset-login-form mt-4" method="POST" action="{{ url('/user/password/reset') }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="token" value="{{ $token }}">

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <span class="log-icon"><i class="fas fa-envelope"></i></span>
                                        <input type="email" id="email" class="form-control" name="email" value="{{ old('fname') }}"
                                               value="{{ $email or old('email') }}" autofocus placeholder="Email"  required="">

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>

                                    {{--<div class="row{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                                        {{--<label for="email">E-Mail Address</label>--}}
                                        {{--<input type="text" id="email" name="email" value="{{ old('fname') }}"--}}
                                               {{--value="{{ $email or old('email') }}" autofocus placeholder="E-mail"  required="">--}}
                                        {{--@if ($errors->has('email'))--}}
                                            {{--<span class="help-block">--}}
                                            {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                        {{--</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <span class="log-icon"><i class="fas fa-lock"></i></span>
                                        <input type="password" class="form-control" id="password" name="password" autofocus placeholder="New Password">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    {{--<div class="row{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                                        {{--<label for="password">Password</label>--}}
                                        {{--<input type="password" id="password" name="password" autofocus>--}}
                                        {{--@if ($errors->has('password'))--}}
                                            {{--<span class="help-block">--}}
                                                {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                            {{--</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}

                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <span class="log-icon"><i class="fas fa-lock"></i></span>
                                        <input type="password" class="form-control" id="password-confirm"  name="password_confirmation" placeholder="Confirm Password" autofocus>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                        @endif
                                    </div>

                                    {{--<div class="row{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">--}}
                                        {{--<label for="password-confirm">Confirm Password</label>--}}
                                        {{--<input type="password" id="password-confirm"  name="password_confirmation" autofocus>--}}
                                        {{--@if ($errors->has('password_confirmation'))--}}
                                            {{--<span class="help-block">--}}
                                            {{--<strong>{{ $errors->first('password_confirmation') }}</strong>--}}
                                        {{--</span>--}}
                                        {{--@endif--}}
                                    {{--</div>--}}

                                    <button type="submit" class="reset-login-btn text-center mx-auto d-block text-capitalize">Reset Password</button>
                                    <a href="{{ url('/user/login') }}" class="reset-return-login">Return To Login</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- GET YOUR PET BACK HOME SECTION START -->
    <section class="getyourpet mt-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="getyourpet-title wow swing" data-wow-duration="1s" data-wow-delay=".3s">Get your pet back home</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <a href="{{ url('/?active-form=1') }}" class="getyourpet-btn mx-auto d-block wow bounceIn" data-wow-duration="1s" data-wow-delay=".3s">Report lost pet</a>
                </div>
            </div>
        </div>
    </section>
    <!-- GET YOUR PET BACK HOME SECTION END -->
@endsection
