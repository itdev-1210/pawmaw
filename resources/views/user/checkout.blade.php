@extends('user.layout.user')
@section('pageHead')
	<script>
	  gtag('event', 'conversion', {
		  'send_to': 'AW-908508792/eMNeCPGr6LcBEPj8mrED',
		  'transaction_id': ''
	  });
	</script>

@endsection
@section('title', 'My-Account - PawMaw')

@section('content')
    @include('user.partials.sidebar')

    <div id="acc-body">
        <h1 class="entry-title">
            <span>Checkout</span>
        </h1>

        <form action='https://sandbox.2checkout.com/checkout/purchase' method='post'>
            <input type='hidden' name='sid' value='901396350' />
            <input type='hidden' name='mode' value='2CO' />
            <input type='hidden' name='li_0_type' value='product' >
            <input type='hidden' name='li_0_name' value='Example Product Name' >
            <input type='hidden' name='li_0_product_id' value='Example Product ID' >
            <input type='hidden' name='li_0__description' value='Example Product Description' >
            <input type='hidden' name='li_0_price' value='10.00' >
            <input type='hidden' name='li_0_quantity' value='2' >
            <input type='hidden' name='li_0_tangible' value='N' >
            <input type='hidden' name='card_holder_name' value='Checkout Shopper' >
            <input type='hidden' name='street_address' value='123 Test St' >
            <input type='hidden' name='street_address2' value='Suite 200' >
            <input type='hidden' name='city' value='Columbus' >
            <input type='hidden' name='state' value='OH' >
            <input type='hidden' name='zip' value='43228' >
            <input type='hidden' name='country' value='USA' >
            <input type='hidden' name='email' value='example@2co.com' >
            <input type='hidden' name='phone' value='614-921-2450' >
            <input type='hidden' name='phone_extension' value='197' >

            <input name='submit' type='submit' value='Checkout' >
        </form>
        <div class="clear"></div>
    </div><!-- /.acc-body -->
@endsection
