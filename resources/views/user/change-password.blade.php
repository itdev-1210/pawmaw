@extends('pawmawFront.layouts.front')
@section('title', 'Change Password - PawMaw')

@section('content')
    <section class="user-panel">
        <div class="user-panel_sidebar">
            <div class="container">
                <div class="row">
                    <!-- SIDEBAR START -->
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        @include('user.partials.sidebar')
                    </div>
                    <!-- SIDEBAR END -->

                    <!-- CONTENT START -->
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <div class="user-panel_inner-content">
                            @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
                            <div class="pet-posted_content">
                                <div class="row">
                                    <div class="col-lg-7 col-md-8 col-sm-8">
                                        <h2 class="about-title_sub pet-posted_content-title change-pass_title">
                                            <span>Chanage Password</span>
                                        </h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <form class="home-next-found-pet_form" method="POST" action="{{ route('changePassword') }}">
                                            {{ csrf_field() }}

                                            <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                                                <label class="change-pass_label" for="password">Current Password <span class="required">*</span></label>
                                                <input id="current-password" type="password" class="form-control" name="current-password" required>
                                                @if ($errors->has('current-password'))
                                                    <span class="help-block">
                                                                        <strong>{{ $errors->first('current-password') }}</strong>
                                                                    </span>
                                                @endif
                                            </div>


                                            <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                                                <label class="change-pass_label" for="new-password">Password <span class="required">*</span></label>
                                                <input id="new-password" type="password" class="form-control" name="new-password" required>
                                                @if ($errors->has('new-password'))
                                                    <span class="help-block">
                                                                        <strong>{{ $errors->first('new-password') }}</strong>
                                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label class="change-pass_label" for="new-password-confirm">Confirm Password <span class="required">*</span></label>
                                                <input id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation" required>
                                            </div>

                                            <button class="change-pass_btn">Save Changes</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- CONTENT END -->
                </div>
            </div>
        </div>
    </section>
@endsection
