@extends('user.layout.user')
@section('title', 'My-Account - PawMaw')

@section('content')
    @include('user.partials.sidebar')

    <div id="acc-body">
        <h1 class="entry-title">
            <span>Promote Entry</span>
        </h1>



        @if($user->verified==0)


            <h1>Please Verified your email</h1>

            <br>

        @endif

        <div class="price-table-wrapper">

            <div class="pricing-table most_popular" id="pricing-table-pk2">

                <form action="{{env('2checkout_url')}}" method='post'>
                    <input type='hidden' name='sid' value='{{env('2checkout_id')}}' />
                    <input type='hidden' name='mode' value='2CO' />
                    <input type='hidden' name='li_0_type' value='product' >
                    <input type='hidden' name='li_0_name' value='FREE' >
                    <input type='hidden' name='li_0_product_id' value='Example Product ID' >
                    <input type='hidden' name='li_0__description' value='Example Product Description' >
                    <input type='hidden' name='li_0_price' value='0.00' >
                    <input type='hidden' name='li_0_quantity' value='1' >
                    <input type='hidden' name='li_0_tangible' value='N' >
                    <input type='hidden' name='card_holder_name' value='Checkout Shopper' >

                    <input type='hidden' name='street_address' value='123 Test St' >
                    <input type='hidden' name='street_address2' value='Suite 200' >
                    <input type='hidden' name='city' value='Columbus' >
                    <input type='hidden' name='state' value='OH' >
                    <input type='hidden' name='zip' value='43228' >
                    <input type='hidden' name='country' value='USA' >
                    <input type='hidden' name='email' value='example@2co.com' >
                    <input type='hidden' name='phone' value='614-921-2450' >
                    <input type='hidden' name='phone_extension' value='197' >
                <h2 class="pricing-table__header">
                    Free
                </h2>
                <h3 class="pricing-table__price">$0.00</h3>
                {{--<button type="submit" class="pricing-table__button p-top_btn">Order!</button>--}}
                <ul class="pricing-table__list">
                    <li><span class="no-dash">&mdash;</span></li>
                    <li><span class="no-dash">&mdash;</span></li>
                    <li class="yes">Custom Poster Design</li>
                    <li class="yes">Feature Post On our website</li>
                    <li class="yes">Real Time Notifications</li>
                    <li class="yes">24/7 Customer Support</li>
                </ul>
                {{--<button type="submit" class="pricing-table__button pbtn">Order!</button>--}}


                </form>
            </div><!-- /.free -->

            <div class="pricing-table" id="pricing-table-pk1">
                <form action="{{env('2checkout_url')}}" method='post'>
                    <input type='hidden' name='sid' value='{{env('2checkout_id')}}' />
                    <input type='hidden' name='mode' value='2CO' />
                    <input type='hidden' name='pet_id' value='{{$petinfo->id}}' />
                    <input type='hidden' name='user_id' value='{{$petinfo->user->id}}' />

                    <input type='hidden' name='li_0_type' value='product' >
                    <input type='hidden' name='li_0_name' value='BASIC' >
                    <input type='hidden' name='li_0_product_id' value='BASIC' >
                    <input type='hidden' name='li_0__description' value='BASIC' >
                    <input type='hidden' name='li_0_price' value='24.99' >
                    <input type='hidden' name='li_0_quantity' value='1' >
                    <input type='hidden' name='li_0_tangible' value='N' >
                    <input type='hidden' name='card_holder_name' value='{{$petinfo->user->fname}}' >
                    <input type='hidden' name='street_address' value='{{$petinfo->user->address}}' >
                    <input type='hidden' name='street_address2' value='{{$petinfo->user->address}}' >
                    <input type='hidden' name='city' value='' >
                    <input type='hidden' name='state' value='' >
                    <input type='hidden' name='zip' value='' >
                    <input type='hidden' name='country' value='' >
                    <input type='hidden' name='email' value='{{$petinfo->user->email}}' >
                    <input type='hidden' name='phone' value='{{$petinfo->user->phone}}' >
                    <input type='hidden' name='phone_extension' value='' >

                    <input name="paypal_direct" type="hidden" value="Y">



                    <h2 class="pricing-table__header">BASIC</h2>
                <h3 class="pricing-table__price">$24.99</h3>
                    <button type="submit" class="pricing-table__button p-top_btn">Order!</button>

                <ul class="pricing-table__list">
                    <li class="yes">Send 1,000 Paw Alert Nearest Pet Lover</li>
                    <li class="yes">3 Days Campaign</li>
                    <li class="yes">Custom Poster Design</li>
                    <li class="yes">Feature Post On our website</li>
                    <li class="yes">Real Time Notifications</li>
                    <li class="yes">24/7 Customer Support</li>
                </ul>
                    <button type="submit" class="pricing-table__button pbtn">Order!</button>
                </form>
            </div>


            <div class="pricing-table most_popular" id="pricing-table-pk2">

                <form action="{{env('2checkout_url')}}" method='post'>
                    <input type='hidden' name='sid' value='{{env('2checkout_id')}}' />
                    <input type='hidden' name='mode' value='2CO' />
                    <input type='hidden' name='pet_id' value='{{$petinfo->id}}' />
                    <input type='hidden' name='user_id' value='{{$petinfo->user->id}}' />
                    <input type='hidden' name='li_0_type' value='product' >
                    <input type='hidden' name='li_0_name' value='STANDARD' >
                    <input type='hidden' name='li_0_product_id' value='STANDARD' >
                    <input type='hidden' name='li_0__description' value='STANDARD' >
                    <input type='hidden' name='li_0_price' value='49.99' >
                    <input type='hidden' name='li_0_quantity' value='1' >
                    <input type='hidden' name='li_0_tangible' value='N' >
                    <input type='hidden' name='card_holder_name' value='{{$petinfo->user->fname}}' >
                    <input type='hidden' name='street_address' value='{{$petinfo->user->address}}' >
                    <input type='hidden' name='street_address2' value='{{$petinfo->user->address}}' >
                    <input type='hidden' name='city' value='' >
                    <input type='hidden' name='state' value='' >
                    <input type='hidden' name='zip' value='' >
                    <input type='hidden' name='country' value='' >
                    <input type='hidden' name='email' value='{{$petinfo->user->email}}' >
                    <input type='hidden' name='phone' value='{{$petinfo->user->phone}}' >
                    <input type='hidden' name='phone_extension' value='' >
                    <input name="paypal_direct" type="hidden" value="Y">



                    <h2 class="pricing-table__header">
                    <em>Most Popular</em>STANDARD
                </h2>
                <h3 class="pricing-table__price">$49.99</h3>
                    <button type="submit" class="pricing-table__button p-top_btn">Order!</button>
                <ul class="pricing-table__list">
                    <li class="yes">Send 10,000 Paw Alert Nearest Pet Lover</li>
                    <li class="yes">7 Days Campaign</li>
                    <li class="yes">Custom Poster Design</li>
                    <li class="yes">Feature Post On our website</li>
                    <li class="yes">Real Time Notifications</li>
                    <li class="yes">24/7 Customer Support</li>
                </ul>
                    <button type="submit" class="pricing-table__button pbtn">Order!</button>

                </form>
            </div>

            <div class="pricing-table" id="pricing-table-pk3">

                <form action="{{env('2checkout_url')}}" method='post'>
                    <input type='hidden' name='sid' value='{{env('2checkout_id')}}' />
                    <input type='hidden' name='mode' value='2CO' />
                    <input type='hidden' name='pet_id' value='{{$petinfo->id}}' />
                    <input type='hidden' name='user_id' value='{{$petinfo->user->id}}' />
                    <input type='hidden' name='li_0_type' value='product' >
                    <input type='hidden' name='li_0_name' value='PREMIUM' >
                    <input type='hidden' name='li_0_product_id' value='PREMIUM' >
                    <input type='hidden' name='li_0__description' value='PREMIUM' >
                    <input type='hidden' name='li_0_price' value='89.99' >
                    <input type='hidden' name='li_0_quantity' value='1' >
                    <input type='hidden' name='li_0_tangible' value='N' >
                    <input type='hidden' name='card_holder_name' value='{{$petinfo->user->fname}}' >
                    <input type='hidden' name='street_address' value='{{$petinfo->user->address}}' >
                    <input type='hidden' name='street_address2' value='{{$petinfo->user->address}}' >
                    <input type='hidden' name='city' value='' >
                    <input type='hidden' name='state' value='' >
                    <input type='hidden' name='zip' value='' >
                    <input type='hidden' name='country' value='' >
                    <input type='hidden' name='email' value='{{$petinfo->user->email}}' >
                    <input type='hidden' name='phone' value='{{$petinfo->user->phone}}' >
                    <input type='hidden' name='phone_extension' value='' >
                    <input name="paypal_direct" type="hidden" value="Y">
                <h2 class="pricing-table__header">PREMIUM</h2>
                <h3 class="pricing-table__price">$89.99</h3>
                    <button type="submit" class="pricing-table__button p-top_btn">Order!</button>
                <ul class="pricing-table__list">
                    <li class="yes">Send 20,000 Paw Alert Nearest Pet Lover</li>
                    <li class="yes">10 Days Campaign</li>
                    <li class="yes">Custom Poster Design</li>
                    <li class="yes">Feature Post On our website</li>
                    <li class="yes">Real Time Notifications</li>
                    <li class="yes">24/7 Customer Support</li>
                </ul>
                    <button type="submit" class="pricing-table__button pbtn">Order!</button>

                </form>
            </div>



        </div>

        <div class="clear"></div>
    </div><!-- /.acc-body -->
@endsection
