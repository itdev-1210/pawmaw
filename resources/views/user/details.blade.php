@extends('pawmawFront.layouts.front')

@section('title', 'Dashboard - PawMaw')

@section('content')
    <link rel="stylesheet" href="{{ asset('pawmaw/css/sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/sweetalert/sweet-alert-animations.min.css') }}">
    <!-- USER PANEL MAIN SECTION START -->
    <section class="user-panel use-panel_bg mt-5">
        <div class="user-panel_inner"
             style="background-image: url({{ asset('pawmaw/img/dashboard-bg.png')}});background-position: center; background-size: contain;">
            <div class="container">
                <div class="row">
                    <!-- SIDEBAR START -->
                @include('user.partials.sidebar_pet')
                <!-- SIDEBAR END -->

                    <!-- CONTENT START -->
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <div class="user-panel_inner-content">
                            <hr class="d-xl-none py-2">
                            <div class="user-panel_inner-content_title">
                                <h2>Pet Profile Details</h2>
                            </div>

                            <div class="pet-profile-details">
                                <div class="pet-profile-details_pet zoomIn">
                                    <figure style="background-image: url({{ url('uploads/pets_image') }}/{{$pet->photo}}) ;background-size: contain;background-position: center;background-repeat: no-repeat; width:70%; margin:auto;height:235px;">

                                    <!--                                    <img src="{{ asset('uploads/pets_image') }}/{{$pet->photo}}" alt="Pet Image" class="img-fluid mx-auto d-block w-50">-->

                                    </figure>
                                </div>
                            </div>

                            <div class="pet-profile-details_info">
                                <ul class="petdetails-lost_wrap">
                                    <li class="petdetails-lost_item">
                                        <h3 style="display:inline;"><span class="petdetails-lost_item-label">ID:</span>
                                            <span class="petdetails-lost_item-value ml-2">#{{$pet->id}}</span></h3>
                                    </li>
                                    <li class="petdetails-lost_item">
                                        <h3 style="display:inline;"><span class="petdetails-lost_item-label mt-3">Area last seen:</span>
                                            <span class="petdetails-lost_item-value ml-2">{{$pet->last_seen}}</span>
                                        </h3>
                                    </li>
                                    <li class="petdetails-lost_item">
                                        <h3 style="display:inline;"><span class="petdetails-lost_item-label mt-3">Cross street:</span><span
                                                    class="petdetails-lost_item-value ml-2">{{$pet->street}}</span></h3>
                                    </li>
                                    <li class="petdetails-lost_item">
                                        <h3 style="display:inline;"><span class="petdetails-lost_item-label mt-3">Species:</span>
                                            <span class="petdetails-lost_item-value ml-2">{{$pet->specie}}</span></h3>
                                    </li>
                                    <li class="petdetails-lost_item">
                                        <h3 style="display:inline;"><span
                                                    class="petdetails-lost_item-label mt-3">Color:</span> <span
                                                    class="petdetails-lost_item-value ml-2">{{$pet->color}}</span></h3>
                                    </li>
                                    <li class="petdetails-lost_item">
                                        <h3 style="display:inline;"><span class="petdetails-lost_item-label mt-3">Gender:</span>
                                            <span class="petdetails-lost_item-value ml-2">{{$pet->gender}}</span></h3>
                                    </li>
                                    <li class="petdetails-lost_item">
                                        <h3 style="display:inline;"><span
                                                    class="petdetails-lost_item-label mt-3">Breed:</span> <span
                                                    class="petdetails-lost_item-value ml-2">{{$pet->breed}}</span></h3>
                                    </li>
                                    <li class="petdetails-lost_item">
                                     @if($pet->type=='lost')
                                       <h3 style="display:inline;"><span class="petdetails-lost_item-label mt-3">Lost Date:</span>
                                     @else
                                      	<h3 style="display:inline;"><span class="petdetails-lost_item-label mt-3">Found Date:</span>
                                     @endif
                                            <span class="petdetails-lost_item-value ml-2">{{$pet->date}}</span></h3>
                                    </li>
                                    <li class="petdetails-lost_item">
                                        <h3 style="display:inline;"><span class="petdetails-lost_item-label mt-3">Description:</span>
                                        </h3>
                                    </li>
                                    <p class="petdetails-lost_des">{{$pet->description}}</p>
                                </ul>
                            </div>

                            @if($pet->package_name && $pet->package_name != 'Alerts Expired!')
                                <h2 class="user-panel_inner-content-msg mt-5 text-center">
                                    Please Go to "My Pet Alert" and Make Sure to Comment On the Post
                                </h2>

                                <div class="alert-map">
                                    <div class="alert-map_btn btn-success">
                                        <a href="{{ url('/user/alerts/' . $pet->id) }}">Your PawMaw Alert Activated!</a>
                                    </div>
                                    @if($pet->lat)
                                        <div id="map" style="width: 100%; height: 320px;"></div>
                                    @else
                                        <img src="{{ asset('pawmaw/img/map.png')}}" alt="Map"
                                             class="img-fluid img-thumbnail mx-auto d-block alert-map_position">
                                    @endif
                                    {{-- <img src="{{ asset('pawmaw/img/map.png') }}" alt="Map" class="img-fluid img-thumbnail mx-auto d-block alert-map_position"> --}}
                                </div>
                            @else
                                <h2 class="user-panel_inner-content-msg mt-5 text-center">
                                    Please active your pawmaw alert to reach more people in your local area.
                                </h2>

                                <div class="alert-map">
                                    <div class="alert-map_overly"></div>
                                    <div class="alert-map_icon"><i class="fas fa-lock"></i></div>
                                    <div class="alert-map_btn">
                                        <a href="{{ url('/user/alerts/' . $pet->id) }}">Active your pawmaw alert</a>
                                    </div>
                                    @if($pet->lat)
                                        <div id="map" style="width: 100%; height: 230px;"></div>
                                    @else
                                        <img src="{{ asset('pawmaw/img/map.png')}}" alt="Map"
                                             class="img-fluid img-thumbnail mx-auto d-block alert-map_position">
                                    @endif
                                </div>
                            @endif
                        </div>
                    </div>
                    <!-- CONTENT END -->
                </div>
            </div>
        </div>
    </section>

    @push('js')
        <script type='text/javascript' src="{{ asset('pawmaw/js/sweetalert.min.js') }}"></script>
        <script type="text/javascript">
            $('.confirm-delete').on('click', function (e) {
                e.preventDefault();
                $this = $(this);
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, delete!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                });
                $('button.confirm').on('click', function () {
                    $this.closest('form').submit();
                });
            });

            $('.confirm-reunited').on('click', function (e) {
                e.preventDefault();
                $this = $(this);
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-success",
                    confirmButtonText: "Yes, reunited!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                });
                $('button.confirm').on('click', function () {
                    $this.closest('form').submit();
                });
            });
        </script>
    @endpush

    <script>
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: {{ $pet->lat }}, lng: {{ $pet->lng }} },
                zoom: 15,
            });

            new google.maps.Marker({
                map: map,
                position: {lat: {{ $pet->lat }}, lng: {{ $pet->lng }} },
                animation: google.maps.Animation.DROP
            })
        }
    </script>

@endsection
