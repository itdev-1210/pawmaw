@extends('pawmawFront.layouts.front') 
@section('title', 'Edit Profile - PawMaw')
 @section('content')
 
<section class="home-next-found-pet"  style="background-image: url({{ asset('pawmaw/img/dashboard-bg.png')}});background-position: center; background-size: contain;">
    <div class="container">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul> 
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif 
     
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <h2 class="home-next-{{$info->type}}-pet_title mb-5">{{ucfirst($info->type)}} pet information</h2>
            </div>
        </div>
        <div class="row justify-content-center mb-5 {{$info->type}}">
            <div class="col-lg-5 col-md-6 col-sm-12">
                <div id="petdetails-img-wrap11" class="{{ $info->type }}">
                        
                         <div class="next-pet_box-img_title" style="border-radius:0px">
                            <h2>
                                <i class="fas fa-bullhorn"></i>
                                PawMaw
                                @if($info->city)
                                    <span> {{ $info->city . ', ' . ($info->country_short == 'US' ? $info->state : $info->country_short) }}</span>
                                @endif
                            </h2>
                        </div>
                    

                    @php
                        $size = @getimagesize(asset('uploads/pets_image') . '/' . $info->photo );
                        $needHeight = 400;
                        if($size){
                            $height = $size[1];
                            $width = $size[0] / ($size[1] / $needHeight);
                        }
                    @endphp

                   {{--   style="background-image: url({{ url('uploads/pets_image') }}/{{$info->photo}}) , url({{ asset('pawmaw/img/pet-bg.jpg')}});background-size: contain,cover; background-position: center; background-repeat:no-repeat; margin: 0;" --}}

                    <div 
                        class="petdetails-img11 {{ $info->type }} " 
                        id="image-to-copy"
                        data-check="true" 
                        data-id="{{ $info->id }}"
                        style="position:relative;margin: 0; background-image: url({{ asset('pawmaw/img/pet-bg.jpg')}}); background-size: cover; background-position: center; background-repeat:no-repeat;  border-radius:0px">
                            @if(!file_exists(public_path("uploads/converting-image/pet-{$info->id}.png")))
                                <img 
                                style="position: relative;height: {{ @$needHeight }}px; width: {{ @$width }}px; margin: 0 auto; display: flex; max-width: 100%; object-fit: contain;"
                                src="{{ url('uploads/pets_image') }}/{{$info->photo}}" alt="">
                            @endif
                       {{--  <div class="next-pet_box-img_title">
                            <h2>
                                <i class="fas fa-bullhorn"></i>
                                PawMaw{{ $info->city ? ',' : '' }}
                                @if($info->city)
                                    <span> {{ $info->city . ', ' . ($info->country_short == 'US' ? $info->state : $info->country_short) }}</span>
                                @endif
                            </h2>
                        </div> --}}
                        
                            <div class="next-pet_box-img_id">

                                <p style="border-radius:0px; background: {{ $info->type == 'lost' ? '#df1d41' : '#ffc000' }}"><span class="mr-3">ID</span>#{{$info->id}}</p>
                           
                            </div>
                            <div class="next-pet_box-img_logo">
                                @if($info->type == 'lost')
                                    <img src="{{ asset('pawmaw/img/fevi.png') }}" alt="Pawmaw" class="img-fluid circle-logo-50">
                                @elseif($info->type == 'found')
                                    <img src="{{ asset('pawmaw/img/flyer-found-image-icon.png') }}" alt="Pawmaw" class="img-fluid circle-logo-50">
                                @else
                                    <img src="{{ asset('pawmaw/img/reunited-logo.png') }}" alt="Pawmaw" class="img-fluid circle-logo-50">
                                @endif
                            </div>
                      
                      
                    </div>
                    
                </div>
                <div class="petdetails-map mb-xs-5" style="margin-top: 40px;">
                    @if($info->lat)
                        <div id="map" style="width: 100%; height: 320px;"></div>
                    @else
                        <img class="img-fluid img-thumbnail" src="{{ asset('pawmaw/img/pets/lost-map.png') }}" alt="Found Pet Image">
                    @endif

                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="home-next-found-pet_form">
                    <form action="{{ url('/user/updatepetinfo') }}" method="post" onSubmit="Export();">
                        {{ csrf_field() }}
                        <input type="hidden" name="pet_id" value="{{$info->id}}">
                        <div class="form-group" style="margin-top: 4px;">
                            <input type="text" class="form-control" id="petName" name="name" value="{{$info->name}}" tabindex="1" placeholder="Pet name" readonly>
                        </div>
                        <div class="form-group">
                            <select class="select-ctrl" name="specie" required>
                                <option value="" hidden selected disabled>Species*</option>
                                <option value="Dog">Dog</option>
                                <option value="Cat">Cat</option>
                                <option value="Bird">Bird</option>
                                <option value="Horse">Horse</option>
                                <option value="Rabbit">Rabbit</option>
                                <option value="Reptile">Reptile</option>
                                <option value="Ferret">Ferret</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="PetColor" name="color" placeholder="Color*" required>
                        </div>
                        <div class="form-group">
                            <select class="select-ctrl" id="breed" name="breed" required>
                                <option value="" hidden selected disabled>Breed*</option>
                                <option value="Mixed Breed">Mixed Breed</option>
                                <option value="Other">Other</option>

                                <option value="Unknown">Unknown</option>
                                <option value="Abyssinian">Abyssinian</option>
                                <option value="American Curl">American Curl</option>
                                <option value="Affenpinscher">Affenpinscher</option>

                                <option value="Afghan Hound">Afghan Hound</option>

                                <option value="Airedale Terrier">Airedale Terrier</option>

                                <option value="Akita">Akita</option>

                                <option value="Alaskan Husky">Alaskan Husky</option>

                                <option value="Alaskan Klee Kai">Alaskan Klee Kai</option>

                                <option value="Alaskan Malamute">Alaskan Malamute</option>

                                <option value="American Bobtail">American Bobtail</option>
                                <option value="American Bulldog">American Bulldog</option>
                                <option value="American Shorthair">American Shorthair</option>
                                <option value="American Wirehair">American Wirehair</option>

                                <option value="American Bully">American Bully</option>

                                <option value="American English Coonhound">American English Coonhound</option>

                                <option value="American Eskimo Dog">American Eskimo Dog</option>

                                <option value="American Foxhound">American Foxhound</option>

                                <option value="American Hairless Terrier">American Hairless Terrier</option>

                                <option value="American Leopard Hound">American Leopard Hound</option>

                                <option value="American Staffordshire Terrier">American Staffordshire Terrier
                                </option>

                                <option value="American Water Spaniel">American Water Spaniel</option>

                                <option value="Anatolian Shepherd Dog">Anatolian Shepherd Dog</option>

                                <option value="Appenzeller Sennenhunde">Appenzeller Sennenhunde</option>

                                <option value="Australian Cattle Dog">Australian Cattle Dog</option>

                                <option value="Australian Kelpie">Australian Kelpie</option>

                                <option value="Australian Shepherd">Australian Shepherd</option>

                                <option value="Australian Terrier">Australian Terrier</option>

                                <option value="Azawakh">Azawakh</option>
                                
                                <option value="Barbet">Barbet</option>
                                <option value="Balinese">Balinese</option>

                                <option value="Basenji">Basenji</option>

                                <option value="Basset Fauve De Bretagne">Basset Fauve De Bretagne</option>

                                <option value="Basset Hound">Basset Hound</option>

                                <option value="Beagle">Beagle</option>

                                <option value="Bearded Collie">Bearded Collie</option>

                                <option value="Beauceron">Beauceron</option>

                                <option value="Bedlington Terrier">Bedlington Terrier</option>

                                <option value="Belgian Laekenois">Belgian Laekenois</option>

                                <option value="Belgian Malinois">Belgian Malinois</option>

                                <option value="Belgian Sheepdog">Belgian Sheepdog</option>

                                <option value="Belgian Tervuren">Belgian Tervuren</option>
                                <option value="Bengal">Bengal</option>

                                <option value="Bergamasco">Bergamasco</option>

                                <option value="Berger Picard">Berger Picard</option>

                                <option value="Bernese Mountain Dog">Bernese Mountain Dog</option>

                                <option value="Bichon Frise">Bichon Frise</option>

                                <option value="Biewer Terrier">Biewer Terrier</option>
                                <option value="Birman">Birman</option>

                                <option value="Black and Tan Coonhound">Black and Tan Coonhound</option>

                                <option value="Black Russian Terrier">Black Russian Terrier</option>

                                <option value="Blackmouth Cur">Blackmouth Cur</option>

                                <option value="Bloodhound">Bloodhound</option>

                                <option value="Bluetick Coonhound">Bluetick Coonhound</option>

                                <option value="Boerboel">Boerboel</option>

                                <option value="Bolognese">Bolognese</option>
                                <option value="Bombay">Bombay</option>

                                <option value="Border Collie">Border Collie</option>

                                <option value="Border Terrier">Border Terrier</option>

                                <option value="Borzoi">Borzoi</option>

                                <option value="Boston Terrier">Boston Terrier</option>

                                <option value="Bouvier des Flandres">Bouvier des Flandres</option>

                                <option value="Boxer">Boxer</option>

                                <option value="Boykin Spaniel">Boykin Spaniel</option>

                                <option value="Bracco Italiano">Bracco Italiano</option>

                                <option value="Braque Du Bourbonnais">Braque Du Bourbonnais</option>

                                <option value="Briard">Briard</option>

                                <option value="Brittany Spaniel">Brittany Spaniel</option>
                                <option value="British Shorthair">British Shorthair</option>

                                <option value="Broholmer">Broholmer</option>

                                <option value="Brussels Griffon">Brussels Griffon</option>

                                <option value="Bull Terrier">Bull Terrier</option>

                                <option value="Bulldog">Bulldog</option>

                                <option value="Bullmastiff">Bullmastiff</option>
                                <option value="Burmese">Burmese</option>
                                
                                <option value="Cairn Terrier">Cairn Terrier</option>

                                <option value="Canaan Dog">Canaan Dog</option>

                                <option value="Cane Corso">Cane Corso</option>

                                <option value="Cardigan Welsh Corgi">Cardigan Welsh Corgi</option>

                                <option value="Catahoula">Catahoula</option>

                                <option value="Caucasian Ovcharka">Caucasian Ovcharka</option>

                                <option value="Cavalier King Charles Spaniel">Cavalier King Charles Spaniel</option>

                                <option value="Central Asian Shepherd Dog">Central Asian Shepherd Dog</option>

                                <option value="Cesky Terrier">Cesky Terrier</option>

                                <option value="Chartreux">Chartreux</option>
                                <option value="Chausie">Chausie</option>
                                <option value="Chesapeake Bay Retriever">Chesapeake Bay Retriever</option>

                                <option value="Chihuahua">Chihuahua</option>

                                <option value="Chinese Crested">Chinese Crested</option>

                                <option value="Chinese Shar-Pei">Chinese Shar-Pei</option>

                                <option value="Chinook">Chinook</option>

                                <option value="Chow Chow">Chow Chow</option>

                                <option value="Cirneco Dell'Etna">Cirneco Dell'Etna</option>

                                <option value="Clumber Spaniel">Clumber Spaniel</option>

                                <option value="Cocker Spaniel">Cocker Spaniel</option>

                                <option value="Collie">Collie</option>

                                <option value="Coton de Tulear">Coton de Tulear</option>
                                <option value="Cornish Rex">Cornish Rex</option>

                                <option value="Curly-Coated Retriever">Curly-Coated Retriever</option>

                                <option value="Czechoslovakian Vlcak">Czechoslovakian Vlcak</option>


                                <option value="Dachshund">Dachshund</option>

                                <option value="Dalmatian">Dalmatian</option>

                                <option value="Dandie Dinmont Terrier">Dandie Dinmont Terrier</option>

                                <option value="Danish-Swedish Farmdog">Danish-Swedish Farmdog</option>

                                <option value="Deutscher Wachtelhund">Deutscher Wachtelhund</option>

                                <option value="Doberman Pinscher">Doberman Pinscher</option>

                                <option value="Dogo Argentino">Dogo Argentino</option>

                                <option value="Dogue de Bordeaux">Dogue de Bordeaux</option>
                                <option value="Donskoy">Donskoy</option>
                                <option value="Devon Rex">Devon Rex</option>

                                <option value="Drentsche Patrijshond">Drentsche Patrijshond</option>

                                <option value="Drever">Drever</option>

                                <option value="Dutch Shepherd">Dutch Shepherd</option>


                                <option value="Egyptian Mau">Egyptian Mau</option>
                                <option value="English Bulldog">English Bulldog</option>

                                <option value="English Cocker Spaniel">English Cocker Spaniel</option>

                                <option value="English Foxhound">English Foxhound</option>

                                <option value="English Pointer">English Pointer</option>

                                <option value="English Setter">English Setter</option>

                                <option value="English Shepherd">English Shepherd</option>

                                <option value="English Springer Spaniel">English Springer Spaniel</option>

                                <option value="English Toy Spaniel">English Toy Spaniel</option>

                                <option value="Entlebucher Mountain Dog">Entlebucher Mountain Dog</option>

                                <option value="Estrela Mountain Dog">Estrela Mountain Dog</option>

                                <option value="Eurasier">Eurasier</option>
                                <option value="Exotic Shorthair">Exotic Shorthair</option>

                                <option value="Field Spaniel">Field Spaniel</option>

                                <option value="Finnish Lapphund">Finnish Lapphund</option>

                                <option value="Finnish Spitz">Finnish Spitz</option>

                                <option value="Flat-Coated Retriever">Flat-Coated Retriever</option>

                                <option value="French Bulldog">French Bulldog</option>

                                <option value="French Spaniel">French Spaniel</option>

                                <option value="German Longhaired Pointer">German Longhaired Pointer</option>

                                <option value="German Pinscher">German Pinscher</option>

                                <option value="German Shepherd">German Shepherd</option>

                                <option value="German Shorthaired Pointer">German Shorthaired Pointer</option>

                                <option value="German Spitz">German Spitz</option>

                                <option value="German Wirehaired Pointer">German Wirehaired Pointer</option>

                                <option value="Giant Schnauzer">Giant Schnauzer</option>

                                <option value="Glen of Imaal Terrier">Glen of Imaal Terrier</option>

                                <option value="Golden Retriever">Golden Retriever</option>

                                <option value="Gordon Setter">Gordon Setter</option>

                                <option value="Grand Basset Griffon Vendeen">Grand Basset Griffon Vendeen</option>

                                <option value="Great Dane">Great Dane</option>

                                <option value="Great Pyrenees">Great Pyrenees</option>

                                <option value="Greater Swiss Mountain Dog">Greater Swiss Mountain Dog</option>

                                <option value="Greyhound">Greyhound</option>
                                <option value="Hamiltonstovare">Hamiltonstovare</option>

                                <option value="Harrier">Harrier</option>

                                <option value="Havanese">Havanese</option>
                                <option value="Havana">Havana</option>

                                <option value="Highlander">Highlander</option>
                                <option value="Himalayan">Himalayan</option>
                                <option value="Hound">Hound</option>
                                <option value="Household Pet Cat">Household Pet Cat</option>
                                <option value="Household Pet Kitten">Household Pet Kitten</option>

                                <option value="Hovawart">Hovawart</option>

                                <option value="Ibizan Hound">Ibizan Hound</option>

                                <option value="Icelandic Sheepdog">Icelandic Sheepdog</option>

                                <option value="Irish Red and White Setter">Irish Red and White Setter</option>

                                <option value="Irish Setter">Irish Setter</option>

                                <option value="Irish Terrier">Irish Terrier</option>

                                <option value="Irish Water Spaniel">Irish Water Spaniel</option>

                                <option value="Irish Wolfhound">Irish Wolfhound</option>

                                <option value="Italian Greyhound">Italian Greyhound</option>

                                <option value="Jack Russell Terrier">Jack Russell Terrier</option>

                                <option value="Jagdterrier">Jagdterrier</option>

                                <option value="Japanese Bobtail">Japanese Bobtail</option>
                                <option value="Japanese Chin">Japanese Chin</option>


                                <option value="Kai Ken">Kai Ken</option>

                                <option value="Keeshond">Keeshond</option>

                                <option value="Kerry Blue Terrier">Kerry Blue Terrier</option>

                                <option value="Kishu Ken">Kishu Ken</option>

                                <option value="Komondor">Komondor</option>

                                <option value="Korat">Korat</option>
                                <option value="Korean Jindo">Korean Jindo</option>

                                <option value="Kromfohrlander">Kromfohrlander</option>

                                <option value="Kurilian Bobtail">Kurilian Bobtail</option>
                                <option value="Kuvasz">Kuvasz</option>

                                <option value="Labrador Retriever">Labrador Retriever</option>

                                <option value="Lagotto Romagnolo">Lagotto Romagnolo</option>

                                <option value="Lakeland Terrier">Lakeland Terrier</option>

                                <option value="Lancashire Heeler">Lancashire Heeler</option>
                                <option value="LaPerm">LaPerm</option>

                                <option value="Leonberger">Leonberger</option>

                                <option value="Lhasa Apso">Lhasa Apso</option>

                                <option value="Löwchen">Löwchen</option>




                                <option value="Maltese">Maltese</option>
                                <option value="Maine Coon">Maine Coon</option>

                                <option value="Manchester Terrier">Manchester Terrier</option>
                                <option value="Manx">Manx</option>

                                <option value="Mastiff">Mastiff</option>

                                <option value="Miniature American Shepherd">Miniature American Shepherd</option>

                                <option value="Miniature Bull Terrier">Miniature Bull Terrier</option>

                                <option value="Miniature Pinscher">Miniature Pinscher</option>

                                <option value="Miniature Schnauzer">Miniature Schnauzer</option>
                                <option value="Minskin">Minskin</option>

                                <option value="Mixed Breed">Mixed Breed</option>

                                <option value="Mudi">Mudi</option>
                                <option value="Munchkin">Munchkin</option>


                                <option value="Neapolitan Mastiff">Neapolitan Mastiff</option>

                                <option value="Nebelung">Nebelung</option>
                                <option value="Nederlandse Kooikerhondje">Nederlandse Kooikerhondje</option>

                                <option value="Newfoundland">Newfoundland</option>

                                <option value="Norfolk Terrier">Norfolk Terrier</option>

                                <option value="Norrbottenspets">Norrbottenspets</option>

                                <option value="Norwegian Buhund">Norwegian Buhund</option>

                                <option value="Norwegian Elkhound">Norwegian Elkhound</option>
                                <option value="Norwegian Forest Cat">Norwegian Forest Cat</option>

                                <option value="Norwegian Lundehund">Norwegian Lundehund</option>

                                <option value="Norwich Terrier">Norwich Terrier</option>

                                <option value="Nova Scotia Duck Tolling Retriever">Nova Scotia Duck Tolling
                                    Retriever
                                </option>



                                <option value="Old English Sheepdog">Old English Sheepdog</option>

                                <option value="Other">Other</option>
                                <option value="Ocicat">Ocicat</option>
                                <option value="Ojos Azules">Ojos Azules</option>

                                <option value="Oriental">Oriental</option>
                                <option value="Otterhound">Otterhound</option>



                                <option value="Papillon">Papillon</option>

                                <option value="Parson Russell Terrier">Parson Russell Terrier</option>

                                <option value="Patterdale Terrier">Patterdale Terrier</option>

                                <option value="Pekingese">Pekingese</option>

                                <option value="Pembroke Welsh Corgi">Pembroke Welsh Corgi</option>

                                <option value="Perro De Presa Canario">Perro De Presa Canario</option>
                                <option value="Persian">Persian</option>

                                <option value="Peruvian Inca Orchid">Peruvian Inca Orchid</option>

                                <option value="Peterbald">Peterbald</option>
                                <option value="Petit Basset Griffon Vendéen">Petit Basset Griffon Vendéen</option>

                                <option value="Pharaoh Hound">Pharaoh Hound</option>

                                <option value="Pit Bull">Pit Bull</option>
                                <option value="Pixiebob">Pixiebob</option>

                                <option value="Plott">Plott</option>

                                <option value="Plott">Plott</option>

                                <option value="Pointer">Pointer</option>

                                <option value="Polish Lowland Sheepdog">Polish Lowland Sheepdog</option>

                                <option value="Pomeranian">Pomeranian</option>

                                <option value="Poodle">Poodle</option>

                                <option value="Portuguese Podengo">Portuguese Podengo</option>

                                <option value="Portuguese Podengo Pequeno">Portuguese Podengo Pequeno</option>

                                <option value="Portuguese Pointer">Portuguese Pointer</option>

                                <option value="Portuguese Sheepdog">Portuguese Sheepdog</option>

                                <option value="Portuguese Water Dog">Portuguese Water Dog</option>

                                <option value="Pug">Pug</option>

                                <option value="Puli">Puli</option>

                                <option value="Puli">Puli</option>

                                <option value="Pumi">Pumi</option>

                                <option value="Pyrenean Mastiff">Pyrenean Mastiff</option>

                                <option value="Pyrenean Shepherd">Pyrenean Shepherd</option>

                                <option value="Queensland Heeler">Queensland Heeler</option>


                                <option value="Rafeiro Do Alentejo">Rafeiro Do Alentejo</option>
                                <option value="Ragdoll">Ragdoll</option>

                                <option value="Rat Terrier">Rat Terrier</option>

                                <option value="Redbone Coonhound">Redbone Coonhound</option>

                                <option value="Rhodesian Ridgeback">Rhodesian Ridgeback</option>

                                <option value="Rottweiler">Rottweiler</option>

                                <option value="Running Walker Foxhound">Running Walker Foxhound</option>

                                <option value="Russian Blue">Russian Blue</option>
                                <option value="Russian Toy">Russian Toy</option>

                                <option value="Russian Tsvetnaya Bolonka">Russian Tsvetnaya Bolonka</option>
                                <option value="Saint Bernard">Saint Bernard</option>

                                <option value="Saluki">Saluki</option>

                                <option value="Samoyed">Samoyed</option>
                                <option value="Savannah">Savannah</option>
                                <option value="Serengeti">Serengeti</option>

                                <option value="Schapendoes">Schapendoes</option>

                                <option value="Schipperke">Schipperke</option>

                                <option value="Schnauzer">Schnauzer</option>

                                <option value="Scottish Deerhound">Scottish Deerhound</option>
                                <option value="Scottish Fold">Scottish Fold</option>

                                <option value="Scottish Terrier">Scottish Terrier</option>

                                <option value="Sealyham Terrier">Sealyham Terrier</option>
                                <option value="Selkirk Rex">Selkirk Rex</option>

                                <option value="Shar-Pei">Shar-Pei</option>

                                <option value="Shetland Sheepdog">Shetland Sheepdog</option>

                                <option value="Siberian">Siberian</option>
                                <option value="Shiba Inu">Shiba Inu</option>

                                <option value="Shih Tzu">Shih Tzu</option>

                                <option value="Shikoku">Shikoku</option>

                                <option value="Siamese">Siamese</option>
                                <option value="Siberian Husky">Siberian Husky</option>

                                <option value="Silky Terrie">Silky Terrier</option>
                                <option value="Singapura">Singapura</option>

                                <option value="Skye Terrier">Skye Terrier</option>

                                <option value="Sloughi">Sloughi</option>

                                <option value="Slovensky Cuvac">Slovensky Cuvac</option>

                                <option value="Slovensky Kopov">Slovensky Kopov</option>

                                <option value="Small Munsterlander Pointer">Small Munsterlander Pointer</option>

                                <option value="Smooth Fox Terrier">Smooth Fox Terrier</option>
                                <option value="Somali">Somali</option>
                                <option value="Snowshoe">Snowshoe</option>

                                <option value="Soft Coated Wheaten Terrier">Soft Coated Wheaten Terrier</option>
                                <option value="Sokoke">Sokoke</option>

                                <option value="Spanish Mastiff">Spanish Mastiff</option>

                                <option value="Spanish Water Dog">Spanish Water Dog</option>

                                <option value="Sphynx">Sphynx</option>
                                <option value="Spinone Italiano">Spinone Italiano</option>

                                <option value="Stabyhoun">Stabyhoun</option>

                                <option value="Staffordshire Bull Terrier">Staffordshire Bull Terrier</option>

                                <option value="Sussex Spaniel">Sussex Spaniel</option>

                                <option value="Swedish Lapphund">Swedish Lapphund</option>

                                <option value="Swedish Vallhund">Swedish Vallhund</option>


                                <option value="Tamaskan">Tamaskan</option>

                                <option value="Terrier Mix">Terrier Mix</option>

                                <option value="Thai">Thai</option>
                                <option value="Thai Ridgeback">Thai Ridgeback</option>

                                <option value="Tibetan Mastiff">Tibetan Mastiff</option>

                                <option value="Tibetan Spaniel">Tibetan Spaniel</option>

                                <option value="Tibetan Terrier">Tibetan Terrier</option>

                                <option value="Tonkinese">Tonkinese</option>
                                <option value="Tornjak">Tornjak</option>

                                <option value="Tosa">Tosa</option>

                                <option value="Toyger">Toyger</option>
                                <option value="Toy Fox Terrier">Toy Fox Terrier</option>

                                <option value="Transylvanian Hound">Transylvanian Hound</option>

                                <option value="Treeing Walker Brindle">Treeing Walker Brindle</option>

                                <option value="Treeing Walker Coonhound">Treeing Walker Coonhound</option>
                                <option value="Turkish Angora">Turkish Angora</option>
                                <option value="Turkish Van">Turkish Van</option>

                                <option value="Unknown">Unknown</option>

                                <option value="Vizsla">Vizsla</option>

                                <option value="Weimaraner">Weimaraner</option>

                                <option value="Welsh Springer Spaniel">Welsh Springer Spaniel</option>

                                <option value="Welsh Terrier">Welsh Terrier</option>

                                <option value="West Highland White Terrier">West Highland White Terrier</option>

                                <option value="Whippet">Whippet</option>

                                <option value="Wire Fox Terrier">Wire Fox Terrier</option>

                                <option value="Wirehaired Pointing Griffon">Wirehaired Pointing Griffon</option>

                                <option value="Wolfdog">Wolfdog</option>

                                <option value="Working Kelpie">Working Kelpie</option>

                                <option value="Xoloitzcuintli">Xoloitzcuintli</option>

                                <option value="Yorkshire Terrier">Yorkshire Terrier</option>
                            </select>
                        </div>
                        <div class="custom-radio_btn">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input id="male" name="sex" type="radio" value="Male" checked required/>
                                        <label for="male">Male</label>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input id="female" name="sex" type="radio" value="Female" />
                                        <label for="female">Female</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="form-group">
                         @if($info->type == 'lost')
                            <input class="form-control" type="text" onkeydown="return false" name="lost_found_date" id="lost_date" placeholder="Lost date*" required>
                            @else
                            <input class="form-control" type="text" onkeydown="return false" name="lost_found_date" id="found_date" placeholder="Found date*" required>
                         @endif
                        </div>
                        <div class="form-group">
                            <textarea class="form-control home-next-found-pet_form-textarea" rows="5" name="description" placeholder="Pet Description*" required></textarea>
                        </div>
                          @php
                           	session()->put('new_user', 0);
                          @endphp
                        
                        <button type="submit" class="home-next-{{$info->type}}-pet_form-btn">Get pet back home</button>
                        <textarea id="map-url" style="display:none;" name="mapUrl"></textarea>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- Include Date Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script>
    function initMap(){
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: {{ $info->lat }}, lng: {{ $info->lng }} },
            zoom: 15,
        });

        new google.maps.Marker({
            map: map,
            position: {lat: {{ $info->lat }}, lng: {{ $info->lng }} },
            animation: google.maps.Animation.DROP
        })
    }

    function Export() {
        //URL of Google Static Maps.
        var staticMapUrl = "https://maps.googleapis.com/maps/api/staticmap";
        
        //Set the Google Map Center.
        staticMapUrl += "?center=" + {{ $info->lat }} + "," + {{ $info->lng }};
        
        //Set the Google Map Size.
        staticMapUrl += "&size=370x265";
        
        //Set the Google Map Zoom.
        staticMapUrl += "&zoom=15";
        
        //Set the Google Map Type.
        staticMapUrl += "&maptype=1";
        //Display the Image of Google Map.
        staticMapUrl += "&markers=color:red|label:{text:O,fontWeight:bold}|" + {{ $info->lat }} + ',' + {{ $info->lng }};;

        staticMapUrl += "&key=AIzaSyAhtpDqV3eFcqXbywJKMnvghzjQkvHAus8";
        
        $("#map-url").val(staticMapUrl);
    }

</script>

<style>
   .fixing_view_point.d-none{
        display: none !important; 
    } 
</style>
<script>
    $(document).ready(function(){
        var date_input=$('input[name="lost_found_date"]');
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
            language: 'da',
            enableOnReadonly: false,
            startDate: '-1830d',
            endDate: '+0d'
        });

        var date_input=$('input[name="found_date"]');
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
            language: 'da',
            enableOnReadonly: false,
            startDate: '-1830d',
            endDate: '+0d'
        });

    });


</script>
@endsection
