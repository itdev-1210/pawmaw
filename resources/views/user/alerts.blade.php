@extends('pawmawFront.layouts.front')

@section('title', 'Dashboard - PawMaw')

@section('content')
    <link rel="stylesheet" href="{{ asset('pawmaw/css/sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/sweetalert/sweet-alert-animations.min.css') }}">

    <!-- USER PANEL MAIN SECTION START -->
    <section class="user-panel use-panel_bg mt-5">
        <div class="user-panel_inner"
             style="background-image: url({{ asset('pawmaw/img/dashboard-bg.png')}});background-position: center; background-size: contain;">
            <div class="container">
                <div class="row">
                    <!-- SIDEBAR START -->
                @include('user.partials.sidebar_pet')
                <!-- SIDEBAR END -->

                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <div class="user-panel_inner-content">
                            <hr class="d-xl-none py-2">
                            <div class="active-alert_text">
                                <h2>Alert Package</h2>
                                <p>Select the package that best soothes you. Paw Alerts features different packages that
                                    help reach out to more people – which in-turn increases the chances of finding your
                                    lost pet on time. The package ranges from <b>Basic, Standard</b> to<b> Premium</b> .
                                    Select your package to reach more people in your area.</p>
                            </div>

                            @if(count($payments) > 0)
                                <div class="active-alert-pack">
                                    <h2 class="active-alert-pack_title">Current
                                        Package: {{ $payments[0]['li_0_name'] }}</h2>
                                </div>
                            @endif

                            <div class="active-alert-pack_list">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-12 mb-sm-5 mb-xs-5">
                                        <div class="package-title_img py-5">
                                            <img src="{{ asset('pawmaw/img/package/basic.png') }}" alt="Basic"
                                                 class="img-fluid mx-auto d-block">
                                        </div>
                                        <div class="card package-wrap_box">
                                            <div class="package-wrap_box-head">
                                                <i class="fas fa-circle"></i>
                                                <h2 class="package-wrap_box-head_title">Basic</h2>
                                                <div class="package-wrap_box-head_price basic">
                                                    <span><i class="fas fa-dollar-sign"></i> 24.</span><span>99</span>
                                                </div>
                                            </div>
                                            <div class="package-wrap_box-body">
                                                <ul>
                                                    <li>
                                                        <i class="fas fa-paper-plane"></i>
                                                        <strong>Send 1,000 Paw Alert Nearest Pet Lover</strong> in your
                                                        area within 10 miles
                                                    </li>
                                                    <li>
                                                        <i class="fas fa-calendar-check"></i> 3 Days Campaign
                                                    </li>
                                                    <li>
                                                        <i class="fas fa-file"></i> Custom Poster Design
                                                    </li>
                                                    <li>
                                                        <i class="fas fa-bookmark"></i> Feature Post On our website
                                                    </li>
                                                    <li>
                                                        <i class="fas fa-bell"></i> Real Time Notifications
                                                    </li>
                                                    <li>
                                                        <i class="fas fa-clock"></i> 24/7 Customer Support
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="package-wrap_box-footer">
                                                <button type="button" class="pb-btn" data-package-code="package-basic">
                                                    <i
                                                            class="fas fa-angle-double-right"></i>
                                                    <span>Place Order</span> <i class="fas fa-angle-double-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 mb-sm-5 mb-xs-5">
                                        <div class="package-title_img py-5">
                                            <img src="{{ asset('pawmaw/img/package/standard.png') }}" alt="Basic"
                                                 class="img-fluid mx-auto d-block">
                                        </div>
                                        <div class="card package-wrap_box">
                                            <div class="package-wrap_box-head">
                                                <i class="fas fa-circle"></i>
                                                <h2 class="package-wrap_box-head_title">Standard</h2>
                                                <div class="package-wrap_box-head_price standard">
                                                    <span><i class="fas fa-dollar-sign"></i> 49.</span><span>99</span>
                                                </div>
                                            </div>
                                            <div class="package-wrap_box-body">
                                                <ul>
                                                    <li>
                                                        <i class="fas fa-paper-plane"></i>
                                                        <strong>Send 5,000 Paw Alert Nearest Pet Lover</strong> in your
                                                        area within 10 miles
                                                    </li>
                                                    <li>
                                                        <i class="fas fa-calendar-check"></i> 7 Days Campaign
                                                    </li>
                                                    <li>
                                                        <i class="fas fa-file"></i> Custom Poster Design
                                                    </li>
                                                    <li>
                                                        <i class="fas fa-bookmark"></i> Feature Post On our website
                                                    </li>
                                                    <li>
                                                        <i class="fas fa-bell"></i> Real Time Notifications
                                                    </li>
                                                    <li>
                                                        <i class="fas fa-clock"></i> 24/7 Customer Support
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="package-wrap_box-footer">
                                                <button type="button" class="ps-btn"
                                                        data-package-code="package-standard"><i
                                                            class="fas fa-angle-double-right"></i>
                                                    <span>Place Order</span> <i class="fas fa-angle-double-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 mb-sm-5 mb-xs-5">
                                        <div class="package-title_img py-5">
                                            <img src="{{ asset('pawmaw/img/package/premium.png') }}" alt="Basic"
                                                 class="img-fluid mx-auto d-block">
                                        </div>
                                        <div class="card package-wrap_box">
                                            <div class="package-wrap_box-head">
                                                <i class="fas fa-circle"></i>
                                                <h2 class="package-wrap_box-head_title">Premium</h2>
                                                <div class="package-wrap_box-head_price premium">
                                                    <span><i class="fas fa-dollar-sign"></i> 89.</span><span>99</span>
                                                </div>
                                            </div>
                                            <div class="package-wrap_box-body">
                                                <ul>
                                                    <li>
                                                        <i class="fas fa-paper-plane"></i>
                                                        <strong>Send 10,000 Paw Alert Nearest Pet Lover</strong> in your
                                                        area within 10 miles
                                                    </li>
                                                    <li>
                                                        <i class="fas fa-calendar-check"></i> 10 Days Campaign
                                                    </li>
                                                    <li>
                                                        <i class="fas fa-file"></i> Custom Poster Design
                                                    </li>
                                                    <li>
                                                        <i class="fas fa-bookmark"></i> Feature Post On our website
                                                    </li>
                                                    <li>
                                                        <i class="fas fa-bell"></i> Real Time Notifications
                                                    </li>
                                                    <li>
                                                        <i class="fas fa-clock"></i> 24/7 Customer Support
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="package-wrap_box-footer">
                                                <button type="button" class="pp-btn"
                                                        data-package-code="package-premium"><i
                                                            class="fas fa-angle-double-right"></i>
                                                    <span>Place Order</span> <i class="fas fa-angle-double-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if(count($payments) > 0)
                                <h2 class="user-panel_inner-content-msg mt-5 text-center">
                                    Please Go to "My Pet Alert" and Make Sure to Comment On the Post
                                </h2>

                                <div class="alert-map">
                                    <div class="alert-map_btn btn-success">
                                        <a href="{{ url('/user/alerts/' . $pet->id) }}">Your PawMaw Alert Activated!</a>
                                    </div>
                                    @if($pet->lat)
                                        <div id="map" style="width: 100%; height: 320px;"></div>
                                    @else
                                        <img src="{{ asset('pawmaw/img/map.png')}}" alt="Map"
                                             class="img-fluid img-thumbnail mx-auto d-block alert-map_position">
                                    @endif
                                    {{-- <img src="{{ asset('pawmaw/img/map.png') }}" alt="Map" class="img-fluid img-thumbnail mx-auto d-block alert-map_position"> --}}
                                </div>
                            @else
                                <h2 class="user-panel_inner-content-msg mt-5 text-center">
                                    Please active your pawmaw alert to reach more people in your local area.
                                </h2>

                                <div class="alert-map">
                                    <div class="alert-map_overly"></div>
                                    <div class="alert-map_icon"><i class="fas fa-lock"></i></div>
                                    <div class="alert-map_btn">
                                        <a href="/user/alerts/{{ @$pet->id }}">Active your pawmaw alert</a>
                                    </div>
                                    @if($pet->lat)
                                        <div id="map" style="width: 100%; height: 320px;"></div>
                                    @else
                                        <img src="{{ asset('pawmaw/img/map.png')}}" alt="Map"
                                             class="img-fluid img-thumbnail mx-auto d-block alert-map_position">
                                    @endif
                                    {{--    <img src="{{ asset('pawmaw/img/map.png') }}" alt="Map" class="img-fluid img-thumbnail mx-auto d-block alert-map_position"> --}}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @push('js')
        <script type="text/javascript"></script>
    @endpush
    <script type='text/javascript' src="{{ asset('pawmaw/js/sweetalert.min.js') }}"></script>

    <script>
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: {{ $pet->lat }}, lng: {{ $pet->lng }} },
                zoom: 15,
            });

            new google.maps.Marker({
                map: map,
                position: {lat: {{ $pet->lat }}, lng: {{ $pet->lng }} },
                animation: google.maps.Animation.DROP
            })
        }

    </script>

    <script>
        (function (document, src, libName, config) {
            var script = document.createElement('script');
            script.src = src;
            script.async = true;
            var firstScriptElement = document.getElementsByTagName('script')[0];
            script.onload = function () {
                for (var namespace in config) {
                    if (config.hasOwnProperty(namespace)) {
                        window[libName].setup.setConfig(namespace, config[namespace]);
                    }
                }
                window[libName].register();
            };

            firstScriptElement.parentNode.insertBefore(script, firstScriptElement);
        })(document, 'https://secure.avangate.com/checkout/client/twoCoInlineCart.js', 'TwoCoInlineCart', {
            "app": {"merchant": "{{env('TWOCHECKOUT_MERCHANT')}}"},
            "cart": {"host": "https:\/\/secure.2checkout.com"}
        });
    </script>
    <script type="text/javascript">
		<?php
		$ref = [
			'user_id' => $pet->user->id,
			'pet_id'  => $pet->id
		];
		?>
        $(document).ready(function () {

            $('.package-wrap_box button').on('click', function (e) {
                e.preventDefault();

                var package_code = $(this).data('package-code');

                // TwoCoInlineCart.billing.setCountry('US'); // customer billing country
                // TwoCoInlineCart.billing.setCity('Townsville'); // customer billing country
                // TwoCoInlineCart.billing.setState('Ohio'); // customer billing country
                // TwoCoInlineCart.billing.setZip('43206'); // customer billing country
                // TwoCoInlineCart.billing.setName('John s'); // customer billing Name
                TwoCoInlineCart.billing.setEmail('{{$pet->user->email}}');

                TwoCoInlineCart.products.removeAll();
                TwoCoInlineCart.products.add({
                    code: package_code,
                    quantity: 1
                });
                TwoCoInlineCart.cart.setTest({{env('TWOCHECKOUT_SANDBOX')}});
                TwoCoInlineCart.cart.setExternalCustomerReference('<?= json_encode( $ref ) ?>');
                TwoCoInlineCart.cart.setCurrency('{{env('TWOCHECKOUT_CURRENCY')}}');
                TwoCoInlineCart.cart.checkout();
            });
        });
    </script>

@endsection
