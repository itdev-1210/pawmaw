@extends('pawmawFront.layouts.front', ['showMoreOption' => true])

@section('title', 'Dashboard - PawMaw')

@section('content')
    <link rel="stylesheet" href="{{ asset('pawmaw/css/sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/sweetalert/sweet-alert-animations.min.css') }}">
    <!-- USER PANEL MAIN SECTION START -->
    <section class="user-panel"
             style="background-image: url({{ asset('pawmaw/img/dashboard-bg.png')}});background-position: center; background-size: contain;">
        <div class="user-panel_sidebar">
            <div class="container">


                <div class="row">
                    <!-- SIDEBAR START -->
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        @include('user.partials.sidebar')
                    </div>
                    <!-- SIDEBAR END -->

                    <!-- CONTENT START -->
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <div class="user-panel_inner-content">
                            <div class="pet-posted_content">
                                <div class="row justify-content-center">
                                 @if(session('new_user') == 1)
                                            <div id="login-alert" class="alert alert-success" style="font-size: 14px;"
                                                 role="alert">
                                                Hello {{ $users->fname}} {{ $users->lname}}

                                                <p>Warm Welcome From PawMaw Team. Here is some advice to get started
                                                    with your PawMaw Account.</p>
                                            </div>

                                            <script>
                                                setTimeout(function () {
                                                    $('#login-alert').fadeOut();
                                                }, 5000);
                                            </script>

                                            @php
                                                session()->put('new_user', 0);
                                            @endphp

                                        @endif
                                
                                    <div class="col-lg-6 col-12">

                                        @if(session('success'))
                                            <div class="alert alert-success" style="font-size: 20px;" id="id">
                                                {{ session('success') }}
                                            </div>

                                            <script>
                                                setTimeout(function () {
                                                    $('.alert-success').fadeOut();
                                                }, 3000);
                                            </script>
                                        @endif

                                        @if(session('danger'))
                                            <div class="alert alert-danger" id="id">
                                                {{session('danger')}}
                                            </div>
                                        @endif

                                       

                                        <h1 class="about-title_sub pet-posted_content-title">
                                            <span>My posted entry</span>
                                        </h1>
                                    </div>
                                </div>
                                <div class="row py-5">
                                    @foreach($pets as $key=>$row)
                                        <div class="col-lg-5 col-md-4 col-sm-6 mb-sm-5 mb-xs-5">
                                            <div class="featured-box">
                                                @if($row->package_name)
                                                    <div class="ribbon ribbon-top-right">
                                                        <span>{{ $row->package_name }}</span></div>
                                                @else
                                                 <div class="ribbon ribbon-top-right">
                                                        <span>Free</span></div>
                                                @endif
                                                <div class="zoomIn">
                                                    <figure style="background-image: url({{ url('uploads/pets_image') }}/{{$row->photo}});background-size: contain;background-position: center;background-repeat: no-repeat;">
                                                    <!--
                                                    @if(file_exists( public_path().'/uploads/pets_image/'.$row->photo ) && $row->photo!='')
                                                        <img class="card-img-top" src="{{ url('uploads/pets_image') }}/{{$row->photo}}" alt="{{$row->name}}">
                                                    @else
                                                        <img class="card-img-top" src="{{ url('pawmaw/img/default.png') }}" alt="{{$row->name}}">
                                                    @endif
                                                            -->
                                                    </figure>
                                                </div>
                                                <div class="pet-posted_content-details">
                                                    <h2 class="pet-posted_content-details_name">{{$row->name}}</h2>
                                                    <p>A {{$row->gender}} {{$row->specie}} <span
                                                                class="tag-{{$row->type}} ml-3">{{ucfirst($row->type)}}</span>
                                                    </p>
                                                    <a href="{{ url('user/pet-details/') }}/{{ $row->slug }}"
                                                       class="view-dashboard_btn mb-4 mt-5">View dashboard</a>
                                                       @if($row->fb_share)
                                                            <a href="http://www.facebook.com/sharer.php?u={{ $row->fb_share }}"
                                                                class="fb-sahre_btn mb-4 mt-5" target="_blank">Share on facebook</a>
                                                       @else
                                                            <a href="#"
                                                                class="fb-sahre_btn mb-4 mt-5">Share on facebook</a>
                                                        @endif
                                                        
                                                        
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- CONTENT END -->
                </div>
            </div>
        </div>
    </section>

    @push('js')
        <script type='text/javascript' src="{{ asset('pawmaw/js/sweetalert.min.js') }}"></script>
        <script type="text/javascript">
            jQuery('.confirm-delete').on('click', function (e) {
                e.preventDefault();
                $this = jQuery(this);
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, delete!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                });
                jQuery('button.confirm').on('click', function () {
                    $this.closest('form').submit();
                });
            });

            jQuery('.confirm-reunited').on('click', function (e) {
                e.preventDefault();
                $this = jQuery(this);
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this data!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-success",
                    confirmButtonText: "Yes, reunited!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                });
                jQuery('button.confirm').on('click', function () {
                    $this.closest('form').submit();
                });
            });
        </script>
    @endpush
@endsection
