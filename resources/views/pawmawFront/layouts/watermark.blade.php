<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name=keywords content="lost pet, lost dog, lost cat, free lost pet alert,  free lost pet poster, find lost dog, find lost cat, find lost pet">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="canonical" href="https://www.pawmaw.com/">

    @if(@$metaData)
        <title>{{ $metaData['title'] }}</title>
        <meta name="description" content="{{ $metaData['description'] }}" />

        <meta itemprop="name" content="{{ $metaData['title'] }}">
        <meta itemprop="description" content="{{ $metaData['description'] }}">
        <meta itemprop="image" content="{{ $metaData['image'] }}">

        <meta name="twitter:title" content="{{ $metaData['title'] }}">
        <meta name="twitter:description" content="{{ $metaData['description'] }}">
        <meta name="twitter:image:src" content="{{ $metaData['image'] }}">

        @if(@count($metaData['facebook']))
            <meta property="og:title" content="{{ $metaData['facebook']['title'] }}" />
            <meta property="og:description" content="{{ $metaData['facebook']['description'] }}" />
        @else
            <meta property="og:title" content="{{ $metaData['title'] }}" />
            <meta property="og:description" content="{{ $metaData['description'] }}" />
        @endif

        <meta property="og:url" content="{{ Request::url() }}" />
     
        <meta property="og:type" content="article" />
        <meta property="og:site_name" content="PawMaw" />
    @else
        <title>@yield('title')</title>

        <meta name="description" content="A brand new way helping lost pets to get back to their family. If you found or lost a pet report it on our website. Notify your neighbors within a minutes to alert them">
    @endif
		<meta name="p:domain_verify" content="088b12248871fe2491e7b0e326be9f12"/>

    <link rel="shortcut icon" type="image/png" href="{{ asset('pawmaw/img/fevi.png') }}">

    <!-- Roboto FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,400,500,700" rel="stylesheet" type="text/css">

    <!-- STYLES -->
    <link rel="stylesheet" href="{{ asset('pawmaw/css/datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/custom/pnotify/pnotify.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/all.min.css') }}">
    {{--<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">--}}
    <link href="https://use.fontawesome.com/releases/v5.0.1/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/additionalStyles.css') }}?{{ time() }}">

    @yield('seo_meta')

    <script src="{{ asset('pawmaw/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('pawmaw/js/exif.js') }}"></script>
    <script src="{{ asset('pawmaw/js/new-validationjqs.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
     <script src='https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js'></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126239303-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-126239303-1');
	</script>
	<script src="https://widget.thechecker.co/pk_f5owucuv2ll63dh2ymt.js"></script>
    {!! NoCaptcha::renderJs() !!}
</head>

<body class="{{ Request::path() ==  'pet' ? 'gray-bg-specific-page' : ''  }}" id="{{ Request::path() ==  'pet' ? 'lost_found_pop' : ''  }}">
    @yield('content')
    <!-- jQuery -->
    <script src="{{ asset('pawmaw/js/popper.min.js') }}"></script>
    <script src="{{ asset('pawmaw/js/bootstrap.min.js') }}"></script>
    {{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
    <script src="{{ asset('pawmaw/js/wow.min.js') }}"></script>
    <script src="{{ asset('pawmaw/js/main.js') }}"></script>
    <script src="{{ asset('pawmaw/js/home.js') }}"></script>
    <script src="{{ asset('pawmaw/js/htmltocanvas.js') }}"></script>
    <script src="{{ asset('pawmaw/libs/dom-to-image-master/dist/dom-to-image.min.js') }}"></script>
    <script src="{{ asset('/pawmaw/custom/pnotify/pnotify.js') }}"></script>
    <script src="{{ asset('/pawmaw/custom/pnotify/notify.js') }}"></script>
</body>

</html>
