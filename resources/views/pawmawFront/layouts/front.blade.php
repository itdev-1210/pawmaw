<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name=keywords content="lost pet, lost dog, lost cat, free lost pet alert,  free lost pet poster, find lost dog, find lost cat, find lost pet">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="canonical" href="{{url()->current()}}">

    @if(@$metaData)
        <title>{{ $metaData['title'] }}</title>
        <meta name="description" content="{{ $metaData['description'] }}" />

        <meta itemprop="name" content="{{ $metaData['title'] }}">
        <meta itemprop="description" content="{{ $metaData['description'] }}">
        <meta itemprop="image" content="{{ $metaData['image'] }}">

        <meta name="twitter:title" content="{{ $metaData['title'] }}">
        <meta name="twitter:description" content="{{ $metaData['description'] }}">
        <meta name="twitter:image:src" content="{{ $metaData['image'] }}">

        @if(@count($metaData['facebook']))
            <meta property="og:title" content="{{ $metaData['facebook']['title'] }}" />
            <meta property="og:description" content="{{ $metaData['facebook']['description'] }}" />
        @else
            <meta property="og:title" content="{{ $metaData['title'] }}" />
            <meta property="og:description" content="{{ $metaData['description'] }}" />
        @endif

        <meta property="og:url" content="{{ Request::url() }}" />
        <meta property="og:image" content="{{ $metaData['image'] }}" />
        <meta property="og:type" content="article" />
        <meta property="og:site_name" content="PawMaw" />
    @else
        <title>@yield('title')</title>

        <meta name="description" content="A brand new way helping lost pets to get back to their family. If you found or lost a pet report it on our website. Notify your neighbors within a minutes to alert them">
    @endif
		<meta name="p:domain_verify" content="088b12248871fe2491e7b0e326be9f12"/>

    <link rel="shortcut icon" type="image/png" href="{{ asset('pawmaw/img/fevi.png') }}">

    <!-- Roboto FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,400,500,700" rel="stylesheet" type="text/css">

    <!-- STYLES -->
    <!--<link rel="stylesheet" href="{{ asset('pawmaw/css/datepicker.css') }}">-->
    <!--<link rel="stylesheet" href="{{ asset('pawmaw/css/bootstrap.min.css') }}">-->
    <link rel="stylesheet" href="{{ asset('pawmaw/custom/pnotify/pnotify.css') }}">
    <!--<link rel="stylesheet" href="{{ asset('pawmaw/css/all.min.css') }}">-->
    {{--<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">--}}
    <!--<link rel="stylesheet" href="{{ asset('pawmaw/css/animate.css') }}">-->
    <link rel="stylesheet" href="{{ asset('pawmaw/css/style.css') }}?{{ time() }}">
    <!--<link rel="stylesheet" href="{{ asset('pawmaw/css/additionalStyles.css') }}?{{ time() }}">-->

    @yield('seo_meta')

    <script src="{{ asset('pawmaw/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('pawmaw/js/exif.js') }}"></script>
    <script src="{{ asset('pawmaw/js/new-validationjqs.js') }}?{{ time() }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src='https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js'></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126239303-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-126239303-1');
	</script>
	<!--<script src="https://widget.thechecker.co/pk_f5owucuv2ll63dh2ymt.js"></script>-->
	<!-- Global site tag (gtag.js) - Google Ads: 908508792 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-908508792"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'AW-908508792');
		function gtag_report_conversion(url) {
			var callback = function () {
				if (typeof(url) != 'undefined') {
				  window.location = url;
				}
			};
			gtag('event', 'conversion', {
		        'send_to': 'AW-908508792/ygPOCJqSzrcBEPj8mrED',
				 'event_callback': callback
			});
			return false;
		}
	</script>
	@yield('other_scripts')
    {!! NoCaptcha::renderJs() !!}
	
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '1748648162119518');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=1748648162119518&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->


</head>

<body class="{{ Request::path() ==  'pet' ? 'gray-bg-specific-page' : ''  }}" id="{{ Request::path() ==  'pet' ? 'lost_found_pop' : ''  }}">

    @include('pawmawFront.global.page_loader')

    <!-- NAVIGATION START -->
    @include('pawmawFront.global.header')
    <!-- NAVIGATION END -->

    @yield('content')

    <!-- FOOTER START -->
    @include('pawmawFront.global.footer')
    <!-- FOOTER END -->

    <!-- BACKTO TOP START -->
    <a href="#" class="scrollup" style="display: block;"><i class="fas fa-angle-double-up"></i></a>
    <!-- BACKTO TOP END -->

    <!-- HELP START -->
    <div class="help-wrap">
        <button type="button" class="help-wrap_btn" data-toggle="collapse" data-target="#helpModal">
        <i class="fas fa-question-circle"></i> <span class="d-xs-none">Help</span>
    </button>
    </div>
    <!-- HELP START -->

    <!-- Modal for quick help -->
    <div class="help-content collapse in" id="helpModal">
        <div class="help-content_inner">
            <div class="help-content_title mb-4">
                <h2>Help</h2>
            </div>
            <div class="help-content_item">
                <a href="tel:+1 909 764 0024" class="help-content_item-link">+1 909 764 0024</a>
                <a href="{{ url('/contact') }}" class="help-content_item-link">Contact Email</a>
                <a href="{{ url('/faqs') }}" class="help-content_item-link">FAQ</a>
            </div>
            <button class="close-x">X</button>
        </div>
    </div>
    <!-- Modal for quick help -->

    <!-- jQuery -->
    <script src="{{ asset('pawmaw/js/allscript.js') }}?{{ time() }}"></script>
    <!--<script src="{{ asset('pawmaw/js/popper.min.js') }}"></script>-->
    <!--<script src="{{ asset('pawmaw/js/bootstrap.min.js') }}"></script>-->
    {{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
    <!--<script src="{{ asset('pawmaw/js/wow.min.js') }}"></script>-->
    <!--<script src="{{ asset('pawmaw/js/main.js') }}?{{ time() }}"></script>-->
    <!--<script src="{{ asset('pawmaw/js/home.js') }}"></script>-->
    <!--<script src="{{ asset('pawmaw/js/htmltocanvas.js') }}"></script>-->
    <script src="{{ asset('pawmaw/libs/dom-to-image-master/dist/dom-to-image.min.js') }}"></script>
    <script src="{{ asset('/pawmaw/custom/pnotify/pnotify.js') }}"></script>
    <script src="{{ asset('/pawmaw/custom/pnotify/notify.js') }}"></script>
    @stack('js') 
    @if ($message = Session::get('error'))
        <script type="text/javascript">
            $(function() {
                $("#datepicker").datepicker();
            });

        </script>
    @endif

    <script>
        var map, infowindow, geocoder;
        /**
         * OMG...
         */
        function initAutocomplete() {
            geocoder = new google.maps.Geocoder();

            infowindow = new google.maps.InfoWindow();

            // Create the search box and link it to the UI element.
            var input1 = document.getElementById('pac-input');
            var input2 = document.getElementById('pac-input2');
            var input3 = document.getElementById('reportlostaddress');
            var input4 = document.getElementById('foundaddress');
            var searchBox = new google.maps.places.SearchBox(input1);
            var searchBox2 = new google.maps.places.SearchBox(input2);
            var searchBox3 = new google.maps.places.SearchBox(input3);
            var searchBox4 = new google.maps.places.SearchBox(input4);
            //  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.


            var markers1 = [];
            var markers2 = [];
            var componentForm = {
                street_number: 'short_name',
                route: 'long_name',
                locality: 'long_name',
                administrative_area_level_1: 'short_name',
                country: 'long_name',
                postal_code: 'short_name',
                sublocality_level_1: 'short_name'
            };

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                $('input[name=postal_code]').val('');
                $('input[name=street_number]').val('')


                map1 = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: -33.8688, lng: 151.2195},
                    zoom: 15,
                    height: 300
                });
                // map1.addListener('bounds_changed', function () {
                //     searchBox.setBounds(map1.getBounds());
                // });
                var places = searchBox.getPlaces();
                if (places.length == 0) {
                    return;
                }
                                
                var myAddress = places[0]["address_components"];
                var subloc = null;
                var loc = null;
                for (let i = 0; i < myAddress.length; i++) {
                    var aData = myAddress[i]['types'][0];
                    if(aData){
                        var val = myAddress[i][componentForm[aData]];
                        // omg...
                        if(aData == 'country'){
                            $('input[name=' + aData + ']').val(val);
                            $('input[name=' + aData + '_short]').val(myAddress[i]['short_name']);
                        } else if (aData == 'sublocality_level_1'){
                            subloc = val;
                        } else if(aData == 'sublocality_level_1') {
                            loc = val;
                            $('input[name=' + aData + ']').val(val);
                        }else {
                            $('input[name=' + aData + ']').val(val);
                        }
                    }
                }
                if(!loc && subloc){
                    $('input[name=locality]').val(subloc);
                }

                $('#lost-cross_street').val('');

                $('input[name=postal]').val('');

                let crossStreet = null;

                if($('input[name=street_number]').val()){
                    crossStreet = $('input[name=street_number]').val();
                }

                if(crossStreet){
                    crossStreet += ', ' + $('input[name=route]').val();
                } else{
                    crossStreet = $('input[name=route]').val();
                }

                $('#lost-cross_street').val(crossStreet); 

                $('input[name=postal]').val(
                    $('input[name=postal_code]').val()
                );       

                $('input[name="lat"]').val(places[0].geometry.location.lat);
                $('input[name="lng"]').val(places[0].geometry.location.lng);

                // Clear out the old markers.
                markers1.forEach(function (marker) {
                    marker.setMap(null);
                });
                markers1 = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    markers1.push(new google.maps.Marker({
                        map: map1,
                        position: place.geometry.location
                        // animation: google.maps.Animation.DROP
                    }));
                    // markers1.forEach(function (marker) {
                        // google.maps.event.addListener(marker, 'click', function () {
                        //     toggleBounce(marker);
                        //     infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
                        //         'Place ID: ' + place.place_id + '<br>' +
                        //         place.formatted_address + '</div>');
                        //     infowindow.open(map1, this);
                        // });

                        // google.maps.event.addListener(marker, 'dragend', function () {

                        //     geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                        //         if (status == google.maps.GeocoderStatus.OK) {

                        //             if (results[0]) {
                        //                 var myAddress = results[0]["address_components"];
                        //                 for (let i = 0; i < myAddress.length; i++) {
                        //                     var aData = myAddress[i]['types'][0];
                        //                     if (aData == "postal_code") {
                        //                         jQuery("#postal_2").val(myAddress[i]["long_name"]);
                        //                         break;
                        //                     }
                        //                 }

                        //                 jQuery('#pac-input').val(results[0].formatted_address);
                        //                 infowindow.setContent(results[0].formatted_address);
                        //                 infowindow.open(map1, marker);
                        //             }
                        //         }
                        //     });
                        // });
                    // });


                    // Create a marker for each place.


                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map1.fitBounds(bounds);

                $('.nearest-map').show();
            });


            searchBox2.addListener('places_changed', function () {
                $('input[name=postal_code].found-pet').val('');
                $('input[name=street_number].found-pet').val('')

                map2 = new google.maps.Map(document.getElementById('map2'), {
                    center: {lat: -33.8688, lng: 151.2195},
                    zoom: 15,
                });

                map2.addListener('bounds_changed', function () {
                    searchBox2.setBounds(map2.getBounds());
                });

                
                var places = searchBox2.getPlaces();


                if (places.length == 0) {
                    return;
                }
                var myAddress = places[0]["address_components"];
                var subloc = null;
                var loc = null;
                for (let i = 0; i < myAddress.length; i++) {
                    var aData = myAddress[i]['types'][0];

                    if(aData){
                        var val = myAddress[i][componentForm[aData]];

                        if(aData == 'country'){
                            $('input[name=' + aData + '].found-pet').val(val);
                            $('input[name=' + aData + '_short].found-pet').val(myAddress[i]['short_name']);
                        }else if (aData == 'sublocality_level_1'){
                            subloc = val;
                        } else if(aData == 'sublocality_level_1') {
                            loc = val;
                            $('input[name=' + aData + '].found-pet').val(val);
                        } else{
                            $('input[name=' + aData + '].found-pet').val(val);
                        }
                    }
                }
                if(!loc && subloc){
                    $('input[name=locality].found-pet').val(subloc);
                }

                $('.found_cross_street').val('');
                $('.zip-found').val('');

                let crossStreet = null;

                if($('input[name=street_number].found-pet').val()){
                    crossStreet = $('input[name=street_number].found-pet').val();
                }

                if(crossStreet){
                    crossStreet += ', ' + $('input[name=route].found-pet').val();
                } else{
                    crossStreet = $('input[name=route].found-pet').val();
                }

                $('.found_cross_street').val(crossStreet);    

                $('.zip-found').val(
                    $('input[name=postal_code].found-pet').val()
                );       

                $('input[name="lat"].found-pet').val(places[0].geometry.location.lat);
                $('input[name="lng"].found-pet').val(places[0].geometry.location.lng);

                // Clear out the old markers.
                markers2.forEach(function (marker) {
                    marker.setMap(null);
                });
                markers2 = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    markers2.push(new google.maps.Marker({
                        map: map2,
                        position: place.geometry.location,
                        animation: google.maps.Animation.DROP,
                        draggable: true,
                    }));
                    markers2.forEach(function (marker) {
                        google.maps.event.addListener(marker, 'click', function () {
                            toggleBounce(marker);
                            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
                                'Place ID: ' + place.place_id + '<br>' +
                                place.formatted_address + '</div>');
                            infowindow.open(map2, this);
                        });

                        google.maps.event.addListener(marker, 'dragend', function () {

                            geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {

                                    if (results[0]) {
                                        var myAddress = results[0]["address_components"];
                                        for (let i = 0; i < myAddress.length; i++) {
                                            var aData = myAddress[i]['types'][0];
                                            if (aData == "postal_code") {
                                                jQuery("#postal_3").val(myAddress[i]["long_name"]);
                                                break;
                                            }
                                        }

                                        jQuery('#pac-input2').val(results[0].formatted_address);
                                        infowindow.setContent(results[0].formatted_address);
                                        infowindow.open(map2, marker);
                                    }
                                }
                            });
                        });
                    });


                    // Create a marker for each place.


                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map2.fitBounds(bounds);

                $('#map2, .report-pet-map').show();
            });
            searchBox3.addListener('places_changed', function () {

                map99 = new google.maps.Map(document.getElementById('map99'), {
                    center: {lat: -33.8688, lng: 151.2195},
                    zoom: 15,
                });
                map99.addListener('bounds_changed', function () {
                    searchBox3.setBounds(map2.getBounds());
                });
                var places = searchBox3.getPlaces();


                if (places.length == 0) {
                    return;
                }
                var myAddress = places[0]["address_components"];
                for (let i = 0; i < myAddress.length; i++) {
                    var aData = myAddress[i]['types'][0];
                    if (aData == "postal_code") {
                        jQuery("#postal_39").val(myAddress[i]["long_name"]);
                        break;
                    }
                }
                // Clear out the old markers.
                markers2.forEach(function (marker) {
                    marker.setMap(null);
                });
                markers2 = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    markers2.push(new google.maps.Marker({
                        map: map99,
                        position: place.geometry.location,
                        animation: google.maps.Animation.DROP,
                        draggable: true,
                    }));
                    markers2.forEach(function (marker) {
                        google.maps.event.addListener(marker, 'click', function () {
                            toggleBounce(marker);
                            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
                                'Place ID: ' + place.place_id + '<br>' +
                                place.formatted_address + '</div>');
                            infowindow.open(map99, this);
                        });

                        google.maps.event.addListener(marker, 'dragend', function () {

                            geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {

                                    if (results[0]) {
                                        var myAddress = results[0]["address_components"];
                                        for (let i = 0; i < myAddress.length; i++) {
                                            var aData = myAddress[i]['types'][0];
                                            if (aData == "postal_code") {
                                                jQuery("#postal_39").val(myAddress[i]["long_name"]);
                                                break;
                                            }
                                        }

                                        jQuery('#pac-input2').val(results[0].formatted_address);
                                        infowindow.setContent(results[0].formatted_address);
                                        infowindow.open(map2, marker);
                                    }
                                }
                            });
                        });
                    });


                    // Create a marker for each place.


                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map2.fitBounds(bounds);


            });

            searchBox4.addListener('places_changed', function () {

                var places = searchBox4.getPlaces();


                if (places.length == 0) {
                    return;
                }
                var myAddress = places[0]["address_components"];
                for (let i = 0; i < myAddress.length; i++) {
                    var aData = myAddress[i]['types'][0];
                    if (aData == "postal_code") {
                        jQuery("#postal_3").val(myAddress[i]["long_name"]);
                        break;
                    }
                }


            });
        }

        function toggleBounce(marker) {
            if (marker.getAnimation() !== null) {
                marker.setAnimation(null);
            } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        }

    </script>
    <script type="text/javascript" async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBK0zK90-BoiWGflg9pXhBWF6DMwOzyTaU&amp;libraries=places&amp;callback=initAutocomplete&language=en"></script>
<!--<script src="https://forms.thechecker.co/5caf297e0bd2b4473742746c.js"></script>-->
    <script src="{{ asset('assets/js/notifications.js') }}"></script>
</body>

</html>
