<div id="login-overlay">
    <form id="login-form" role="form" method="POST" action="{{ url('/user/login') }}">
        {{ csrf_field() }}

        <span class="genericon genericon-close-alt" id="close-login"></span>
        <strong id="login-title">Login</strong>
        <div class="clear"></div>

        <div class="row{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email">Email <span class="required">*</span></label>
            <div class="inputWithIcon">
                <input id="email" type="email" name="email" id="login_email" placeholder="Email" value="{{ old('email') }}" autofocus>
                <span class="genericon genericon-mail"></span>
            </div>

            @if ($errors->has('email'))
                <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
            @endif
        </div>

        <div class="row{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password">Password <span class="required">*</span></label>
            <div class="inputWithIcon">
                <input id="password" type="password" name="password" id="login_password" placeholder="Password">
                <span class="genericon genericon-lock"></span>
            </div>

            @if ($errors->has('password'))
                <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
            @endif
        </div>

        <div class="clear"></div>
        <div class="login_checkbox-area">

            <div class="row left-row">
                <input type="checkbox" id="rememberMe" name="remember" value="Remember me" checked>
                <label for="rememberMe" class="rememberMe">Remember me</label>
            </div>
            <div class="clear"></div>
            <div class="row right-row">
                <button class="btn login-btn">Login</button>
            </div>

        </div>

        <div class="clear"></div>

        <div class="forget_btn-area">
            <div class="row left-row">
                <button class="btn forget-pass">
                    <a class="forget-pass-link" href="{{ route('resendactivation') }}">
                        Reset Activation Link
                    </a>
                </button>
            </div>

            <div class="row right-row">
                <button class="btn forget-pass">
                    <a class="forget-pass-link" href="{{ url('/user/password/reset') }}">
                        Forgot Password?
                    </a>
                </button>
            </div>
        </div>
        <div class="clear"></div>
    </form>
</div><!-- /.login-overlay -->


