<nav class="navbar navbar-expand-lg navbar-dark fixed-top @if(Request::segment(1) && Request::segment(2) !== 'login' && Request::segment(1) !== 'contact'){{'except-home'}}@endif" @if(Request::segment(1)=='' || Request::segment(2)=='login' || Request::segment(1)=='contact' ) id="mainNav" @endif>
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img class="img-fluid logo" src="{{ asset('pawmaw/img/logo.png') }}" alt="PawMaw">
        </a>
        
        <!--
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
-->
       
        <div class="hamburger d-none">
            <img class="img-fluid" src="{{ asset('pawmaw/img/hamburger-line.png') }}" alt="PawMaw">
            <img class="img-fluid" src="{{ asset('pawmaw/img/hamburger-line.png') }}" alt="PawMaw">
            <img class="img-fluid" src="{{ asset('pawmaw/img/hamburger-line.png') }}" alt="PawMaw">
        </div>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item" id="lost_found">
                    <a   class="nav-link {{ Request::path() ==  'lost-found-pets' ? 'active' : ''  }}" href="{{ url('/lost-found-pets') }}"><i class="fas fa-search"></i> Lost/Found</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::path() ==  'how-it-works' ? 'active' : ''  }}" href="{{ url('/how-it-works') }}"><i class="fas fa-cogs"></i> How it works</a>
                </li>
                @if (!Auth::guard('user')->user())
                <li class="nav-item">
                    <a class="nav-link {{ Request::path() ==  'user/login' ? 'active' : ''  }} || {{ Request::path() ==  'user/register' ? 'active' : ''  }}" href="{{ url('user/login') }}"><i class="fas fa-user-lock"></i> Join/Login</a>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link {{ Request::path() ==  'user/my-account' ? 'active' : ''  }}" href="{{ url('/user/my-account') }}"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
                </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link  report-outline {{ Request::path() ==  '/' ? 'active' : ''  }} active-form-button" href="{{ Request::path() ==  '/' ? 'javascript:void(0);' : url('/?active-form=1') }}">Report Pet</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

@if(@$_GET['active-form'] == 1)
    <script>
        $(document).ready(function(){
            $('#lost-name').focus();

            window.history.replaceState({}, document.title, "/");
        })
    </script>
@endif