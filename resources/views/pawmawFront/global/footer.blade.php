<footer class="footer">

    <!--   responsive div for bg =tab screen=  -->

    <div class="footer-top-md d-none" style="background-image: url({{ asset('pawmaw/img/footer-right.png') }}) , url({{ asset('pawmaw/img/footer-left.png') }}) , url({{ asset('pawmaw/img/footer-dog.png') }}) , url({{ asset('pawmaw/img/footer-line-bg.png') }});background-size: 130px, 130px, 570px, auto; background-position: right bottom, left bottom, center -4px, center 93px;background-repeat: no-repeat;"></div>

    <!--   responsive div for bg =desktop mini screen=  -->

    <div class="footer-top-lg d-none" style="background-image: url({{ asset('pawmaw/img/footer-right.png') }}) , url({{ asset('pawmaw/img/footer-left.png') }}) , url({{ asset('pawmaw/img/footer-dog.png') }}) , url({{ asset('pawmaw/img/footer-line-bg.png') }});background-size: 155px, 155px, 642px, auto;background-position: right bottom,left bottom,center -33px,center 77px;background-repeat: no-repeat;"></div>

    <!--   responsive div for bg =small screen=  -->

    <div class="footer-top-sm d-none" style="background-image: url({{ asset('pawmaw/img/footer-right.png') }}) , url({{ asset('pawmaw/img/footer-left.png') }}) , url({{ asset('pawmaw/img/footer-dog.png') }}) , url({{ asset('pawmaw/img/footer-line-bg.png') }});background-size: 70px, 70px, 472px, auto; background-position: right bottom, left bottom, center 21px, center 102px;background-repeat: no-repeat;"></div>

    <!--  desktop div for bg    -->

    <div class="footer-top" style="background-image: url({{ asset('pawmaw/img/footer-right.png') }}) , url({{ asset('pawmaw/img/footer-left.png') }}) , url({{ asset('pawmaw/img/footer-dog.png') }}) , url({{ asset('pawmaw/img/footer-line-bg.png') }});background-size: 166px,166px,contain,cover; background-position: right bottom,left bottom,center -10px,center 170px;"></div>
    <div class="footer-middle">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12 mb-sm-5 mb-xs-5 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
                    <img src="{{ asset('pawmaw/img/logo.png') }}" alt="PawMaw" class="footer-middle_logo img-fluid">
                    <p class="footer-middle_about">PawMaw is actively helping to search for your lost pets in your local area. Report your lost/found pets here and send a free alert instantly.</p>
                </div>
                <div class="col-lg-2 col-md-12 col-sm-12 mb-sm-5 mb-xs-5 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
                    <h3 class="footer-middle_quick-head">Quick links</h3>
                    <ul class="list-group border-0 footer-middle_quick-body">
                        <li class="list-group-item">
                            <a href="{{ url('blog') }}" class="footer-middle_quick-link"><i class="fas fa-angle-right"></i> Blog</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ url('faqs') }}" class="footer-middle_quick-link"><i class="fas fa-angle-right"></i> FAQ</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ url('about-us') }}" class="footer-middle_quick-link"><i class="fas fa-angle-right"></i> About us</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ url('contact') }}" class="footer-middle_quick-link"><i class="fas fa-angle-right"></i> Contact
                                us</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ url('/join-our-network') }}" class="footer-middle_quick-link"><i class="fas fa-angle-right"></i> Join our
                                network</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ url('privacy-policy') }}" class="footer-middle_quick-link"><i class="fas fa-angle-right"></i> Privacy
                                policy</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ url('refund-policy') }}" class="footer-middle_quick-link"><i class="fas fa-angle-right"></i> Refund
                                policy</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12 mb-sm-5 mb-xs-5 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">
                    <h3 class="footer-middle_touch-head">Get in touch</h3>
                    <ul class="list-group border-0 footer-middle_touch-body">
                        
                        <li class="list-group-item">
                            <a href="tel:+1 909 7640024" class="footer-middle_touch-link"><i class="fas fa-phone"></i> +1 909 764
                                0024</a>
                        </li>
                        <li class="list-group-item">
                            <a href="mailto:info@pawmaw.com" class="footer-middle_touch-link"><i class="fas fa-envelope"></i> info@pawmaw.com</a>
                        </li>
                        <li class="list-group-item">
                            <a href="mailto:sales@pawmaw.com" class="footer-middle_touch-link"><i class="fas fa-envelope"></i>
                                sales@pawmaw.com</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12 mb-sm-5 mb-xs-5 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".9s">
                    <div class="footer-middle-newsletter">
                        <h3 class="footer-middle-newsletter_head">Sign Up for Newsletter</h3>
                        <div class="input-group py-5">
                                <form role="form" action="{{ url('/newsletter/newsletterstore') }}" method="POST">
                                    @csrf
                            <input type="email" name="email" id="news_email" class="form-control" placeholder="Enter your mail" required>
                            <button type="submit" class="email-icon"><i class="fas fa-envelope"></i></button>
                       
                                </form>
                        </div>
                    </div>
                    <div class="social-links">
                        <ul>
                            <li><a class="facebook" href="https://www.facebook.com/PawMawOfficial" target="_blank"><i class="fab fa-facebook-square"></i></a></li>
                            <li><a class="twitter" href="https://twitter.com/PawMawOfficial" target="_blank"><i class="fab fa-twitter-square"></i></a></li>
                            <!-- <li><a class="instagram" href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a></li> -->
                            <li><a class="pinterest" target="_blank" href="https://www.pinterest.com/PawMawOfficial/"><i class="fab fa-pinterest"></i></a></li>
                            <li><a class="youtube" href="https://www.youtube.com/channel/UCnIlAOqCFQA6AMcWliy_hkw?view_as=subscriber" target="_blank"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p class="footer-bottom_copyright wow pulse" data-wow-duration="1s" data-wow-delay=".3s">
                        <a href="{{ url('/terms') }}" target="_blank" style="color: #fff;">Terms of our Service</a> | Copyright &copy;
                        <script>
                            document.write(new Date().getFullYear());

                        </script>&nbsp; <a href="{{ url('/') }}" style="color: #fff; text-decoration: underline;">PawMaw</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="fixing_view_point d-none">
        <div class="col-12">
            <div class="dashboard_link">
                <div class="crossi_con">
                    <i class="fas fa-times"></i>
                </div>
                <div class="link_items">
                    <ul class="user_nav">
                        <li class="user_nav-item">
                            <a href="{{ url('user/join_network') }}" class="user_nav-link @if(Request::segment(2)=='join_network'){{'active'}}@endif"><i class="fas fa-network-wired"></i> &nbsp; <span>Join our network</span></a>
                        </li>
                        <li class="user_nav-item">
                            <a href="{{ url('user/my-account') }}" class="user_nav-link @if(Request::segment(2)=='user/my-account'){{'active'}}@endif"><i class="fas fa-paw"></i> &nbsp; <span>My posted entry</span></a>
                        </li>
                        <li class="user_nav-item">
                            <a href="{{ url('/user/report') }}" class="user_nav-link @if(Request::segment(2)=='report'){{'active'}}@endif"><i class="fas fa-user-plus"></i> &nbsp; <span>Report another pet</span></a>
                        </li>

                        <li class="user_nav-item">
                            <a href="{{ url('/user/changePassword') }}" class="user_nav-link @if(Request::segment(2)=='changePassword'){{'active'}}@endif"><i class="fas fa-unlock"></i> &nbsp; <span>Change password</span></a>
                        </li>

                        @if(@$pet->id)
                            <li class="user_nav-item">
                                <a href="{{ url('/user/pet_alert/' . $pet->id) }}" class="user_nav-link @if(Request::segment(2)=='pet_alert'){{'active'}}@endif"><i class="fas fa-bell"></i> &nbsp; <span>My Pet Alert</span></a>
                            </li>

                            <li class="user_nav-item">
                                <a href="{{ url('/user/update_listing/' . $pet->id) }}" class="user_nav-link @if(Request::segment(2)=='update_listing'){{'active'}}@endif"><i class="fas fa-sync-alt"></i> &nbsp; <span>Update Listing</span></a>
                            </li>

                            <li class="user_nav-item">
                                <a href="{{ url('/user/faqs/' . $pet->id) }}" class="user_nav-link @if(Request::segment(2)=='faqs'){{'active'}}@endif"><i class="fas fa-unlock"></i> &nbsp; <span>FAQ</span></a>
                            </li>
                        @endif

                        <li class="user_nav-item">
                            <a href="{{ url('/user/logout') }}" onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();" class="user_nav-link"><i class="fas fa-question-circle"></i> &nbsp; <span>Logout</span></a>

                            <form id="logout-form" action="{{ url('/user/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>

                    </ul>
                </div>
            </div>

            @if (Auth::guard('user')->user() && (isset($showMoreOption) || isset($pet) ))
            <div class="footer_link">
                <div class="row justify-content-center">
                    @if(@$pet->id) 
                        <div class="col-3 m-0 p-0">
                            <div class="footer_link_items text-center">
                                <a href="/user/pet-details/{{ @$pet->slug }}">
                                    <i class="fas fa-cat"></i> <h3>Pet Info</h3>
                                </a>
                            </div>
                        </div>

                        <div class="col-3 m-0 p-0">
                            <div class="footer_link_items text-center">
                                <a href="/user/alerts/{{ @$pet->id }}">
                                    <i class="far fa-bell"></i> <h3>Alert</h3>
                                </a>
                            </div>
                        </div>

                        <div class="col-3 m-0 p-0">
                            <div class="footer_link_items text-center">
                                <a href="/user/download-flyer/{{ @$pet->id }}">
                                    <i class="fas fa-file-download"></i> <h3>Flyer</h3>
                                </a>
                            </div>
                        </div>
                    @endif
					
                    <div class="col-3 m-0 p-0">
                        <div class="footer_link_items text-center" id="more_btn" style="cursor:pointer">
                            <i class="fas fa-ellipsis-h"></i>
                            <a href="#" style="pointer-events: none; cursor: default;">
                                <h3>More</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            @endif

        </div>
    </div>

</footer>
