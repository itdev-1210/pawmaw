@extends('pawmawFront.layouts.front')

@section('title', 'Contact Us - PawMaw')

@section('content')
 <!-- ABOUT HERO SECTION START -->
<section class="contact">

<!--    <div class="contact-bg" data-type="parallax" data-speed="-2" style="background-image: url({{ asset('pawmaw/img/common-bg.png')}});background-position: left -60px;background-size: cover;"></div>-->
   
   
    <!--  mobile version -->
    
    
    
    <div class="section-dog-bg-mb d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 109px,center;background-size: 262px,cover,cover;background-repeat: no-repeat;"></div>

    <!--  landscape version -->
    
    
    <div class="section-dog-bg-sm d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 125px,center;background-size: 430px,cover,cover;background-repeat: no-repeat;"></div>


    <!--  tab version -->
    
    <div class="section-dog-bg-md d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 182px,center; background-size: 600px,cover,cover;background-repeat: no-repeat;"></div>



    <!--  full screen version -->
    <div class="section-dog-bg" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}}); background-position: center bottom,center 213px,center; background-size: 820px,cover,cover;"></div>

    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-8 offset-sm-2">
                <div class="others-page-title mx-auto d-block mb-5">
                    <h1>
                        <img src="{{ asset('pawmaw/img/icons/hand-black.png')}}" alt="Hand" class="img-fluid"> <span>Contact us</span> <img src="{{ asset('pawmaw/img/icons/hand-black.png')}}" alt="Hand" class="img-fluid">
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="contact-form">
                    <h2 class="contact-title">
                        <span>Write us:</span>
                    </h2>
                    @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                    <form id="contact-form" role="form" action="{{ url('/contact/contactstore') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="name" id="name" tabindex="1"
                                   class="form-control" placeholder="Name">
                        </div>


                        <div class="form-group">
                            <input type="text" name="phone" id="phone" tabindex="1"
                                   class="form-control" placeholder="Phone">
                        </div>

                        <div class="form-group">
                            <input type="email" name="email" id="email" tabindex="2"
                                   class="form-control" placeholder="Email*">
                        </div>

                        <div class="form-group">
                            <input type="text" name="address" id="name" tabindex="1"
                                   class="form-control" placeholder="Address">
                        </div>

                        <div class="form-group">
                            <textarea name="message" id="message" tabindex="2"
                                      class="form-control contact-form_textarea" rows="3" placeholder="Message"></textarea>
                        </div>
                  
                        <div class="clearfix"></div>

                        <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                            <div class="col-md-6" style="padding-left:0px">
                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="help-block" style="color: #dd2929 !important;">
                                        {{ $errors->first('g-recaptcha-response') }}
                                    </span>
                                @endif
                                <span class="help-block" id="capchaerror" style="color: #dd2929 !important;">
                                        
                                </span>
                            </div>
                        </div>
                        <button
							type="button"
							class="email-finder-btn contactbtn g-recaptcha"
							data-sitekey="{{env('INVISIBLE_RECAPTCHA_SITEKEY')}}"
							data-callback="onSubmit"
							data-badge="inline"
						>
							Send
						</button>



                    </form>
                </div>
            </div>
            <div class="col-lg-1 col-md-1 d-xs-none">
                <div class="vertical-line my-5"></div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12">
                <div class="contact-touch">
                    <h2 class="contact-title">
                        <span>Get in touch</span>
                    </h2>
                    <ul class="list-group border-0 contact-touch_body">
                        
                        <li class="list-group-item">
                            <a href="tel:+1 909 764 0024" class="contact-touch_link"><i class="fas fa-phone-square"></i> +1 909 764 0024</a>
                        </li>
                        <li class="list-group-item">
                            <a href="mailto:info@pawmaw.com" class="contact-touch_link"><i class="fas fa-envelope"></i> info@pawmaw.com</a>
                        </li>
                        <li class="list-group-item">
                            <a href="mailto:sales@pawmaw.com" class="contact-touch_link"><i class="fas fa-envelope"></i>
                                sales@pawmaw.com</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-5">
                <div class="contact-faq">
                    <p>If you have any questions or concerns, just let us know. Also, you can visit our <a href="{{ url('faqs') }}">FAQ</a> as many questions can be answered there quickly.</p>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">&nbsp;</div>
            <div class="col-lg-5 col-md-5 col-sm-5">
                <div class="contact-eye">
                    <figure><img src="{{ asset('pawmaw/img/contact-eye.png')}}" alt="Contact Eye" class="img-fluid"></figure>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ABOUT HERO SECTION END -->

<!-- GET YOUR PET BACK HOME SECTION START -->
<section class="getyourpet">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="getyourpet-title wow swing" data-wow-duration="1s" data-wow-delay=".3s">Get your pet back home</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <a href="{{ url('/?active-form=1') }}" class="getyourpet-btn mx-auto d-block wow bounceIn" data-wow-duration="1s" data-wow-delay=".3s">Report lost pet</a>
            </div>
        </div>
    </div>
</section>
<!-- GET YOUR PET BACK HOME SECTION END -->

<script>

function onSubmit(token) {
	$("#contact-form").submit();
}
/*
 $(".contactbtn").click(function() {
	var v = grecaptcha.getResponse();
	if(v.length == 0)
    {   
        document.getElementById('capchaerror').innerHTML="You can't leave Captcha Code empty";
        return false;
    }
    else
    {
        document.getElementById('capchaerror').innerHTML="";
        $("#contact-form").submit();
    }

});
*/
</script>

@endsection

