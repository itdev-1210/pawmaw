<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name=keywords content="lost pet, lost dog, lost cat, free lost pet alert,  free lost pet poster, find lost dog, find lost cat, find lost pet">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="canonical" href="https://www.pawmaw.com/">
        <title>{{ $metaData['title'] }}</title>
        <meta name="description" content="{{ $metaData['description'] }}" />

        <meta itemprop="name" content="{{ $metaData['title'] }}">
        <meta itemprop="description" content="{{ $metaData['description'] }}">
        <meta itemprop="image" content="{{ $metaData['image'] }}">

        <meta name="twitter:title" content="{{ $metaData['title'] }}">
        <meta name="twitter:description" content="{{ $metaData['description'] }}">
        <meta name="twitter:image:src" content="{{ $metaData['image'] }}">

        <meta property="og:url"                content="Request::url()" />
        <meta property="og:type"               content="article" />
        <meta property="og:title"              content="{{ $metaData['title'] }}" />
        <meta property="og:description"        content="{{ $metaData['description'] }}" />
        <meta property="og:image"              content="{{ $metaData['image'] }}" />
        
        <meta property="og:url" content="{{ Request::url() }}" />
     
        <meta property="og:type" content="article" />
        <meta property="og:site_name" content="PawMaw" />
    </head>
<body>
    {{$details}}
</body>    
</html>    