@extends('pawmawFront.layouts.front') @section('title', 'PawMaw - Lost and Found Cats, Dogs, and Pets Rescue Service') @section('content')
<script src="https://forms.thechecker.co/5caf297e0bd2b4473742746c.js"></script>

<!-- HEADER START -->
<header class="masthead">

    <!-- HEADER BACKGROUND FOR DESKTOP -->
    <div class="bg-video d-xs-none">
        <div class="bg-overly"></div>
        <video class="bg-video__content" autoplay muted loop>
            <source src="{{ asset('pawmaw/img/header_bg.mp4') }}" type="video/mp4">
            <!--<source src="{{ asset('pawmaw/img/header_bg.mp4') }}" type="video/webm">-->
            Your browser is not supported!
        </video>
    </div>
    <!-- HEADER BACKGROUND FOR DESKTOP -->

    <div class="container">
        <div class="row justify-content-between">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="hero-attention d-xs-none">
                    <h1 class="hero-attention_title">Is your pet lost?</h1>
                    <ul class="list-group border-0">
                        <li class="list-group-item"><span><i class="fas fa-clipboard-list"></i></span> Create free listing
                        </li>
                        <li class="list-group-item"><span><i class="fas fa-paper-plane"></i></span> Send free alert instantly
                        </li>
                        <li class="list-group-item"><span><i class="far fa-plus-square"></i></span> Create free lost/found flyer
                        </li>
                        <li class="list-group-item"><span><i class="fas fa-users"></i></span> Reach more people in your local area
                        </li>
                    </ul>
                </div>
                <div class="hero-attention_mobile d-xl-none">
                    <h2 class="hero-attention_mobile-title">Is your pet lost?</h2>
                    <p class="hero-attention_mobile-text">Create free listing &amp; Send free alert instantly.</p>
                </div>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <div class="hero-form_wrap">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger alert-no-border alert-txt-colored alert-close alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        <strong>Whoops!</strong> There were some problems with your input.
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                   
                    @if(isset($_GET['invalidemail']))
                    <div class="alert alert-danger alert-no-border alert-txt-colored alert-close alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        <strong>Whoops!</strong> There were some problems with your input.
                        <ul>
                            
                            <li>Please Enter valid email address.</li>
                            
                        </ul>
                    </div>
                    @endif
                    <div class="panel panel-login hero-form_wrap-content">
                        <div class="panel-heading">
                            <div class="row no-gutters">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <a href="#" class="active float-left" id="lost-form-link">
                                        <div class="form-check form-check-inline m-0">
                                            <input class="form-check-input " type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" checked>
                                        </div>
                                        Lost pet
                                    </a>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <a href="#" class="float-right" id="found-form-link">
                                        <div class="form-check form-check-inline m-0">
                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option2">
                                        </div>
                                        Found pet
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body py-5">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form 
                                        id="lost-form" 
                                        role="form" 
                                        action="{{ !auth()->user() ? url('/user/register') : url('/user/report-store') }}" 
                                        enctype="multipart/form-data" 
                                        method="post" 
                                        style="display: block;">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="request" value="1">
                                        <input type="hidden" name="action" value="lost">
                                        <div class="form-group">
                                            <input type="text" name="name" id="lost-name" tabindex="1" class="form-control input-ctrl" placeholder="Write your pet name" required>
                                        </div>

                                        {{--
                                        <div class="form-group file-upload">--}} {{--
                                            <div class="file">--}} {{--
                                                <label for="file" class="custom-file-upload">--}}
                                                    {{--Choose Photo--}}
                                                {{--</label>--}} {{--
                                                <input type="file" class="form-control-file" id="lost_preview" name="pet_photo" required="" />--}} {{--
                                            </div>--}} {{--
                                            <img id="img-upload-lost">--}} {{--
                                        </div>--}}

                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                            <span class="btn btn-default btn-file report_upload-title">
                                                                choose photo <input type="file" style="width:0%;height:0%" name="pet_photo" id="lost_preview" required>
                                                            </span>
                                                </span>
                                                <input type="text" class="form-control report_upload-bg report_upload-bg-lost report-another_lost-tag" readonly> {{--
                                                <div class="preview_img" style="background-image: url({{ asset('pawmaw/img/contact-eye.png')}});"></div>--}}
                                                <div class="preview_img" id="lost_preview-img"></div>
                                               
                                            </div>
                                        </div>

                                      {{--   <div class="image-preview" style="width: 300px; height: 300px;">
                                            <img src="" id="test-image" alt="" style="width: 300px; height: 300px;">
                                        </div> --}}

                                        <div class="form-group">
                                            <input type="text" name="address" id="pac-input" tabindex="1" class="form-control input-ctrl" placeholder="Nearest last seen area" onfocus="initAutocomplete();" autocomplete="off" required>
                                        </div>
                                        <div id="fillinaddress1">
                                            <input 
                                                type="hidden" 
                                                name="street_number" 
                                                class="route">
                                            <input 
                                                type="hidden" 
                                                name="route" 
                                                class="route">
                                            <input 
                                                type="hidden" 
                                                name="sublocality_level_1" 
                                                class="sublocality_level_1">
                                            <input 
                                                type="hidden" 
                                                name="locality" 
                                                class="locality">
                                            <input 
                                                type="hidden" 
                                                name="administrative_area_level_3" 
                                                class="administrative_area_level_3">
                                            <input 
                                                type="hidden" 
                                                name="administrative_area_level_2" 
                                                class="administrative_area_level_2">
                                            <input 
                                                type="hidden" 
                                                name="administrative_area_level_1" 
                                                class="administrative_area_level_1">
                                            <input 
                                                type="hidden" 
                                                name="country" 
                                                class="country">
                                            <input 
                                                type="hidden" 
                                                name="country_short" 
                                                 id="country_short"
                                                class="country" required>
                                            <input 
                                                type="hidden" 
                                                name="postal_code" 
                                                class="postal_code">
                                            <input 
                                                type="hidden" 
                                                name="lat" 
                                                class="postal_code">
                                            <input 
                                                type="hidden" 
                                                name="lng" 
                                                class="postal_code">
                                        </div>
                                        <div class="form-group">
                                            <div class="nearest-map" id="map"><span>Map</span></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group lost-cross_street">
                                                        <input type="text" name="street" id="lost-cross_street" tabindex="2" class="form-control input-ctrl" placeholder="Cross street" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group lost-zip">
                                                        <input type="text" name="postal" id="postal_2" tabindex="3" class="form-control input-ctrl" placeholder="Zip" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if(!auth()->user())
                                            <div class="form-group">
                                                <input type="email" name="email" id="lost-email" tabindex="4" class="form-control input-ctrl" placeholder="Email" required>
                                            </div>
                                          
                                            <div class="form-group">
                                                <input type="password" name="password" id="lost-password" tabindex="5" class="form-control input-ctrl" placeholder="password" required>
                                            </div>
                                        @endif
                                        
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-8 mb-sm-5 mb-xs-5">
                                                    <div class="form-group">
                                                        <input type="text" name="phone" id="phone" tabindex="6" class="form-control input-ctrl" placeholder="Phone" {{ !auth()->user() ? 'required' : '' }}>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group form-group_next">
                                                        <button type="submit" class="next-btn">Next <i
                                                                class="fas fa-angle-double-right"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <form 
                                        id="found-form" 
                                        role="form" 
                                        action="{{ !auth()->user() ? url('/user/register') : url('/user/report-store') }}"
                                        enctype="multipart/form-data" 
                                        method="post" 
                                        style="display: none;">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="request" value="1">
                                        <input type="hidden" name="action" value="found">
                                        <div class="form-group">
                                            <input type="text" name="name" id="found_name" tabindex="1" class="form-control input-ctrl" placeholder="Unknown name">
                                        </div>

                                        {{--
                                        <div class="form-group file-upload">--}} {{--
                                            <div class="file">--}} {{--
                                                <label for="file" class="custom-file-upload">--}}
                                                    {{--Choose Photo--}}
                                                {{--</label>--}} {{--
                                                <input type="file" class="form-control-file" id="found_preview" name="pet_photo" required="" />--}} {{--
                                            </div>--}} {{--
                                            <img id="img-upload-lost">--}} {{--
                                        </div>--}}

                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                            <span class="btn btn-default btn-file report_upload-title">
                                                                choose photo <input type="file" style="width:0%;height:0%" name="pet_photo" id="found_preview" required>
                                                            </span>
                                                </span>
                                                <input type="text" class="form-control report_upload-bg report_upload-bg-lost report-another_lost-tag" readonly>
                                                <!--                                                <img id='found_preview-img' />-->

                                                <div class="preview_img" id="found_preview-img"></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <input type="text" name="address" id="pac-input2" tabindex="2" class="form-control input-ctrl" placeholder="Nearest last seen area" onfocus="initAutocomplete();" autocomplete="off" required>
                                        </div>
                                        <div class="form-group">
                                            <div class="nearest-map" id="map2"><span>Map</span></div>
                                        </div>
                                        <div id="fillinaddress1">
                                            <input 
                                                type="hidden" 
                                                name="street_number" 
                                                class="route found-pet">
                                            <input 
                                                type="hidden" 
                                                name="route" 
                                                class="route found-pet">
                                            <input 
                                                type="hidden" 
                                                name="sublocality_level_1" 
                                                class="sublocality_level_1 found-pet">
                                            <input 
                                                type="hidden" 
                                                name="locality" 
                                                class="locality found-pet">
                                            <input 
                                                type="hidden" 
                                                name="administrative_area_level_3" 
                                                class="administrative_area_level_3 found-pet">
                                            <input 
                                                type="hidden" 
                                                name="administrative_area_level_2" 
                                                class="administrative_area_level_2 found-pet">
                                            <input 
                                                type="hidden" 
                                                name="administrative_area_level_1" 
                                                class="administrative_area_level_1 found-pet">
                                            <input 
                                                type="hidden" 
                                                name="country" 
                                                class="country found-pet">
                                            <input 
                                                type="hidden" 
                                                name="country_short" 
                                                id="country_short"
                                                class="country found-pet" required>
                                            <input 
                                                type="hidden" 
                                                name="postal_code" 
                                                class="postal_code found-pet">
                                            <input 
                                                type="hidden" 
                                                name="lat" 
                                                class="postal_code found-pet">
                                            <input 
                                                type="hidden" 
                                                name="lng" 
                                                class="postal_code found-pet">
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="form-group cross_street">
                                                        <input type="text" name="street" id="cross_street" tabindex="2" class="form-control input-ctrl found-pet found_cross_street" placeholder="Cross street" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group zip">
                                                        <input type="text" name="postal" id="zip" tabindex="3" class="form-control input-ctrl found-pet zip-found" placeholder="Zip" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if(!auth()->user())
                                            <div class="form-group">
                                                <input type="email" name="email" id="email" tabindex="4" class="form-control input-ctrl" placeholder="Email" required>
                                            </div>
                                     
                                            <div class="form-group">
                                                <input type="password" name="password" id="found-password" tabindex="5" class="form-control input-ctrl" placeholder="password" required>
                                            </div>
                                        @endif
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-8 mb-sm-5 mb-xs-5">
                                                    <div class="form-group">
                                                        <input type="text" name="phone" id="phone" tabindex="6" class="form-control input-ctrl" placeholder="Phone" {{ !auth()->user() ? 'required' : '' }}>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group form-group_next">
                                                        <button 
                                                            type="submit" 
                                                            class="next-btn">
                                                                Next 
                                                                <i
                                                                class="fas fa-angle-double-right"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- HEADER END -->

<!-- FEATURED SECTION START -->
<section class="featured">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="featured-content">
                    <h2 class="featured-content_title">Featured Post</h2>
                    <h2 class="featured-content_title-sub text-center">When you report a lost/found pet your post will appeared here as featured post</h2>
                </div>
                <div class="row py-3 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
                    @foreach($pets as $key=>$row)
                    <div class="col-lg-4 col-md-4 col-sm-6 mb-sm-5 mb-xs-5">
                        <div class="card featured-box">
                            <a href="{{ url('/pet-details') }}/{{$row->slug}}" class="zoomIn">
                                <figure style="background-image: url({{ url('uploads/pets_image') }}/{{$row->photo}});background-size: contain;background-position: center;background-repeat: no-repeat;">
<!--
                                    @if(file_exists( public_path().'/uploads/pets_image/'.$row->photo ) && $row->photo!='')
                                    <img class="card-img-top" src="{{ url('uploads/pets_image') }}/{{$row->photo}}" alt="{{$row->name}}"> @else
                                    <img class="card-img-top" src="{{ url('pawmaw/img/default.png') }}" alt="{{$row->name}}"> @endif
-->
                                </figure>
                            </a>
                            <div class="card-body">
                                <div class="featured-box_content">
                                    <h2 class="card-title ">{{$row->name}}</h2>
                                    <span class="tag-{{$row->type}} ">{{ucfirst($row->type)}} {{$row->specie}}</span>
                                </div>
                                <div class="clearfix"></div>
                                <div class="featured-box_details">
                                    <h2>Gender: {{$row->gender}}</h2>
                                    <p class="card-text">{{$row->last_seen}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <!-- third 3 items -->
                </div>
            </div>
            <div class="col-lg-3">
                <div class="featured-network wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
                    <h2 class="featured-network_title">Join our network</h2>
                    <img class="card-img-top img-fluid my-5" src="{{ asset('pawmaw/img/icons/join.png') }}" alt="Join Network Icon">
                    <div class="form-group has-search mx-auto d-block">
                        <span class="form-control-zip"><i class="fas fa-search-location"></i></span>
                        <input type="text" class="form-control" placeholder="ZIP Code" id="move-to-search-zip-code">
                        <button onclick="location.href = '/join-our-network?zip_code=' + $('#move-to-search-zip-code').val();" type="submit" class="featured-network_code-btn zip-code_btn mx-auto d-block">Search</button>
                    </div>


                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2&appId=1863631387098360&autoLogAppEvents=1';
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
<center>
    <div class="fb-like" data-href="https://www.facebook.com/PawMawOfficial/" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>              <img class="img-fluid mx-auto d-block" src="{{ asset('pawmaw/img/home/join-pet.png') }}" alt="Joining Pet">

</center>
              </div>
            </div>
        </div>
    </div>
</section>
<!-- FEATURED SECTION END -->

<!-- HOW IT WORKS SECTION START -->
<section class="howitworks">
    <h2 class="howitworks-title s-title mb-5">How it works</h2>
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="card howitworks-box wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">

                    <div class="animational_img animational_img_one">
                        <img src="{{ asset('pawmaw/img/home/10.png') }}" alt="Report Pet" class="card-img-top howitworks-box_report-img img-fluid mx-auto d-block">
                        <div class="animation">
                            <img src="{{ asset('pawmaw/img/home/animation-shape-1.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/animation-shape-2.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/animation-shape-3.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/animation-shape-4.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/animation-shape-5.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/animation-shape-6.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/line-animation-box.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/1.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/2.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/3.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/4.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/5.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/7.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/9.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/4.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/4.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/4.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/8.png') }}" alt="line">
                        </div>
                    </div>
                    <div class="card-body">
                        <a href="#" class="howitworks-box_btn howitworks-box_report-btn mx-auto d-block">Report a
                            pet</a>
                        <h2 class="card-title">Report</h2>
                        <p class="card-text">
                            Tell us about your lost/found pet; by posting a shout out on our website. By that, you will get concerned people in your neighborhoods right on the spot to help search for your pet.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card howitworks-box wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">

                    <div class="animational_img animational_img_two">
                        <img class="card-img-top howitworks-box_promote-img img-fluid mx-auto d-block" src="{{ asset('pawmaw/img/home/work_img-2.png') }}" alt="Promote Pet">
                        <div class="animation">
                            <img src="{{ asset('pawmaw/img/home/promo-shape-1.png') }}" alt="promote-img">
                            <img src="{{ asset('pawmaw/img/home/promo-shape-2.png') }}" alt="promote-img">
                            <img src="{{ asset('pawmaw/img/home/promo-shape-3.png') }}" alt="promote-img">
                            <img src="{{ asset('pawmaw/img/home/promo-shape-1.png') }}" alt="promote-img">
                            <img src="{{ asset('pawmaw/img/home/promo-shape-3.png') }}" alt="promote-img">
                            <img src="{{ asset('pawmaw/img/home/horn_img.png') }}" alt="promote-img">
                        </div>
                    </div>
                    <div class="card-body">
                        <a href="#" class="howitworks-box_btn howitworks-box_promote-btn mx-auto d-block">Promote</a>
                        <h2 class="card-title">PROMOTE</h2>
                        <p class="card-text">
                            Pawmaw has a feature that called PawMaw Alert – by activating the alert you can make your search effort highly effective and reach thousands of people to the residents of the area where your pet went missing.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card howitworks-box wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">
                    <div class="animational_img">
                        <img class="card-img-top howitworks-box_reunited-img img-fluid mx-auto d-block" src="{{ asset('pawmaw/img/home/work_img-3.png') }}" alt="reunited Pet">
                        <div class="animation">
                            <img src="{{ asset('pawmaw/img/home/love_shape.png') }}" alt="love-shape">
                            <img src="{{ asset('pawmaw/img/home/love_shape.png') }}" alt="love-shape">
                            <img src="{{ asset('pawmaw/img/home/love_shape.png') }}" alt="love-shape">
                            <img src="{{ asset('pawmaw/img/home/love_shape.png') }}" alt="love-shape">
                        </div>
                    </div>
                    <div class="card-body">
                        <a href="#" class="howitworks-box_btn howitworks-box_reunited-btn mx-auto d-block">REUNITED </a>
                        <h2 class="card-title">reunited</h2>
                        <p class="card-text">
                            We are known for what we do! Every day we help distressed pet owners to find their lost, scared, and often hungry pets. And it has always been an atmosphere of cheers and happiness, every time we reunited with their beloved one.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3 py-5">
                <a href="/how-it-works" class="howitworks-more_btn mx-auto d-block wow bounceIn" data-wow-duration="1s" data-wow-delay=".4s">Learn more <i class="fas fa-arrow-right"></i></a>
            </div>
        </div>
    </div>
</section>
<!-- HOW IT WORKS SECTION END -->

<!-- OUR BLOGS SECTION START -->
<section class="ourblogs">
    <h2 class="ourblogs-title s-title mb-5">Our blogs <i class="fas fa-pen-alt"></i></h2>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h2 class="ourblogs-title_sub ss-title">Check Out Our Recent Blog/Articles and Lost/Found Tips & More...
                </h2>
            </div>
        </div>
        <div class="row">
            @foreach($posts as $post)
                <div class="col-lg-4 col-md-6 col-sm-12 mb-sm-5 mb-xs-5">
                    <div class="card ourblogs-box wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
                        <a href="{{ url('/blog/' . $post['slug']) }}" class="zoomOut">
                            <figure>
                                <img class="card-img-top ourblogs-box_item-img img-fluid mx-auto d-block" src="/img/{{ $post['image'] }}" alt="Blog Item Images">
                            </figure>
                        </a>
                        <div class="card-body">
                            <a href="{{ url('/blog/' . $post['slug']) }}" class="card-title">{{ $post['title'] }}</a>
                            <p class="card-text">
                                {{ $post['excerpt'] }}
                            </p>
                            <a href="{{ url('/blog/' . $post['slug']) }}" class="ourblogs-box_btn">Read more</a>
                        </div>
                    </div>
                </div>
            @endforeach
            {{-- <div class="col-lg-4 col-md-6 col-sm-12 mb-sm-5 mb-xs-5">
                <div class="card ourblogs-box wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
                    <a href="#" class="zoomOut">
                        <figure>
                            <img class="card-img-top ourblogs-box_item-img img-fluid mx-auto d-block" src="{{ asset('pawmaw/img/home/blog-items.png') }}" alt="Blog Item Images">
                        </figure>
                    </a>
                    <div class="card-body">
                        <a href="#" class="card-title">TOP 10 WAYS TO FIND YOUR MISSING CATS AND DOGS.</a>
                        <p class="card-text">
                            Losing our beloved Cats and Dogs is always traumatic. It doesn’t hurt an inch less than losing a family member. Losing your pets is scary. You will ... ...become worried, fear for their current well-being. But rather than drowning in pain, you should start searching for your.
                        </p>
                        <a href="#" class="ourblogs-box_btn">Read more</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 mb-sm-5 mb-xs-5">
                <div class="card ourblogs-box wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
                    <a href="#" class="zoomOut">
                        <figure>
                            <img class="card-img-top ourblogs-box_item-img img-fluid mx-auto d-block" src="{{ asset('pawmaw/img/home/blog-items.png') }}" alt="Blog Item Images">
                        </figure>
                    </a>
                    <div class="card-body">
                        <a href="#" class="card-title">TOP 10 WAYS TO FIND YOUR MISSING CATS AND DOGS.</a>
                        <p class="card-text">
                            Losing our beloved Cats and Dogs is always traumatic. It doesn’t hurt an inch less than losing a family member. Losing your pets is scary. You will ... ...become worried, fear for their current well-being. But rather than drowning in pain, you should start searching for your.
                        </p>
                        <a href="#" class="ourblogs-box_btn">Read more</a>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
</section>
<!-- OUR BLOGS SECTION END -->

<!-- GET YOUR PET BACK HOME SECTION START -->
<section class="getyourpet">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="getyourpet-title wow swing" data-wow-duration="1s" data-wow-delay=".3s">Get your pet back home</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <a href="{{ url('/?active-form=1') }}" class="getyourpet-btn mx-auto d-block wow bounceIn" data-wow-duration="1s" data-wow-delay=".3s">Report lost pet</a>
            </div>
        </div>
    </div>
</section>
<!-- GET YOUR PET BACK HOME SECTION END -->


@endsection


