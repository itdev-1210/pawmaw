@extends('pawmawFront.layouts.front')

@section('title', 'Terms - PawMaw')

@section('content')
<!-- ABOUT HERO SECTION START -->
<section class="refound-policy">
<!--    <div class="section-dog-bg"style="background-image: url({{ asset('pawmaw/img/common-bg.png')}});background-position: left -60px;background-size: cover;"></div>-->
   
   
   
    <!--  mobile version -->
    
    
    
    <div class="section-dog-bg-mb d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 109px,center;background-size: 262px,cover,cover;background-repeat: no-repeat;"></div>

    <!--  landscape version -->
    
    
    <div class="section-dog-bg-sm d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 125px,center;background-size: 430px,cover,cover;background-repeat: no-repeat;"></div>


    <!--  tab version -->
    
    <div class="section-dog-bg-md d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 182px,center; background-size: 600px,cover,cover;background-repeat: no-repeat;"></div>



    <!--  full screen version -->
    <div class="section-dog-bg" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}}); background-position: center bottom,center 213px,center; background-size: 820px,cover,cover;"></div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-6  col-sm-8 ">
                <div class="refound-policy_title others-page-title  d-block mb-5">
                    <h1>
                        <img src="{{ asset('pawmaw/img/icons/hand-black.png')}}" alt="Hand" class="img-fluid"> <span>Terms of our service</span> <img src="{{ asset('pawmaw/img/icons/hand-black.png')}}" alt="Hand" class="img-fluid">
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="refound-policy_details py-5">
                    <p class="refound-policy_details-text mb-3" style="font-size:20px">
                       Kindly read the terms of services completely before creating a pet alert using <a href="https://www.pawmaw.com/" target="_blank" style="color:#df1d41;">www.pawmaw.com.</a> PawMaw.com is proudly powered and sponsored by pawmaw.com. This written agreement helps to put into the documentation, the legal rights governing our terms and conditions to be considered while carrying out any activities on <a href="https://www.pawmaw.com/" target="_blank" style="color:#df1d41;">www.pawmaw.com.</a>
                    </p>
                    <p class="refound-policy_details-text mb-3" style="font-size:20px">
                       Therefore, carrying out any activities on this website (accessing, viewing, browsing through the website, or creating your content on the site) means you have agreed and bound by the terms and conditions put in place. More so, the use of the website leaves you to conform to our privacy policies and other policies attached to <a href="https://www.pawmaw.com/" target="_blank" style="color:#df1d41;">www.pawmaw.com.</a>
                    </p>
                    <p class="refound-policy_details-text mb-3" style="font-size:20px">
                       <b>Note:</b>don’t purchase any of our services if you do not agree to be bound by our terms and conditions.
                    </p>
<!--
                    <h2 class="refound-policy_sub mb-3">SOME SITUATIONS ARE LISTED BELOW:</h2>
                    <div class="row">
                        <div class="col-lg-4 offset-lg-4 col-6-md offset-md-3 col-sm-8 offset-sm-2">
                            <ul class="refound-policy_details-list">
                                <li><i class="fas fa-angle-right"></i>  If we are unable to make Flyer/ Poster.</li>
                                <li><i class="fas fa-angle-right"></i>  If we are unable to notify your neighbor.</li>
                                <li><i class="fas fa-angle-right"></i>  If we are unable, send Paw Alert.</li>
                            </ul>
                        </div>
                    </div>
                    <p class="refound-policy_details-text mt-5">If you have any questions on how to return or refund, please contact us on <a href="#" class="pawInfo">info@PawMaw.com</a></p>
-->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h2 class="refound-policy_sub mt-5" style="font-size:24px; font-weight:600;">
                    Property rights
                </h2>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="refound-policy_details py-5">
                    <p class="refound-policy_details-text mb-3" style="font-size:20px">
                       The website and the entire original contents are the property of <a href="https://www.pawmaw.com/" target="_blank" style="color:#df1d41;">www.pawmaw.com.</a> And all these properties are well protected by international copyright and other legal property rights.
                    </p>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h2 class="refound-policy_sub mt-5" style="font-size:24px; font-weight:600;">
                    Account creation
                </h2>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="refound-policy_details py-5">
                    <p class="refound-policy_details-text mb-3" style="font-size:20px">
                       Opening account with us is an easy task. But to privately own an account with us, you must be at least 14 years of age. And perhaps, you know, anyone under the age of 14, who has an account with us, kindly report them to us.
                    </p>
                    <p class="refound-policy_details-text mb-3" style="font-size:20px">
                       More so, you take full responsibility for all the activities carried out with your account on our website. If a third party log into your account, you will be held responsible for the security of your account, and also for all the actions carried out on it. So if you feel your account is compromised, report to us.
                    </p>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h2 class="refound-policy_sub mt-5" style="font-size:24px; font-weight:600;">
                    Emails & Posting Permission
                </h2>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="refound-policy_details py-5">
                    <p class="refound-policy_details-text mb-3" style="font-size:20px">
                       By reporting a pet, you are giving us full rights and permission to post them on our Facebook page. Or other social media like<a href="#" target="_blank" style="color:#df1d41;">Twitter</a> , <a href="#" target="_blank" style="color:#df1d41;">Pinterest</a> , <a href="#" target="_blank" style="color:#df1d41;">Instagram</a> or other social media or anywhere else we so choose. When you just joining us you may receive notifications about lost & found pets near me. But there always an option to unsubscribe to emails. 
                    </p>
                    

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h2 class="refound-policy_sub mt-5" style="font-size:24px; font-weight:600;">
                    Termination
                </h2>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="refound-policy_details py-5">
                    <p class="refound-policy_details-text mb-3" style="font-size:20px">
                       <a href="https://www.pawmaw.com/" target="_blank" style="color:#df1d41;">www.pawmaw.com</a> has the right to deactivate or deny you access to your account without any form of awareness.
                    </p>
                    

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h2 class="refound-policy_sub mt-5" style="font-size:24px; font-weight:600;">
                    Cookie Policy
                </h2>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="refound-policy_details py-5">
                    <p class="refound-policy_details-text mb-3" style="font-size:20px">
                       We use cookies to improve your browsing experience and the functionality of our site.
                    </p>
                    <p class="refound-policy_details-text mb-3" style="font-size:20px">
                       If you’d like to delete cookies or instruct your web browser to delete or refuse cookies, please visit the help pages of your web browser.
                    </p>
                    <p class="refound-policy_details-text mb-3" style="font-size:20px">
                      Please note, however, that if you delete cookies or refuse to accept them, you might not be able to use all of the features we offer, you may not be able to store your preferences, and some of our pages might not display properly.
                    </p>
                    

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h2 class="refound-policy_sub mt-5" style="font-size:24px; font-weight:600;">
                    Legal rights
                </h2>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="refound-policy_details py-5">
                    <p class="refound-policy_details-text mb-3" style="font-size:20px">
                       The terms and conditions of pawmaw.com shall be governed by and are written in support with the laws of the United States of America, excluding reference to conflict of laws provisions, and to the rules guiding matters affecting copyrights, patents, and trademarks, by United States federal law. Therefore, if any claim, dispute or controversy arising in connection with the terms and conditions, it shall be resolved by the binding arbitration following the rules of the American Arbitration Association.
                    </p>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h2 class="refound-policy_sub mt-5" style="font-size:24px; font-weight:600;">
                    Change of Agreement
                </h2>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="refound-policy_details py-5">
                    <p class="refound-policy_details-text mb-3" style="font-size:20px">
                       <a href="https://www.pawmaw.com/" target="_blank" style="color:#df1d41;">www.pawmaw.com</a> holds the right to review and modify these terms and conditions at any time. This will be done via posting of contents that will draw our member's knowledge of the updated terms and conditions on the website. Then you are left to decide to continue accessing and making use of the site after the changes have been made. Choosing to keep-up with your activities on this website shows your formal acceptance of the update made to the terms and conditions.
                    </p>
                    <p class="refound-policy_details-text mb-3" style="font-size:20px">
                       Therefore, we urge you to check and review this agreement for an update occasionally. And perhaps, you do not support the agreement or the update made to this agreement; we urge you to immediately refrain from accessing or making use of the <a href="https://www.pawmaw.com/" target="_blank" style="color:#df1d41;">www.pawmaw.com</a> website.
                    </p>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h2 class="refound-policy_sub mt-5" style="font-size:24px; font-weight:600;">
                    Contact us
                </h2>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="refound-policy_details py-5">
                    <p class="refound-policy_details-text mb-3" style="font-size:20px">
                       Should you have any questions or inquiries about this agreement, kindly contact us at <a href="mailto:info@pawmaw.com" style="color:#df1d41;">info@pawmaw.com</a>
                    </p>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- ABOUT HERO SECTION END -->

<!-- GET YOUR PET BACK HOME SECTION START -->
<section class="getyourpet">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="getyourpet-title wow swing" data-wow-duration="1s" data-wow-delay=".3s">Get your pet back home</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <a href="{{ url('/?active-form=1') }}" class="getyourpet-btn mx-auto d-block wow bounceIn" data-wow-duration="1s" data-wow-delay=".3s">Report lost pet</a>
            </div>
        </div>
    </div>
</section>
<!-- GET YOUR PET BACK HOME SECTION END -->
@endsection
