@extends('pawmawFront.layouts.front')

{{-- @section('seo_meta')
    <meta name=keywords content="{{ $pet->meta_keywords }}" />
    <meta name=description content="{{ $pet->meta_description }}" />
@endsection  --}}

@section('title')
    @if($pet->name == null) Found Pet - A {{ $pet->gender }} {{ $pet->specie }} {{ ucfirst($pet->type) }} - {{$pet->last_seen}} - PawMaw
    @else {{$pet->name}} - A {{ $pet->gender }} {{$pet->specie}} {{ ucfirst($pet->type) }} - {{$pet->last_seen}} - PawMaw
    @endif
@endsection

@section('content')
<!-- PET DETAILS SECTION START -->
<section class="petdetails">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <h1 class="petdetails-title">
                    <span>{{ $pet->name }} &ndash; a {{ $pet->gender }} {{ $pet->specie }} {{ $pet->type }} in {{ $pet->city }}, {{ $pet->state ? $pet->state : $pet->country_short }}, {{ $pet->zip_code }}</span>
                </h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-10">
            @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <script>
                setTimeout(function () {
                 $('.alert-success').fadeOut();
                }, 3000);
                 </script>
                <div class="share-btns  mb-sm-3 mb-xs-3">
                    <a href="{{ ($facebookShare)?$facebookShare->viewfacebookpost:'https://www.facebook.com/PawMawOfficial/' }}" target="_blank" class="fb-btn facebook-post-btn"><i class="fas fa-eye"></i> View facebook post</a>

						@if($pet->type == 'lost')
                     		<img src="{{ asset('pawmaw/img/like_facebook_lost.png')}}" alt="Like facebook" id="facebook_like_img" >
                     		<img src="{{ asset('pawmaw/img/like_mobile_lost.png')}}" alt="Like facebook" id="fbImgMob" style="display: none">

                        @elseif($pet->type == 'found')
                     		<img src="{{ asset('pawmaw/img/like_facebook_found.png')}}" alt="Like facebook" id="facebook_like_img" >
                     		<img src="{{ asset('pawmaw/img/like_mobile_found.png')}}" alt="Like facebook" id="fbImgMob" style="display: none">
                        @else
                     		<img src="{{ asset('pawmaw/img/like_facebook_reunited.png')}}" alt="Like facebook" id="facebook_like_img" >
                     		<img src="{{ asset('pawmaw/img/like_mobile_reunited.png')}}" alt="Like facebook" id="fbImgMob" style="display: none">
                        @endif

                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2&appId=1863631387098360&autoLogAppEvents=1';
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
					<div class="fb-like" data-href="{{ ($fbpages)?$fbpages->link:'https://www.facebook.com/PawMawOfficial/' }}" data-layout="button" data-action="like" data-size="large" data-show-faces="true" data-share="false"></div>

                </div>
            </div>
        </div>
        <div class="row pb-5 justify-content-center">
            <div class="col-lg-5 col-md-6 col-sm-12">
            <a href="{{ url('pet-image') }}/{{$pet->slug}}" target="_blank">


               <div id="petdetails-img-wrap11" class="{{ $pet->type }}">

                         <div class="next-pet_box-img_title" style="border-radius:0px">
                            <h2>
                                <i class="fas fa-bullhorn"></i>
                                PawMaw{{ $pet->city ? ',' : '' }}
                                @if($pet->city)
                                    <span> {{ $pet->city . ', ' . ($pet->country_short == 'US' ? $pet->state : $pet->country_short) }}</span>
                                @endif
                            </h2>
                        </div>


                    @php
                        // $size = @getimagesize(asset('uploads/pets_image') . '/' . $pet->photo );
                        // $needHeight = 400;
                        // if($size){
                        //     $height = $size[1];
                        //     $width = $size[0] / ($size[1] / $needHeight);
                        // }
                    @endphp

                   {{--   style="background-image: url({{ url('uploads/pets_image') }}/{{$pet->photo}}) , url({{ asset('pawmaw/img/pet-bg.jpg')}});background-size: contain,cover; background-position: center; background-repeat:no-repeat; margin: 0;" --}}

                    <div
                        class="petdetails-img11 {{ $pet->type }}"
                        id="image-to-copy"
                        data-check="true"
                        data-id="{{ $pet->id }}"
                        style="position: relative;margin: 0; background-image: url({{ asset('pawmaw/img/pet-bg.jpg')}}); background-size: cover; background-position: center; background-repeat:no-repeat;  border-radius:0px">

                                <img
                                style="height: {{ @$needHeight }}px; width: {{ @$width }}px; margin: 0 auto; display: flex; max-width: 100%; object-fit: contain;"
                                src="{{ asset('uploads/pets_image') }}/{{$pet->photo}}" alt="">

                       {{--  <div class="next-pet_box-img_title">
                            <h2>
                                <i class="fas fa-bullhorn"></i>
                                PawMaw{{ $pet->city ? ',' : '' }}
                                @if($pet->city)
                                    <span> {{ $pet->city . ', ' . ($pet->country_short == 'US' ? $pet->state : $pet->country_short) }}</span>
                                @endif
                            </h2>
                        </div> --}}

                            <div class="next-pet_box-img_id">

                                <p style="border-radius:0px; background: {{ $pet->type == 'lost' ? '#df1d41' : '#ffc000' }}"><span class="mr-3">ID</span>#{{$pet->id}}</p>

                            </div>
                            <div class="next-pet_box-img_logo">
                                @if($pet->type == 'lost')
                                    <img src="{{ asset('pawmaw/img/fevi.png') }}" alt="Pawmaw" class="img-fluid circle-logo-50">
                                @elseif($pet->type == 'found')
                                    <img src="{{ asset('pawmaw/img/flyer-found-image-icon.png') }}" alt="Pawmaw" class="img-fluid circle-logo-50">
                                @else
                                    <img src="{{ asset('pawmaw/img/reunited-logo.png') }}" alt="Pawmaw" class="img-fluid circle-logo-50">
                                @endif
                            </div>


                    </div>

                </div>

            </a>
                <div class="petdetails-map mb-xs-5" style="margin-top: 40px;">
                    @if($pet->lat)
                        <div id="map" style="width: 100%; height: 320px;"></div>
                        {{-- <a href="{{ url('map-image') }}/{{$pet->slug}}" target="_blank">
                            <img style="width:100%;" src="{{ asset('uploads/maps_image/'.$pet->map_img) }}" alt="">
                        </a> --}}
                    @else
                        <img class="img-fluid img-thumbnail" src="{{ asset('pawmaw/img/pets/lost-map.png') }}" alt="Found Pet Image">
                    @endif

                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="petdetails-{{$pet->type}}">
                    <div class="tag-{{$pet->type}}">{{ucfirst($pet->type)}}</div>
                    <ul class="petdetails-lost_wrap">
                        <li class="petdetails-lost_item">
                            <h3 style="display:inline"><span class="petdetails-lost_item-label">ID:</span> <span class="petdetails-lost_item-value ml-2">#{{$pet->id}}</span></h3>
                        </li>
                        <li class="petdetails-lost_item">
                            <h3 style="display:inline"><span class="petdetails-lost_item-label mt-3">Area last seen:</span> <span class="petdetails-lost_item-value ml-2">{{$pet->last_seen}}</span></h3>
                        </li>
                        <li class="petdetails-lost_item">
                            <h3 style="display:inline"><span class="petdetails-lost_item-label mt-3">Cross street:</span><span class="petdetails-lost_item-value ml-2">{{$pet->street}}</span> </h3>
                        </li>
                        <li class="petdetails-lost_item">
                            <h3 style="display:inline"><span class="petdetails-lost_item-label mt-3">Species:</span> <span class="petdetails-lost_item-value ml-2">{{$pet->specie}}</span></h3>
                        </li>
                        <li class="petdetails-lost_item">
                            <h3 style="display:inline"> <span class="petdetails-lost_item-label mt-3">Color:</span> <span class="petdetails-lost_item-value ml-2">{{$pet->color}}</span></h3>
                        </li>
                        <li class="petdetails-lost_item">
                            <h3 style="display:inline"><span class="petdetails-lost_item-label mt-3">Gender:</span> <span class="petdetails-lost_item-value ml-2">{{$pet->gender}}</span></h3>
                        </li>
                        <li class="petdetails-lost_item">
                            <h3 style="display:inline"><span class="petdetails-lost_item-label mt-3">Breed:</span> <span class="petdetails-lost_item-value ml-2">{{$pet->breed}}</span></h3>
                        </li>
                        <li class="petdetails-lost_item">
                            <h3 style="display:inline"><span class="petdetails-lost_item-label mt-3">{{ ucfirst($pet->type) }} Date:</span> <span class="petdetails-lost_item-value ml-2">{{$pet->date}}</span></h3>
                        </li>
                        <li class="petdetails-lost_item">
                            <h3 style="display:inline"><span class="petdetails-lost_item-label mt-3">Description:</span></h3>
                        </li>
                        <p class="petdetails-lost_des">{{$pet->description}}</p>
                    </ul>
                </div>
            </div>
        </div>
        <div class="share-btns">
            @if($pet->fb_share)
                <a href="http://www.facebook.com/sharer.php?u={{ $pet->fb_share }}" class="fb-btn details-btn mb-sm-5 mb-xs-5" target="_blank"><i class="fab fa-facebook-f mr-2"></i> Share on facebook</a>
            @else
                <a href="#" class="fb-btn details-btn mb-sm-5 mb-xs-5"><i class="fab fa-facebook-f mr-2"></i> Share on facebook</a>
            @endif

            <a href="#" style="background: {{ $pet->type == 'found' ? '#ffc000' : '#df1d41' }}" class="email-btn details-btn mb-sm-5 mb-xs-5" ><i class="fas fa-envelope"></i> Contact {{ $pet->type == 'found' ? 'Finder' : 'Owner' }}</a>
{{--            <a href="javascript:" class="phone-btn details-btn mb-sm-5" style="background: {{ $pet->type == 'found' ? '#ffc000' : '#df1d41' }}" data-text="Phone {{ $pet->type == 'found' ? 'Finder' : 'Owner' }}"  data-phone="{{ $pet->phone }}" style="cursor:pointer"><i class="fas fa-phone"></i> Phone {{ $pet->type == 'found' ? 'Finder' : 'Owner' }}</a>--}}


        </div>

        <!-- Email Finder Modal -->
        <div class="modal fade" id="emailFinder" tabindex="-1" role="dialog" aria-labelledby="emailFinderLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="padding:2rem; border-radius:10px;">
                    <div class="modal-header">
                        <h5 class="modal-title email-finder-title" id="emailFinderLabel">Send Message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body email-finder-body">
                        <form action="{{ url('/email/emailstore') }}" method="POST">
                            {{ csrf_field() }}

                             <input type="hidden" value="{{$pet->contact_email}}" name="contact_email">
                              <input type="hidden" value="{{$pet->id}}" name="pet_id">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" required>
                            </div>
                            <div class="form-group">
                                <label for="yourMessage">Message</label>
                                <textarea class="form-control" id="yourMessage" rows="2" name="message" placeholder="Write your message" required></textarea>
                            </div>
                            <button type="submit" class="email-finder-btn">Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Email Finder Modal -->

        <!-- Catpcha Modal -->
        <div class="modal fade" id="captchaModal" tabindex="-1" role="dialog" aria-labelledby="captchaModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="padding:2rem; border-radius:10px;">
                    <div class="modal-header">
                        <h5 class="modal-title email-finder-title" id="captchaLabel">Email Owner</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body captcha-body">
						<div class="form-group">
                        	<div class="g-recaptcha" data-sitekey="6LcmfsMUAAAAAAjfuQM6-LIEWcq742bEfOtM3D2K">
							</div>
						<span id="captchaMsg" style="color:red"></span>
						<input type="hidden" value=1 id="captchaType"/>

						</div>

                            <button id="captchaButton" class="email-finder-btn">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Captcha Modal -->


    </div>
</section>
<!-- PET DETAILS SECTION END -->


<!-- GET YOUR PET BACK HOME SECTION START -->
<section class="getyourpet">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="getyourpet-title wow swing" data-wow-duration="1s" data-wow-delay=".3s">Get your pet back home</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <a href="{{ url('/?active-form=1') }}" class="getyourpet-btn mx-auto d-block wow bounceIn" data-wow-duration="1s" data-wow-delay=".3s">Report lost pet</a>
            </div>
        </div>
    </div>
</section>

<script>
$("#captchaButton").click(function() {
	var v = grecaptcha.getResponse();
	 if(v.length == 0)
	    {
	        document.getElementById('captchaMsg').innerHTML="You can't leave Captcha Code empty";
	        return false;
	    }
	    else
	    {
		    if(document.getElementById('captchaType').value==1){
	        $('#emailFinder').modal('show');
		    }else{
		    	let phone = $(".phone-btn").data('phone');
				let text = $(".phone-btn").data('text');

				if (!$(".phone-btn").hasClass('show')) {
					$(".phone-btn ").text(phone);
					$(".phone-btn ").attr("href", "tel:"+phone)
					$(".phone-btn")
					.css('background-color', '#0ab819')
					.addClass('show');

					}
			    }
	      	$('#captchaModal').modal('hide');

	    }

});

$(".phone-btn").click(function () {

	var text=$(".phone-btn").text();

	grecaptcha.reset();
    document.getElementById('captchaMsg').innerHTML="";
    document.getElementById('captchaLabel').innerHTML=text.trim();
    document.getElementById('captchaButton').innerHTML="show";
    $('#captchaModal').modal('show');
    document.getElementById('captchaType').value=2;

});
$(".email-btn ").click(function () {
	grecaptcha.reset();
    document.getElementById('captchaMsg').innerHTML="";
    document.getElementById('captchaLabel').innerHTML="Email Owner";
    document.getElementById('captchaButton').innerHTML="submit";
    $('#captchaModal').modal('show');
    document.getElementById('captchaType').value=1;
});
  function initMap(){
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: {{ $pet->lat }}, lng: {{ $pet->lng }} },
            zoom: 15,
        });

        new google.maps.Marker({
            map: map,
            position: {lat: {{ $pet->lat }}, lng: {{ $pet->lng }} },
            animation: google.maps.Animation.DROP
        })
    }

</script>

<!-- GET YOUR PET BACK HOME SECTION END -->
@endsection
