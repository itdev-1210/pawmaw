@extends('pawmawFront.layouts.watermark')


@section('content')

@php
    $bigImg = false;
    if(file_exists(public_path("uploads/converting-image/pet-big-{$pet->id}.png"))) {
     $bigImg = true;
    }
@endphp

<!-- PET DETAILS SECTION START -->
<section style="padding-top:20px;background:#000;height:100vh" class="petdetails">
    <div class="container">
        <div class="row pb-5 justify-content-center">
            <div class="col-lg-8 col-md-offset-2">    
                <div id="petdetails-img-wrap11" class="{{ $pet->type }}">
                     @if($bigImg)   
                         <div id="copy-image11">
                             
                                <img src="{{ asset('uploads/converting-image/pet-big-') }}{{$pet->id}}.png" alt="">
                         </div>
                    @else
                     
                     @if(!$ismobile)
                     
                     <div class="next-pet_box-img_title" style="border-radius:0px">
                            <h2>
                                <i class="fas fa-bullhorn"></i>
                                PawMaw{{ $pet->city ? ',' : '' }}
                                @if($pet->city)
                                    <span> {{ $pet->city . ', ' . ($pet->country_short == 'US' ? $pet->state : $pet->country_short) }}</span>
                                @endif
                            </h2>
                        </div>
                     
                     @php
                        $size = @getimagesize(asset('uploads/pets_image') . '/' . $pet->photo );
                        $needHeight = 500;
                        if($size){
                            $height = $size[1];
                            $width = $size[0] / ($size[1] / $needHeight);
                        }
                    @endphp
                    
                    <div class="petdetails-img {{ $pet->type }} {{ file_exists(public_path("uploads/converting-image/pet-big-{$pet->id}.png")) ? 'auto-height' : '' }}" 
                        id="image-to-copy-watermark"
                        data-check="{{ file_exists(public_path("uploads/converting-image/pet-big-{$pet->id}.png")) }}" 
                        data-id="{{ $pet->id }}"
                        style="margin-top: 0;position: relative;max-height:500px;height:500px;margin: 0; background-image: url({{ asset('pawmaw/img/pet-bg.jpg')}}); background-size: cover; background-position: center; background-repeat:no-repeat;  border-radius:0px">
                            @if(!file_exists(public_path("uploads/converting-image/pet-big-{$pet->id}.png")))
                                <img 
                                style="height: {{ @$needHeight }}px; width: {{ @$width }}px; margin: 0 auto; display: flex; max-width: 100%; object-fit: contain;"
                                src="{{ url('uploads/pets_image') }}/{{$pet->photo}}" alt="">
                            @endif
                       {{--  <div class="next-pet_box-img_title">
                            <h2>
                                <i class="fas fa-bullhorn"></i>
                                PawMaw{{ $pet->city ? ',' : '' }}
                                @if($pet->city)
                                    <span> {{ $pet->city . ', ' . ($pet->country_short == 'US' ? $pet->state : $pet->country_short) }}</span>
                                @endif
                            </h2>
                        </div> --}}
                        @if(!file_exists(public_path("uploads/converting-image/pet-big-{$pet->id}.png")))
                            <div class="next-pet_box-img_id">

                                <p style="border-radius:0px; background: {{ $pet->type == 'lost' ? '#df1d41' : '#ffc000' }}"><span class="mr-3">ID</span>#{{$pet->id}}</p>
                           
                            </div>
                            <div class="next-pet_box-img_logo">
                                @if($pet->type == 'lost')
                                    <img src="{{ asset('pawmaw/img/fevi.png') }}" alt="Pawmaw" class="img-fluid circle-logo-50">
                                @elseif($pet->type == 'found')
                                    <img src="{{ asset('pawmaw/img/flyer-found-image-icon.png') }}" alt="Pawmaw" class="img-fluid circle-logo-50">
                                @else
                                    <img src="{{ asset('pawmaw/img/reunited-logo.png') }}" alt="Pawmaw" class="img-fluid circle-logo-50">
                                @endif
                            </div>
                        @endif
                      @if(file_exists(public_path("uploads/converting-image/pet-big-{$pet->id}.png")))
                            <div id="copy-image11">
                                <img  src="/uploads/converting-image/pet-big-{{ $pet->id }}.png" alt="">
                            </div> 
                        @else
                            <div id="copy-image11" class="need"></div>
                        @endif
                    </div>
                    @else 
                      <div class="next-pet_box-img_title" style="border-radius:0px">
                            <h2>
                                <i class="fas fa-bullhorn"></i>
                                PawMaw{{ $pet->city ? ',' : '' }}
                                @if($pet->city)
                                    <span> {{ $pet->city . ', ' . ($pet->country_short == 'US' ? $pet->state : $pet->country_short) }}</span>
                                @endif
                            </h2>
                        </div>
                     
                     @php
                        $size = @getimagesize(asset('uploads/pets_image') . '/' . $pet->photo );
                        $needHeight = 500;
                        if($size){
                            $height = $size[1];
                            $width = $size[0] / ($size[1] / $needHeight);
                        }
                    @endphp
                    
                    <div class="petdetails-img {{ $pet->type }} 
                        id="image-to-copy-watermark"
                        data-check="true" 
                        data-id="{{ $pet->id }}"
                        style="margin-top: 0;position: relative;max-height:500px;height:500px;margin: 0; background-image: url({{ asset('pawmaw/img/pet-bg.jpg')}}); background-size: cover; background-position: center; background-repeat:no-repeat;  border-radius:0px">
                        @php
                            if($mapFlag == 1) {
                                $folder_url = 'uploads/maps_image';
                            } else {
                                $folder_url = 'uploads/pets_image';
                            }
                        @endphp  
                                <img 
                                style="height: {{ @$needHeight }}px; width: {{ @$width }}px; margin: 0 auto; display: flex; max-width: 100%; object-fit: contain;"
                                src="{{ url($folder_url) }}/{{$pet->photo}}" alt="">
                            
                       {{--  <div class="next-pet_box-img_title">
                            <h2>
                                <i class="fas fa-bullhorn"></i>
                                PawMaw{{ $pet->city ? ',' : '' }}
                                @if($pet->city)
                                    <span> {{ $pet->city . ', ' . ($pet->country_short == 'US' ? $pet->state : $pet->country_short) }}</span>
                                @endif
                            </h2>
                        </div> --}}
                        
                            <div class="next-pet_box-img_id">

                                <p style="border-radius:0px; background: {{ $pet->type == 'lost' ? '#df1d41' : '#ffc000' }}"><span class="mr-3">ID</span>#{{$pet->id}}</p>
                           
                            </div>
                            <div class="next-pet_box-img_logo">
                                @if($pet->type == 'lost')
                                    <img src="{{ asset('pawmaw/img/fevi.png') }}" alt="Pawmaw" class="img-fluid circle-logo-50">
                                @elseif($pet->type == 'found')
                                    <img src="{{ asset('pawmaw/img/flyer-found-image-icon.png') }}" alt="Pawmaw" class="img-fluid circle-logo-50">
                                @else
                                    <img src="{{ asset('pawmaw/img/reunited-logo.png') }}" alt="Pawmaw" class="img-fluid circle-logo-50">
                                @endif
                            </div>
                     
                    </div>
                    @endif
                    @endif

                </div>
               
            </div>
        </div>
    </div>
</section>
<!-- PET DETAILS SECTION END -->

@php

if(!$bigImg) {

@endphp

<script>
    $(function(){
       initializePetImageConvertWatermark(); 
    });

function initializePetImageConvertWatermark() {
    if ($('#image-to-copy-watermark').data('check') == '1')
        return;
    let elementToCopy = document.getElementById('petdetails-img-wrap11');
    let elementToPut = document.getElementById('copy-image11');
    let petId = $('#image-to-copy-watermark').data('id');
    domtoimage
    .toPng(elementToCopy)
    .then(function (dataUrl) {
    	
        let img = new Image();

        img.src = dataUrl;

        //elementToPut.html(img);

        $.ajax({
            type: 'POST',
            url: '/save-watermark-converting-image',
            data: {
                _token: $('meta[name=csrf-token]').attr('content'),
                image: dataUrl,
                id: petId
            },
            success: function (response) {
               var html = "";
               html += "<div id='copy-image11'><img src='"+response.image+"'/></div>";
               $('#copy-image img').attr('src', response.image);
            }
        });
    });
}
</script>
@php
 } 
@endphp
<!-- GET YOUR PET BACK HOME SECTION END -->
@endsection
