@extends('pawmawFront.layouts.front') @section('title', 'top-10-ways-to-find-missing-pets - PawMaw') @section('content')

<section class="blog_Page_header">
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-lg-6">
                <div class="blog_banner text-center">
                    <h1><span>Welcome </span> to <span>PawMaw</span> blogs!</h1>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="blog_contents">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 mt-5">


                <div id="primary" class="mt-5">
                    <article id="post-1295" class="post-1295 post type-post status-publish format-standard has-post-thumbnail hentry category-pawmaw-blog odd">
                        <h1 class="entry-title">
                            <span>Found a Pet? Here What You Should Do If You Found Pets</span>
                        </h1>

                        <div class="post-thumbnail singular">
                            <a class="img-fluid full-image-link fancybox" href="{{ asset('pawmaw/img/blogimg/found-pet.jpg') }}" title="Found a Pet? Here What You Should Do If You Found Pets">
                            <span class="genericon genericon-zoom"></span>
                        </a>

                            <div class="col-lg-12">
                                <img class="img-fluid attachment-social-thumbnails size-social-thumbnails wp-post-image" alt="Found a Pet? Here What You Should Do If You Found Pets" width="492" height="276" src="{{ asset('pawmaw/img/header-bg.png') }}">
                            </div>
                        </div>

                        <div class="entry-content">
                            <p>&nbsp;</p>
                            <p>The other morning, I went to a local park for a walk with my dog. It was a beautiful &amp; shiny morning. All on a sudden I spotted a dog on the opposite side of the park. I thought It’s lost, and I was searching for its owner. Unfortunately, the dog wasn’t wearing any tag on its collar. I was confused. What should I do now?</p>
                            <p>If you ever face this situation, don’t become nervous. There is a very general process to find the owner of a found pet. However, remember that you are not the only one who faces this problem. There are thousands of other people who undergo with the same situation just like you and me.</p>
                            <p>
                                A <a href="#"><strong>report of</strong></a> the American Society for the Prevention of Cruelty to Animals (<a href="#"><strong>ASPCA</strong></a>) shows that nationwide about 6.5 million animals enter the US animal shelter every year. 3.2 million are cats, and 3.3 million are dogs. Among them, almost 1.5 million pets are euthanized&nbsp;every year.
                            </p>
                            <p>Approximately 710,000 animals who enter the shelter home returns to their owner. 620,000 are dogs, and 90,000 are cats among them. So, who help them to return to their owner?</p>
                            <p>Yes, they are those people like you and me. So, it is very effective to know about what should you do when you found a lost pet. As I have already faced this situation, so I think why shouldn’t I share my experience with my friends? This blog is just the result of my thought!</p>
                            <p>In this blog, I am gonna inform you about 10 things you should do if you found a dog. Continue scrolling to know the in-depth process –</p>
                            <div class="col-lg-12">

                                <img class="img-fluid attachment-social-thumbnails size-social-thumbnails wp-post-image" alt="Found a Pet? Here What You Should Do If You Found Pets" src="{{ asset('pawmaw/img/header-bg.png') }}">
                            </div>
                            <h2></h2>
                            <h2>
                                <strong>Safety First</strong>
                            </h2>
                            <p>As soon as someone spots a lost pet, they want to catch them. But it doesn’t result well every time. You may try to pick the pet. But if the pet doesn’t seem cooperative, don’t try to capture.</p>
                            <p>Instead of catching the found dog, try to determine whether the dog is in a safe place or not. If it is on a busy road with heavy traffic, keep it away from that. But if it is on a sidewalk, don’t push it to do anything.</p>
                            <h2></h2>
                            <p>&nbsp;</p>
                            <h2>
                                <strong>Build Trust</strong>
                            </h2>
                            <p>When found pets don’t approach to you by itself, try to build its trust in you. You may buy some high-quality food and sit on the ground. Try to sit as close as possible.</p>
                            <p>It will be more effective if you can surround yourself with food. Now, wait patiently for the moment. It may take a while to build trust. As time passes, the dog becomes close to you. It means you have successfully built trust.</p>
                            <h2></h2>
                            <p>&nbsp;</p>
                            <h2>
                                <strong>Watch Out for Aggressive Dogs</strong>
                            </h2>
                            <p>The street dogs are divided into two different categories. Few of them has an owner. The owner raises them and keeps the pets with them. But a few others do not have any home or family. Both of these dogs might be aggressive or fearful when approached by an unknown person like you and me. So, be careful!</p>
                            <p>Generally, there are a few noticeable things for the family-owned dogs. They are normally well groomed, clean and doesn’t look too much aggressive. On the contrary, the street dogs are too much aggressive and not groomed at all.</p>
                            <h2></h2>
                            <p>&nbsp;</p>
                            <h2>
                                <strong>Check for Obvious Signs of Disease</strong>
                            </h2>
                            <p>As it is a lost and found pet, so you don’t have any idea about its health issues. If the dog seems like biting, snipping or loudly barking at everyone, it is not a sign of good health. Even if the dog looks very weak, don’t want to walk, or salivate a lot, it means this pet is physically sick. Don’t get close to a dog if you notice any of these issues.</p>
                            <p>Notify the police or local animal shelter service as soon as you notice any of those symptoms. A dog might be affected by a lot of diseases. The most common diseases are –</p>
                            <ul>
                                <li>Distemper</li>
                                <li>Fleas, ticks, or mange</li>
                                <li>Rabies</li>
                                <li>Heartworms</li>
                                <li>Kennel cough</li>
                                <li>Ringworm or any other fungal diseases</li>
                                <li>Intestinal parasites:&nbsp;tapeworms, roundworms, whipworms, or hookworms.</li>
                            </ul>
                            <h2></h2>
                            <p>&nbsp;</p>
                            <h2>
                                <strong>Check for any identity</strong>
                            </h2>
                            <p>Nowadays most of the pet owner add a microchip on the collar of their pet. It became easier to find their dog if the lost.</p>
                            <p>When I found that dog in the park, I’ve searched something which can ensure an identity for that found dogs near me. If you can locate the owner information, the next step is straightforward. Contact the owner and handle the dog to him.</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <h2>
                                <strong>File a “Found Dog” Report on Local Animal Shelter</strong>
                            </h2>
                            <p>What will you do if you lost your pet? Yes, everyone starts their search from the local animal shelter center. So, you should inform the local shelter as soon as you found a cat or dog.</p>
                            <h2></h2>
                            <p>&nbsp;</p>
                            <h2><strong>Check Local Newspapers, Magazines, and <a href="https://www.craigslist.org">Craigslist</a></strong></h2>
                            <p>When the pet owner notice that his pet is missing, he definitely shares ads on local magazines and newspapers. So, you should check the ‘lost &amp; found’ columns of recent newspapers and magazines. There is a huge possibility to find something that relates to the found pets.</p>
                            <p>
                                You may also check the <a href="#">Craigslist</a>. Most of the pet owner share the lost ads on online, primarily social media and Craigslist.
                            </p>
                            <p>Also, don’t forget to check the <a href="#">Found dog website</a> too!</p>
                            <h2></h2>
                            <p>&nbsp;</p>
                            <h2>
                                <strong>Share A ‘Found Cat or Dog’ Picture on your social media</strong>
                            </h2>
                            <p>If you can’t locate the owner still now,&nbsp; Include a clear and details photo of the found bird or pet. Publish them on your social media sites.</p>
                            <h2></h2>
                            <p>&nbsp;</p>
                            <h2>
                                <strong>Hang Flyers On Crowded Places</strong>
                            </h2>
                            <p>
                                Along with sharing online, try to hang some attractive and eye-catchy flyer. Don’t forget to include your contact no. on that
                                <a href="#">flyer</a>.
                            </p>
                            <h2></h2>
                            <p>&nbsp;</p>
                            <h2>
                                <strong>Notify Local Veterinary Clinics</strong>
                            </h2>
                            <p>Generally, the pet owners check every local veterinary clinic for their lost pets. So, you should inform the local clinics about your found bird.</p>
                            <p><strong>&nbsp;</strong></p>
                            <p>&nbsp;</p>
                            <h2>
                                <strong>What can we do to help you with your found pet?</strong>
                            </h2>
                            <p>
                                <a href="#">PawMaw</a> is actively helping found pet to make their way to back home. We provide a few most effective service to find a lost/found pet. Few of our services are –</p>
                            <ol>
                                <li>You can send <a href="{{ url('/') }}">Paw-Alerts</a> to the residents about your Found pet.</li>
                                <li>
                                    You can create
                                    <a href="{{ url('/') }}">eye-catchy flyers</a> from the
                                    <a href="{{ url('/') }}">website</a>. It’s pretty handy and free.
                                </li>
                                <li>
                                    Your post will be displayed as a feature post on our site. Hundreds of peoples within a second can see the
                                    <a href="{{ url('/') }}">featured post</a>!
                                </li>
                            </ol>
                            <p>So, have you found a pet? <a href="{{ url('/') }}">Report it here</a>.</p>
                            <p>
                                <!--
                                <img width="854" height="791" alt="5 Essential Tips If You Found A PET" class="wp-image-1300" title="5 Essential Tips If You Found A PET" src="{{ asset('pawmaw/img/blog/5-Essential-Tips-If-You-Found-A-PET-300x278.jpg') }}" srcset="{{ asset('pawmaw/img/blog/5-Essential-Tips-If-You-Found-A-PET-300x278.jpg') }} 300w,
                                                {{ asset('pawmaw/img/blog/5-Essential-Tips-If-You-Found-A-PET-1024x950.jpg') }} 1024w" sizes="(max-width: 854px) 100vw, 854px">
-->
                                <img class="img-fluid attachment-social-thumbnails size-social-thumbnails wp-post-image" alt="Found a Pet? Here What You Should Do If You Found Pets" src="{{ asset('pawmaw/img/header-bg.png') }}">
                            </p>
                            <h3>
                                <strong>Final Words</strong>
                            </h3>
                            <p>If you follow these steps, hopefully, the found pets owner meet with their pets within a few hours. That means you become a part of creating an excellent task with a happy ending.</p>
                            <p>Do you have any more effective tips, comment below!</p>
                            <p><strong>&nbsp;</strong></p>
                            <p>&nbsp;</p>
                        </div>
                    </article>
                    <!-- You can start editing here. -->

                    <!-- If comments are open, but there are no comments. -->
                    {{--
                    <div id="respond" class="comment-respond">--}} {{--
                        <h3 id="reply-title" class="comment-reply-title">--}} {{--Leave a Reply--}} {{--
                            <small>--}}
                        {{--<a rel="nofollow" id="cancel-comment-reply-link" href="#" style="display:none;">Cancel reply</a>--}}
                    {{--</small>--}} {{--
                        </h3>--}} {{--

                        <form action="#" method="post" id="commentform" class="comment-form" novalidate="">--}} {{--
                            <p class="logged-in-as">--}} {{--
                                <a href="#" aria-label="Logged in as Courtney L. Luna. Edit your profile.">Logged in as Courtney L. Luna</a>.--}} {{--
                                <a href="#">Log out?</a>--}} {{--
                            </p>--}} {{--

                            <p class="comment-form-comment">--}} {{--
                                <label for="comment">Comment</label>--}} {{--
                                <textarea id="comment" name="comment" cols="45" rows="8" maxlength="65525" required="required" style="z-index: auto; position: relative; line-height: 16px; font-size: 14px; transition: none 0s ease 0s; background: transparent !important;"></textarea>--}} {{--
                            </p>--}} {{--
                            <p class="form-submit">--}} {{--
                                <input name="submit" type="submit" id="submit" class="submit" value="Post Comment">--}} {{--
                                <input type="hidden" name="comment_post_ID" value="1295" id="comment_post_ID">--}} {{--
                                <input type="hidden" name="comment_parent" id="comment_parent" value="0">--}} {{--
                            </p>--}} {{--
                        </form>--}} {{--
                    </div>
                    <!-- #respond -->--}}
                </div>

                <div id="secondary">
                    <div id="widget-area" class="widget-area" role="complementary">
                        <aside id="recent-posts-2" class="widget widget_recent_entries">
                            <h2 class="widget-title">
                                <span>Recent Posts</span></h2>
                            <ul>
                                <li>
                                    <a href="{{ url('/guide-for-found-pets') }}">Found a Pet? Here What You Should Do If You Found Pets</a>
                                    <span class="post-date">September 30, 2018</span>
                                </li>
                                <li>
                                    <a href="{{ asset('/top-10-ways-to-find-missing-pets') }}">Top 10 Ways to Find Your Missing Cats and Dogs</a>
                                    <span class="post-date">September 22, 2018</span>
                                </li>
                            </ul>
                        </aside>

                        {{--
                        <aside id="recent-comments-2" class="widget widget_recent_comments">--}} {{--
                            <h2 class="widget-title">--}} {{--
                                <span>Recent Comments</span>--}} {{--
                            </h2>--}} {{--
                            <ul id="recentcomments"></ul>--}} {{--
                        </aside>--}}
                    </div>
                </div>




                <div id="primary">
                    <article id="post-1235" class="post-1235 post type-post status-publish format-standard has-post-thumbnail hentry category-pawmaw-blog odd">
                        <h1 class="entry-title">
                            <span>Top 10 Ways to Find Your Missing Cats and Dogs</span>
                        </h1>

                        <div class="post-thumbnail singular">
                            <a class="img-fluid full-image-link fancybox" href="{{ asset('pawmaw/img/blog/How-to-Find-Missing-Pet.jpg') }}" title="Top 10 Ways to Find Your Missing Cats and Dogs">
                            <span class="genericon genericon-zoom"></span>
                        </a>

                            <div class="col-lg-12">

                                <img class="img-fluid attachment-social-thumbnails size-social-thumbnails wp-post-image" alt="Found a Pet? Here What You Should Do If You Found Pets" src="{{ asset('pawmaw/img/header-bg.png') }}">
                            </div>
                        </div>

                        <div class="entry-content">
                            <p>Losing our beloved Cats and Dogs is always traumatic. It doesn’t hurt an inch less than losing a family member.</p>
                            <p>Losing your pets is scary. You will become worried, fear for their current well-being. But rather than drowning in pain, you should start searching for your missing pet.</p>
                            <p>
                                According to a survey of American Society for the Prevention of Cruelty to Animals (
                                <a href="#">ASPCA</a>), 85% of lost cats and dogs were recovered in the last five years.
                            </p>
                            <p>So, there is a huge possibility to find your missing pet within a few days. If your pet is missing, follow this guideline without being panicked. It will help you to find your lost dogs as quickly as possible. Proven Method!</p>
                            <p>Let’s take a look at what you shouldn’t do and what you should do after losing your pet.</p>

                            <h2>
                                <strong>What you shouldn’t do?</strong>
                            </h2>

                            <ol>
                                <li>Don’t get panicked</li>
                                <li>Do not wait for others help</li>
                                <li>Don’t believe everything people are saying &amp;amp;amp;</li>
                                <li>Do not give up</li>
                            </ol>

                            <p>&nbsp;</p>

                            <h2>
                                <strong>What should you do?</strong>
                            </h2>

                            <ol>
                                <li>Take quick actions</li>
                                <li>Put all prior commitments on hold</li>
                                <li>Check your Home Security Systems footages very clearly</li>
                                <li>Search every corner of your neighborhood houses</li>
                                <li>Inform your neighbors and local animal shelters</li>
                                <li>Stay glued with your phone</li>
                                <li>Create a big neon lost dog poster</li>
                                <li>Leave your pets favorite food outside</li>
                                <li>Use your personal social media</li>
                                <li>Follow every single lead</li>
                            </ol>

                            <p>&nbsp;</p>

                            <h2>
                                <strong>Let’s discuss these potentialities briefly. Start with the forbidden things. </strong>
                            </h2>

                            <h3>
                                <strong>1.Don’t get panicked</strong>
                            </h3>

                            <p>Realizing that your cat or dog is missing takes a few moments. Generally, we assume he might be sleeping in the back or pursuing neighboring yard. But when he doesn’t paw in the door in time and misses the dinner, we start searching. As the time passes, our tension also increases with time. And most importantly this missing issue pulled a panic trigger on our mind and made us paralyzed.</p>
                            <p>But taking the positive steps will free you from the grip of panic. It also energies your mind and helps to focus on your thoughts.</p>

                            <h3>
                                <strong>&nbsp;2.&nbsp;</strong>
                                <strong>Do not wait for others help</strong>
                            </h3>

                            <p>If you are a pet lover like me, you must feel your child is missing. I know that harsh feeling!</p>
                            <p>But when you start to search for your lost pets, people will tell you about their lost and found pets. But, don’t fall for that. They will tell you that they find their missing pets after a few days or several weeks.</p>
                            <p>It’s true that pets return home after some unexplained tours. Almost 85% of cats return to their home after inexplicable vanishing. But it happens after several days or weeks. How can I live for several weeks without him?</p>
                            <p>So, before the police, neighbors and lost dog finders start their search, don’t sit idle. Start your search before them.</p>

                            <h3>
                                <strong>3. Don’t believe everything people are saying</strong>
                            </h3>

                            <p>When you start talking with your neighbors or relatives, someone might tell you about some disappointing issues. Even I heard someone telling me that a predator kills my cat!</p>
                            <p>In Western Washington, few people blindly believe that coyote kills their cats and dogs. But fewer than 3% of pets are found killed when they rescued. You see that number? It’s only three percent. So, don’t panic. Search effectively &amp; you will find your pet.</p>

                            <h3>
                                <strong>4. Don’t give up</strong>
                            </h3>

                            <p>Here is the most important lesson. In my ‘Helping lost pet found’ career, I’ve met different types of pet owners. The scariest one was a businessman guy who doesn’t even care about his missing pet! I was feeling like – why he keeps a pet with himself? Argh!</p>
                            <p>Well, I know you are not like him. So, whatever happens, don’t give up. Keep believing that your pet will be home. It’s just a matter of time.</p>
                            <div class="col-lg-12">
                                <!--
                            <img
                                    width="798" height="503"
                                    alt="Bets-Ways-to-Find-Lost-Pets."
                                    class="alignnone wp-image-1261 "
                                    title="Bets Ways to Find Lost Pets"
                                    src="{{ asset('pawmaw/img/blog/Bets-Ways-to-Find-Lost-Pets-300x189.jpg') }}"
                                    srcset="{{ asset('pawmaw/img/blog/Bets-Ways-to-Find-Lost-Pets-300x189.jpg') }} 300w,
                                            {{ asset('pawmaw/img/blog/Bets-Ways-to-Find-Lost-Pets-768x485.jpg') }} 768w,
                                            {{ asset('pawmaw/img/blog/Bets-Ways-to-Find-Lost-Pets.jpg') }} 900w"
                                    sizes="(max-width: 798px) 100vw, 798px"
                            >
                        
-->


                                <img class="img-fluid attachment-social-thumbnails size-social-thumbnails wp-post-image" alt="Found a Pet? Here What You Should Do If You Found Pets" src="{{ asset('pawmaw/img/header-bg.png') }}">
                            </div>

                            <h2>Let’s talk about what we should do to rescue the pet.</h2>

                            <p>&nbsp;</p>

                            <h3>
                                <strong>1. Take quick actions</strong>
                            </h3>

                            <p>Just like I said before, don’t wait to get help from others. It will only increase your tension. Nothing else!</p>
                            <p>Before seeking help from others, start searching for your own. Research shows most of the lost pets are found within a circle of 2/3 km.</p>

                            <h3>
                                <strong>&nbsp;2.&nbsp;</strong>
                                <strong>Put all prior commitments on hold</strong>
                            </h3>

                            <p>Peoples are busy by born. We have to work every day to earn our daily meal. However, when your pet goes missing, the situation is different. I will suggest you hold other commitments on hold and put the highest priority on searching for your pet.</p>
                            <p>I know a bride who becomes late on the church on her marriage day. And the reason is she was searching for her missing pet! If she can hold her marriage, can’t we carry other tasks only for a few hours?</p>

                            <h3>
                                <strong>3. Check your Home Security Systems footages very clearly</strong>
                            </h3>

                            <p>Searching the home security system will be a great help to find your missing pet. You can track the latest movement by analyzing your security camera footage.</p>
                            <p>Most of the case, those footages will provide a clear concept of where your cat is.</p>

                            <h3>
                                <strong>4. Search every corner of your neighborhood houses</strong>
                            </h3>

                            <p>There is a higher possibility that your cat is hiding within your neighbor’s house. So, check every corner of your neighbor’s houses. Further, don’t forget your one! Search every corner of your house including the backyard, bedroom or even the underground.</p>

                            <h3>
                                <strong>5. Inform your neighbors and local animal shelters</strong>
                            </h3>

                            <p>Before searching the neighbor’s houses, inform them that your pet is missing for hours. There is a slight possibility that they may find your dog in their yard.</p>
                            <p>Further, look at the local animal shelter. Inform them about your missing pet. They will inform you as soon as they find your pet.</p>

                            <h3>
                                <strong>6. Stay glued with your phone</strong>
                            </h3>

                            <p>I hope you already inform the pet rescue team, friends and neighbors about your missing kitten. So, they will contact you as soon as they found your kitten. Farther, they might need to know further information about your kitten. So, stay glued to your phone. Who knows when you will get that desired phone call!</p>

                            <h3>
                                <strong>&nbsp;7.&nbsp;</strong>
                                <strong>Create a big neon lost dog poster</strong>
                            </h3>

                            <p>A big poster can reduce plenty of your time. After searching within your neighbor’s houses, it’s time to do something big.</p>
                            <p>Contact with local advertising media to create a lost dog poster or a lost cat poster. It will help you to spread the news like the wind.</p>

                            <h3>
                                <strong>8. Leave your pets favorite food outside</strong>
                            </h3>

                            <p>Usually, the missing cats and dogs become hungry after several hours. So, they also began to search for foods. That’s why you should put your pet’s favorite food outside. You know pets can smell foods from a huge distance. If you are lucky enough, your pet will appear on your door only because of that food.</p>

                            <h3>
                                <strong>9. </strong>
                                <strong>Use your social media</strong>
                            </h3>

                            <p>Social media has a significant impact on our daily life. From the lame news to an important government notice, everything spreads like fire on social media websites. So, why don’t you use that?</p>
                            <p>Simply select a clear and beautiful photo of your lost pet and post it. Your friends will start searching, and you will get your desired cat within a few hours.</p>

                            <h3>
                                <strong>10. Follow every single lead</strong>
                            </h3>

                            <p>A real pet lover does everything he can do to find his missing cat, dog or other pets. I know you won’t be panicked when you lost your pet.</p>
                            <p>But you shouldn’t just sit idle either. Follow every single lead. Don’t ignore even the slightest possibilities. Who knows where you will find him?</p>
                            <p>&nbsp;</p>

                            <h2>
                                <strong>What can we do to help you with your lost pet?</strong>
                            </h2>

                            <p><strong>&nbsp;</strong></p>

                            <p>
                                To make your searching job more effective, you can report it to
                                <a href="{{ url('/') }}">PawMaw</a>.
                            </p>
                            <p>We are providing some pretty amazing services to you. For example:</p>

                            <ol>
                                <li>We will send Paw-alert the resident’s lives close to your house. So they can recognize your beloved pet as soon as they saw that.</li>
                                <li>We can create you a beautiful and eye-catchy poster. But, you don’t need to think so much about the poster. You can create an eye-catchy poster from this site within a minute by logging into your account.</li>
                                <li>
                                    Whatever action you may take after losing your pet, there is only one target – spread the notice vastly. Well, you can spread the notice by using feature post on PawMaw. It will reach the visitor on the
                                    <a href="{{ url('/') }}">website</a>.
                                </li>
                            </ol>

                            <div class="col-lg-12">

                                <!--
                            <img
                                    alt="Family Pet"
                                    width="822" height="548"
                                    class="alignnone wp-image-1250 "
                                    title="Family Pet"
                                    src="{{ asset('pawmaw/img/blog/Family-Pet-300x200.jpg') }}"
                                    srcset="{{ asset('pawmaw/img/blog/Family-Pet-300x200.jpg') }} 300w,
                                            {{ asset('pawmaw/img/blog/Family-Pet-768x512.jpg') }} 768w,
                                            {{ asset('pawmaw/img/Family-Pet-1024x683.jpg') }} 1024w"
                                    sizes="(max-width: 822px) 100vw, 822px"
                            >
-->
                                <img class="img-fluid attachment-social-thumbnails size-social-thumbnails wp-post-image" alt="Found a Pet? Here What You Should Do If You Found Pets" src="{{ asset('pawmaw/img/header-bg.png') }}">

                            </div>

                            <p>Final Verdict:</p>
                            <p>My pet is my life. It fits with most of the other pet lovers too. If the pet is lost, it becomes a nightmare. But the ultimate mantra is, don’t panic. Stay strong and search for your pet by following these instructions.</p>
                            <p>After finding the pet, prevent your pets from getting lost again!</p>
                        </div>
                    </article>
                    <!-- You can start editing here. -->

                    <!-- If comments are open, but there are no comments. -->
                    {{--
                    <div id="respond" class="comment-respond">--}} {{--
                        <h3 id="reply-title" class="comment-reply-title">--}} {{--Leave a Reply--}} {{--
                            <small>--}}
                            {{--<a rel="nofollow" id="cancel-comment-reply-link" href="#" style="display:none;">--}}
                                {{--Cancel reply--}}
                            {{--</a>--}}
                        {{--</small>--}} {{--
                        </h3>--}} {{--

                        <form action="#" method="post" id="commentform" class="comment-form" novalidate="">--}} {{--
                            <p class="logged-in-as">--}} {{--
                                <a href="#" aria-label="Logged in as Courtney L. Luna. Edit your profile.">--}}
                                {{--Logged in as Courtney L. Luna--}}
                            {{--</a>.--}} {{--

                                <a href="#">Log out?</a>--}} {{--
                            </p>--}} {{--
                            <p class="comment-form-comment">--}} {{--
                                <label for="comment">Comment</label>--}} {{--
                                <textarea id="comment" name="comment" cols="45" rows="8" maxlength="65525" required="required"></textarea>--}} {{--
                            </p>--}} {{--
                            <p class="form-submit">--}} {{--
                                <input name="submit" type="submit" id="submit" class="submit" value="Post Comment">--}} {{--
                                <input type="hidden" name="comment_post_ID" value="1235" id="comment_post_ID">--}} {{--
                                <input type="hidden" name="comment_parent" id="comment_parent" value="0">--}} {{--
                            </p>--}} {{--
                        </form>--}} {{--
                    </div>
                    <!-- #respond -->--}}
                </div>

                <!--  Blog comment start        -->

                <div class="blog_posting mt-5 mb-5" id="secondary">
                    <h2><span>Contributions</span></h2>
                    <p style="font-size:18px;">39 comments on “Why Is My Dog Vomiting White Foam?”</p>
                    <div class="row justify-content-center mt-4">
                        <div class="col-lg-2 col-3 ">
                            <img src="{{ asset('pawmaw/img/fevi.png') }}" class="img-fluid" alt="blog_img">
                        </div>
                        <div class="col-lg-10 col-9 ">
                            <h4><i>February 2, 2018 at 2:44 am</i></h4>
                            <h4 class="mb-3"><i>Sheena Mullen says:</i></h4>
                            <p style="font-size:18px;">Well my baby be okay can he die from it he eat bread from deale on sandwich after he this up he went to sleep do I need to worry</p>
                            <button style="font-size:18px;padding:7px 20px; background:#eee; border:1px solid #d6d6d6; border-radius:5px;color:#333;cursor:pointer;font-weight:500;margin-top:15px;"><i class="fas fa-reply-all mr-3"></i>Reply</button>
                        </div>
                    </div>

                    <h2 class="mt-5"><span>Leave a Reply</span></h2>
                    <form>
                        <div class="form-group">
                            <label style="font-size:18px;" for="exampleFormControlTextarea1">Comment</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write Your Comment" style="font-size:18px;" required></textarea>
                        </div>
                        <div class="form-group">
                            <label style="font-size:18px;" for="exampleInputName1">Your Name</label>
                            <input type="text" class="form-control" id="exampleInputName1" aria-describedby="emailHelp" placeholder="Enter Your Name" style="font-size:18px; padding:20px 10px;" required>
                        </div>
                        <div class="form-group">
                            <label style="font-size:18px;" for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" style="font-size:18px; padding:20px 10px;" required>
                        </div>
                        <button style="font-size:18px;background:#df1d41; padding:7px 20px; border-radius:5px;color:#fff;font-weight:500;" type="submit" class="btn mt-4">Submit</button>
                    </form>
                </div>

                <!--  Blog comment end        -->


                <div id="secondary">
                    <div id="widget-area" class="widget-area" role="complementary">
                        <aside id="recent-posts-2" class="widget widget_recent_entries">
                            <h2 class="widget-title">
                                <span>Recent Posts</span>
                            </h2>

                            <ul>
                                <li>
                                    <a href="{{ url('/guide-for-found-pets') }}">Found a Pet? Here What You Should Do If You Found Pets</a>
                                    <span class="post-date">September 30, 2018</span>
                                </li>
                                <li>
                                    <a href="{{ asset('/top-10-ways-to-find-missing-pets') }}">Top 10 Ways to Find Your Missing Cats and Dogs</a>
                                    <span class="post-date">September 22, 2018</span>
                                </li>
                            </ul>
                        </aside>

                        {{--
                        <aside id="recent-comments-2" class="widget widget_recent_comments">--}} {{--
                            <h2 class="widget-title">--}} {{--
                                <span>Recent Comments</span>--}} {{--
                            </h2>--}} {{--
                            <ul id="recentcomments"></ul>--}} {{--
                        </aside>--}}
                    </div>
                </div>


            </div>
            <div class="col-lg-4 mt-5">
                <div class="blog_side_bar">
                    <div class="search_post mt-5 blog_post_search">
                        <form action="">
                            <div class="form-group">
                                <input type="text" class="form-control" id="blog_search-field" placeholder="Search Blog">
                            </div>
                        </form>
                        <i class="fas fa-search hide_blog_search-icon"></i>
                    </div>
                </div>

                <div class="blog_side_bar_tab">
                    <div class="tab_button">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 m-0 p-0">
                                <button class="active" id="popular_blog-tab_btn">Popular Post</button>
                            </div>
                            <div class="col-lg-6 col-sm-6 m-0">
                                <button class="recent_btn" id="recent_blog-tab_btn">Recent Post</button>
                            </div>
                        </div>
                    </div>

                    <div id="popular_blog-tab_items" style="display: block;">
                        <div class="blog_tab_titles">
                            <div class="row">
                                <div class="col-lg-3 col-3 m-0 p-0">
                                    <div class="blog_titles_img">
                                        <img src="{{ asset('pawmaw/img/pets/lost-pet.png') }}" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-9 col-9 m-0">
                                    <div class="blog_title_description">
                                        <h4>
                                            TOP 10 WAYS TO FIND YOUR MISSING CATS AND DOGS.
                                            <span>25 December 2018</span>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog_tab_titles">
                            <div class="row">
                                <div class="col-lg-3 col-3 m-0 p-0">
                                    <div class="blog_titles_img">
                                        <img src="{{ asset('pawmaw/img/pets/lost-pet.png') }}" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-9 col-9 m-0">
                                    <div class="blog_title_description">
                                        <h4>
                                            TOP 10 WAYS TO FIND YOUR MISSING CATS AND DOGS.
                                            <span>25 December 2018</span>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog_tab_titles">
                            <div class="row">
                                <div class="col-lg-3 col-3 m-0 p-0">
                                    <div class="blog_titles_img">
                                        <img src="{{ asset('pawmaw/img/pets/lost-pet.png') }}" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-9 col-9 m-0">
                                    <div class="blog_title_description">
                                        <h4>
                                            TOP 10 WAYS TO FIND YOUR MISSING CATS AND DOGS.
                                            <span>25 December 2018</span>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog_tab_titles">
                            <div class="row">
                                <div class="col-lg-3 col-3 m-0 p-0">
                                    <div class="blog_titles_img">
                                        <img src="{{ asset('pawmaw/img/pets/lost-pet.png') }}" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-9 col-9 m-0">
                                    <div class="blog_title_description">
                                        <h4>
                                            TOP 10 WAYS TO FIND YOUR MISSING CATS AND DOGS.
                                            <span>25 December 2018</span>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="recent_blog-tab_items" style="display: none;">
                        <div class="blog_tab_titles">
                            <div class="row">
                                <div class="col-lg-3 col-3 m-0 p-0">
                                    <div class="blog_titles_img">
                                        <img src="{{ asset('pawmaw/img/pets/lost-pet.png') }}" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-9 col-9 m-0">
                                    <div class="blog_title_description">
                                        <h4>
                                            TOP 10 WAYS TO FIND YOUR MISSING CATS AND DOGS.
                                            <span>25 December 2018</span>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog_tab_titles">
                            <div class="row">
                                <div class="col-lg-3 col-3 m-0 p-0">
                                    <div class="blog_titles_img">
                                        <img src="{{ asset('pawmaw/img/pets/lost-pet.png') }}" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-9 col-9 m-0">
                                    <div class="blog_title_description">
                                        <h4>
                                            TOP 10 WAYS TO FIND YOUR MISSING CATS AND DOGS.
                                            <span>25 December 2018</span>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog_tab_titles">
                            <div class="row">
                                <div class="col-lg-3 col-3 m-0 p-0">
                                    <div class="blog_titles_img">
                                        <img src="{{ asset('pawmaw/img/pets/lost-pet.png') }}" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-9 col-9 m-0">
                                    <div class="blog_title_description">
                                        <h4>
                                            TOP 10 WAYS TO FIND YOUR MISSING CATS AND DOGS.
                                            <span>25 December 2018</span>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog_tab_titles">
                            <div class="row">
                                <div class="col-lg-3 col-3 m-0 p-0">
                                    <div class="blog_titles_img">
                                        <img src="{{ asset('pawmaw/img/pets/lost-pet.png') }}" class="img-fluid" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-9 col-9 m-0">
                                    <div class="blog_title_description">
                                        <h4>
                                            TOP 10 WAYS TO FIND YOUR MISSING CATS AND DOGS.
                                            <span>25 December 2018</span>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="blog_catagory">
                    <h3>categories</h3>
                    <div class="catagory_list">
                        <a href="#">All Post<i class="fas fa-angle-right "></i></a>
                    </div>
                    <div class="catagory_list">
                        <a href="#">Lost and Found Tips<i class="fas fa-angle-right "></i></a>
                    </div>
                    <div class="catagory_list">
                        <a href="#">Pet Health<i class="fas fa-angle-right "></i></a>
                    </div>
                    <div class="catagory_list">
                        <a href="#">Others<i class="fas fa-angle-right "></i></a>
                    </div>
                </div>


                <div class="featured-network wow fadeInUp mt-5" data-wow-duration="1s" data-wow-delay=".3s">
                    <h1 class="featured-network_title">Join our network</h1>
                    <img class="card-img-top img-fluid my-5" src="{{ asset('pawmaw/img/icons/join.png') }}" alt="Join Network Icon">
                    <div class="form-group has-search mx-auto d-block">
                        <span class="form-control-zip"><i class="fas fa-search-location"></i></span>
                        <input type="text" class="form-control" placeholder="ZIP Code">
                        <button type="submit" class="featured-network_code-btn zip-code_btn mx-auto d-block">Search</button>
                    </div>

                    <h2 class="featured-network_title-sub py-5">Get Lost and Found Pet Alerts in Your Area.</h2>
                    <img class="img-fluid mx-auto d-block" src="{{ asset('pawmaw/img/home/join-pet.png') }}" alt="Joining Pet">
                </div>

            </div>

        </div>
    </div>
</section>

@endsection
