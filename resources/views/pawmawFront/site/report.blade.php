@extends('pawmawFront.layouts.front')

@section('title', 'Report - PawMaw')

@section('content')
    <div class="site-content">
    <div class="warper">
        <div id="anim-form">
            <div id="anim-form-left">
                <h1 class="entry-title">
                    <span>Report Lost Pet</span>
                </h1>

                <form id="lost-form" action="{{ url('/user/register') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="request" value="1">
                    <div class="row">
                        <label for="pet_name_id_2">Pet Name or ID <span class="required">*</span></label>
                        <input type="text" name="name" id="pet_name_id_2" placeholder="Pet Name or ID" required="">
                    </div>

                    <div class="row">
                        <label for="pet_photo_2">Pet Photo</label>
                        <input type="file" name="pet_photo" id="pet_photo_2" accept="image/*" required="">
                    </div>

                    <div class="clear"></div>

                    <div class="row">
                        <label for="specie_2">Specie</label>
                        <select name="specie" id="specie_2">
                            <option value="Dog">Dog</option>
                            <option value="Cat">Cat</option>
                            <option value="Bird">Bird</option>
                            <option value="Horse">Horse</option>
                            <option value="Rabbit">Rabbit</option>
                            <option value="Reptile">Reptile</option>
                            <option value="Ferret">Ferret</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>

                    <div class="row">

                        <label for="sex_2">Gender</label>
                        <select name="sex" id="sex_2">
                            <option value="Unknown">Unknown</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>

                    <div class="row left-row">
                        <label for="bride_1">Breed</label>


                        <select   name="breed" id="bride_1" required>




                            <option value="Mixed Breed">Mixed Breed</option>
                            <option value="Other">Other</option>
                            <option value="Unknown" selected>Unknown</option>


                            <option value="Affenpinscher">Affenpinscher</option>

                            <option value="Afghan Hound">Afghan Hound</option>

                            <option value="Airedale Terrier">Airedale Terrier</option>

                            <option value="Akita">Akita</option>

                            <option value="Alaskan Husky">Alaskan Husky</option>

                            <option value="Alaskan Klee Kai">Alaskan Klee Kai</option>

                            <option value="Alaskan Malamute">Alaskan Malamute</option>

                            <option value="American Bulldog">American Bulldog</option>

                            <option value="American Bully">American Bully</option>

                            <option value="American English Coonhound">American English Coonhound</option>

                            <option value="American Eskimo Dog">American Eskimo Dog</option>

                            <option value="American Foxhound">American Foxhound</option>

                            <option value="American Hairless Terrier">American Hairless Terrier</option>

                            <option value="American Leopard Hound">American Leopard Hound</option>

                            <option value="American Staffordshire Terrier">American Staffordshire Terrier</option>

                            <option value="American Water Spaniel">American Water Spaniel</option>

                            <option value="Anatolian Shepherd Dog">Anatolian Shepherd Dog</option>

                            <option value="Appenzeller Sennenhunde">Appenzeller Sennenhunde</option>

                            <option value="Australian Cattle Dog">Australian Cattle Dog</option>

                            <option value="Australian Kelpie">Australian Kelpie</option>

                            <option value="Australian Shepherd">Australian Shepherd</option>

                            <option value="Australian Terrier">Australian Terrier</option>

                            <option value="Azawakh">Azawakh</option>

                            <option value="Barbet">Barbet</option>

                            <option value="Basenji">Basenji</option>

                            <option value="Basset Fauve De Bretagne">Basset Fauve De Bretagne</option>

                            <option value="Basset Hound">Basset Hound</option>

                            <option value="Beagle">Beagle</option>

                            <option value="Bearded Collie">Bearded Collie</option>

                            <option value="Beauceron">Beauceron</option>

                            <option value="Bedlington Terrier">Bedlington Terrier</option>

                            <option value="Belgian Laekenois">Belgian Laekenois</option>

                            <option value="Belgian Malinois">Belgian Malinois</option>

                            <option value="Belgian Sheepdog">Belgian Sheepdog</option>

                            <option value="Belgian Tervuren">Belgian Tervuren</option>

                            <option value="Bergamasco">Bergamasco</option>

                            <option value="Berger Picard">Berger Picard</option>

                            <option value="Bernese Mountain Dog">Bernese Mountain Dog</option>

                            <option value="Bichon Frise">Bichon Frise</option>

                            <option value="Biewer Terrier">Biewer Terrier</option>

                            <option value="Black and Tan Coonhound">Black and Tan Coonhound</option>

                            <option value="Black Russian Terrier">Black Russian Terrier</option>

                            <option value="Blackmouth Cur">Blackmouth Cur</option>

                            <option value="Bloodhound">Bloodhound</option>

                            <option value="Bluetick Coonhound">Bluetick Coonhound</option>

                            <option value="Boerboel">Boerboel</option>

                            <option value="Bolognese">Bolognese</option>

                            <option value="Border Collie">Border Collie</option>

                            <option value="Border Terrier">Border Terrier</option>

                            <option value="Borzoi">Borzoi</option>

                            <option value="Boston Terrier">Boston Terrier</option>

                            <option value="Bouvier des Flandres">Bouvier des Flandres</option>

                            <option value="Boxer">Boxer</option>

                            <option value="Boykin Spaniel">Boykin Spaniel</option>

                            <option value="Bracco Italiano">Bracco Italiano</option>

                            <option value="Braque Du Bourbonnais">Braque Du Bourbonnais</option>

                            <option value="Briard">Briard</option>

                            <option value="Brittany Spaniel">Brittany Spaniel</option>

                            <option value="Broholmer">Broholmer</option>

                            <option value="Brussels Griffon">Brussels Griffon</option>

                            <option value="Bull Terrier">Bull Terrier</option>

                            <option value="Bulldog">Bulldog</option>

                            <option value="Bullmastiff">Bullmastiff</option>

                            <option value="Cairn Terrier">Cairn Terrier</option>

                            <option value="Canaan Dog">Canaan Dog</option>

                            <option value="Cane Corso">Cane Corso</option>

                            <option value="Cardigan Welsh Corgi">Cardigan Welsh Corgi</option>

                            <option value="Catahoula">Catahoula</option>

                            <option value="Caucasian Ovcharka">Caucasian Ovcharka</option>

                            <option value="Cavalier King Charles Spaniel">Cavalier King Charles Spaniel</option>

                            <option value="Central Asian Shepherd Dog">Central Asian Shepherd Dog</option>

                            <option value="Cesky Terrier">Cesky Terrier</option>

                            <option value="Chesapeake Bay Retriever">Chesapeake Bay Retriever</option>

                            <option value="Chihuahua">Chihuahua</option>

                            <option value="Chinese Crested">Chinese Crested</option>

                            <option value="Chinese Shar-Pei">Chinese Shar-Pei</option>

                            <option value="Chinook">Chinook</option>

                            <option value="Chow Chow">Chow Chow</option>

                            <option value="Cirneco Dell'Etna">Cirneco Dell'Etna</option>

                            <option value="Clumber Spaniel">Clumber Spaniel</option>

                            <option value="Cocker Spaniel">Cocker Spaniel</option>

                            <option value="Collie">Collie</option>

                            <option value="Coton de Tulear">Coton de Tulear</option>

                            <option value="Curly-Coated Retriever">Curly-Coated Retriever</option>

                            <option value="Czechoslovakian Vlcak">Czechoslovakian Vlcak</option>

                            <option value="Dachshund">Dachshund</option>

                            <option value="Dalmatian">Dalmatian</option>

                            <option value="Dandie Dinmont Terrier">Dandie Dinmont Terrier</option>

                            <option value="Danish-Swedish Farmdog">Danish-Swedish Farmdog</option>

                            <option value="Deutscher Wachtelhund">Deutscher Wachtelhund</option>

                            <option value="Doberman Pinscher">Doberman Pinscher</option>

                            <option value="Dogo Argentino">Dogo Argentino</option>

                            <option value="Dogue de Bordeaux">Dogue de Bordeaux</option>

                            <option value="Drentsche Patrijshond">Drentsche Patrijshond</option>

                            <option value="Drever">Drever</option>

                            <option value="Dutch Shepherd">Dutch Shepherd</option>

                            <option value="English Bulldog">English Bulldog</option>

                            <option value="English Cocker Spaniel">English Cocker Spaniel</option>

                            <option value="English Foxhound">English Foxhound</option>

                            <option value="English Pointer">English Pointer</option>

                            <option value="English Setter">English Setter</option>

                            <option value="English Shepherd">English Shepherd</option>

                            <option value="English Springer Spaniel">English Springer Spaniel</option>

                            <option value="English Toy Spaniel">English Toy Spaniel</option>

                            <option value="Entlebucher Mountain Dog">Entlebucher Mountain Dog</option>

                            <option value="Estrela Mountain Dog">Estrela Mountain Dog</option>

                            <option value="Eurasier">Eurasier</option>

                            <option value="Field Spaniel">Field Spaniel</option>

                            <option value="Finnish Lapphund">Finnish Lapphund</option>

                            <option value="Finnish Spitz">Finnish Spitz</option>

                            <option value="Flat-Coated Retriever">Flat-Coated Retriever</option>

                            <option value="French Bulldog">French Bulldog</option>

                            <option value="French Spaniel">French Spaniel</option>

                            <option value="German Longhaired Pointer">German Longhaired Pointer</option>

                            <option value="German Pinscher">German Pinscher</option>

                            <option value="German Shepherd">German Shepherd</option>

                            <option value="German Shorthaired Pointer">German Shorthaired Pointer</option>

                            <option value="German Spitz">German Spitz</option>

                            <option value="German Wirehaired Pointer">German Wirehaired Pointer</option>

                            <option value="Giant Schnauzer">Giant Schnauzer</option>

                            <option value="Glen of Imaal Terrier">Glen of Imaal Terrier</option>

                            <option value="Golden Retriever">Golden Retriever</option>

                            <option value="Gordon Setter">Gordon Setter</option>

                            <option value="Grand Basset Griffon Vendeen">Grand Basset Griffon Vendeen</option>

                            <option value="Great Dane">Great Dane</option>

                            <option value="Great Pyrenees">Great Pyrenees</option>

                            <option value="Greater Swiss Mountain Dog">Greater Swiss Mountain Dog</option>

                            <option value="Greyhound">Greyhound</option>

                            <option value="Hamiltonstovare">Hamiltonstovare</option>

                            <option value="Harrier">Harrier</option>

                            <option value="Havanese">Havanese</option>

                            <option value="Hound">Hound</option>

                            <option value="Hovawart">Hovawart</option>

                            <option value="Ibizan Hound">Ibizan Hound</option>

                            <option value="Icelandic Sheepdog">Icelandic Sheepdog</option>

                            <option value="Irish Red and White Setter">Irish Red and White Setter</option>

                            <option value="Irish Setter">Irish Setter</option>

                            <option value="Irish Terrier">Irish Terrier</option>

                            <option value="Irish Water Spaniel">Irish Water Spaniel</option>

                            <option value="Irish Wolfhound">Irish Wolfhound</option>

                            <option value="Italian Greyhound">Italian Greyhound</option>

                            <option value="Jack Russell Terrier">Jack Russell Terrier</option>

                            <option value="Jagdterrier">Jagdterrier</option>

                            <option value="Japanese Chin">Japanese Chin</option>

                            <option value="Kai Ken">Kai Ken</option>

                            <option value="Keeshond">Keeshond</option>

                            <option value="Kerry Blue Terrier">Kerry Blue Terrier</option>

                            <option value="Kishu Ken">Kishu Ken</option>

                            <option value="Komondor">Komondor</option>

                            <option value="Korean Jindo">Korean Jindo</option>

                            <option value="Kromfohrlander">Kromfohrlander</option>

                            <option value="Kuvasz">Kuvasz</option>

                            <option value="Labrador Retriever">Labrador Retriever</option>

                            <option value="Lagotto Romagnolo">Lagotto Romagnolo</option>

                            <option value="Lakeland Terrier">Lakeland Terrier</option>

                            <option value="Lancashire Heeler">Lancashire Heeler</option>

                            <option value="Leonberger">Leonberger</option>

                            <option value="Lhasa Apso">Lhasa Apso</option>

                            <option value="Löwchen">Löwchen</option>

                            <option value="Maltese">Maltese</option>

                            <option value="Manchester Terrier">Manchester Terrier</option>

                            <option value="Mastiff">Mastiff</option>

                            <option value="Miniature American Shepherd">Miniature American Shepherd</option>

                            <option value="Miniature Bull Terrier">Miniature Bull Terrier</option>

                            <option value="Miniature Pinscher">Miniature Pinscher</option>

                            <option value="Miniature Schnauzer">Miniature Schnauzer</option>

                            <option value="Mixed Breed">Mixed Breed</option>

                            <option value="Mudi">Mudi</option>

                            <option value="Neapolitan Mastiff">Neapolitan Mastiff</option>

                            <option value="Nederlandse Kooikerhondje">Nederlandse Kooikerhondje</option>

                            <option value="Newfoundland">Newfoundland</option>

                            <option value="Norfolk Terrier">Norfolk Terrier</option>

                            <option value="Norrbottenspets">Norrbottenspets</option>

                            <option value="Norwegian Buhund">Norwegian Buhund</option>

                            <option value="Norwegian Elkhound">Norwegian Elkhound</option>

                            <option value="Norwegian Lundehund">Norwegian Lundehund</option>

                            <option value="Norwich Terrier">Norwich Terrier</option>

                            <option value="Nova Scotia Duck Tolling Retriever">Nova Scotia Duck Tolling Retriever</option>

                            <option value="Old English Sheepdog">Old English Sheepdog</option>

                            <option value="Other">Other</option>

                            <option value="Otterhound">Otterhound</option>

                            <option value="Papillon">Papillon</option>

                            <option value="Parson Russell Terrier">Parson Russell Terrier</option>

                            <option value="Patterdale Terrier">Patterdale Terrier</option>

                            <option value="Pekingese">Pekingese</option>

                            <option value="Pembroke Welsh Corgi">Pembroke Welsh Corgi</option>

                            <option value="Perro De Presa Canario">Perro De Presa Canario</option>

                            <option value="Peruvian Inca Orchid">Peruvian Inca Orchid</option>

                            <option value="Petit Basset Griffon Vendéen">Petit Basset Griffon Vendéen</option>

                            <option value="Pharaoh Hound">Pharaoh Hound</option>

                            <option value="Pit Bull">Pit Bull</option>

                            <option value="Plott">Plott</option>

                            <option value="Plott">Plott</option>

                            <option value="Pointer">Pointer</option>

                            <option value="Polish Lowland Sheepdog">Polish Lowland Sheepdog</option>

                            <option value="Pomeranian">Pomeranian</option>

                            <option value="Poodle">Poodle</option>

                            <option value="Portuguese Podengo">Portuguese Podengo</option>

                            <option value="Portuguese Podengo Pequeno">Portuguese Podengo Pequeno</option>

                            <option value="Portuguese Pointer">Portuguese Pointer</option>

                            <option value="Portuguese Sheepdog">Portuguese Sheepdog</option>

                            <option value="Portuguese Water Dog">Portuguese Water Dog</option>

                            <option value="Pug">Pug</option>

                            <option value="Puli">Puli</option>

                            <option value="Puli">Puli</option>

                            <option value="Pumi">Pumi</option>

                            <option value="Pyrenean Mastiff">Pyrenean Mastiff</option>

                            <option value="Pyrenean Shepherd">Pyrenean Shepherd</option>

                            <option value="Queensland Heeler">Queensland Heeler</option>

                            <option value="Rafeiro Do Alentejo">Rafeiro Do Alentejo</option>

                            <option value="Rat Terrier">Rat Terrier</option>

                            <option value="Redbone Coonhound">Redbone Coonhound</option>

                            <option value="Rhodesian Ridgeback">Rhodesian Ridgeback</option>

                            <option value="Rottweiler">Rottweiler</option>

                            <option value="Running Walker Foxhound">Running Walker Foxhound</option>

                            <option value="Russian Toy">Russian Toy</option>

                            <option value="Russian Tsvetnaya Bolonka">Russian Tsvetnaya Bolonka</option>

                            <option value="Saint Bernard">Saint Bernard</option>

                            <option value="Saluki">Saluki</option>

                            <option value="Samoyed">Samoyed</option>

                            <option value="Schapendoes">Schapendoes</option>

                            <option value="Schipperke">Schipperke</option>

                            <option value="Schnauzer">Schnauzer</option>

                            <option value="Scottish Deerhound">Scottish Deerhound</option>

                            <option value="Scottish Terrier">Scottish Terrier</option>

                            <option value="Sealyham Terrier">Sealyham Terrier</option>

                            <option value="Shar-Pei">Shar-Pei</option>

                            <option value="Shetland Sheepdog">Shetland Sheepdog</option>

                            <option value="Shiba Inu">Shiba Inu</option>

                            <option value="Shih Tzu">Shih Tzu</option>

                            <option value="Shikoku">Shikoku</option>

                            <option value="Siberian Husky">Siberian Husky</option>

                            <option value="Silky Terrie">Silky Terrier</option>

                            <option value="Skye Terrier">Skye Terrier</option>

                            <option value="Sloughi">Sloughi</option>

                            <option value="Slovensky Cuvac">Slovensky Cuvac</option>

                            <option value="Slovensky Kopov">Slovensky Kopov</option>

                            <option value="Small Munsterlander Pointer">Small Munsterlander Pointer</option>

                            <option value="Smooth Fox Terrier">Smooth Fox Terrier</option>

                            <option value="Soft Coated Wheaten Terrier">Soft Coated Wheaten Terrier</option>

                            <option value="Spanish Mastiff">Spanish Mastiff</option>

                            <option value="Spanish Water Dog">Spanish Water Dog</option>

                            <option value="Spinone Italiano">Spinone Italiano</option>

                            <option value="Stabyhoun">Stabyhoun</option>

                            <option value="Staffordshire Bull Terrier">Staffordshire Bull Terrier</option>

                            <option value="Sussex Spaniel">Sussex Spaniel</option>

                            <option value="Swedish Lapphund">Swedish Lapphund</option>

                            <option value="Swedish Vallhund">Swedish Vallhund</option>

                            <option value="Tamaskan">Tamaskan</option>

                            <option value="Terrier Mix">Terrier Mix</option>

                            <option value="Thai Ridgeback">Thai Ridgeback</option>

                            <option value="Tibetan Mastiff">Tibetan Mastiff</option>

                            <option value="Tibetan Spaniel">Tibetan Spaniel</option>

                            <option value="Tibetan Terrier">Tibetan Terrier</option>

                            <option value="Tornjak">Tornjak</option>

                            <option value="Tosa">Tosa</option>

                            <option value="Toy Fox Terrier">Toy Fox Terrier</option>

                            <option value="Transylvanian Hound">Transylvanian Hound</option>

                            <option value="Treeing Walker Brindle">Treeing Walker Brindle</option>

                            <option value="Treeing Walker Coonhound">Treeing Walker Coonhound</option>

                            <option value="Unknown">Unknown</option>

                            <option value="Vizsla">Vizsla</option>

                            <option value="Weimaraner">Weimaraner</option>

                            <option value="Welsh Springer Spaniel">Welsh Springer Spaniel</option>

                            <option value="Welsh Terrier">Welsh Terrier</option>

                            <option value="West Highland White Terrier">West Highland White Terrier</option>

                            <option value="Whippet">Whippet</option>

                            <option value="Wire Fox Terrier">Wire Fox Terrier</option>

                            <option value="Wirehaired Pointing Griffon">Wirehaired Pointing Griffon</option>

                            <option value="Wolfdog">Wolfdog</option>

                            <option value="Working Kelpie">Working Kelpie</option>

                            <option value="Xoloitzcuintli">Xoloitzcuintli</option>

                            <option value="Yorkshire Terrier">Yorkshire Terrier</option>
                        </select>
                    </div>

                    <div class="clear"></div>

                    <div class="row">
                        <label for="color">Color</label>
                        <input type="text" name="color" id="color1" placeholder="Color">
                    </div>

                    <div class="row">
                        <label for="lost_found_date1">Lost Date</label>
                        <input type="text" name="lost_found_date" id="lost_found_date1" placeholder="Lost Date">
                    </div>

                    <div class="row">
                        <label for="autocomplete_2">Nearest Address Last Seen <span class="required">*</span></label>
                        <input type="text" name="address" id="reportlostaddress" placeholder="Nearest Address Last Seen" onfocus="geolocate()" required="" autocomplete="off">
                    </div>

                    <div class="row">
                        <label for="postal_2">Postal</label>
                        <input type="text" name="postal" id="postal_2" placeholder="Postal Code">
                    </div>



                    <div class="row">
                        <label for="description_2">Description</label>
                        <input type="text" name="description" id="description_2" placeholder="Description">
                    </div>


                    <div class="row">
                        <label for="fname_2">Contact Name <span class="required">*</span></label>
                        <input type="text" name="fname" id="fname_2" placeholder="Contact Name" required="">
                    </div>

                    <div class="row">
                        <label for="phone_2">Contact Phone <span class="required">*</span></label>
                        <input type="text" name="phone" id="phone_2" placeholder="Contact Phone" required="">
                    </div>



                    <div class="row">
                        <label for="email_2">Contact Email <span class="required">*</span></label>
                        <input type="email" name="email" id="email_2" placeholder="Contact Email" required="">
                    </div>


                    <div class="row">
                        <label for="password_2">Password <span class="required">*</span></label>
                        <input type="password" name="password" id="password_2" placeholder="Password" required="">
                    </div>

                    <div class="clear"></div>
                    <button class="btn" type="submit">Get Your Pet Back Home</button>
                    <input type="hidden" name="action" value="lost">
                    <div class="clear"></div>

                    <div id="fillinaddress2">
                        <input type="hidden" name="route" class="route">
                        <input type="hidden" name="sublocality_level_1" class="sublocality_level_1">
                        <input type="hidden" name="locality" class="locality">
                        <input type="hidden" name="administrative_area_level_3" class="administrative_area_level_3">
                        <input type="hidden" name="administrative_area_level_2" class="administrative_area_level_2">
                        <input type="hidden" name="administrative_area_level_1" class="administrative_area_level_1">
                        <input type="hidden" name="country" class="country">
                        <input type="hidden" name="postal_code" class="postal_code">
                    </div>
                </form>

                <div class="anim-form-overlay">
                    <div class="hover">
                        <h2 class="anim-form-secondary-title">
                            Lost a Pet?
                        </h2>
                        <p class="anim-form-text">
                            Report it Here. We Can Help You to Find You Family Member More Quickly.
                        </p>
                    </div>
                    <div class="link">
                        Lost a Pet?
                    </div>
                </div>

            </div>

            <div id="anim-form-right">
                <h1 class="entry-title">
                    <span>Report Found Pet</span>
                </h1>

                <form id="found-form" action="{{ url('/user/register') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="request" value="1">
                    <div class="row left-row">
                        <label for="pet_name_id_3">Pet Name or ID</label>
                        <input type="text" name="name" id="pet_name_id_3" placeholder="Pet Name or ID">
                    </div>

                    <div class="row right-row">
                        <label for="pet_photo_3">Pet Photo*</label>
                        <input type="file" name="pet_photo" id="pet_photo_3" accept="image/*" required="">
                    </div>

                    <div class="clear"></div>

                    <div class="row left-row">
                        <label for="specie_3">Specie</label>
                        <select name="specie" id="specie_3">
                            <option value="Dog">Dog</option>
                            <option value="Cat">Cat</option>
                            <option value="Bird">Bird</option>
                            <option value="Horse">Horse</option>
                            <option value="Rabbit">Rabbit</option>
                            <option value="Reptile">Reptile</option>
                            <option value="Ferret">Ferret</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>

                    <div class="row right-row">
                        <label for="sex_3">Gender</label>
                        <select name="sex" id="sex_3">
                            <option value="Unknown">Unknown</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>

                    <div class="row left-row">
                        <label for="bride_1">Breed</label>


                        <select   name="breed" id="bride_2" required>



                            <option value="Mixed Breed">Mixed Breed</option>
                            <option value="Other">Other</option>
                            <option value="Unknown" selected>Unknown</option>


                            <option value="Affenpinscher">Affenpinscher</option>

                            <option value="Afghan Hound">Afghan Hound</option>

                            <option value="Airedale Terrier">Airedale Terrier</option>

                            <option value="Akita">Akita</option>

                            <option value="Alaskan Husky">Alaskan Husky</option>

                            <option value="Alaskan Klee Kai">Alaskan Klee Kai</option>

                            <option value="Alaskan Malamute">Alaskan Malamute</option>

                            <option value="American Bulldog">American Bulldog</option>

                            <option value="American Bully">American Bully</option>

                            <option value="American English Coonhound">American English Coonhound</option>

                            <option value="American Eskimo Dog">American Eskimo Dog</option>

                            <option value="American Foxhound">American Foxhound</option>

                            <option value="American Hairless Terrier">American Hairless Terrier</option>

                            <option value="American Leopard Hound">American Leopard Hound</option>

                            <option value="American Staffordshire Terrier">American Staffordshire Terrier</option>

                            <option value="American Water Spaniel">American Water Spaniel</option>

                            <option value="Anatolian Shepherd Dog">Anatolian Shepherd Dog</option>

                            <option value="Appenzeller Sennenhunde">Appenzeller Sennenhunde</option>

                            <option value="Australian Cattle Dog">Australian Cattle Dog</option>

                            <option value="Australian Kelpie">Australian Kelpie</option>

                            <option value="Australian Shepherd">Australian Shepherd</option>

                            <option value="Australian Terrier">Australian Terrier</option>

                            <option value="Azawakh">Azawakh</option>

                            <option value="Barbet">Barbet</option>

                            <option value="Basenji">Basenji</option>

                            <option value="Basset Fauve De Bretagne">Basset Fauve De Bretagne</option>

                            <option value="Basset Hound">Basset Hound</option>

                            <option value="Beagle">Beagle</option>

                            <option value="Bearded Collie">Bearded Collie</option>

                            <option value="Beauceron">Beauceron</option>

                            <option value="Bedlington Terrier">Bedlington Terrier</option>

                            <option value="Belgian Laekenois">Belgian Laekenois</option>

                            <option value="Belgian Malinois">Belgian Malinois</option>

                            <option value="Belgian Sheepdog">Belgian Sheepdog</option>

                            <option value="Belgian Tervuren">Belgian Tervuren</option>

                            <option value="Bergamasco">Bergamasco</option>

                            <option value="Berger Picard">Berger Picard</option>

                            <option value="Bernese Mountain Dog">Bernese Mountain Dog</option>

                            <option value="Bichon Frise">Bichon Frise</option>

                            <option value="Biewer Terrier">Biewer Terrier</option>

                            <option value="Black and Tan Coonhound">Black and Tan Coonhound</option>

                            <option value="Black Russian Terrier">Black Russian Terrier</option>

                            <option value="Blackmouth Cur">Blackmouth Cur</option>

                            <option value="Bloodhound">Bloodhound</option>

                            <option value="Bluetick Coonhound">Bluetick Coonhound</option>

                            <option value="Boerboel">Boerboel</option>

                            <option value="Bolognese">Bolognese</option>

                            <option value="Border Collie">Border Collie</option>

                            <option value="Border Terrier">Border Terrier</option>

                            <option value="Borzoi">Borzoi</option>

                            <option value="Boston Terrier">Boston Terrier</option>

                            <option value="Bouvier des Flandres">Bouvier des Flandres</option>

                            <option value="Boxer">Boxer</option>

                            <option value="Boykin Spaniel">Boykin Spaniel</option>

                            <option value="Bracco Italiano">Bracco Italiano</option>

                            <option value="Braque Du Bourbonnais">Braque Du Bourbonnais</option>

                            <option value="Briard">Briard</option>

                            <option value="Brittany Spaniel">Brittany Spaniel</option>

                            <option value="Broholmer">Broholmer</option>

                            <option value="Brussels Griffon">Brussels Griffon</option>

                            <option value="Bull Terrier">Bull Terrier</option>

                            <option value="Bulldog">Bulldog</option>

                            <option value="Bullmastiff">Bullmastiff</option>

                            <option value="Cairn Terrier">Cairn Terrier</option>

                            <option value="Canaan Dog">Canaan Dog</option>

                            <option value="Cane Corso">Cane Corso</option>

                            <option value="Cardigan Welsh Corgi">Cardigan Welsh Corgi</option>

                            <option value="Catahoula">Catahoula</option>

                            <option value="Caucasian Ovcharka">Caucasian Ovcharka</option>

                            <option value="Cavalier King Charles Spaniel">Cavalier King Charles Spaniel</option>

                            <option value="Central Asian Shepherd Dog">Central Asian Shepherd Dog</option>

                            <option value="Cesky Terrier">Cesky Terrier</option>

                            <option value="Chesapeake Bay Retriever">Chesapeake Bay Retriever</option>

                            <option value="Chihuahua">Chihuahua</option>

                            <option value="Chinese Crested">Chinese Crested</option>

                            <option value="Chinese Shar-Pei">Chinese Shar-Pei</option>

                            <option value="Chinook">Chinook</option>

                            <option value="Chow Chow">Chow Chow</option>

                            <option value="Cirneco Dell'Etna">Cirneco Dell'Etna</option>

                            <option value="Clumber Spaniel">Clumber Spaniel</option>

                            <option value="Cocker Spaniel">Cocker Spaniel</option>

                            <option value="Collie">Collie</option>

                            <option value="Coton de Tulear">Coton de Tulear</option>

                            <option value="Curly-Coated Retriever">Curly-Coated Retriever</option>

                            <option value="Czechoslovakian Vlcak">Czechoslovakian Vlcak</option>

                            <option value="Dachshund">Dachshund</option>

                            <option value="Dalmatian">Dalmatian</option>

                            <option value="Dandie Dinmont Terrier">Dandie Dinmont Terrier</option>

                            <option value="Danish-Swedish Farmdog">Danish-Swedish Farmdog</option>

                            <option value="Deutscher Wachtelhund">Deutscher Wachtelhund</option>

                            <option value="Doberman Pinscher">Doberman Pinscher</option>

                            <option value="Dogo Argentino">Dogo Argentino</option>

                            <option value="Dogue de Bordeaux">Dogue de Bordeaux</option>

                            <option value="Drentsche Patrijshond">Drentsche Patrijshond</option>

                            <option value="Drever">Drever</option>

                            <option value="Dutch Shepherd">Dutch Shepherd</option>

                            <option value="English Bulldog">English Bulldog</option>

                            <option value="English Cocker Spaniel">English Cocker Spaniel</option>

                            <option value="English Foxhound">English Foxhound</option>

                            <option value="English Pointer">English Pointer</option>

                            <option value="English Setter">English Setter</option>

                            <option value="English Shepherd">English Shepherd</option>

                            <option value="English Springer Spaniel">English Springer Spaniel</option>

                            <option value="English Toy Spaniel">English Toy Spaniel</option>

                            <option value="Entlebucher Mountain Dog">Entlebucher Mountain Dog</option>

                            <option value="Estrela Mountain Dog">Estrela Mountain Dog</option>

                            <option value="Eurasier">Eurasier</option>

                            <option value="Field Spaniel">Field Spaniel</option>

                            <option value="Finnish Lapphund">Finnish Lapphund</option>

                            <option value="Finnish Spitz">Finnish Spitz</option>

                            <option value="Flat-Coated Retriever">Flat-Coated Retriever</option>

                            <option value="French Bulldog">French Bulldog</option>

                            <option value="French Spaniel">French Spaniel</option>

                            <option value="German Longhaired Pointer">German Longhaired Pointer</option>

                            <option value="German Pinscher">German Pinscher</option>

                            <option value="German Shepherd">German Shepherd</option>

                            <option value="German Shorthaired Pointer">German Shorthaired Pointer</option>

                            <option value="German Spitz">German Spitz</option>

                            <option value="German Wirehaired Pointer">German Wirehaired Pointer</option>

                            <option value="Giant Schnauzer">Giant Schnauzer</option>

                            <option value="Glen of Imaal Terrier">Glen of Imaal Terrier</option>

                            <option value="Golden Retriever">Golden Retriever</option>

                            <option value="Gordon Setter">Gordon Setter</option>

                            <option value="Grand Basset Griffon Vendeen">Grand Basset Griffon Vendeen</option>

                            <option value="Great Dane">Great Dane</option>

                            <option value="Great Pyrenees">Great Pyrenees</option>

                            <option value="Greater Swiss Mountain Dog">Greater Swiss Mountain Dog</option>

                            <option value="Greyhound">Greyhound</option>

                            <option value="Hamiltonstovare">Hamiltonstovare</option>

                            <option value="Harrier">Harrier</option>

                            <option value="Havanese">Havanese</option>

                            <option value="Hound">Hound</option>

                            <option value="Hovawart">Hovawart</option>

                            <option value="Ibizan Hound">Ibizan Hound</option>

                            <option value="Icelandic Sheepdog">Icelandic Sheepdog</option>

                            <option value="Irish Red and White Setter">Irish Red and White Setter</option>

                            <option value="Irish Setter">Irish Setter</option>

                            <option value="Irish Terrier">Irish Terrier</option>

                            <option value="Irish Water Spaniel">Irish Water Spaniel</option>

                            <option value="Irish Wolfhound">Irish Wolfhound</option>

                            <option value="Italian Greyhound">Italian Greyhound</option>

                            <option value="Jack Russell Terrier">Jack Russell Terrier</option>

                            <option value="Jagdterrier">Jagdterrier</option>

                            <option value="Japanese Chin">Japanese Chin</option>

                            <option value="Kai Ken">Kai Ken</option>

                            <option value="Keeshond">Keeshond</option>

                            <option value="Kerry Blue Terrier">Kerry Blue Terrier</option>

                            <option value="Kishu Ken">Kishu Ken</option>

                            <option value="Komondor">Komondor</option>

                            <option value="Korean Jindo">Korean Jindo</option>

                            <option value="Kromfohrlander">Kromfohrlander</option>

                            <option value="Kuvasz">Kuvasz</option>

                            <option value="Labrador Retriever">Labrador Retriever</option>

                            <option value="Lagotto Romagnolo">Lagotto Romagnolo</option>

                            <option value="Lakeland Terrier">Lakeland Terrier</option>

                            <option value="Lancashire Heeler">Lancashire Heeler</option>

                            <option value="Leonberger">Leonberger</option>

                            <option value="Lhasa Apso">Lhasa Apso</option>

                            <option value="Löwchen">Löwchen</option>

                            <option value="Maltese">Maltese</option>

                            <option value="Manchester Terrier">Manchester Terrier</option>

                            <option value="Mastiff">Mastiff</option>

                            <option value="Miniature American Shepherd">Miniature American Shepherd</option>

                            <option value="Miniature Bull Terrier">Miniature Bull Terrier</option>

                            <option value="Miniature Pinscher">Miniature Pinscher</option>

                            <option value="Miniature Schnauzer">Miniature Schnauzer</option>

                            <option value="Mixed Breed">Mixed Breed</option>

                            <option value="Mudi">Mudi</option>

                            <option value="Neapolitan Mastiff">Neapolitan Mastiff</option>

                            <option value="Nederlandse Kooikerhondje">Nederlandse Kooikerhondje</option>

                            <option value="Newfoundland">Newfoundland</option>

                            <option value="Norfolk Terrier">Norfolk Terrier</option>

                            <option value="Norrbottenspets">Norrbottenspets</option>

                            <option value="Norwegian Buhund">Norwegian Buhund</option>

                            <option value="Norwegian Elkhound">Norwegian Elkhound</option>

                            <option value="Norwegian Lundehund">Norwegian Lundehund</option>

                            <option value="Norwich Terrier">Norwich Terrier</option>

                            <option value="Nova Scotia Duck Tolling Retriever">Nova Scotia Duck Tolling Retriever</option>

                            <option value="Old English Sheepdog">Old English Sheepdog</option>

                            <option value="Other">Other</option>

                            <option value="Otterhound">Otterhound</option>

                            <option value="Papillon">Papillon</option>

                            <option value="Parson Russell Terrier">Parson Russell Terrier</option>

                            <option value="Patterdale Terrier">Patterdale Terrier</option>

                            <option value="Pekingese">Pekingese</option>

                            <option value="Pembroke Welsh Corgi">Pembroke Welsh Corgi</option>

                            <option value="Perro De Presa Canario">Perro De Presa Canario</option>

                            <option value="Peruvian Inca Orchid">Peruvian Inca Orchid</option>

                            <option value="Petit Basset Griffon Vendéen">Petit Basset Griffon Vendéen</option>

                            <option value="Pharaoh Hound">Pharaoh Hound</option>

                            <option value="Pit Bull">Pit Bull</option>

                            <option value="Plott">Plott</option>

                            <option value="Plott">Plott</option>

                            <option value="Pointer">Pointer</option>

                            <option value="Polish Lowland Sheepdog">Polish Lowland Sheepdog</option>

                            <option value="Pomeranian">Pomeranian</option>

                            <option value="Poodle">Poodle</option>

                            <option value="Portuguese Podengo">Portuguese Podengo</option>

                            <option value="Portuguese Podengo Pequeno">Portuguese Podengo Pequeno</option>

                            <option value="Portuguese Pointer">Portuguese Pointer</option>

                            <option value="Portuguese Sheepdog">Portuguese Sheepdog</option>

                            <option value="Portuguese Water Dog">Portuguese Water Dog</option>

                            <option value="Pug">Pug</option>

                            <option value="Puli">Puli</option>

                            <option value="Puli">Puli</option>

                            <option value="Pumi">Pumi</option>

                            <option value="Pyrenean Mastiff">Pyrenean Mastiff</option>

                            <option value="Pyrenean Shepherd">Pyrenean Shepherd</option>

                            <option value="Queensland Heeler">Queensland Heeler</option>

                            <option value="Rafeiro Do Alentejo">Rafeiro Do Alentejo</option>

                            <option value="Rat Terrier">Rat Terrier</option>

                            <option value="Redbone Coonhound">Redbone Coonhound</option>

                            <option value="Rhodesian Ridgeback">Rhodesian Ridgeback</option>

                            <option value="Rottweiler">Rottweiler</option>

                            <option value="Running Walker Foxhound">Running Walker Foxhound</option>

                            <option value="Russian Toy">Russian Toy</option>

                            <option value="Russian Tsvetnaya Bolonka">Russian Tsvetnaya Bolonka</option>

                            <option value="Saint Bernard">Saint Bernard</option>

                            <option value="Saluki">Saluki</option>

                            <option value="Samoyed">Samoyed</option>

                            <option value="Schapendoes">Schapendoes</option>

                            <option value="Schipperke">Schipperke</option>

                            <option value="Schnauzer">Schnauzer</option>

                            <option value="Scottish Deerhound">Scottish Deerhound</option>

                            <option value="Scottish Terrier">Scottish Terrier</option>

                            <option value="Sealyham Terrier">Sealyham Terrier</option>

                            <option value="Shar-Pei">Shar-Pei</option>

                            <option value="Shetland Sheepdog">Shetland Sheepdog</option>

                            <option value="Shiba Inu">Shiba Inu</option>

                            <option value="Shih Tzu">Shih Tzu</option>

                            <option value="Shikoku">Shikoku</option>

                            <option value="Siberian Husky">Siberian Husky</option>

                            <option value="Silky Terrie">Silky Terrier</option>

                            <option value="Skye Terrier">Skye Terrier</option>

                            <option value="Sloughi">Sloughi</option>

                            <option value="Slovensky Cuvac">Slovensky Cuvac</option>

                            <option value="Slovensky Kopov">Slovensky Kopov</option>

                            <option value="Small Munsterlander Pointer">Small Munsterlander Pointer</option>

                            <option value="Smooth Fox Terrier">Smooth Fox Terrier</option>

                            <option value="Soft Coated Wheaten Terrier">Soft Coated Wheaten Terrier</option>

                            <option value="Spanish Mastiff">Spanish Mastiff</option>

                            <option value="Spanish Water Dog">Spanish Water Dog</option>

                            <option value="Spinone Italiano">Spinone Italiano</option>

                            <option value="Stabyhoun">Stabyhoun</option>

                            <option value="Staffordshire Bull Terrier">Staffordshire Bull Terrier</option>

                            <option value="Sussex Spaniel">Sussex Spaniel</option>

                            <option value="Swedish Lapphund">Swedish Lapphund</option>

                            <option value="Swedish Vallhund">Swedish Vallhund</option>

                            <option value="Tamaskan">Tamaskan</option>

                            <option value="Terrier Mix">Terrier Mix</option>

                            <option value="Thai Ridgeback">Thai Ridgeback</option>

                            <option value="Tibetan Mastiff">Tibetan Mastiff</option>

                            <option value="Tibetan Spaniel">Tibetan Spaniel</option>

                            <option value="Tibetan Terrier">Tibetan Terrier</option>

                            <option value="Tornjak">Tornjak</option>

                            <option value="Tosa">Tosa</option>

                            <option value="Toy Fox Terrier">Toy Fox Terrier</option>

                            <option value="Transylvanian Hound">Transylvanian Hound</option>

                            <option value="Treeing Walker Brindle">Treeing Walker Brindle</option>

                            <option value="Treeing Walker Coonhound">Treeing Walker Coonhound</option>

                            <option value="Unknown">Unknown</option>

                            <option value="Vizsla">Vizsla</option>

                            <option value="Weimaraner">Weimaraner</option>

                            <option value="Welsh Springer Spaniel">Welsh Springer Spaniel</option>

                            <option value="Welsh Terrier">Welsh Terrier</option>

                            <option value="West Highland White Terrier">West Highland White Terrier</option>

                            <option value="Whippet">Whippet</option>

                            <option value="Wire Fox Terrier">Wire Fox Terrier</option>

                            <option value="Wirehaired Pointing Griffon">Wirehaired Pointing Griffon</option>

                            <option value="Wolfdog">Wolfdog</option>

                            <option value="Working Kelpie">Working Kelpie</option>

                            <option value="Xoloitzcuintli">Xoloitzcuintli</option>

                            <option value="Yorkshire Terrier">Yorkshire Terrier</option>
                        </select>
                    </div>

                    <div class="clear"></div>

                    <div class="row">
                        <label for="color">Color</label>
                        <input type="text" name="color" id="color2" placeholder="Color">
                    </div>

                    <div class="row left-row">
                        <label for="description_3">Description</label>
                        <input type="text" name="description" id="description_3" placeholder="Description">
                    </div>

                    <div class="clear"></div>

                    <div class="row left-row">
                        <label for="circumstances_3">Circumstances <span class="required">*</span></label>
                        <select name="circumstances" id="circumstances_3">
                            <option value="In my possession">In my possession</option>
                            <option value="Sighting">Sighting (still roaming)</option>
                            <option value="Deceased">Deceased</option>
                        </select>
                    </div>

                    <div class="row">
                        <label for="lost_found_date">Found Date</label>
                        <input type="text" name="lost_found_date" id="lost_found_date2" placeholder="Found Date">
                    </div>

                    <div class="row right-row">
                        <label for="autocomplete_3">Nearest Address Last Seen <span class="required">*</span></label>
                        <input type="text" name="address" id="foundaddress" placeholder="Nearest Address Last Seen" onfocus="geolocate()" required="" autocomplete="off">
                    </div>


                    <div class="row left-row">
                        <label for="postal_3">Postal</label>
                        <input type="text" name="postal" id="postal_3" placeholder="Postal Code">
                    </div>

                    <div class="row">
                        <label for="fname_3">Contact Name <span class="required">*</span></label>
                        <input type="text" name="fname" id="fname_3" placeholder="Contact Name" required="">
                    </div>

                    <div class="row">
                        <label for="phone_3">Contact Phone <span class="required">*</span></label>
                        <input type="text" name="phone" id="phone_3" placeholder="Contact Phone" required="">
                    </div>


                    <div class="row">
                        <label for="email_3">Contact Email <span class="required">*</span></label>
                        <input type="email" name="email" id="email_3" placeholder="Contact Email" required="">
                    </div>

                    <div class="row">
                        <label for="password_3">Password <span class="required">*</span></label>
                        <input type="password" name="password" id="password_3" placeholder="Password" required="">
                    </div>

                    <div class="clear"></div>

                    <button class="btn">Get This Pet Back Home</button>
                    <input type="hidden" name="action" value="found">
                    <div class="clear"></div>

                    <div id="fillinaddress3">
                        <input type="hidden" name="route" class="route">
                        <input type="hidden" name="sublocality_level_1" class="sublocality_level_1">
                        <input type="hidden" name="locality" class="locality">
                        <input type="hidden" name="administrative_area_level_3" class="administrative_area_level_3">
                        <input type="hidden" name="administrative_area_level_2" class="administrative_area_level_2">
                        <input type="hidden" name="administrative_area_level_1" class="administrative_area_level_1">
                        <input type="hidden" name="country" class="country">
                        <input type="hidden" name="postal_code" class="postal_code">
                    </div>
                </form>

                <div class="anim-form-overlay">
                    <div class="hover">
                        <h2 class="anim-form-secondary-title">
                            Found A Pet?
                        </h2>
                        <p class="anim-form-text">
                            Report it here. We Can Help the Pet to Get Back Home More Quickly.
                        </p>
                    </div>
                    <div class="link">
                        Found A Pet?
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>

    </div><!-- /.warper -->
    <div class="clear"></div>


    </div>
@endsection
