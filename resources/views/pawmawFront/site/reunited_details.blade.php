@extends('pawmawFront.layouts.front')

@section('title', 'Found Pet - PawMaw')

@section('content')
    <div class="site-content">
        <div class="warper pet-single">
            <div id="primary">
                <h1 class="entry-title">
                    <span>Found Pet - <em>A Female Dog reunited</em></span>
                </h1>

                <div class="clear"></div>

                <div class="post-thumbnail singular">
                    <a class="full-image-link fancybox" href="{{ asset('pawmaw/img/latest_alerts/2.jpg') }}" title="Found Pet">
                        <span class="genericon genericon-zoom"></span>
                    </a>

                    <img
                            class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="Found Pet"
                            width="250" height="250"
                            src="{{ asset('pawmaw/img/latest_alerts/2-250x250.jpg') }}"
                            srcset="{{ asset('pawmaw/img/latest_alerts/2-250x250.jpg') }} 250w,
                                                            {{ asset('pawmaw/img/latest_alerts/2-150x150.jpg') }} 150w,
                                                            {{ asset('pawmaw/img/latest_alerts/2-300x300.jpg') }} 300w,
                                                            {{ asset('pawmaw/img/latest_alerts/2-100x100.jpg') }} 100w"
                            sizes="(max-width: 250px) 100vw, 250px"
                    >
                </div>

                <div class="entry-content">
                    <div class="tag reunited">reunited</div>

                    <div class="clear"></div>

                    <strong class="petid">ID: #1178</strong>

                    <div class="info-row">
                        Area last seen:
                        <a href="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=Pulteney+St%2C+Adelaide+SA+5000%2C+Australia&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" class="fancyiframe info-value">
                            Pulteney St, Adelaide SA 5000, Australia
                        </a>
                    </div>

                    <div class="info-row">
                        Circumstances: <span class="info-value">In my possession</span>
                    </div>

                    <div class="info-row">
                        Species: <span class="info-value">Dog</span>
                    </div>

                    <div class="info-row">
                        Color: <span class="info-value">WHITE, TAN-CREAM</span>
                    </div>

                    <div class="info-row">
                        Gender: <span class="info-value">Male</span>
                    </div>

                    <div class="description">
                        <p>Small, approxitmately 5 lps. Unaltered, I would guess his age is between 18 months and 3 years. Cauliflower ears, overall in good health.</p>
                    </div>

                    <div class="clear"></div>

                    <span class="sbtn share" onclick="javascript:facebookSNCustom('#', 'Found Pet - A Male Dog Found');">
                        <span class="display genericon genericon-facebook"></span>
                        Share on Facebook
                    </span>

                    <div class="clear"></div>

                </div>
                <div class="clear"></div>
            </div>

            <div id="secondary">
                <h2 class="widget-title">
                    <span>Contact Finder</span>
                </h2>

                <img src="{{ asset('pawmaw/img/user.jpg') }}" class="user-photo">

                <div class="user-fullname">
                    Amber  Sennitt
                </div>

                <div class="user-location">
                    <span class="genericon genericon-location"></span>
                    Pulteney St, Adelaide SA 5000, Australia
                </div><a href="#" class="btn" id="sendmsg"><span class="genericon genericon-mail"></span>E-mail Finder</a><a href="tel:08 9324 1007" class="btn"><span class="genericon genericon-handset"></span>Call Finder</a>
            </div>
            <div class="clear"></div>
        </div>


        <style type="text/css">
            #sendsms-overlay {
                position: fixed;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background: rgba(0,0,0,0.5);
                z-index: 99999;
                overflow: auto;
                display: none;
            }
            #sendsms-form {
                background: #fff;
                max-width: 100%;
                box-sizing: border-box;
                position: fixed;
                right: -120%;
                top: 0;
                bottom: 0;
                width: 500px;
                padding: 10px 35px;
                overflow: auto;
            }
            #close-sendsms {
                position: absolute;
                top: 20px;
                right: 20px;
                cursor: pointer;
            }
            .post-thumbnail.singular {
                float: left;
                margin-right: 0;
                width: 40%;
            }
            body.single .entry-content {
                float: right;
                width: 56%;
            }
            .sbtn.share {
                margin-right: 0;
                border-color: #405D9B;
                color: #405D9B;
                padding: 10px 0;
                max-width: 100%;
                width: 250px;
                border-radius: 5px;
            }
            .sbtn.share .genericon {
                line-height: 26px;
                margin-right: 2px;
            }
            #secondary .btn {
                width:100%;
            }
            #secondary .btn .genericon {
                line-height: 24px;
                margin-right: 5px;
            }
            #secondary .user-fullname {
                text-align: center;
                font-size: 24px;
                margin-bottom: 15px;
            }
            #secondary .user-location {
                text-align: center;
                margin-bottom: 20px;
                color: #999;
            }
            #secondary .user-location .genericon {
                line-height: 24px;
                color: #ccc;
            }
            #secondary .user-photo {
                display: block;
                border-radius: 50%;
                width: 150px;
                min-height:150px;
                margin-bottom: 15px;
                background:url(https://www.pawmaw.com/wp-content/themes/pet-finder/img/user.jpg);
                background-size:cover;
            }
            .tag {
                float: left;
                position: relative;
                width: auto;
                padding: 6px 15px;
                color: #fff;
                font-size: 18px;
                font-weight: 600;
                text-decoration: none;
                margin-bottom: 15px;
                border-radius: 20px 3px;
                text-shadow: 1px 1px 1px rgba(0,0,0,0.3);
                text-transform: uppercase;
                letter-spacing: 1px;
            }
            .tag.lost {
                background:#dc3545;
            }
            .tag.found {
                background:#ec4b50;
            }
            .tag.reunited {
                background:#393;
            }
            .info-row {
                font-size: 18px;
                line-height: 120%;
                margin-bottom: 10px;
                color: #999;
            }
            .info-value {
                color:#ec4b50;
            }
            a.info-value:hover {
                text-decoration:underline;
            }
            @media screen and (max-width: 980px) {
                #secondary {
                    margin-bottom: 30px;
                }
            }
            @media screen and (max-width: 540px) {
                .post-thumbnail.singular {
                    float: none;
                    margin: 0 0 20px;
                    width: 100%;
                }
                body.single .entry-content {
                    float: none;
                    width: 100%;
                }
                .sbtn.share {
                    width: 100%;
                }
                .tag {
                    margin: 0 auto 20px;
                    float: none;
                    text-align: center;
                }
            }
        </style>

        <div id="sendsms-overlay">
            <form action="" method="post" id="sendsms-form">
                <span class="genericon genericon-close-alt" id="close-sendsms"></span>
                <strong id="login-title">Send Message</strong>
                <div class="clear"></div>



                <div class="row">
                    <label for="msg_name">Name</label>
                    <input type="text" name="msg_name" id="msg_name" placeholder="Name">
                </div>

                <div class="row">
                    <label for="msg_email">Email <span class="required">*</span></label>
                    <input type="email" name="msg_email" id="msg_email" placeholder="Email" required="">
                </div>

                <div class="row">
                    <label for="msg_message">Message <span class="required">*</span></label>
                    <textarea id="msg_message" name="msg_message" placeholder="Your message..." required=""></textarea>
                </div>

                <div class="clear"></div>
                <button class="btn">Send</button>
                <div class="clear"></div>
            </form>
        </div>

        <script type="text/javascript">
            jQuery(document).ready(function()
            {
                jQuery("#sendmsg").on("click", function()
                {
                    jQuery("#sendsms-overlay").fadeIn(function()
                    {
                        jQuery("#sendsms-form").animate({"right":"0"}, 600);
                    });
                    return false;
                });

                jQuery("#close-sendsms").on("click", function()
                {
                    jQuery("#sendsms-form").animate({"right":"-120%"}, 600, function()
                    {
                        jQuery("#sendsms-overlay").fadeOut();
                    });
                });
            });
        </script>

        <div class="clear"></div>
    </div>
@endsection