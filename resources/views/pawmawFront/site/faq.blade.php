@extends('pawmawFront.layouts.front')

@section('title', 'FAQ - PawMaw')

@section('content')
<!-- FAQ HERO SECTION START -->
<section class="faq">


   
   
    <!--  mobile version -->
    
    
    
    <div class="section-dog-bg-mb d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 109px,center;background-size: 262px,cover,cover;background-repeat: no-repeat;"></div>

    <!--  landscape version -->
    
    
    <div class="section-dog-bg-sm d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 125px,center;background-size: 430px,cover,cover;background-repeat: no-repeat;"></div>


    <!--  tab version -->
    
    <div class="section-dog-bg-md d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 182px,center; background-size: 600px,cover,cover;background-repeat: no-repeat;"></div>



    <!--  full screen version -->
    <div class="section-dog-bg" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}}); background-position: center bottom,center 213px,center; background-size: 820px,cover,cover;"></div>

   
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1 col-md-12 col-sm-12">
                <div class="faq-title others-page-title mx-auto d-block mb-5">
                    <h1 class="wow pulse" data-wow-duration="1s" data-wow-delay=".3s">
                        <img src="{{ asset('pawmaw/img/icons/hand-black.png')}}" alt="Hand" class="img-fluid"> <span>Frequently Asked Questions (FAQ)</span> <img src="{{ asset('pawmaw/img/icons/hand-black.png')}}" alt="Hand" class="img-fluid">
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="accordion" id="accordionFAQ">
                    <div class="card border-0 arrow-angle-toggle">
                        <a class="card-header text-center mb-3 active" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            How PawMaw works?
                        </a>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionFAQ">
                            <div class="card-body">
                                <h2>
                                    <img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand Icon" class="img-fluid">
                                    PawMaw provides Lost and Found Cats, Dogs, and Pets Rescue Service. By reporting a pet your post will be seen by local people in your area. It's always gratifying when a loving owner is reunited with their beloved pet. See more on how it works page
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="card border-0 arrow-angle-toggle">
                        <a class="card-header mb-3 collapsed text-center" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Where can I report pet? 
                        </a>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionFAQ">
                            <div class="card-body">
                                <h2>
                                    <img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand Icon" class="img-fluid">
                                    You can easily report LOST/FOUND pets from the home page or by logging into your PawMaw account. 
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="card border-0 arrow-angle-toggle">
                        <a class="card-header mb-3 collapsed text-center" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                           Where can I see my post?
                        </a>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionFAQ">
                            <div class="card-body">
                                <h2>
                                    <img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand Icon" class="img-fluid">
                                    You can view your post where it posted by logging in to your account. Also, you will receive a mail with the post link. Where you will be able to see your post. You can also see your post as a featured post on the website. 
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="card border-0 arrow-angle-toggle">
                        <a class="card-header mb-3 collapsed text-center" id="headingFour" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            What is featured post?
                        </a>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionFAQ">
                            <div class="card-body">
                                <h2>
                                    <img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand Icon" class="img-fluid">
                                   Featured post displays your post on the top of the page on the website. And it can be seen by more people by the website visitor.
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="card border-0 arrow-angle-toggle">
                        <a class="card-header mb-3 collapsed text-center" id="headingFive" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            How can I reach more people on my post?
                        </a>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFour" data-parent="#accordionFAQ">
                            <div class="card-body">
                                <h2>
                                    <img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand Icon" class="img-fluid">
                                    Yes! You can reach more people with your post by activating the PawMaw alert in your local area. 
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="card border-0 arrow-angle-toggle">
                        <a class="card-header mb-3 collapsed text-center" id="headingSix" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            What is Paw-Maw Alert?
                        </a>
                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionFAQ">
                            <div class="card-body">
                                <h2>
                                    <img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand Icon" class="img-fluid">
                                    By activating PawMaw alert you can instantly reach 1000s of peoples in your area within 10 km radius.  It’s a very effective way to find your lost pet quickly. 
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="card border-0 arrow-angle-toggle">
                        <a class="card-header mb-3 collapsed text-center" id="headingSeven" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                            What is update listing?
                        </a>
                        <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionFAQ">
                            <div class="card-body">
                                <h2>
                                    <img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand Icon" class="img-fluid">
                                    Update listing helps your post to come up on the top of the feature post and it can be seen by more people. You can Easily update your listing by logging into your account >View Pet Dashboard >Update Listing.  
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="card border-0 arrow-angle-toggle">
                        <a class="card-header mb-3 collapsed text-center" id="headingEight" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                            How can I download the LOST/FOUND pet flyer? 
                        </a>
                        <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionFAQ">
                            <div class="card-body">
                                <h2>
                                    <img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand Icon" class="img-fluid">
                                    After you have report a pet you can easily download it by logging into your account >View Pet Dashboard >Download flyer. Or You will get an automated email with the flyer. Just click on the download. 
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="card border-0 arrow-angle-toggle">
                        <a class="card-header mb-3 collapsed text-center" id="heading9" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
                           I have reported a pet with wrong information, how do I change it?
                        </a>
                        <div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#accordionFAQ">
                            <div class="card-body">
                                <h2>
                                    <img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand Icon" class="img-fluid">
                                    You can Easily Edit your post by logging into your account >View Dashboard > Click Edit icon.
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="card border-0 arrow-angle-toggle">
                        <a class="card-header mb-3 collapsed text-center" id="heading10" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse10">
                           How do I change the Phone number on my account?
                        </a>
                        <div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#accordionFAQ">
                            <div class="card-body">
                                <h2>
                                    <img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand Icon" class="img-fluid">
                                    You can Easily Change your phone number it by logging into your account >View Dashboard > Click Edit icon
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="card border-0 arrow-angle-toggle">
                        <a class="card-header mb-3 collapsed text-center" id="heading11" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
                          I have found my lost pet, what should I do now?
                        </a>
                        <div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#accordionFAQ">
                            <div class="card-body">
                                <h2>
                                    <img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand Icon" class="img-fluid">
                                   You can easily mark as reunited your post by logging into your account >View Pet Dashboard >Mark as a Reunited. 
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="card border-0 arrow-angle-toggle">
                        <a class="card-header mb-3 collapsed text-center" id="heading12" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapse12">
                           If I found a pet that already posted on this website what should I do?
                        </a>
                        <div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#accordionFAQ">
                            <div class="card-body">
                                <h2>
                                    <img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand Icon" class="img-fluid">
                                    You can easily contact the owner of that pet. In the pet details there is Owner/Finder email and phone number. You can easily contact them by email or phone.
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="card border-0 arrow-angle-toggle">
                        <a class="card-header mb-3 collapsed text-center" id="heading13" data-toggle="collapse" data-target="#collapse13" aria-expanded="false" aria-controls="collapse13">
                           Do you guarantee to find my lost or stolen pet?
                        </a>
                        <div id="collapse13" class="collapse" aria-labelledby="heading13" data-parent="#accordionFAQ">
                            <div class="card-body">
                                <h2>
                                    <img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand Icon" class="img-fluid">
                                    Although we do our best to find your LOST/STOLEN pet, we cannot guarantee that your pet will be found. Our service is an effective way to find a missing pet. 
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="card border-0 arrow-angle-toggle">
                        <a class="card-header mb-3 collapsed text-center" id="heading14" data-toggle="collapse" data-target="#collapse14" aria-expanded="false" aria-controls="collapse14">
                            How can I delete my post?
                        </a>
                        <div id="collapse14" class="collapse" aria-labelledby="heading14" data-parent="#accordionFAQ">
                            <div class="card-body">
                                <h2>
                                    <img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand Icon" class="img-fluid">
                                    You can easily delete your pet by sending us an email with the pet ID. We will delete your post within 24 hours.  
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="card border-0 arrow-angle-toggle">
                        <a class="card-header mb-3 collapsed text-center" id="heading15" data-toggle="collapse" data-target="#collapse15" aria-expanded="false" aria-controls="collapse15">
                            Why my post was deleted? 
                        </a>
                        <div id="collapse15" class="collapse" aria-labelledby="heading15" data-parent="#accordionFAQ">
                            <div class="card-body">
                                <h2>
                                    <img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand Icon" class="img-fluid">
                                    Maybe there wasn’t proper information to post it. Or wrong photo was posted with the pet. But you can still report a pet with proper information. 
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="card border-0 arrow-angle-toggle">
                        <a class="card-header mb-3 collapsed text-center" id="heading16" data-toggle="collapse" data-target="#collapse16" aria-expanded="false" aria-controls="collapse16">
                           Why should I join PawMaw Network?
                        </a>
                        <div id="collapse16" class="collapse" aria-labelledby="heading16" data-parent="#accordionFAQ">
                            <div class="card-body">
                                <h2>
                                    <img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand Icon" class="img-fluid">
                                    It's a great way to helping others with their lost pet. They can find that lost pet quickly with a little help from you. 
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="card border-0 arrow-angle-toggle">
                        <a class="card-header mb-3 collapsed text-center" id="heading17" data-toggle="collapse" data-target="#collapse17" aria-expanded="false" aria-controls="collapse17">
                           I have more questions. What should I do?
                        </a>
                        <div id="collapse17" class="collapse" aria-labelledby="heading17" data-parent="#accordionFAQ">
                            <div class="card-body">
                                <h2>
                                    <img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand Icon" class="img-fluid">
                                   Simply email us <a href="mailto:info@pawmaw.com">info@pawmaw.com</a> or message us on PawMaw official Facebook page or give us a call on <a href="tel:+1 909 764 0024">+1 909 764 0024.</a> We will happy to answer all the questions.
                                </h2>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- FAQ HERO SECTION END -->

<!-- GET YOUR PET BACK HOME SECTION START -->
<section class="getyourpet">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="getyourpet-title wow swing" data-wow-duration="1s" data-wow-delay=".3s">Get your pet back home</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <a href="{{ url('/?active-form=1') }}" class="getyourpet-btn mx-auto d-block wow bounceIn" data-wow-duration="1s" data-wow-delay=".3s">Report lost pet</a>
            </div>
        </div>
    </div>
</section>
<!-- GET YOUR PET BACK HOME SECTION END -->
@endsection
