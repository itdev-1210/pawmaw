@extends('pawmawFront.layouts.front')

@section('title', 'Privacy Policy - PawMaw')

@section('content')
<!-- ABOUT HERO SECTION START -->
<section class="privacy-policy">
<!--    <div class="section-dog-bg"style="background-image: url({{ asset('pawmaw/img/common-bg.png')}});background-position: left -60px;background-size: cover;"></div>-->
   
   
    <!--  mobile version -->
    
    
    
    <div class="section-dog-bg-mb d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 109px,center;background-size: 262px,cover,cover;background-repeat: no-repeat;"></div>

    <!--  landscape version -->
    
    
    <div class="section-dog-bg-sm d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 125px,center;background-size: 430px,cover,cover;background-repeat: no-repeat;"></div>


    <!--  tab version -->
    
    <div class="section-dog-bg-md d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 182px,center; background-size: 600px,cover,cover;background-repeat: no-repeat;"></div>



    <!--  full screen version -->
    <div class="section-dog-bg" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}}); background-position: center bottom,center 213px,center; background-size: 820px,cover,cover;"></div>

    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-8 offset-sm-2">
                <div class="privacy-policy_title others-page-title mx-auto d-block mb-5">
                    <h1>
                        <img src="{{ asset('pawmaw/img/icons/hand-black.png')}}" alt="Hand" class="img-fluid"> <span>Privacy Policy</span> <img src="{{ asset('pawmaw/img/icons/hand-black.png')}}" alt="Hand" class="img-fluid">
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h2 class="privacy-policy_sub mt-5">
                    THIS POLICY EXPLAINS HOW WE WILL UTILIZE OUR CUSTOMER’S information
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="privacy-policy_details py-5">
                    <p class="privacy-policy_details-text mb-3">
                        We take your privacy seriously and will take all measures to protect your personal information. This Privacy Policy describes how we <strong>collect, use, share, protect and process</strong> your information.
                    </p>
                    <p class="privacy-policy_details-text mb-3">
                        Any information that you provide to us is used strictly to process and fulfill your purchase and/or for internal analytical purposes, as described in this Privacy Policy. We only share information for the purposes set out in this Privacy Policy, and we will not sell or redistribute any of your information to any third party. We use commercially reasonable efforts to protect your information from unauthorized disclosure. We may use the information from your order to process your purchase, provide a refund, provide customer service, improve or customize our products and services or fulfill applicable financial, tax, legal or compliance requirements.
                    </p>
                    <p class="privacy-policy_details-text mb-3">
                        We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.
                    </p>
                    <p class="privacy-policy_details-text mb-3">
                        While we use encryption to protect sensitive information transmitted online, we also protect your information offline.
                    </p>
                    <p class="privacy-policy_details-text mb-3">
                        If you have any questions or concerns related to this Privacy Policy, please contact us at
                        <a href="#" class="pawInfo">info@PawMaw.com</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ABOUT HERO SECTION END -->

<!-- GET YOUR PET BACK HOME SECTION START -->
<section class="getyourpet">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="getyourpet-title wow swing" data-wow-duration="1s" data-wow-delay=".3s">Get your pet back home</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <a href="{{ url('/?active-form=1') }}" class="getyourpet-btn mx-auto d-block wow bounceIn" data-wow-duration="1s" data-wow-delay=".3s">Report lost pet</a>
            </div>
        </div>
    </div>
</section>
<!-- GET YOUR PET BACK HOME SECTION END -->
@endsection
