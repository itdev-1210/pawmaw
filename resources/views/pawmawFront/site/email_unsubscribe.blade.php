@extends('pawmawFront.layouts.front')

@section('title', 'Unsubscribe - PawMaw')

@section('content')
    <section class="login-bg h-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-sm-12">
                    <div class="reset-login-box mx-auto d-block">
                        <div class="reset-login-box-content pass-reset-pass-page">
                            <div class="warper">
                                <span class="reset-log-title-icon text-center"></span>
                                <h2 class="reset-login-title">Unsubscribe Emails</h2>
										@if(session('success'))
                                            <div class="alert alert-success">
                                                {{ session('success') }}
                                            </div>

                                            <script>
                                                setTimeout(function () {
                                                    $('.alert-success').fadeOut();
                                                }, 3000);
                                            </script>
                                        @endif
                                <form class="reset-login-form mt-4" method="POST" action="{{ url('/unsubscribe') }}">
                                    {{ csrf_field() }}
                                 
									<input type="hidden" id="user_email" name="user_email" ></input>
									<h3 style="color:white">Click on unsubscribe to not recieve any future emails from pawmaw.</h3>
                                    <button type="submit" class="reset-login-btn text-center mx-auto d-block text-capitalize">Unsubscribe</button>
                                    <script>
                                    function getUrlVars()
                                    {
                                        var vars = [], hash;
                                        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                                        for(var i = 0; i < hashes.length; i++)
                                        {
                                            hash = hashes[i].split('=');
                                            vars.push(hash[0]);
                                            vars[hash[0]] = hash[1];
                                        }
                                        return vars;
                                    }
                                    var email=getUrlVars()["email"];
                                    $('#user_email').val(email);
                                    </script>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- GET YOUR PET BACK HOME SECTION START -->
    <section class="getyourpet mt-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="getyourpet-title wow swing" data-wow-duration="1s" data-wow-delay=".3s">Get your pet back home</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <a href="{{ url('/?active-form=1') }}" class="getyourpet-btn mx-auto d-block wow bounceIn" data-wow-duration="1s" data-wow-delay=".3s">Report lost pet</a>
                </div>
            </div>
        </div>
    </section>
    <!-- GET YOUR PET BACK HOME SECTION END -->
@endsection
