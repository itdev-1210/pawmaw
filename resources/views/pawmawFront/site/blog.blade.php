@extends('pawmawFront.layouts.front')
@section('title', 'Blog - PawMaw')
@section('content')

<section class="blog_Page_header">
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-lg-6">
                <div class="blog_banner text-center">
                    <h1><span>Welcome </span> to <span>PawMaw</span> blog!</h1>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="blog_contents">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 mt-5">
                
                @foreach($posts as $post)
                    <div class="blog_details mt-5">
                        <div class="blog_details_title">
                            <div class="row">
                                <div class="col-lg-2 col-2 m-0 p-0">
                                    <div class="blog_date text-center">
                                        <h4>
                                            {{ $post['created_at']->format('d') }}
                                            <span>
                                                {{ $post['created_at']->format('F') }} 
                                                <br> 
                                                <span>{{ $post['created_at']->format('Y') }}</span>
                                            </span>
                                        </h4>
                                    </div>
                                </div>
                                <div class="col-lg-10 col-10 m-0 p-0">
                                    <div class="blog_title">
                                        <h2>{{ $post['title'] }}</h2>
                                        <h4>Posted by <span>PawMaw</span></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blog_details_img">
                            <a href="{{ url('/blog/' . $post['slug']) }}"> 
                                <img src="{{ asset('img') . '/' . $post['image'] }}" class="img-fluid" alt="">
                            </a>
                        </div>
                        <p>
                            {!! html_entity_decode($post['excerpt']) !!}
                        </p>
                        <div class="blog_btn">
                            <a href="{{ url('/blog/' . $post['slug']) }}">Read More</a>
                        </div>
                        {{-- <div class="blog_view_btn text-center">
                            <div class="row">
                                <div class="col-lg-6 col-sm-6">
                                    <a href="#"><i class="fas fa-map-pin mr-3"></i>Pin blog</a>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <a href="#"><i class="fas fa-eye mr-3"></i>1234 <span>Views</span></a>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                @endforeach
                <div class="pagination" style="margin-top: 30px;">
                    <center>{{ $posts->appends(request()->query())->links( "pagination::bootstrap-4") }}</center>
                </div>
            </div>
            <div class="col-lg-4 mt-5">
                <div class="blog_side_bar">
                    <div class="search_post mt-5 blog_post_search">
                        <form action="">
                            <div class="form-group">
                                <input 
                                    type="text" 
                                    class="form-control" 
                                    id="blog_search-field" 
                                    name="name"
                                    value="{{ @request('name') }}"
                                    placeholder="Search Blog">
                            </div>
                        </form>
                        <i class="fas fa-search hide_blog_search-icon"></i>
                    </div>
                </div>

                <div class="blog_catagory">
                    <h3>categories</h3>
                    <div class="catagory_list">
                        <a href="/blog">All Post<i class="fas fa-angle-right "></i></a>
                    </div>
                    @foreach($categories as $category)
                        <div class="catagory_list">
                            <a href="/blog/category/{{ $category['slug'] }}">
                                <span>{{ $category['name'] }}</span>
                                <i class="fas fa-angle-right"></i>
                            </a>
                        </div>
                    @endforeach
                   {{--  <div class="catagory_list">
                        <a href="#">Lost and Found Tips<i class="fas fa-angle-right "></i></a>
                    </div>
                    <div class="catagory_list">
                        <a href="#">Pet Health<i class="fas fa-angle-right "></i></a>
                    </div>
                    <div class="catagory_list">
                        <a href="#">Others<i class="fas fa-angle-right "></i></a>
                    </div> --}}
                </div>

                <div class="blog_side_bar_tab">
                    <div class="tab_button">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 m-0 p-0">
                                <button class="active" id="popular_blog-tab_btn">Popular Post</button>
                            </div>
                            <div class="col-lg-6 col-sm-6 m-0">
                                <button class="recent_btn" id="recent_blog-tab_btn">Recent Post</button>
                            </div>
                        </div>
                    </div>

                    <div id="popular_blog-tab_items" style="display: block;">
                        @foreach($popular_post as $recent)
                            <div class="blog_tab_titles">
                                <div class="row">
                                    <div class="col-lg-3 col-3 m-0 p-0">
                                        <div class="blog_titles_img">
                                            <a href="/blog/{{ $recent['slug'] }}"><img src="{{ asset('/img/' . $recent['image']) }}" class="img-fluid" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-9 m-0">
                                        <div class="blog_title_description">
                                            <a href="/blog/{{ $recent['slug'] }}">
                                                <h4 style="color: #3e3e3e !important;">{{ $recent['title'] }}
                                                <span>
                                                    {{ $recent['created_at']->format('d') }} 
                                                    {{ $recent['created_at']->format('F') }}
                                                    {{ $recent['created_at']->format('Y') }}
                                                </span>
                                            </h4></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div id="recent_blog-tab_items" style="display: none;">
                        @foreach($recent_post as $recent)
                            <div class="blog_tab_titles">
                                <div class="row">
                                    <div class="col-lg-3 col-3 m-0 p-0">
                                        <div class="blog_titles_img">
                                            <a href="/blog/{{ $recent['slug'] }}"><img src="{{ asset('/img/' . $recent['image']) }}" class="img-fluid" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-9 m-0">
                                        <div class="blog_title_description">
                                            <a href="/blog/{{ $recent['slug'] }}">
                                                <h4 style="color: #3e3e3e !important;">{{ $recent['title'] }}
                                                <span>
                                                    {{ $recent['created_at']->format('d') }} 
                                                    {{ $recent['created_at']->format('F') }}
                                                    {{ $recent['created_at']->format('Y') }}
                                                </span>
                                            </h4></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="featured-network wow fadeInUp mt-5" data-wow-duration="1s" data-wow-delay=".3s">
                    <h2 class="featured-network_title">Join our network</h2>
                    <img class="card-img-top img-fluid my-5" src="{{ asset('pawmaw/img/icons/join.png') }}" alt="Join Network Icon">
                    <div class="form-group has-search mx-auto d-block">
                        <span class="form-control-zip"><i class="fas fa-search-location"></i></span>
                        <input type="text" class="form-control" placeholder="ZIP Code" id="move-to-search-zip-code">
                        <button onclick="location.href = '/join-our-network?zip_code=' + $('#move-to-search-zip-code').val();" type="submit" class="featured-network_code-btn zip-code_btn mx-auto d-block">Search</button>
                    </div>

                    <h2 class="featured-network_title-sub py-5">Get Lost and Found Pet Alerts in Your Area.</h2>
                    <img class="img-fluid mx-auto d-block" src="{{ asset('pawmaw/img/home/join-pet.png') }}" alt="Joining Pet">
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
