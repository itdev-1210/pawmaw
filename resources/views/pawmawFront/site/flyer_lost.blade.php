<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name=keywords content="lost pet, lost dog, lost cat, free lost pet alert,  free lost pet poster, find lost dog, find lost cat, find lost pet">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="canonical" href="https://www.pawmaw.com/">

    <meta name="description" content="A brand new way helping lost pets to get back to their family. If you found or lost a pet report it on our website. Notify your neighbors within a minutes to alert them">

    <link rel="shortcut icon" type="image/png" href="{{ asset('pawmaw/img/fevi.png') }}">

    <!-- Roboto FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,400,500,700" rel="stylesheet" type="text/css">

    <!-- STYLES -->
    <link rel="stylesheet" href="{{ asset('pawmaw/css/datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/custom/pnotify/pnotify.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/all.min.css') }}">
    <link href="https://use.fontawesome.com/releases/v5.0.1/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('pawmaw/css/style.css') }}">

    <title>@yield('title')</title>

    @yield('seo_meta')

    <script src="{{ asset('pawmaw/js/jquery-3.3.1.min.js') }}"></script>
</head>

<body class="{{ Request::path() ==  'pet' ? 'gray-bg-specific-page' : ''  }}">

    <div class="container" style=" margin:0 auto; padding:0;border:2px solid rgb(255, 0, 25); border-radius:10px;">
        <!-- flyer title start  -->
        <section class="flyer_header_part">
            <div class="container m-0 p-0">
                <div class="row justify-content-center m-0 p-0">
                    <div class="col-lg-6 col-6 p-0 m-0">
                        <div class="logo" style="width:50%; margin:0 auto; padding:30px 0px;">
                            <img src="{{ asset('pawmaw/img/Flyer-img/logo-found.png') }}" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center m-0 p-0">
                    <div class="col-lg-6 col-10 p-0 m-0">
                        <div class="title_flyer text-center">
                            <h1 >“LOST DOG”</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!--  flyer lost image start   -->
            <div class="flyer_img_part m-0 p-0">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="flyer_img">
                                <img src="{{ asset('pawmaw/img/Flyer-img/found-img.png') }}" class="img-fluid" alt="">
                                <div class="heading_flyer">
                                    <div class="horn_img">
                                        <img src="{{ asset('pawmaw/img/horn-2.png') }}" alt="">
                                    </div>
                                    <div class="title-flyer-img">
                                        <h3>PawMaw,</h3>
                                        <h4>Sparks, NV</h4>
                                    </div>
                                </div>
                                <div class="flyer_number">
                                    <div class="Id_number">
                                        <h3>ID#</h3><span class="ml-3">942</span>
                                    </div>
                                    <div class="flyer_img_icon mr-4">
                                        <img src="{{ asset('pawmaw/img/fevi.png') }}" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--  flyer lost image end   -->
            <!--  flyer lost description start      -->
            <div class="flyer_lost_description">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-10 text-center">
                            <div class="description_headings">
                                <h3>Have You Seen Me?</h3>
                                <h3 class="mt-3 mb-3">Name: <span >Bull Dog</span></h3>
                                <h2 class="">Description:  </h2>
                                <p >
                                    As the day has progressed, and as Pujara has prospered along with the lower-order, India's first session.As the day has progressed, and as Pujara has prospered along with the lower-order, India's first
                                </p>
                                <h2 class="mb-5" ><span >Area Last Seen:</span> Adelaide Oval, Adelaide</h2>
                                <div class="date_gender mb-5" style="display:block">
                                    <div class="last_date" style="display:inline-block; ">
                                        <h4 > <span>Lost Date:</span> 02/05/18</h4>
                                    </div>
                                    <div class="gender" style="display:inline-block; padding-left:40px;">
                                        <h4 style=""><span >Gender:</span> Female</h4>
                                    </div>
                                </div>
                                <h3 >If You Have Any Information Please Contact:</h3>
                                <div class="large_numder" style="">
                                    <h1 >(357) 256  2154</h1>
                                    <h3 >saintsmith@gmail.com</h3>
                                </div>
                                <h2 class="mt-3 mb-5">Free Flyer Made By: <span >PawMaw.com</span></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--  flyer lost description end      -->
        </section>

        <!-- flyer title end  -->

    </div>


    <!-- jQuery -->
    <script src="{{ asset('pawmaw/js/popper.min.js') }}"></script>
    <script src="{{ asset('pawmaw/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('pawmaw/js/wow.min.js') }}"></script>
    <script src="{{ asset('pawmaw/js/main.js') }}"></script>
    <script src="{{ asset('pawmaw/js/home.js') }}"></script>
    <script src="{{ asset('/pawmaw/custom/pnotify/pnotify.js') }}"></script>
    <script src="{{ asset('/pawmaw/custom/pnotify/notify.js') }}"></script>
    @stack('js') @if ($message = Session::get('error'))
    <script type="text/javascript">
        $(document).ready(function() {
            var msg = "{{ $message }}";
            notify('error', msg);
        });

    </script>
    @endif
    <script>
        var map, infowindow, geocoder;

        function initAutocomplete() {
            geocoder = new google.maps.Geocoder();

            infowindow = new google.maps.InfoWindow();

            // Create the search box and link it to the UI element.
            var input1 = document.getElementById('pac-input');
            var input2 = document.getElementById('pac-input2');
            var input3 = document.getElementById('reportlostaddress');
            var input4 = document.getElementById('foundaddress');
            var searchBox = new google.maps.places.SearchBox(input1);
            var searchBox2 = new google.maps.places.SearchBox(input2);
            var searchBox3 = new google.maps.places.SearchBox(input3);
            var searchBox4 = new google.maps.places.SearchBox(input4);
            //  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.


            var markers1 = [];
            var markers2 = [];

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                map1 = new google.maps.Map(document.getElementById('map'), {
                    center: {
                        lat: -33.8688,
                        lng: 151.2195
                    },
                    zoom: 15,
                    height: 300
                });
                map1.addListener('bounds_changed', function() {
                    searchBox.setBounds(map1.getBounds());
                });
                var places = searchBox.getPlaces();


                if (places.length == 0) {
                    return;
                }
                var myAddress = places[0]["address_components"];
                for (let i = 0; i < myAddress.length; i++) {
                    var aData = myAddress[i]['types'][0];
                    if (aData == "postal_code") {
                        jQuery("#postal_2").val(myAddress[i]["long_name"]);
                        break;
                    }
                }
                // Clear out the old markers.
                markers1.forEach(function(marker) {
                    marker.setMap(null);
                });
                markers1 = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    markers1.push(new google.maps.Marker({
                        map: map1,
                        position: place.geometry.location,
                        animation: google.maps.Animation.DROP,
                        draggable: true,
                    }));
                    markers1.forEach(function(marker) {
                        google.maps.event.addListener(marker, 'click', function() {
                            toggleBounce(marker);
                            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
                                'Place ID: ' + place.place_id + '<br>' +
                                place.formatted_address + '</div>');
                            infowindow.open(map1, this);
                        });

                        google.maps.event.addListener(marker, 'dragend', function() {

                            geocoder.geocode({
                                'latLng': marker.getPosition()
                            }, function(results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {

                                    if (results[0]) {
                                        var myAddress = results[0]["address_components"];
                                        for (let i = 0; i < myAddress.length; i++) {
                                            var aData = myAddress[i]['types'][0];
                                            if (aData == "postal_code") {
                                                jQuery("#postal_2").val(myAddress[i]["long_name"]);
                                                break;
                                            }
                                        }

                                        jQuery('#pac-input').val(results[0].formatted_address);
                                        infowindow.setContent(results[0].formatted_address);
                                        infowindow.open(map1, marker);
                                    }
                                }
                            });
                        });
                    });


                    // Create a marker for each place.


                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map1.fitBounds(bounds);
            });


            searchBox2.addListener('places_changed', function() {
                map2 = new google.maps.Map(document.getElementById('map2'), {
                    center: {
                        lat: -33.8688,
                        lng: 151.2195
                    },
                    zoom: 15,
                });
                map2.addListener('bounds_changed', function() {
                    searchBox2.setBounds(map2.getBounds());
                });
                var places = searchBox2.getPlaces();


                if (places.length == 0) {
                    return;
                }
                var myAddress = places[0]["address_components"];
                for (let i = 0; i < myAddress.length; i++) {
                    var aData = myAddress[i]['types'][0];
                    if (aData == "postal_code") {
                        jQuery("#postal_3").val(myAddress[i]["long_name"]);
                        break;
                    }
                }
                // Clear out the old markers.
                markers2.forEach(function(marker) {
                    marker.setMap(null);
                });
                markers2 = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    markers2.push(new google.maps.Marker({
                        map: map2,
                        position: place.geometry.location,
                        animation: google.maps.Animation.DROP,
                        draggable: true,
                    }));
                    markers2.forEach(function(marker) {
                        google.maps.event.addListener(marker, 'click', function() {
                            toggleBounce(marker);
                            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
                                'Place ID: ' + place.place_id + '<br>' +
                                place.formatted_address + '</div>');
                            infowindow.open(map2, this);
                        });

                        google.maps.event.addListener(marker, 'dragend', function() {

                            geocoder.geocode({
                                'latLng': marker.getPosition()
                            }, function(results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {

                                    if (results[0]) {
                                        var myAddress = results[0]["address_components"];
                                        for (let i = 0; i < myAddress.length; i++) {
                                            var aData = myAddress[i]['types'][0];
                                            if (aData == "postal_code") {
                                                jQuery("#postal_3").val(myAddress[i]["long_name"]);
                                                break;
                                            }
                                        }

                                        jQuery('#pac-input2').val(results[0].formatted_address);
                                        infowindow.setContent(results[0].formatted_address);
                                        infowindow.open(map2, marker);
                                    }
                                }
                            });
                        });
                    });


                    // Create a marker for each place.


                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map2.fitBounds(bounds);
            });
            searchBox3.addListener('places_changed', function() {

                var places = searchBox3.getPlaces();


                if (places.length == 0) {
                    return;
                }
                var myAddress = places[0]["address_components"];
                for (let i = 0; i < myAddress.length; i++) {
                    var aData = myAddress[i]['types'][0];
                    if (aData == "postal_code") {
                        jQuery("#postal_2").val(myAddress[i]["long_name"]);
                        break;
                    }
                }


            });

            searchBox4.addListener('places_changed', function() {

                var places = searchBox4.getPlaces();


                if (places.length == 0) {
                    return;
                }
                var myAddress = places[0]["address_components"];
                for (let i = 0; i < myAddress.length; i++) {
                    var aData = myAddress[i]['types'][0];
                    if (aData == "postal_code") {
                        jQuery("#postal_3").val(myAddress[i]["long_name"]);
                        break;
                    }
                }


            });
        }

        function toggleBounce(marker) {
            if (marker.getAnimation() !== null) {
                marker.setAnimation(null);
            } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        }

    </script>


    <script type="text/javascript" async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBK0zK90-BoiWGflg9pXhBWF6DMwOzyTaU&amp;libraries=places&amp;callback=initAutocomplete"></script>

    {{--for mail validation--}} {{--

    <script src="https://widget.thechecker.co/pk_f5owucuv2ll63dh2ymt.js"></script>--}}

</body>

</html>
