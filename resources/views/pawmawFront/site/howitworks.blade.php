@extends('pawmawFront.layouts.front') @section('title', 'How it Works - PawMaw') @section('content')
<!-- HOW PAWMAW HELP YOU SECTION START -->
<section class="howpmhelpu" style="background-image: url({{ asset('pawmaw/img/common-bg.png')}});">
    <div class="container">
        <div class="row">
           <div class="col-12">
                <h1 class="howpmhelpu-title others-page_hero-title">How pawmaw help you to find your lost pet</h1>
            </div>
        </div>
    </div>
</section>


<div class="videosection">
            <div class="videolink">
               <div class="col-12">
                   <div class="row">
                        <div class="col-md-6">
                           <div class="videoimage">
                               <img src="{{ asset('pawmaw/img/youtube thamble.png')}}"/>
                               <div class="vplayicon"><i class="fa fa-play"></i></div>
                           </div> 
                        </div>
                        <div class="col-md-6">
                            <div class="description">
                               <h3>PawMaw helps you find your missing pet quickly.</h3>
                               <p>You need to act quickly to find your missing pet. Report your lost dog, cat, rabbit and other pets today and boost recovery chances to find your lost pet.</p>
                               <a href="{{url('/')}}">Get started now! </a>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
</div>
<!-- -->
<!-- HOW PAWMAW HEKP YOU SECTION END -->

<!-- HOW IT WORKS SECTION START -->
<section class="howitworks">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <p class="text-center pt-3 pb-5" style="font-size:20px;">
                    A platform that reunites lost and stray pets to their owners. All you need to do to get our service is to make a report to us, post a shout out; and we will start looking for your family member. Here is how we work; we feature three stages to get you the best end you seek.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="card howitworks-box wow swing" data-wow-duration="1s" data-wow-delay=".3s">
                    <div class="animational_img animational_img_one">
                        <img src="{{ asset('pawmaw/img/home/10.png') }}" alt="Report Pet" class="card-img-top howitworks-box_report-img img-fluid mx-auto d-block">
                        <div class="animation">
                            <img src="{{ asset('pawmaw/img/home/animation-shape-1.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/animation-shape-2.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/animation-shape-3.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/animation-shape-4.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/animation-shape-5.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/animation-shape-6.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/line-animation-box.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/1.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/2.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/3.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/4.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/5.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/7.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/9.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/4.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/4.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/4.png') }}" alt="line">
                            <img src="{{ asset('pawmaw/img/home/8.png') }}" alt="line">
                        </div>
                    </div>
                    <div class="card-body">
                        <a href="#" class="howitworks-box_btn howitworks-box_report-btn mx-auto d-block">Report a
                            pet</a>
                        <p class="card-text">
                            Tell us about your lost/found pet; by posting a shout out on our website. By that, you will get concerned people in your neighborhoods right on the spot to help search for your pet.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card howitworks-box wow swing" data-wow-duration="1s" data-wow-delay=".5s">
                    <div class="animational_img animational_img_two">
                        <img class="card-img-top howitworks-box_promote-img img-fluid mx-auto d-block" src="{{ asset('pawmaw/img/home/work_img-2.png') }}" alt="Promote Pet">
                        <div class="animation">
                            <img src="{{ asset('pawmaw/img/home/promo-shape-1.png') }}" alt="promote-img">
                            <img src="{{ asset('pawmaw/img/home/promo-shape-2.png') }}" alt="promote-img">
                            <img src="{{ asset('pawmaw/img/home/promo-shape-3.png') }}" alt="promote-img">
                            <img src="{{ asset('pawmaw/img/home/promo-shape-1.png') }}" alt="promote-img">
                            <img src="{{ asset('pawmaw/img/home/promo-shape-3.png') }}" alt="promote-img">
                            <img src="{{ asset('pawmaw/img/home/horn_img.png') }}" alt="promote-img">
                        </div>
                    </div>
                    <div class="card-body">
                        <a href="#" class="howitworks-box_btn howitworks-box_promote-btn mx-auto d-block">Promote</a>
                        <p class="card-text">
                            Pawmaw has a feature that called PawMaw Alert – by activating the alert you can make your search effort highly effective and reach thousands of people to the residents of the area where your pet went missing.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card howitworks-box wow swing" data-wow-duration="1s" data-wow-delay=".7s">
                    <div class="animational_img">
                        <img class="card-img-top howitworks-box_reunited-img img-fluid mx-auto d-block" src="{{ asset('pawmaw/img/home/work_img-3.png') }}" alt="reunited Pet">
                        <div class="animation">
                            <img src="{{ asset('pawmaw/img/home/love_shape.png') }}" alt="love-shape">
                            <img src="{{ asset('pawmaw/img/home/love_shape.png') }}" alt="love-shape">
                            <img src="{{ asset('pawmaw/img/home/love_shape.png') }}" alt="love-shape">
                            <img src="{{ asset('pawmaw/img/home/love_shape.png') }}" alt="love-shape">
                        </div>
                    </div>
                    <div class="card-body">
                        <a href="#" class="howitworks-box_btn howitworks-box_reunited-btn mx-auto d-block">REUNITED</a>
                        <p class="card-text">
                            We are known for what we do! Every day we help distressed pet owners to find their lost, scared, and often hungry pets. And it has always been an atmosphere of cheers and happiness, every time we reunited with their beloved one.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3 py-5">
                <h2 href="#" class="howitworks-quick_tour mx-auto d-block wow bounceIn" data-wow-duration="1s" data-wow-delay=".3s">Take a quick tour</h2>
            </div>
        </div>
    </div>
</section>
<!-- HOW IT WORKS SECTION END -->

<!-- HOW IT WORKS ITEM SECTION START -->
<section class="howitworks_items">
    <div class="howitworks_items-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="howitworks_items-box mb-xs-5">
                        <div class="howitworks_items-box-inner">
                            <div class="howitworks_items-box-inner_icon">
                                <img src="{{ asset('pawmaw/img/icons/missing.png') }}" alt="Missing" class="img-fluid mx-auto d-block">
                            </div>
                            <div class="howitworks_items-box-inner_content">
                                <h2>Print a flyer</h2>
                                <p>Download and print a high-quality flyer for your lost/found pet</p>
                            </div>
                        </div>
                        <a href="{{ url('/') }}" class="howitworks_items-box_btn">Get started</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="howitworks_items-box mb-xs-5">
                        <div class="howitworks_items-box-inner">
                            <div class="howitworks_items-box-inner_icon">
                                <img src="{{ asset('pawmaw/img/icons/rescue.png') }}" alt="Rescue" class="img-fluid mx-auto d-block">
                            </div>
                            <div class="howitworks_items-box-inner_content">
                                <h2>Alert Rescue Squad</h2>
                                <p>Shout out to pet lovers around you</p>
                            </div>
                        </div>
                        <a href="{{ url('/user/register') }}" class="howitworks_items-box_btn">Learn more</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="howitworks_items-box mb-xs-5">
                        <div class="howitworks_items-box-inner">
                            <div class="howitworks_items-box-inner_icon">
                                <img src="{{ asset('pawmaw/img/icons/lost-found.png') }}" alt="Lost and Found" class="img-fluid mx-auto d-block">
                            </div>
                            <div class="howitworks_items-box-inner_content">
                                <h2>Report lost and found pet</h2>
                                <p>Enlist your pet on our lost and found database</p>
                            </div>
                        </div>
                        <a href="{{ url('/') }}" class="howitworks_items-box_btn">Report pet</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="howitworks_items-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="howitworks_items-box gray-bg-4 mb-xs-5">
                        <div class="howitworks_items-box-inner">
                            <div class="howitworks_items-box-inner_icon">
                                <img src="{{ asset('pawmaw/img/icons/facebook.png') }}" alt="Facebook" class="img-fluid mx-auto d-block">
                            </div>
                            <div class="howitworks_items-box-inner_content">
                                <h2>Free Facebook Post</h2>
                                <p>Get your post on PawMaw lost and found Facebook page.</p>
                            </div>
                        </div>
                        <a href="{{ url('/join-our-network') }}" class="howitworks_items-box_btn">Join our network</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="howitworks_items-box gray-bg-4 mb-xs-5">
                        <div class="howitworks_items-box-inner">
                            <div class="howitworks_items-box-inner_icon">
                                <img src="{{ asset('pawmaw/img/icons/blog.png') }}" alt="Blog" class="img-fluid mx-auto d-block">
                            </div>
                            <div class="howitworks_items-box-inner_content">
                                <h2>Read our blog</h2>
                                <p>Get updated with the recent blog articles and the lost/found tips & more.</p>
                            </div>
                        </div>
                        <a href="{{ url('/blog') }}" class="howitworks_items-box_btn">Read blog</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="howitworks_items-box gray-bg-4 mb-xs-5">
                        <div class="howitworks_items-box-inner">
                            <div class="howitworks_items-box-inner_icon">
                                <img src="{{ asset('pawmaw/img/icons/search.png') }}" alt="Search" class="img-fluid mx-auto d-block">
                            </div>
                            <div class="howitworks_items-box-inner_content">
                                <h2>Search Lost & Found Pet</h2>
                                <p>Find both lost & found pets in your area. </p>
                            </div>
                        </div>
                        <a href="{{ url('/lost-found-pets') }}" class="howitworks_items-box_btn">Search pets</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- HOW IT WORKS ITEM SECTION END -->

<!-- GET YOUR PET BACK HOME SECTION START -->
<section class="getyourpet">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="getyourpet-title wow swing" data-wow-duration="1s" data-wow-delay=".3s">Get your pet back home</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <a href="{{ url('/?active-form=1') }}" class="getyourpet-btn mx-auto d-block wow bounceIn" data-wow-duration="1s" data-wow-delay=".3s">Report lost pet</a>
            </div>
        </div>
    </div>
</section>
<!-- GET YOUR PET BACK HOME SECTION END -->
<!-- Button to Open the Modal -->



<!-- The Modal -->
<div class="modal videomodel" id="myModal">
   <iframe  src="" frameborder="0"  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<script>
    
   $(".vplayicon").click(function(){
       var url = 'https://www.youtube.com/embed/2qQULoFc2Ts?autoplay=1';
       $("iframe")[0].src = url;
       $('#myModal').modal("show");
   });
   
   $('#myModal').on('hidden.bs.modal', function () {
       $("iframe")[0].src = '';
   }); 

</script>
@endsection


