@extends('pawmawFront.layouts.front') @section('title', 'Join Our Network - PawMaw') @section('content')
<!-- JOIN OUR NETWORK SECTION START -->
<section class="joinournetwork">
    <div class="joinournetwork-bg" style="background-image: url({{ asset('pawmaw/img/common-bg.png')}});">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="joinournetwork-bg_title others-page_hero-title">JOIN OUR PAWMAW NETWORK</h1>
                    <p class="joinournetwork-bg_tag-line">We know how much you care about pets! </p>
                </div>
            </div>
        </div>
    </div>
    <div class="joinournetwork-content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="joinournetwork-content_inner">
                        <h2 class="joinournetwork-content_inner-title mb-5">OUR PET IS LIKE OUR FAMILY MEMBER!</h2>
                        <p class="joinournetwork-content_inner-text mb-5 text-center">
                            PawMaw network is created because of the vast increase in the pet lovers community, pet professionals, pet volunteers, and other amazing like-minded people who have the understanding of how disturbing a pet owner can be if their beloved one goes missing. Pets are more than a companion; they are more like a member of our family. This has been the brain behind the creation of this network.
                        </p>
                        <p class="joinournetwork-content_inner-text mb-5 text-center">
                            So, to be part of our network, all you need to do is to search with your zip code and join your local area on our Facebook page and groups.
                        </p>

                        <div class="joinournetwork-content_inner-search mb-3"  data-zip-code="{{ @$_GET['zip_code'] }}">
                            <img src="{{ asset('pawmaw/img/icons/join.png')}}" alt="Join Icon" class="img-fluid mx-auto d-block mb-5">
                            <div class="form-group has-search mx-auto d-block">
                                <span class="form-control-zip"><i class="fas fa-search-location"></i></span>                                
                                <input type="text" id="search-zip-code" class="form-control" placeholder="ZIP Code" >
                                <iput type="hidden" id="test">
                            </div>
                            <button type="button" id="searchButton" onclick="location.href = '/join-our-network?zip_code=' + $('#search-zip-code').val();" class="joinournetwork-content_inner-search-btn zip-code_btn mx-auto d-block">Search by ZIP</button>
                        </div>
                        <script>
                        function getUrlVars()
                        {
                            var vars = [], hash;
                            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                            for(var i = 0; i < hashes.length; i++)
                            {
                                hash = hashes[i].split('=');
                                vars.push(hash[0]);
                                vars[hash[0]] = hash[1];
                            }
                            return vars;
                        }
                        var zip=getUrlVars()["zip_code"];
                        $('#search-zip-code').val(zip);
                        $('#search-zip-code').keypress(function (e) {
                        		var key = e.which;
                        		if(key == 13) 
                        		{
                        		$('#searchButton').click();
                        		return false;  
                        		}
                        }); 
                        </script>
                        @if(@$_GET['zip_code'])
                            <script>
                                $('html, body').animate({
                                    scrollTop: $('#search-zip-code').offset().top - 100
                                }, 1000);
                            </script>
                            
                        @endif
                        

                        <div class="joinournetwork-content_inner-social" id="facebook-list-result" >
                            <div class="joinournetwork-content_inner-social_body">
                                <div class="joinournetwork-content_inner-social_body-item shadow zip_code_item" style="{{ @$facebook ? '' : 'display: none' }};">
                                    <a href="{{ @$facebook->link }}">{{ @$facebook->name }}</a>
                                    <div class="fb-like" data-href="{{ @$facebook->link }}" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
                                </div>
                                <div class="joinournetwork-content_inner-social_body-item shadow">
                                    <a href="https://www.facebook.com/PawMawOfficial/">PawMaw</a>
                                    <div class="fb-like" data-href="https://www.facebook.com/PawMawOfficial/" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
                                </div>
                                <div class="joinournetwork-content_inner-social_body-item shadow">
                                    <a href="https://www.facebook.com/groups/PawMawOfficial/">Join Our Official Group- PawMaw</a>
                                    <a style="background: #4267b2 !important; width: 85px !important; height: 30px !important; color: #fff !important; border-radius: 5px !important; font-size: 14px; display: flex; justify-content: center; align-items: center; white-space: nowrap;" href="https://www.facebook.com/groups/PawMawOfficial/" target="_blank">Join group</a>
                                </div>
                                <div id="fb-root"></div>
                                <script>(function(d, s, id) {
                                  var js, fjs = d.getElementsByTagName(s)[0];
                                  if (d.getElementById(id)) return;
                                  js = d.createElement(s); js.id = id;
                                  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2&appId=1863631387098360&autoLogAppEvents=1';
                                  fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- JOIN OUR NETWORK SECTION END -->

<!-- SEARCH CATEGORY SECTION START -->
<section class="jns-category">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 class="jns-category_title text-center">Search by Country/States</h3>
            </div>
            <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-sm-10 offset-sm-1">
                <div class="jns-category_select">
                    <div class="form-group">
                        <select class="form-control">
                            <option value="1">United States</option>
                            <option value="2">Canada</option>
                            <option value="3">United Kingdom</option>
                            <option value="4">Australia</option>
                            <option value="5">Netherland</option>
                            <option value="6">Sweden</option>
                            <option value="7">Austria</option>
                            <option value="8">Belgium</option>
                            <option value="9">Czech-Republic</option>
                            <option value="10">France</option>
                            <option value="11">Switzerland</option>
                            <option value="12">Italy</option>
                            <option value="13">Germany</option>
                            <option value="14">Poland</option>
                            <option value="15">Denmark</option>
                            <option value="16">Finland</option>
                            <option value="17">Ireland</option>
                            <option value="18">Norway</option>
                            <option value="19">Spain</option>
                            <option value="20">Mexico</option>
                            <option value="21">Brazil</option>
                            <option value="22">New Zealand</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="jns-category_result">
                    <div class="row justify-content-center">
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <ul class="jns-category_result-item">
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Alabama-309450416253627/" target="_blank">Alabama</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Alaska-1809575252680331/" target="_blank">Alaska</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Arizona-363332140840984/" target="_blank">Arizona</a></li>                                
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Arkansas-1437176813076482/" target="_blank">Arkansas</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-California-199125610887681/" target="_blank">California</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Colorado-188306091810674/" target="_blank">Colorado</a></li>                                
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Connecticut-223468098387644/" target="_blank">Connecticut</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Delaware-751446668391858/" target="_blank">Delaware</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Florida-2048207835447357/" target="_blank">Florida</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Georgia-1938231426218825/" target="_blank">Georgia</a></li>
                                <li><a href="https://www.facebook.com/pg/Lost-Found-Pets-Hawaii-187788638522779" target="_blank">Hawaii</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Idaho-183961358924162/" target="_blank">Idaho</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Illinois-1988070031453697/" target="_blank">Illinois</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <ul class="jns-category_result-item">
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Indiana-527158754344433" target="_blank">Indiana</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Iowa-421699874967723/" target="_blank">Iowa</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Kansas-1912498642413971" target="_blank">Kansas</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Kentucky-1245490962251593/" target="_blank">Kentucky</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Louisiana-472888893125281/" target="_blank">Louisiana</a></li>
                                <li><a href="https://www.facebook.com/pg/Lost-Found-Pets-Maine-2088328694716943" target="_blank">Maine</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Maryland-198724627597409" target="_blank">Maryland</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Massachusetts-197588401031020" target="_blank">Massachusetts</a></li>
                                <li><a href="https://www.facebook.com/pg/Lost-Found-Pets-Michigan-171960236847590" target="_blank">Michigan</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Minnesota-700807346709928/" target="_blank">Minnesota</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Mississippi-428260954295294/" target="_blank">Mississippi</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Missouri-356771824830372" target="_blank">Missouri</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Montana-1472011199574404/" target="_blank">Montana</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <ul class="jns-category_result-item">
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Nebraska-1992555394394049/" target="_blank">Nebraska</a></li>                            
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Nevada-235763917169646" target="_blank">Nevada</a></li>                            
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-New-Hampshire-171348513686059" target="_blank">New Hampshire</a></li>                            
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-New-Jersey-2127700933925836" target="_blank">New Jersey</a></li>                            
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-New-Mexico-1470128386442262" target="_blank">New Mexico</a></li>                            
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-New-York-216033392318310" target="_blank">New York</a></li>                            
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-North-Carolina-1841193476177855/" target="_blank">North Carolina</a></li>                            
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-North-Dakota-592875101096134/" target="_blank">North Dakota</a></li>                            
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Ohio-1144780449012682/" target="_blank">Ohio</a></li>                            
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Oklahoma-654159864966000/" target="_blank">Oklahoma</a></li>                            
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Oregon-1297810823653193/" target="_blank">Oregon</a></li>                            
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Pennsylvania-199389904199504/" target="_blank">Pennsylvania</a></li>                            
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Rhode-Island-646827508994469/" target="_blank">Rhode Island</a></li>                            
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <ul class="jns-category_result-item">                            
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-South-Carolina-247964702611351/" target="_blank">South Carolina</a></li>                                  
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-South-Dakota-1623607051021589/" target="_blank">South Dakota</a></li>                                  
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Tennessee-362059324326071/" target="_blank">Tennessee</a></li>                                  
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Texas-443961469368643/" target="_blank">Texas</a></li>                                  
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Utah-165877580789978/" target="_blank">Utah</a></li>                                  
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Vermont-452591481846087/" target="_blank">Vermont</a></li>                                  
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Virginia-159889701373785/" target="_blank">Virginia</a></li>                                  
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Washington-213362089249384/" target="_blank">Washington</a></li>                                  
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-West-Virginia-2048202245398096/" target="_blank">West Virginia</a></li>                                  
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Wisconsin-243618182876943/" target="_blank">Wisconsin</a></li>                                  
                                <li><a href="https://www.facebook.com/Lost-Found-Pets-Wyoming-2141474109201082/" target="_blank">Wyoming</a></li>                                  
                                <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-District-of-Columbia-315926455850148" target="_blank">District of Columbia</a></li>                                  
                            </ul>
                        </div>
                    </div>

<!--
                    <div class="row justify-content-center">
                        <div class="col-lg-2">
                            <a href="#" class="jns-category_result-more mx-auto d-block">View More</a>
                        </div>
                    </div>
-->
                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-2 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <ul class="jns-category_result-item">
                                <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Alberta-452674935172391" target="_blank">Alberta</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-British-Columbia-1657125547709669" target="_blank">British Columbia</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Manitoba-484141295335106" target="_blank">Manitoba</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-New-Brunswick-825344664322644" target="_blank">New Brunswick</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <ul class="jns-category_result-item">
                                <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Nova-Scotia-597070853991036" target="_blank">Nova Scotia</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Ontario-190222738281677" target="_blank">Ontario</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Quebec-578044445891121/" target="_blank">Quebec</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <ul class="jns-category_result-item">
                                <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Saskatchewan-1981520378830185" target="_blank">Saskatchewan</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Newfoundland-and-Labrador-559636754484054" target="_blank">Newfoundland and Labrador</a></li>
                                <li><a href="https://www.facebook.com/pg/Lost-Found-Cats-Dogs-Pets-Prince-Edward-Island-193885838227085" target="_blank">Prince Edward Island</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <ul class="jns-category_result-item">
                                <li><a href="https://www.facebook.com/pg/Lost-Found-Cats-Dogs-Pets-Northwest-Territories-794151917583227/" target="_blank">Northwest Territories</a></li>
                                <li><a href="https://www.facebook.com/pg/Lost-Found-Cats-Dogs-Pets-Yukon-775064989505269/" target="_blank">Yukon</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Nunavut-345860082904341" target="_blank">Nunavut</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-3 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <ul class="jns-category_result-item">
                                <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Newcastle-1897966123657522" target="_blank">Newcastle</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Manchester-285741078912329" target="_blank">Manchester</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Nottingham-240979943256569/" target="_blank">Nottingham</a></li>
                                <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Brighton-449524242121612" target="_blank">Brighton</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <ul class="jns-category_result-item">
                                  <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Bristol-2209597525952607" target="_blank">Bristol</a></li>                               
                                  <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Cardiff-461377754367891" target="_blank">Cardiff</a></li>                               
                                  <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Edinburgh-1369918053142278" target="_blank">Edinburgh</a></li>                               
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Leeds-717655101901994" target="_blank">Leeds</a></li>                                 
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-London-2156032101383966/" target="_blank">London</a></li>                                 
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Liverpool-492052844597104/" target="_blank">Liverpool</a></li>                                 
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Leicester-537493353370135/" target="_blank">Leicester</a></li> 
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Oxford-1943897039245626/" target="_blank">Oxford</a></li> 
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Cambridge-1822701994504866" target="_blank">Cambridge</a></li> 
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-4 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/pg/Lost-Found-Cats-Dogs-Pets-Western-Australia-378225042994880" target="_blank">Western Australia</a></li>
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-New-South-Wales-272866250000829" target="_blank">New South Wales</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <ul class="jns-category_result-item">     
                                    <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Australian-Capital-Territory-683340185370703/" target="_blank">Australian Capital Territory</a></li>                    
                                    <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Victoria-245192059412739" target="_blank">Victoria</a></li>                    
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <ul class="jns-category_result-item">    
                                    <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Queensland-315558245916660/" target="_blank">Queensland</a></li>                    
                                    <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-South-Australia-219183208977253/" target="_blank">South-Australia</a></li>                         
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <ul class="jns-category_result-item">
                                    <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Tasmania-502412933600240/" target="_blank">Tasmania</a></li>                    
                                    <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Northern-Territory-2184842265175748/" target="_blank">Northern Territory</a></li> 
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-5 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/pg/Lost-Found-Cats-Dogs-Pets-Netherlands-323772724848969" target="_blank">Netherlands</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-6 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Sweden-327466064491837/" target="_blank">Sweden</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-7 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Austria-481562085696519/" target="_blank">Austria</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-8 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Belgium-2185669031663236/" target="_blank">Belgium</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-9 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Czech-Republic-492906584509946/" target="_blank">Czech Republic</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-10 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-France-2151222518244120/" target="_blank">France</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-11 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Switzerland-1726033967493904/" target="_blank">Switzerland</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-12 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Italy-630276914040666/" target="_blank">Italy</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-13 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Germany-231325857535723/" target="_blank">Germany</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-14 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Poland-242574736447191" target="_blank">Poland</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-15 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Denmark-709889239403972" target="_blank">Denmark</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-16 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Finland-517670525385552" target="_blank">Finland</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-17 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-Ireland-568463986948776" target="_blank">Ireland</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-18 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/pg/Lost-Found-Cats-Dogs-Pets-Norway-1920845891295939" target="_blank">Norway</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-19 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/pg/Lost-Found-Cats-Dogs-Pets-Spain-343290942890622" target="_blank">Spain</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-20 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/pg/Lost-Found-Cats-Dogs-Pets-Mexico-2107555742623087" target="_blank">Mexico</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-21 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/pg/Lost-Found-Cats-Dogs-Pets-Brazil-1989084497845857" target="_blank">Brazil</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <div class="jns-category_result-22 d-none">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="jns-category_result-item">
                                 <li><a href="https://www.facebook.com/Lost-Found-Cats-Dogs-Pets-New-Zealand-337116433767005" target="_blank">New Zealand</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- SEARCH CATEGORY SECTION END -->

<!-- NEWSLETTER SECTION START -->
<section class="newsletter">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="newsletter-outer shadow">
                    <div class="row">
                        <div class="col-lg-6 offset-lg-3">
                            <div class="newsletter-inner">
                                <h2 class="wow swing" data-wow-duration="1s" data-wow-delay=".3s">Sign Up for Newsletter</h2>
                                <form role="form" action="{{ url('/newsletter/newsletterstore') }}" method="POST">
                                    @csrf

                                    <div class="input-group py-5 wow bounceIn" data-wow-duration="1s" data-wow-delay=".3s">
                                        <input type="email" name="email" class="form-control" placeholder="Enter your mail" required>
                                        <span class="email-icon"><i class="fas fa-envelope"></i></span>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="newsletter-inner_btn mx-auto d-block">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- NEWSLETTER SECTION END -->
@endsection
