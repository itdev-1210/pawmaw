@extends('pawmawFront.layouts.front') 

@section('title', $post['title']) 

@section('content')

<section class="blog_Page_header">
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-lg-6">
                <div class="blog_banner text-center">
                    <h2 
                            style="font-family: 'Hobo Std Medium', cursive; font-size: 50px; color: #fff;">
                            <span>Welcome </span> to <span>PawMaw</span> blogs!</h2>
                </div>
            </div>
        </div>
    </div>
</section>

@if (session('message'))
    <script>
        setTimeout(function(){
            $('.blog-message').fadeOut();
        }, 5000);
    </script>

    <div class="alert alert-success blog-message">
        {{ session('message') }}
    </div>
@endif

<section class="blog_contents">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7 mt-5">

                <div id="primary" class="mt-5">
                    <article id="post-1295" class="post-1295 post type-post status-publish format-standard has-post-thumbnail hentry category-pawmaw-blog odd">
                        <h1 class="entry-title">
                            <span>{{ $post['title'] }}</span>
                        </h1>

                        <div class="post-thumbnail singular" style="margin-top: 30px;">
                            <a 
                                class="full-image-link fancybox" 
                                href="{{ asset('pawmaw/img/' . $post['image']) }}" 
                                title="{{ $post['title'] }}"></span>
                        </a>

                            <div class="col-lg-12">
                                <img class="attachment-social-thumbnails size-social-thumbnails wp-post-image img-fluid" alt="{{ $post['title'] }}" src="{{ asset('img/' . $post['image']) }}">
                            </div>
                        </div>

                        <div class="entry-content" style="margin-top: 30px;">
                            <p>{!! html_entity_decode($post['content']) !!}</p>
                        </div>

                        <div class="social-networks">
                            <h2 class="widget-title" style="margin-bottom: 20px;">
                                <span>Share post in social network</span>
                            </h2>
                            <div class="social-networks__items">
                                <a href="http://www.facebook.com/sharer.php?u={{ URL::current() }}" target="_blank">
                                    <img src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" />
                                </a>

                                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{ URL::current() }}" target="_blank">
                                    <img src="https://simplesharebuttons.com/images/somacro/linkedin.png" alt="LinkedIn" />
                                </a>

                                <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">
                                        <img src="https://simplesharebuttons.com/images/somacro/pinterest.png" alt="Pinterest" />
                                    </a>

                                <a href="http://www.tumblr.com/share/link?url={{ URL::current() }}&amp;title={{ $post['title'] }}" target="_blank">
                                    <img src="https://simplesharebuttons.com/images/somacro/tumblr.png" alt="Tumblr" />
                                </a>

                                <a href="http://reddit.com/submit?url={{ URL::current() }}&amp;title={{ $post['title'] }}" target="_blank">
                                    <img src="https://simplesharebuttons.com/images/somacro/reddit.png" alt="Reddit" />
                                </a>

                                <a href="https://twitter.com/share?url={{ URL::current() }}&amp;text={{ urlencode($post['title']) }}&amp;hashtags={{ $post['title'] }}" target="_blank">
                                    <img src="https://simplesharebuttons.com/images/somacro/twitter.png" alt="Twitter" />
                                </a>

                                <a href="mailto:?Subject={{ $post['title'] }}&amp;Body=I%20saw%20this%20and%20thought%20of%20you!%20{{ URL::current() }}">
                                    <img src="https://simplesharebuttons.com/images/somacro/email.png" alt="Email" />
                                </a>
                            </div>
                        </div>
                    </article>
                </div>

                <div class="blog_posting mt-5 mb-5" id="secondary">
                    <h2><span>Contributions</span></h2>
                    @if(count($comments) > 0)
                        <p style="font-size:18px;">
                            {{ count($comments) }} {{ count($comments) == 1 ? 'comment' : 'comments'  }} on “{{ $post['title'] }}”</p>
                  
                        @foreach($comments as $comment)
                            <div class="row justify-content-center mt-4 comment__item">
                                <div class="col-lg-2 col-3 ">
                                    <div class="comment-author-name">
                                        {{ $comment['name'][0] }}
                                    </div>
                                    {{-- <img src="{{ asset('pawmaw/img/fevi.png') }}" class="img-fluid" alt="blog_img"> --}}
                                </div>
                                <div class="col-lg-10 col-9 ">
                                    <h4>
                                        <i>
                                            {{ $comment['created_at']->format('F') }} 
                                            {{ $comment['created_at']->format('d') }}, 
                                             {{ $comment['created_at']->format('Y') }}
                                            at {{ $comment['created_at']->format('h:i') }}</i></h4>
                                    <h4 class="mb-3"><i>{{ $comment['name'] }} says:</i></h4>
                                    <p style="font-size:18px;">{{ $comment['comment'] }}</p>
                                    <button 
                                        style="font-size:18px;padding:7px 20px; background:#eee; border:1px solid #d6d6d6; border-radius:5px;color:#333;cursor:pointer;font-weight:500;margin-top:15px;"
                                        onclick="commentReply({{ $comment['id'] }}, '{{ $comment['name'] }}');">
                                            <i class="fas fa-reply-all mr-3"></i>
                                            Reply
                                    </button>
                                </div>
                            </div>
                            
                            @if(count($comment['replies']))
                                @foreach($comment['replies'] as $reply)
                                    <div class="row justify-content-end mt-5 reply__item">
                                        <div class="col-lg-2 col-3">
                                            <div class="comment-author-name">
                                                {{ $reply['name'][0] }}
                                            </div>
                                            {{-- <img src="{{ asset('pawmaw/img/fevi.png') }}" class="img-fluid" alt="blog_img"> --}}
                                        </div>
                                        <div class="col-lg-9 col-8">
                                            <h4>
                                                <i>
                                                    {{ $reply['created_at']->format('F') }} 
                                                    {{ $reply['created_at']->format('d') }}, 
                                                     {{ $reply['created_at']->format('Y') }}
                                                    at {{ $reply['created_at']->format('h:i') }}</i></h4>
                                            <h4 class="mb-3"><i>{{ $reply['name'] }} says:</i></h4>
                                            <p style="font-size:18px;">{{ $reply['comment'] }}</p>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        @endforeach

                    @else

                        <p class="empty-comments">This post doesn't have comments</p>

                    @endif
                        
                    
                    <h2 class="mt-5"><span>Leave a Comment</span></h2>
                    <form action="/blog/create-comment" method="POST" id="create-comment-form">
                        @csrf
                        <input type="hidden" name="post_id" value="{{ $post['id'] }}">
                        <input type="hidden" name="comment_id" value="0">
                        <input type="hidden" name="checked" value="0">
                        <div class="form-group">
                            <label style="font-size:18px;" for="exampleFormControlTextarea1">Comment</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Write Your Comment" style="font-size:18px;" name="comment" required></textarea>
                        </div>
                        <div class="form-group">
                            <label style="font-size:18px;" for="exampleInputName1">Your Name</label>
                            <input type="text" class="form-control" id="exampleInputName1" aria-describedby="emailHelp" placeholder="Enter Your Name" name="name" style="font-size:18px; padding:20px 10px;" required>
                        </div>
                        <div class="form-group">
                            <label style="font-size:18px;" for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="email" style="font-size:18px; padding:20px 10px;" required>
                        </div>
                        <button style="font-size:18px;background:#df1d41; padding:7px 20px; border-radius:5px;color:#fff;font-weight:500;" type="submit" class="btn mt-4">Submit</button>
                    </form>
                </div>

                <div id="secondary" style="margin-top: 50px;">
                    <div id="widget-area" class="widget-area" role="complementary">
                        <aside id="recent-posts-2" class="widget widget_recent_entries">
                            <h2 class="widget-title" style="margin-bottom: 20px;">
                                <span>Recent Posts</span>
                            </h2>
                            <ul>
                                @foreach($recent_posts as $recent_post)
                                
                                    <li>
                                        <a href="{{ url('/blog/' . $recent_post['slug']) }}">{{ $recent_post['title'] }}</a>
                                        <span class="post-date">{{ $recent_post['created_at']->format('F') }} {{ $recent_post['created_at']->format('d') }}, {{ $recent_post['created_at']->format('Y') }}</span>
                                    </li>
                                @endforeach
                            </ul>
                        </aside>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mt-5">
                <div class="blog_side_bar">
                    <div class="search_post mt-5 blog_post_search">
                        <form action="/blog">
                            <div class="form-group">
                                <input type="text" class="form-control" id="blog_search-field" placeholder="Search Blog" name="name">
                            </div>
                        </form>
                        <i class="fas fa-search hide_blog_search-icon"></i>
                    </div>
                </div>

                <div class="blog_catagory">
                    <h3>categories</h3>
                    <div class="catagory_list">
                        <a href="/blog">All Post<i class="fas fa-angle-right "></i></a>
                    </div>
                    @foreach($categories as $category)
                        <div class="catagory_list">
                            <a href="/blog/category/{{ $category['slug'] }}">
                                {{ $category['name'] }}
                                <i class="fas fa-angle-right"></i>
                            </a>
                        </div>
                    @endforeach
                </div>

                <div class="blog_side_bar_tab">
                    <div class="tab_button">
                        <div class="row">
                            <div class="col-lg-6 col-sm-6 m-0 p-0">
                                <button class="active" id="popular_blog-tab_btn">Popular Post</button>
                            </div>
                            <div class="col-lg-6 col-sm-6 m-0">
                                <button class="recent_btn" id="recent_blog-tab_btn">Recent Post</button>
                            </div>
                        </div>
                    </div>

                    <div id="popular_blog-tab_items" style="display: block;">
                        @foreach($popular_post as $recent)
                            <div class="blog_tab_titles">
                                <div class="row">
                                    <div class="col-lg-3 col-3 m-0 p-0">
                                        <div class="blog_titles_img">
                                            <a href="/blog/{{ $recent['slug'] }}"><img src="{{ asset('/img/' . $recent['image']) }}" class="img-fluid" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-9 m-0">
                                        <div class="blog_title_description">
                                            <a href="/blog/{{ $recent['slug'] }}">
                                                <h4 style="color: #3e3e3e !important;">{{ $recent['title'] }}
                                                <span>
                                                    {{ $recent['created_at']->format('d') }} 
                                                    {{ $recent['created_at']->format('F') }}
                                                    {{ $recent['created_at']->format('Y') }}
                                                </span>
                                            </h4></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div id="recent_blog-tab_items" style="display: none;">
                        @foreach($recent_post_blog as $recent)
                            <div class="blog_tab_titles">
                                <div class="row">
                                    <div class="col-lg-3 col-3 m-0 p-0">
                                        <div class="blog_titles_img">
                                            <a href="/blog/{{ $recent['slug'] }}"><img src="{{ asset('/img/' . $recent['image']) }}" class="img-fluid" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-9 m-0">
                                        <div class="blog_title_description">
                                            <a href="/blog/{{ $recent['slug'] }}">
                                                <h4 style="color: #3e3e3e !important;">{{ $recent['title'] }}
                                                <span>
                                                    {{ $recent['created_at']->format('d') }} 
                                                    {{ $recent['created_at']->format('F') }}
                                                    {{ $recent['created_at']->format('Y') }}
                                                </span>
                                            </h4></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>

                <div class="featured-network wow fadeInUp mt-5" data-wow-duration="1s" data-wow-delay=".3s">
                    <h2 class="featured-network_title">Join our network</h2>
                    <img class="card-img-top img-fluid my-5" src="{{ asset('pawmaw/img/icons/join.png') }}" alt="Join Network Icon">
                    <div class="form-group has-search mx-auto d-block">
                        <span class="form-control-zip"><i class="fas fa-search-location"></i></span>
                        <input type="text" class="form-control" placeholder="ZIP Code" id="move-to-search-zip-code">
                        <button onclick="location.href = '/join-our-network?zip_code=' + $('#move-to-search-zip-code').val();" type="submit" class="featured-network_code-btn zip-code_btn mx-auto d-block">Search</button>
                    </div>

                    <h2 class="featured-network_title-sub py-5">Get Lost and Found Pet Alerts in Your Area.</h2>
                    <img class="img-fluid mx-auto d-block" src="{{ asset('pawmaw/img/home/join-pet.png') }}" alt="Joining Pet">
                </div>

                </div>

            </div>
        </div>
    </div>
</section>

<script>
    function commentReply(id, name){
        $('html, body').animate({
            scrollTop: $('#create-comment-form').offset().top - 100
        }, 500);
        $('input[name=comment_id]').val(id);
        $('textarea[name=comment]').text(name + ', ').focus();
    }
</script>

{{-- <div class="sharethis-inline-share-buttons"></div> --}}

<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c59c47f83748d0011314eed&product=inline-share-buttons' async='async'></script>

@endsection
