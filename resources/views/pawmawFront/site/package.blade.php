@extends('pawmawFront.layouts.front')

@section('title', 'Package - PawMaw')

@section('content')
    <div class="site-content">
        <div class="warper">
            <h1 class="package-entry-title">
                <span class="pk-primary-title">Choose your paw alert</span><br>
                <span class="pk-secondary-title">Over 50,000 pets found &amp; counting</span>
            </h1>

            <div class="price-table-wrapper">

                <div class="pricing-table most_popular" id="pricing-table-pk2">
                    <h2 class="pricing-table__header">
                        Free
                    </h2>
                    <h3 class="pricing-table__price">$0.00</h3>
                    <a class="pricing-table__button" href="{{ url('user/my-account') }}">
                        Order!
                    </a>
                    <ul class="pricing-table__list">
                        <li><span class="no-dash">&mdash;</span></li>
                        <li><span class="no-dash">&mdash;</span></li>
                        <li class="yes">Custom Poster Design</li>
                        <li class="yes">Feature Post On our website</li>
                        <li class="yes">Real Time Notifications</li>
                        <li class="yes">24/7 Customer Support</li>
                    </ul>
                    <a class="pricing-table__button pbtn" href="{{ url('user/my-account') }}">
                        Order!
                    </a>
                </div><!-- /.free -->

                <div class="pricing-table" id="pricing-table-pk1">
                    <h2 class="pricing-table__header">BASIC</h2>
                    <h3 class="pricing-table__price">$24.99</h3>
                    <a class="pricing-table__button" href="{{ url('user/my-account') }}">
                        Order!
                    </a>
                    <ul class="pricing-table__list">
                        <li class="yes">Send 1,000 Paw Alert Nearest Pet Lover</li>
                        <li class="yes">3 Days Campaign</li>
                        <li class="yes">Custom Poster Design</li>
                        <li class="yes">Feature Post On our website</li>
                        <li class="yes">Real Time Notifications</li>
                        <li class="yes">24/7 Customer Support</li>
                    </ul>
                    <a class="pricing-table__button pbtn" href="{{ url('user/my-account') }}">
                        Order!
                    </a>
                </div>

                <div class="pricing-table most_popular" id="pricing-table-pk2">
                    <h2 class="pricing-table__header">
                        <em>Most Popular</em>STANDARD
                    </h2>
                    <h3 class="pricing-table__price">$49.99</h3>
                    <a class="pricing-table__button" href="{{ url('user/my-account') }}">
                        Order!
                    </a>
                    <ul class="pricing-table__list">
                        <li class="yes">Send 5,000 Paw Alert Nearest Pet Lover</li>
                        <li class="yes">7 Days Campaign</li>
                        <li class="yes">Custom Poster Design</li>
                        <li class="yes">Feature Post On our website</li>
                        <li class="yes">Real Time Notifications</li>
                        <li class="yes">24/7 Customer Support</li>
                    </ul>
                    <a class="pricing-table__button pbtn" href="{{ url('user/my-account') }}">
                        Order!
                    </a>
                </div>

                <div class="pricing-table" id="pricing-table-pk3">
                    <h2 class="pricing-table__header">PREMIUM</h2>
                    <h3 class="pricing-table__price">$89.99</h3>
                    <a class="pricing-table__button" href="{{ url('user/my-account') }}">
                        Order!
                    </a>
                    <ul class="pricing-table__list">
                        <li class="yes">Send 10,000 Paw Alert Nearest Pet Lover</li>
                        <li class="yes">10 Days Campaign</li>
                        <li class="yes">Custom Poster Design</li>
                        <li class="yes">Feature Post On our website</li>
                        <li class="yes">Real Time Notifications</li>
                        <li class="yes">24/7 Customer Support</li>
                    </ul>
                    <a class="pricing-table__button pbtn" href="{{ url('user/my-account') }}">
                        Order!
                    </a>
                </div>
            </div>

            <div class="clear"></div>
        </div>
    </div><!-- /.site-content -->
@endsection
