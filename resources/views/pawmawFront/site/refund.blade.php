@extends('pawmawFront.layouts.front')

@section('title', 'Refund Policy - PawMaw')

@section('content')
<!-- ABOUT HERO SECTION START -->
<section class="refound-policy">
<!--    <div class="section-dog-bg"style="background-image: url({{ asset('pawmaw/img/common-bg.png')}});background-position: left -60px;background-size: cover;"></div>-->
   
   
   
    <!--  mobile version -->
    
    
    
    <div class="section-dog-bg-mb d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 109px,center;background-size: 262px,cover,cover;background-repeat: no-repeat;"></div>

    <!--  landscape version -->
    
    
    <div class="section-dog-bg-sm d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 125px,center;background-size: 430px,cover,cover;background-repeat: no-repeat;"></div>


    <!--  tab version -->
    
    <div class="section-dog-bg-md d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 182px,center; background-size: 600px,cover,cover;background-repeat: no-repeat;"></div>



    <!--  full screen version -->
    <div class="section-dog-bg" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}}); background-position: center bottom,center 213px,center; background-size: 820px,cover,cover;"></div>

    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-8 offset-sm-2">
                <div class="refound-policy_title others-page-title mx-auto d-block mb-5">
                    <h1>
                        <img src="{{ asset('pawmaw/img/icons/hand-black.png')}}" alt="Hand" class="img-fluid"> <span>refund Policy</span> <img src="{{ asset('pawmaw/img/icons/hand-black.png')}}" alt="Hand" class="img-fluid">
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h2 class="refound-policy_sub mt-5">
                    refund policy for service:
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="refound-policy_details py-5">
                    <p class="refound-policy_details-text mb-3">
                        We offer a 100% refund with your purchase within 120 days from the purchase date. We will process a full refund, if unable to do what we offer in our
                        services.
                    </p>
                    <h2 class="refound-policy_sub mb-3">SOME SITUATIONS ARE LISTED BELOW:</h2>
                    <div class="row">
                        <div class="col-lg-4 offset-lg-4 col-6-md offset-md-3 col-sm-8 offset-sm-2">
                            <ul class="refound-policy_details-list">
                                <li><i class="fas fa-angle-right"></i>  If we are unable to make Flyer/ Poster.</li>
                                <li><i class="fas fa-angle-right"></i>  If we are unable to notify your neighbor.</li>
                                <li><i class="fas fa-angle-right"></i>  If we are unable, send Paw Alert.</li>
                            </ul>
                        </div>
                    </div>
                    <p class="refound-policy_details-text mt-5">If you have any questions on how to return or refund, please contact us on <a href="#" class="pawInfo">info@PawMaw.com</a></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ABOUT HERO SECTION END -->

<!-- GET YOUR PET BACK HOME SECTION START -->
<section class="getyourpet">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="getyourpet-title wow swing" data-wow-duration="1s" data-wow-delay=".3s">Get your pet back home</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <a href="{{ url('/?active-form=1') }}" class="getyourpet-btn mx-auto d-block wow bounceIn" data-wow-duration="1s" data-wow-delay=".3s">Report lost pet</a>
            </div>
        </div>
    </div>
</section>
<!-- GET YOUR PET BACK HOME SECTION END -->
@endsection
