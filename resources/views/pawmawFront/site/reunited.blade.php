@extends('pawmawFront.layouts.front')

@section('title', 'Pets Archive - PawMaw')

@section('content')
    <div class="site-content">
        <div id="common-search-form" class="parallax" style="background-position: 50% 0px;">
            <div class="warper">
                <div id="hero-regi-form-reunited">
                    <form id="search-form" action="{{ url('/pet') }}" method="get">
                        <div class="row left-row">
                            <label for="pet_name_id">Pet Name or ID</label>
                            <input type="text" name="name" id="pet_name_id" placeholder="Pet Name or ID" value="">
                        </div>

                        <div class="row right-row">
                            <label for="status">Status</label>
                            <select name="status" id="status">
                                <option value="">All</option>
                                <option value="lost">Lost</option>
                                <option value="found">Found</option>
                                <option value="reunited" selected>reunited</option>
                            </select>
                        </div>

                        <div class="row left-row">
                            <label for="autocomplete_1">City, Zip, or Address</label>
                            <input type="text" name="address" id="autocomplete_1" placeholder="City, Zip, or Address" value="" onfocus="geolocate()" autocomplete="off">
                        </div>

                        <div class="row right-row">
                            <label for="species">Species</label>
                            <select name="species" id="species">
                                <option value="" selected="selected">All Species</option>
                                <option value="Dog">Dog</option>
                                <option value="Cat">Cat</option>
                                <option value="Bird">Bird</option>
                                <option value="Horse">Horse</option>
                                <option value="Rabbit">Rabbit</option>
                                <option value="Reptile">Reptile</option>
                                <option value="Ferret">Ferret</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>

                        <div class="clear"></div>
                        <button class="btn">Search Pet</button>
                        <div class="clear"></div>
                        <div id="fillinaddress1">
                            <input type="hidden" name="route" class="route">
                            <input type="hidden" name="sublocality_level_1" class="sublocality_level_1">
                            <input type="hidden" name="locality" class="locality">
                            <input type="hidden" name="administrative_area_level_3" class="administrative_area_level_3">
                            <input type="hidden" name="administrative_area_level_2" class="administrative_area_level_2">
                            <input type="hidden" name="administrative_area_level_1" class="administrative_area_level_1">
                            <input type="hidden" name="country" class="country">
                            <input type="hidden" name="postal_code" class="postal_code">
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- /#common-search-form -->

        <div class="warper" id="search-result">
            <h1 class="entry-title">
                @if($pets->total() > 0)
                    <span>reunited PETS
</span>
                @else
                    <span>Nothing Found</span>
                @endif
            </h1>

            <div class="clear"></div>
            @if($pets->total() > 0)
                @foreach($pets as $key => $row)
                    <article class="pet-box {{$row->type}} post-1180 pet type-pet status-publish has-post-thumbnail hentry  @if($key%2 == 0){{'even'}}@else{{'odd'}}@endif">
                        <span class="pet-statue-tag {{$row->type}}">{{ucfirst($row->type)}}</span>

                        <div class="post-thumbnail">
                            <a class="full-post-link" href="{{ url('/pet-details') }}/{{$row->slug}}">
                                <span class="genericon genericon-link"></span>
                            </a>

                            <a class="full-image-link fancybox" href="{{ asset('uploads/pets_image') }}/{{$row->photo}}" title="{{$row->name}}">
                                <span class="genericon genericon-zoom"></span>
                            </a>

                            <img
                                    class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="Pet Image"
                                    width="250" height="250"
                                    src="{{ asset('uploads/pets_image') }}/{{$row->photo}}"
                                    srcset="{{ asset('uploads/pets_image') }}/{{$row->photo}} 250w"
                                    sizes="(max-width: 250px) 100vw, 250px"
                            >
                        </div>

                        <h2 class="entry-title">
                            <a href="{{ url('/pet-details') }}/{{$row->slug}}">{{$row->name}} <br> <em>A {{$row->gender}} {{$row->specie}} {{ucfirst($row->type)}}</em></a>
                        </h2>

                        <strong class="petid">ID: #{{$row->id}}</strong>

                        <div class="entry-content">
                            <div class="clear"></div>
                            <a class="sbtn details" href="{{ url('/pet-details') }}/{{$row->slug}}">
                                <span class="display genericon genericon-external"></span> Details
                            </a>
                        </div>
                    </article>
                @endforeach

            @else
                <h3>
                    Sorry, but nothing matched your search terms. Please try again with some different keywords.
                </h3>
            @endif
            <div class="clear"></div>

            <div class="custom-pagination">
                {{$pets->appends(request()->query())->links( "pagination::bootstrap-4") }}
            </div>
        </div>
        <div class="clear"></div>
    </div><!-- /.site-content-->
@endsection