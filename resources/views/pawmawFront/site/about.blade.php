@extends('pawmawFront.layouts.front') @section('title', 'About - PawMaw') @section('content')
<!-- ABOUT HERO SECTION START -->
<section class="about">


    <!--  mobile version -->
    
    
    
    <div class="section-dog-bg-mb d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 109px,center;background-size: 262px,cover,cover;background-repeat: no-repeat;"></div>

    <!--  landscape version -->
    
    
    <div class="section-dog-bg-sm d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 125px,center;background-size: 430px,cover,cover;background-repeat: no-repeat;"></div>


    <!--  tab version -->
    
    <div class="section-dog-bg-md d-none" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}});background-position: center bottom,center 182px,center; background-size: 600px,cover,cover;background-repeat: no-repeat;"></div>



    <!--  full screen version -->
    <div class="section-dog-bg" style="background-image: url({{ asset('pawmaw/img/common-bg-dog.png')}}),url({{ asset('pawmaw/img/common-bg-white.png')}}),url({{ asset('pawmaw/img/common-bg-1.png')}}); background-position: center bottom,center 213px,center; background-size: 820px,cover,cover;"></div>


    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-8 offset-sm-2">
                <div class="about-title others-page-title mx-auto d-block mb-5">
                    <h1 class="wow pulse" data-wow-duration="1s" data-wow-delay=".3s">
                        <img src="{{ asset('pawmaw/img/icons/hand-black.png')}}" alt="Hand" class="img-fluid"> <span>About us</span> <img src="{{ asset('pawmaw/img/icons/hand-black.png')}}" alt="Hand" class="img-fluid">
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-sm-12">
                <h2 class="about-title_sub mt-5">
                    <span>Welcome to PawMaw</span>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="about-details py-5">
                    <p class="about-details_text mb-5">
                        Welcome to PawMaw. Finding lost & found pets are our sole priority here at <a href="https://www.pawmaw.com/" target="_blank" style="color:#df1d41;">www.pawmaw.com.</a> We started off creating a platform that provides a service, whereby we reunite lost & found pets in their family, with less or no stress at all. Anyone who has once lost a pet can attest that it is a difficult task finding your pet on your own and alone. So PawMaw is here to stand in the gap, by creating a technological means of proffering a lasting solution that reunites pet owners with their lost & found pets.
                    </p>
                    <p class="about-details_text mb-5">
                        We have developed a system that can reach out to the concerned residents in the neighborhood where the pet got missing in a few minutes, and by this, we create a better awareness for a lost & found pet to everyone – which is more effective and easier than going alone to look for them. Our system helps to create an active neighborhood search, thus increasing your chance of finding your pet.  That’s why PawMaw is here to create a story that has a happy ending. 
                    </p>
                    <h3 style="font-size:24px;margin-bottom:15px;">Our missions are as follows:</h3>
                    <ul class="about-details_list mb-5">
                        <li class="about-details_list_item"><img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand" class="img-fluid"> To consistently create a regularly upgraded system to help boost the chances of getting to reunite lost and found pets back to their family.</li>
                        <li class="about-details_list_item"><img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand" class="img-fluid"> To provide the best and most effective tools that can be useful in recovering lost & found pets, which will effectively provide actual results to the owners of the pets.</li>
                    </ul>
                    
                    <h3 style="font-size:22px;margin-bottom:15px;">You can reach out to us at via:</h3>
                    <ul class="about-details_list mb-5">
                        <li class="about-details_list_item"><img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand" class="img-fluid">Email using <a href="mailto:info@pawmaw.com" style="color:#df1d41;">info@pawmaw.com</a> and  <a href="mailto:sales@pawmaw.com" style="color:#df1d41;">sales@pawmaw.com</a></li>
                        <li class="about-details_list_item"><img src="{{ asset('pawmaw/img/icons/hand-red.png')}}" alt="Hand" class="img-fluid"> Our contact number <a href="tel:+1 909 764 0024" style="color:#df1d41;">+1 909 764 0024.</a> </li>
                    </ul>
                    <p class="about-details_text">
                        Every day we help distressed pet owners find their lost, scared, and often hungry pet. It's always gratifying when a loving owner is reunited with their beloved pet. This is a story that always has a happy ending!</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ABOUT HERO SECTION END -->

<!-- GET YOUR PET BACK HOME SECTION START -->
<section class="getyourpet">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="getyourpet-title wow swing" data-wow-duration="1s" data-wow-delay=".3s">Get your pet back home</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <a href="{{ url('/?active-form=1') }}" class="getyourpet-btn mx-auto d-block wow bounceIn" data-wow-duration="1s" data-wow-delay=".3s">Report lost pet</a>
            </div>
        </div>
    </div>
</section>
<!-- GET YOUR PET BACK HOME SECTION END -->
@endsection
