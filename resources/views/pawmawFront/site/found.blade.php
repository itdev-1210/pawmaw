@extends('pawmawFront.layouts.front')

@section('title', 'Pets Archive - PawMaw')

@section('content')


<!-- pop up start   -->
<div class="pop_up_banner">
    <div class="col-lg-12">
        <h2>IS YOUR PET LOST?</h2>
        <h4>Create a FREE Listing and Alert People in Your Area Add Your Lost/Found Pets to Our Database.</h4>
        <div class="PopUp_btn">
            <a href="https://www.pawmaw.com/report">Report Lost Pet</a>
            <a href="https://www.pawmaw.com/join-our-network">Join Our Network</a>
        </div>
        <div class="PopUp_close">
            <p>No Thanks, my pet is not missing!</p>
        </div>
    </div>
</div>
<div class="PopUp_overlay"></div>
<!-- pop up end   -->

<!-- SEARCH SECTION START -->
<section class="search" style="background-color: #e4dfd6;">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="search-title">
                    <h1>Search your lost & found pets</h1>
                    <h2 class="text-center">Find lost & found dogs, cats & other pet in your area</h2>
                    <a href="{{ $facebook ? $facebook->link : 'https://www.facebook.com/PawMawOfficial/' }}" target="_blank" class="fb-btn mb-sm-5 mb-xs-5 text-center"><i class="fab fa-facebook-f mr-4"></i> Join our network</a>
                </div>

                <div class="search-bg" style="background-image: url({{ asset('pawmaw/img/search-page_bg.png')}});">
                    <div class="search-box wow pulse" data-wow-duration="1s" data-wow-delay=".3s">
                        <div class="search-box_inner">
                            <form  action="{{ url('/pet') }}" method="get">
                                <div class="form-row mb-5">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <input type="hidden" name="search" value="1">
                                        <div class="form-group">
                                            <label for="pet_id">Pet ID or Name:</label>
                                            <input type="text" name="name" value="{{$name}}" class="form-control" id="pet_id" placeholder="Pet ID or Name">
                                        </div>

                                        <div class="form-group">
                                            <label>Status:</label> <br>
                                            <div class="form-check form-check-inline">
                                                <label class="custom-control overflow-checkbox">
                                                    <input type="checkbox" class="overflow-control-input form-check-input" value="lost" name="status[]" @if(in_array('lost',$status)){{'checked'}}@endif>
                                                           <span class="overflow-control-indicator"></span>
                                                    <span class="overflow-control-description">Lost</span>
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <label class="custom-control overflow-checkbox">
                                                    <input type="checkbox" class="overflow-control-input form-check-input" value="found" name="status[]" @if(in_array('found',$status)){{'checked'}}@endif>
                                                           <span class="overflow-control-indicator"></span>
                                                    <span class="overflow-control-description">Found</span>
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <label class="custom-control overflow-checkbox">
                                                    <input type="checkbox" class="overflow-control-input form-check-input" value="reunited" name="status[]" @if(in_array('reunited',$status)){{'checked'}}@endif >
                                                           <span class="overflow-control-indicator"></span>
                                                    <span class="overflow-control-description">Reunited</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="species">Species:</label>
                                            <select class="select-ctrl" id="species" name="species">
                                                <option value="0">species</option>
                                                <option value="Dog" @if($species=='Dog'){{'selected'}}@endif>Dog</option>
                                                <option value="Cat" @if($species=='Cat'){{'selected'}}@endif>Cat</option>
                                                <option value="Bird" @if($species=='Bird'){{'selected'}}@endif>Bird</option>
                                                <option value="Horse" @if($species=='Horse'){{'selected'}}@endif>Horse</option>
                                                <option value="Rabbit" @if($species=='Rabbit'){{'selected'}}@endif>Rabbit</option>
                                                <option value="Reptile" @if($species=='Reptile'){{'selected'}}@endif>Reptile</option>
                                                <option value="Ferret" @if($species=='Ferret'){{'selected'}}@endif>Ferret</option>
                                                <option value="Other" @if($species=='Other'){{'selected'}}@endif>Other</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="location">Location:</label>
                                            <input type="text" class="form-control" id="location" name="address" value="{{ $address }}" placeholder="Zip Code/ City Name/ State">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-lg-6 offset-lg-3 col-md-6 offset-md-3">
                                        <button type="submit" class="search-box_inner-btn mx-auto d-block">search <i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- SEARCH RESULTS ITME SECTION START -->
                <div class="search-items">
                    <div class="row">
                        @if($pets->total() > 0)
                        @foreach($pets as $key=>$row)
                        <div class="col-lg-4 col-md-4 col-sm-6 mb-5">
                            <div class="search-items_box mb-sm-5 mb-xs-5 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
                                <a href="{{ url('/pet-details') }}/{{$row->slug}}" class="zoomIn" style="width:100%;height:235px;">
                                    <figure 
                                        style="height:235px;">
                                        @if(file_exists( public_path().'/uploads/pets_image/'.$row->photo ) && $row->photo!='')
                                        <img class="img-fluid search-items_box-img" src="{{ url('uploads/pets_image') }}/{{$row->photo}}" alt="{{$row->name}}">
                                        @else
                                        <img class="img-fluid search-items_box-img" src="{{ url('pawmaw/img/default.png') }}" alt="{{$row->name}}">
                                        @endif


                                    </figure>
                                </a>
                                <div class="search-items_box-body">
                                    <h2 class="search-items_box-body_title">{{$row->name}}</h2>
                                    <h3 class="search-items_box-body_gender tag-{{$row->type}}">A {{ucfirst($row->type)}} {{$row->specie}}</h3>
                                    <p class="search-items_box-body_id">ID:#{{$row->id}}</p>
                                    <h2 class="search-items_box-body_text">{{$row->last_seen}}</h2>
                                    <div class="">
                                        <p class="search-items_box-body_date tag-{{$row->type}}">{{ucfirst($row->type)}}: {{date('d.m.Y',strtotime($row->date))}}</p>
                                    </div>
                                    <div class="search-items_box-body_share">
                                        <a href="http://www.facebook.com/sharer.php?u={{ url('/pet-details') }}/{{$row->slug}}" target="_blank" class="search-items_box-body_share-btn mx-auto">
                                            <i class="fab fa-facebook-f m-3"></i><span>Share</span>
                                        </a>
                                        {{-- / --}}
                                    </div>
                                    <a href="{{ url('/pet-details') }}/{{$row->slug}}" class="search-items_box-body_details-btn {{$row->type}}-btn">Details</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <span><h2>Sorry, but nothing matched your search terms. Please try again with some different keywords.</h2></span>
                        @endif
                    </div>
                    <div class="pagination">
                        <center>{{$pets->appends(request()->query())->onEachSide(5)->links( "pagination::bootstrap-4") }}</center>
                    </div>
                </div>
                <!-- SEARCH RESULTS ITME SECTION END -->
            </div>

            <div class="col-lg-3">
                <div class="search-side_bar wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
                    <div class="search-side_bar-join mt-2">
                        <img src="{{ asset('pawmaw/img/icons/search-page-like.png')}}" alt="Like" class="img-fluid mx-auto d-block like-img">
                        <h2 class="py-5">Join our family</h2>
                       <p>{{ $facebook ? $facebook->name : 'Lost & Found Pets' }}</p>
                       <!--  <p>Lost & Found Pets</p> -->
                    </div>
                    <div class="search-side_bar-bg" style="background-image: url({{ asset('pawmaw/img/search-page-side_img.png')}});">
                        <div class="search-side_bar-fblike">
                            <!--                            <div class="fb-like" data-href="{{ $facebook ? $facebook->link : 'https://www.facebook.com/PawMawOfficial/' }}" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>-->
                            <div class="fb-like" data-href="{{ $facebook ? $facebook->link : 'https://www.facebook.com/PawMawOfficial/' }}" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
                        </div>
                        <div id="fb-root"></div>
                        <script>(function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id))
                                    return;
                                js = d.createElement(s);
                                js.id = id;
                                js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2&appId=1863631387098360&autoLogAppEvents=1';
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                    </div>
                    <div class="search-side_bar-learnmore">
                        <a href="/join-our-network" class="search-side_bar-learnmore_btn">Learn more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- SEARCH SECTION END -->

<!-- GET YOUR PET BACK HOME SECTION START -->
<section class="getyourpet">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="getyourpet-title wow swing" data-wow-duration="1s" data-wow-delay=".3s">Get your pet back home</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <a href="{{ url('/?active-form=1') }}" class="getyourpet-btn mx-auto d-block wow bounceIn" data-wow-duration="1s" data-wow-delay=".3s">Report lost pet</a>
            </div>
        </div>
    </div>
</section>
<!-- GET YOUR PET BACK HOME SECTION END -->
@endsection