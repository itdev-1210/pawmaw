<div class="tbl">
    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Name:</div>
        <div class="tbl-cell">
            {{ $user->name }}
        </div>
    </div>
    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Email:</div>
        <div class="tbl-cell">{{ $user->email }}</div>
    </div>
</div>