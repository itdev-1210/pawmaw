<style type="text/css">
    .light.red{
        color: #ff0000;
    }
    .light.green{
        color: #00a752;
    }
</style>
{!!Form::model($user,['method'=>'PETCH','route'=>['admin_password',$user->id]])!!}
<div class="box-typical box-typical-padding password_form">
    <div class="row">
        <div class="col-lg-12">
            <fieldset class="form-group">
                <label class="form-label semibold" for="password">
                    Password
                    <span class="color-red">*</span>
                </label>
                <input type="password" class="form-control required" name="password" placeholder="Password" id="password" required="">
            </fieldset>
            <fieldset class="form-group">
                <label class="form-label semibold" for="password_confirmation">
                    Confirm Password
                    <span class="color-red">*</span>
                    <span class="light">
                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                    </span>
                </label>
                <input type="password" class="form-control required" name="password_confirmation" placeholder="Confirm Password" id="password_confirmation" required="">
            </fieldset>
        </div>
    </div><!--.row-->
</div>
{!! Form::close() !!}
<script type="text/javascript">
    $('#password_confirmation').on('keyup',function(){
        var pass1 = $('#password').val();
        var pass2 = $(this).val();
        if(pass1 == pass2){
            $('.light').removeClass('red');
            $('.light').addClass('green');
        }else{
            $('.light').removeClass('green');
            $('.light').addClass('red');
        }
    });
</script>