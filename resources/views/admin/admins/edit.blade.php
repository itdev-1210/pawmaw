@extends('admin.layout.admin')

@section('content')
@push('css-head')
<link rel="stylesheet" href="{{ asset('/assets/css/separate/elements/steps.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/pnotify.min.css') }}">
<style type="text/css">
    .light.red{
        color: #ff0000;
    }
    .light.green{
        color: #00a752;
    }
</style>
@endpush
<div class="page-content">
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3>Update Admin</h3>
                        <ol class="breadcrumb breadcrumb-simple">
                            <li>
                                <a href="{{ url('admin/admins') }}">Admin List</a>
                            </li>
                            <li class="active">Update</li>
                        </ol>
                    </div>
                </div>
            </div>
        </header>

        <div class="box-typical box-typical-padding">
            @if (count($errors) > 0)
                <div class="alert alert-danger alert-no-border alert-txt-colored alert-close alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                </div>
            @endif

            {!!Form::model($user,['method'=>'PETCH','route'=>['admins_update',$user->id]])!!}
            <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold" for="name">
                                    Name
                                    <span class="color-red">*</span>
                                </label>
                                <input type="text" class="form-control required" name="name" value="{{$user->name}}" placeholder="name" required="">
                            </fieldset>
                        </div>
                        <div class="col-md-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold" for="email">
                                    Email
                                    <span class="color-red">*</span>
                                </label>
                                <input type="email" class="form-control required" name="email" value="{{$user->email}}" placeholder="Email" required="">
                            </fieldset>
                        </div>
                        <div class="col-md-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold" for="password">
                                    Password
                                </label>
                                <input type="password" class="form-control required" name="password" placeholder="Password" id="password">
                            </fieldset>
                        </div>
                        <div class="col-md-12">
                            <fieldset class="form-group">
                                <label class="form-label semibold" for="password_confirmation">
                                    Confirm Password
                                    <span class="light">
                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                    </span>
                                </label>
                                <input type="password" class="form-control required" name="password_confirmation" placeholder="Confirm Password" id="password_confirmation">
                            </fieldset>
                        </div>
                        <div class="col-md-12">
                            <fieldset class="form-group">
                                <label for="role" class="form-label semibold">
                                    Role
                                    <span class="color-red">*</span>
                                </label>
                                <select id="role" name="role" class="form-control required" required="">
                                    <option selected="" disabled="">Choose one..</option>
                                    @foreach($roles as $row)
                                    <option value="{{$row->id}}" @if(@$assigned_role[0] == $row->id){{'selected'}}@endif>{{$row->name}}</option>
                                    @endforeach
                                </select>
                            </fieldset>
                        </div>
                    </div><!--.row-->
                </div>
                <div class="col-md-2">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <a href="{{ url('admin/admins/edit') }}/{{$user->id}}" class="btn btn-inline btn-secondary pull-left">
                        <span class="btn-icon"> 
                            <i class="fa fa-refresh"></i>
                        </span>
                        Refresh
                    </a>
                    <button type="submit" class="btn btn-inline btn-success pull-right">
                        <span class="btn-icon"> 
                            <i class="fa fa-edit"></i>
                        </span>
                        Update
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div><!--.box-typical-->
    </div><!--.container-fluid-->
</div><!--.page-content-->
@push('scripts')
    <script src="{{ asset('/assets/js/lib/pnotify/pnotify.js') }}"></script>
    <script src="{{ asset('/assets/custom/js/notify.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            
        });

        $('#password_confirmation').on('keyup',function(){
            var pass1 = $('#password').val();
            var pass2 = $(this).val();
            if(pass1 == pass2){
                $('.light').removeClass('red');
                $('.light').addClass('green');
            }else{
                $('.light').removeClass('green');
                $('.light').addClass('red');
            }
        });
    </script>
@endpush
@endsection