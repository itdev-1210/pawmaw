<!DOCTYPE html>
<html lang="">
    <head lang="en">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Admin || {{ config('app.name') }}</title>

        <!-- <link href="{{ asset('/assets/img/favicon.144x144.png')}}" rel="apple-touch-icon" type="image/png" sizes="144x144">
        <link href="{{ asset('/assets/img/favicon.114x114.png')}}" rel="apple-touch-icon" type="image/png" sizes="114x114">
        <link href="{{ asset('/assets/img/favicon.72x72.png')}}" rel="apple-touch-icon" type="image/png" sizes="72x72">
        <link href="{{ asset('/assets/img/favicon.57x57.png')}}" rel="apple-touch-icon" type="image/png">
        <link href="{{ asset('/assets/img/favicon.png')}}" rel="icon" type="image/png">
        <link href="{{ asset('/assets/img/favicon.ico')}}" rel="shortcut icon"> -->

        <link href="{{ asset('/assets/custom/img/favicon.png')}}" rel="icon" type="image/png">

        <link rel="stylesheet" href="{{ asset('/assets/css/separate/pages/login.min.css')}}">
        <link rel="stylesheet" href="{{ asset('/assets/css/lib/font-awesome/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{ asset('/assets/css/lib/bootstrap/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/pnotify.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/assets/css/main.css')}}">
        <link rel="stylesheet" href="{{ asset('/assets/custom/css/main.css')}}">

        <script src="{{ asset('/assets/js/lib/jquery/jquery-3.2.1.min.js')}}"></script>
    </head>
    <body>
        <div class="page-center">
            <div class="page-center-in">
                <div class="container-fluid">
                    <form class="sign-box" method="POST" action="{{ url('/admin/login') }}">
                        {{ csrf_field() }}
                        <div class="sign-logo-box">
                            <center>
                                <a href="{{ url('/admin') }}">
                                    <img height="60" src="{{ asset('pawmaw/img/logo.png') }}" alt="">
                                </a>
                            </center>
                        </div>
                        <header class="sign-title">Sign In</header>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" placeholder="E-Mail" required="" />
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Password" required="" />
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        {{--<div class="form-group">--}}
                            {{--<div class="checkbox float-left">--}}
                                {{--<input type="checkbox" name="remember" id="signed-in" {{ old('remember') ? 'checked' : '' }}>--}}
                                {{--<label for="signed-in">Keep me signed in</label>--}}
                            {{--</div>--}}
                            {{--<div class="float-right reset">--}}
                                {{--<a href="{{ url('/admin/password/reset') }}">Reset Password</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <button type="submit" class="btn btn-rounded">Sign in</button>
                        {{--<p class="sign-note">New to our website? <a href="{{ url('/admin/register') }}">Sign up</a></p>--}}
                        <!--<button type="button" class="close">
                            <span aria-hidden="true">&times;</span>
                        </button>-->
                    </form>
                    <div class="sign-box" style="padding: 5px;">
                       <table>
                           <tbody>

                           </tbody>
                       </table>
                       <p class="msg">** This box is using just for development purpose.</p>

                       <style type="text/css">
                           table{
                            margin: 0px auto;
                           }
                           td{
                                padding: 5px;
                                font-size: 16px;
                           }
                           .copy{
                                min-width: 50px !important;
                                font-size: 11px !important;
                                padding: 5px !important;
                                margin-top: 10px !important;
                           }
                           .msg{
                            font-size: 12px;
                            padding-left: 15px;
                            color: #f00;
                           }
                       </style>
                       <script type="text/javascript">
                           $('.copy').on('click', function(){
                                var mail = $(this).closest('tr').find('.email').html();
                                var pass = $(this).closest('tr').find('.password').html();
                                $('form').find('input[name=email]').val(mail);
                                $('form').find('input[name=password]').val(pass);
                           });
                       </script>
                    </div>
                </div>
            </div>
        </div>
        @if ($message = Session::get('error'))
        <script type="text/javascript">
            $(document).ready(function(){
                var msg = "{{ $message }}";
                notify('error',msg);
            });
        </script>
        @endif
        <script src="{{ asset('/assets/js/lib/popper/popper.min.js')}}"></script>
        <script src="{{ asset('/assets/js/lib/tether/tether.min.js')}}"></script>
        <script src="{{ asset('/assets/js/lib/bootstrap/bootstrap.min.js')}}"></script>
        <script src="{{ asset('/assets/js/plugins.js')}}"></script>
        <script src="{{ asset('/assets/js/lib/pnotify/pnotify.js') }}"></script>
        <script src="{{ asset('/assets/custom/js/notify.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/assets/js/lib/match-height/jquery.matchHeight.min.js')}}"></script>
        <script>
            $(function() {
                $('.page-center').matchHeight({
                    target: $('html')
                });

                $(window).resize(function(){
                    setTimeout(function(){
                        $('.page-center').matchHeight({ remove: true });
                        $('.page-center').matchHeight({
                            target: $('html')
                        });
                    },100);
                });
            });
        </script>
        <script src="{{ asset('/assets/js/app.js')}}"></script>
        <style type="text/css">
            .alert-with-icon .alert .font-icon{
                top: -2px !important;
            }
        </style>
    </body>
</html>