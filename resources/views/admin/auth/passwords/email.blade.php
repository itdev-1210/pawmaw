<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head lang="en">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Admin || {{ config('app.name') }}</title>

        <!-- <link href="{{ asset('/assets/img/favicon.144x144.png')}}" rel="apple-touch-icon" type="image/png" sizes="144x144">
        <link href="{{ asset('/assets/img/favicon.114x114.png')}}" rel="apple-touch-icon" type="image/png" sizes="114x114">
        <link href="{{ asset('/assets/img/favicon.72x72.png')}}" rel="apple-touch-icon" type="image/png" sizes="72x72">
        <link href="{{ asset('/assets/img/favicon.57x57.png')}}" rel="apple-touch-icon" type="image/png">
        <link href="{{ asset('/assets/img/favicon.png')}}" rel="icon" type="image/png">
        <link href="{{ asset('/assets/img/favicon.ico')}}" rel="shortcut icon"> -->

        <link href="{{ asset('/assets/custom/img/favicon.png')}}" rel="icon" type="image/png">

        <link rel="stylesheet" href="{{ asset('/assets/css/separate/pages/login.min.css')}}">
        <link rel="stylesheet" href="{{ asset('/assets/css/lib/font-awesome/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{ asset('/assets/css/lib/bootstrap/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{ asset('/assets/css/main.css')}}">
        <link rel="stylesheet" href="{{ asset('/assets/custom/css/main.css')}}">

        <script src="{{ asset('/assets/js/lib/jquery/jquery-3.2.1.min.js')}}"></script>
    </head>
    <body>
        <div class="page-center">
            <div class="page-center-in">
                <div class="container-fluid">
                    <form class="sign-box reset-password-box" role="form" method="POST" action="{{ url('/admin/password/email') }}">
                        {{ csrf_field() }}
                        <div class="sign-logo-box">
                            <center>
                                <img height="60" src="{{ asset('pawmaw/img/logo.png') }}" alt="">
                            </center>
                        </div>
                        <header class="sign-title">Reset Password</header>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email"  value="{{ old('email') }}"" placeholder="E-Mail"/>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-rounded">Send Request</button>
                        or <a href="{{ url('/admin') }}">Sign in</a>
                    </form>
                </div>
            </div>
        </div><!--.page-center-->
        <script src="{{ asset('/assets/js/lib/popper/popper.min.js')}}"></script>
        <script src="{{ asset('/assets/js/lib/tether/tether.min.js')}}"></script>
        <script src="{{ asset('/assets/js/lib/bootstrap/bootstrap.min.js')}}"></script>
        <script src="{{ asset('/assets/js/plugins.js')}}"></script>
            <script type="text/javascript" src="{{ asset('/assets/js/lib/match-height/jquery.matchHeight.min.js')}}"></script>
            <script>
                $(function() {
                    $('.page-center').matchHeight({
                        target: $('html')
                    });

                    $(window).resize(function(){
                        setTimeout(function(){
                            $('.page-center').matchHeight({ remove: true });
                            $('.page-center').matchHeight({
                                target: $('html')
                            });
                        },100);
                    });
                });
            </script>
        <script src="{{ asset('/assets/js/app.js')}}"></script>
    </body>
</html>
