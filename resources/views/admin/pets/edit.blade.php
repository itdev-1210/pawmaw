@extends('admin.layout.admin')

@section('content')
@push('css-head')

@endpush
<div class="page-content">
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3>Update Update</h3>
                        <ol class="breadcrumb breadcrumb-simple">
                            <li>
                                <a href="{{ url('admin/pets') }}">Pets List</a>
                            </li>
                            <li class="active">Update</li>
                        </ol>
                    </div>
                </div>
            </div>
        </header>

        <div class="box-typical box-typical-padding">
            @if (count($errors) > 0)
                <div class="alert alert-danger alert-no-border alert-txt-colored alert-close alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                </div>
            @endif
            {!!Form::model($pet,['method'=>'PETCH', 'enctype'=>'multipart/form-data', 'route'=>['admin_pets_update',$pet->id]])!!}
                <div class="row">
                    <div class="col-lg-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="name">
                                Name
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="name" value="{{$pet->name}}" id="name" placeholder="Name">
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="form-label" for="logo">
                                Select Image
                                <span class="color-red">*</span>
                            </label>
                            <div class="row">
                                <div class="col-lg-6">
                                    <img src="{{ asset('/uploads/pets_image') }}/{{$pet->photo}}" class="img-responsive" height="50">
                                </div>
                                <div class="col-lg-6">
                                    <input type="file" name="pet_photo" id="logo" accept="image/*">
                                </div>
                            </div>
                        </fieldset>




                        <fieldset class="form-group">
                            <label for="specie" class="form-label semibold">
                                Specie
                                <span class="color-red">*</span>
                            </label>
                            <select id="specie" name="specie" class="form-control required" required="">
                                <option selected="" disabled="">Choose one..</option>
                                <option value="Dog" @if($pet->specie=='Dog'){{'selected'}}@endif>Dog</option>
                                <option value="Cat" @if($pet->specie=='Cat'){{'selected'}}@endif>Cat</option>
                                <option value="Bird" @if($pet->specie=='Bird'){{'selected'}}@endif>Bird</option>
                                <option value="Horse" @if($pet->specie=='Horse'){{'selected'}}@endif>Horse</option>
                                <option value="Rabbit" @if($pet->specie=='Rabbit'){{'selected'}}@endif>Rabbit</option>
                                <option value="Reptile" @if($pet->specie=='Reptile'){{'selected'}}@endif>Reptile</option>
                                <option value="Ferret" @if($pet->specie=='Ferret'){{'selected'}}@endif>Ferret</option>
                                <option value="Other" @if($pet->specie=='Other'){{'selected'}}@endif>Other</option>
                            </select>
                        </fieldset>



                        <fieldset class="form-group">
                            <label for="sex" class="form-label semibold">
                                Gender
                                <span class="color-red">*</span>
                            </label>
                            <select id="sex" name="sex" class="form-control required" required="">
                                <option selected="" disabled="">Choose one..</option>
                                <option value="Unknown" @if($pet->gender=='Unknown'){{'selected'}}@endif>Unknown</option>
                                <option value="Male" @if($pet->gender=='Male'){{'selected'}}@endif>Male</option>
                                <option value="Female" @if($pet->gender=='Female'){{'selected'}}@endif>Female</option>
                            </select>
                        </fieldset>


                        <input type="hidden" name="action" value="{{$pet->type}}">





                        <fieldset class="form-group">
                            <label for="breed" class="form-label semibold">
                                Breed
                                <span class="color-red">*</span>
                            </label>
                            <select id="breed" name="breed" class="form-control required" required="">
                                <option selected="" disabled="">Choose one..</option>
                                <option value="Mixed Breed" @if($pet->breed=='Mixed Breed'){{'selected'}}@endif>Mixed Breed</option>

                                <option value="Unknown" @if($pet->breed=='Unknown'){{'selected'}}@endif>Unknown</option>

                                <option value="Other" @if($pet->breed=='Other'){{'selected'}}@endif>Other</option>

                                <option value="Affenpinscher" @if($pet->breed=='Affenpinscher'){{'selected'}}@endif>Affenpinscher</option>

                                <option value="Afghan Hound" @if($pet->breed=='Afghan Hound'){{'selected'}}@endif>Afghan Hound</option>

                                <option value="Airedale Terrier" @if($pet->breed=='Airedale Terrier'){{'selected'}}@endif>Airedale Terrier</option>

                                <option value="Akita" @if($pet->breed=='Akita'){{'selected'}}@endif>Akita</option>

                                <option value="Alaskan Husky" @if($pet->breed=='Alaskan Husky'){{'selected'}}@endif>Alaskan Husky</option>

                                <option value="Alaskan Klee Kai" @if($pet->breed=='Alaskan Klee Kai'){{'selected'}}@endif>Alaskan Klee Kai</option>

                                <option value="Alaskan Malamute" @if($pet->breed=='Alaskan Malamute'){{'selected'}}@endif>Alaskan Malamute</option>

                                <option value="American Bulldog" @if($pet->breed=='American Bulldog'){{'selected'}}@endif>American Bulldog</option>

                                <option value="American Bully" @if($pet->breed=='American Bully'){{'selected'}}@endif>American Bully</option>

                                <option value="American English Coonhound" @if($pet->breed=='American English Coonhound'){{'selected'}}@endif>American English Coonhound</option>

                                <option value="American Eskimo Dog" @if($pet->breed=='American Eskimo Dog'){{'selected'}}@endif>American Eskimo Dog</option>

                                <option value="American Foxhound" @if($pet->breed=='American Foxhound'){{'selected'}}@endif>American Foxhound</option>

                                <option value="American Hairless Terrier" @if($pet->breed=='American Hairless Terrier'){{'selected'}}@endif>American Hairless Terrier</option>

                                <option value="American Leopard Hound" @if($pet->breed=='American Leopard Hound'){{'selected'}}@endif>American Leopard Hound</option>

                                <option value="American Staffordshire Terrier" @if($pet->breed=='American Staffordshire Terrier'){{'selected'}}@endif>American Staffordshire Terrier</option>

                                <option value="American Water Spaniel" @if($pet->breed=='American Water Spaniel'){{'selected'}}@endif>American Water Spaniel</option>

                                <option value="Anatolian Shepherd Dog" @if($pet->breed=='Anatolian Shepherd Dog'){{'selected'}}@endif>Anatolian Shepherd Dog</option>

                                <option value="Appenzeller Sennenhunde" @if($pet->breed=='Appenzeller Sennenhunde'){{'selected'}}@endif>Appenzeller Sennenhunde</option>

                                <option value="Australian Cattle Dog" @if($pet->breed=='Australian Cattle Dog'){{'selected'}}@endif>Australian Cattle Dog</option>

                                <option value="Australian Kelpie" @if($pet->breed=='Australian Kelpie'){{'selected'}}@endif>Australian Kelpie</option>

                                <option value="Australian Shepherd" @if($pet->breed=='Australian Shepherd'){{'selected'}}@endif>Australian Shepherd</option>

                                <option value="Australian Terrier" @if($pet->breed=='Australian Terrier'){{'selected'}}@endif>Australian Terrier</option>

                                <option value="Azawakh" @if($pet->breed=='Azawakh'){{'selected'}}@endif>Azawakh</option>

                                <option value="Barbet" @if($pet->breed=='Barbet'){{'selected'}}@endif>Barbet</option>

                                <option value="Basenji" @if($pet->breed=='Basenji'){{'selected'}}@endif>Basenji</option>

                                <option value="Basset Fauve De Bretagne" @if($pet->breed=='Basset Fauve De Bretagne'){{'selected'}}@endif>Basset Fauve De Bretagne</option>

                                <option value="Basset Hound" @if($pet->breed=='Basset Hound'){{'selected'}}@endif>Basset Hound</option>

                                <option value="Beagle" @if($pet->breed=='Beagle'){{'selected'}}@endif>Beagle</option>

                                <option value="Bearded Collie" @if($pet->breed=='Bearded Collie'){{'selected'}}@endif>Bearded Collie</option>

                                <option value="Beauceron" @if($pet->breed=='Beauceron'){{'selected'}}@endif>Beauceron</option>

                                <option value="Bedlington Terrier" @if($pet->breed=='Bedlington Terrier'){{'selected'}}@endif>Bedlington Terrier</option>

                                <option value="Belgian Laekenois" @if($pet->breed=='Belgian Laekenois'){{'selected'}}@endif>Belgian Laekenois</option>

                                <option value="Belgian Malinois" @if($pet->breed=='Belgian Malinois'){{'selected'}}@endif>Belgian Malinois</option>

                                <option value="Belgian Sheepdog" @if($pet->breed=='Belgian Sheepdog'){{'selected'}}@endif>Belgian Sheepdog</option>

                                <option value="Belgian Tervuren" @if($pet->breed=='Belgian Tervuren'){{'selected'}}@endif>Belgian Tervuren</option>

                                <option value="Bergamasco" @if($pet->breed=='Bergamasco'){{'selected'}}@endif>Bergamasco</option>

                                <option value="Berger Picard" @if($pet->breed=='Berger Picard'){{'selected'}}@endif>Berger Picard</option>

                                <option value="Bernese Mountain Dog" @if($pet->breed=='Bernese Mountain Dog'){{'selected'}}@endif>Bernese Mountain Dog</option>

                                <option value="Bichon Frise" @if($pet->breed=='Bichon Frise'){{'selected'}}@endif>Bichon Frise</option>

                                <option value="Biewer Terrier" @if($pet->breed=='Biewer Terrier'){{'selected'}}@endif>Biewer Terrier</option>

                                <option value="Black and Tan Coonhound" @if($pet->breed=='Black and Tan Coonhound'){{'selected'}}@endif>Black and Tan Coonhound</option>

                                <option value="Black Russian Terrier" @if($pet->breed=='Black Russian Terrier'){{'selected'}}@endif>Black Russian Terrier</option>

                                <option value="Blackmouth Cur" @if($pet->breed=='Blackmouth Cur'){{'selected'}}@endif>Blackmouth Cur</option>

                                <option value="Bloodhound" @if($pet->breed=='Bloodhound'){{'selected'}}@endif>Bloodhound</option>

                                <option value="Bluetick Coonhound" @if($pet->breed=='Bluetick Coonhound'){{'selected'}}@endif>Bluetick Coonhound</option>

                                <option value="Boerboel" @if($pet->breed=='Boerboel'){{'selected'}}@endif>Boerboel</option>

                                <option value="Bolognese" @if($pet->breed=='Bolognese'){{'selected'}}@endif>Bolognese</option>

                                <option value="Border Collie" @if($pet->breed=='Border Collie'){{'selected'}}@endif>Border Collie</option>

                                <option value="Border Terrier" @if($pet->breed=='Border Terrier'){{'selected'}}@endif>Border Terrier</option>

                                <option value="Borzoi" @if($pet->breed=='Borzoi'){{'selected'}}@endif>Borzoi</option>

                                <option value="Boston Terrier" @if($pet->breed=='Boston Terrier'){{'selected'}}@endif>Boston Terrier</option>

                                <option value="Bouvier des Flandres" @if($pet->breed=='Bouvier des Flandres'){{'selected'}}@endif>Bouvier des Flandres</option>

                                <option value="Boxer" @if($pet->breed=='Boxer'){{'selected'}}@endif>Boxer</option>

                                <option value="Boykin Spaniel" @if($pet->breed=='Boykin Spaniel'){{'selected'}}@endif>Boykin Spaniel</option>

                                <option value="Bracco Italiano" @if($pet->breed=='Bracco Italiano'){{'selected'}}@endif>Bracco Italiano</option>

                                <option value="Braque Du Bourbonnais" @if($pet->breed=='Braque Du Bourbonnais'){{'selected'}}@endif>Braque Du Bourbonnais</option>

                                <option value="Briard" @if($pet->breed=='Briard'){{'selected'}}@endif>Briard</option>

                                <option value="Brittany Spaniel" @if($pet->breed=='Brittany Spaniel'){{'selected'}}@endif>Brittany Spaniel</option>

                                <option value="Broholmer" @if($pet->breed=='Broholmer'){{'selected'}}@endif>Broholmer</option>

                                <option value="Brussels Griffon" @if($pet->breed=='Brussels Griffon'){{'selected'}}@endif>Brussels Griffon</option>

                                <option value="Bull Terrier" @if($pet->breed=='Bull Terrier'){{'selected'}}@endif>Bull Terrier</option>

                                <option value="Bulldog" @if($pet->breed=='Bulldog'){{'selected'}}@endif>Bulldog</option>

                                <option value="Bullmastiff" @if($pet->breed=='Bullmastiff'){{'selected'}}@endif>Bullmastiff</option>

                                <option value="Cairn Terrier" @if($pet->breed=='Cairn Terrier'){{'selected'}}@endif>Cairn Terrier</option>

                                <option value="Canaan Dog" @if($pet->breed=='Canaan Dog'){{'selected'}}@endif>Canaan Dog</option>

                                <option value="Cane Corso" @if($pet->breed=='Cane Corso'){{'selected'}}@endif>Cane Corso</option>

                                <option value="Cardigan Welsh Corgi" @if($pet->breed=='Cardigan Welsh Corgi'){{'selected'}}@endif>Cardigan Welsh Corgi</option>

                                <option value="Catahoula" @if($pet->breed=='Catahoula'){{'selected'}}@endif>Catahoula</option>

                                <option value="Caucasian Ovcharka" @if($pet->breed=='Caucasian Ovcharka'){{'selected'}}@endif>Caucasian Ovcharka</option>

                                <option value="Cavalier King Charles Spaniel" @if($pet->breed=='Cavalier King Charles Spaniel'){{'selected'}}@endif>Cavalier King Charles Spaniel</option>

                                <option value="Central Asian Shepherd Dog" @if($pet->breed=='Central Asian Shepherd Dog'){{'selected'}}@endif>Central Asian Shepherd Dog</option>

                                <option value="Cesky Terrier" @if($pet->breed=='Cesky Terrier'){{'selected'}}@endif>Cesky Terrier</option>

                                <option value="Chesapeake Bay Retriever" @if($pet->breed=='Chesapeake Bay Retriever'){{'selected'}}@endif>Chesapeake Bay Retriever</option>

                                <option value="Chihuahua" @if($pet->breed=='Chihuahua'){{'selected'}}@endif>Chihuahua</option>

                                <option value="Chinese Crested" @if($pet->breed=='Chinese Crested'){{'selected'}}@endif>Chinese Crested</option>

                                <option value="Chinese Shar-Pei" @if($pet->breed=='Chinese Shar-Pei'){{'selected'}}@endif>Chinese Shar-Pei</option>

                                <option value="Chinook" @if($pet->breed=='Chinook'){{'selected'}}@endif>Chinook</option>

                                <option value="Chow Chow" @if($pet->breed=='Chow Chow'){{'selected'}}@endif>Chow Chow</option>

                                <option value="Cirneco Dell'Etna" @if($pet->breed=="Cirneco Dell'Etna"){{'selected'}}@endif>Cirneco Dell'Etna</option>

                                <option value="Clumber Spaniel" @if($pet->breed=='Clumber Spaniel'){{'selected'}}@endif>Clumber Spaniel</option>

                                <option value="Cocker Spaniel" @if($pet->breed=='Cocker Spaniel'){{'selected'}}@endif>Cocker Spaniel</option>

                                <option value="Collie" @if($pet->breed=='Collie'){{'selected'}}@endif>Collie</option>

                                <option value="Coton de Tulear" @if($pet->breed=='Coton de Tulear'){{'selected'}}@endif>Coton de Tulear</option>

                                <option value="Curly-Coated Retriever" @if($pet->breed=='Curly-Coated Retriever'){{'selected'}}@endif>Curly-Coated Retriever</option>

                                <option value="Czechoslovakian Vlcak" @if($pet->breed=='Czechoslovakian Vlcak'){{'selected'}}@endif>Czechoslovakian Vlcak</option>

                                <option value="Dachshund" @if($pet->breed=='Dachshund'){{'selected'}}@endif>Dachshund</option>

                                <option value="Dalmatian" @if($pet->breed=='Dalmatian'){{'selected'}}@endif>Dalmatian</option>

                                <option value="Dandie Dinmont Terrier" @if($pet->breed=='Dandie Dinmont Terrier'){{'selected'}}@endif>Dandie Dinmont Terrier</option>

                                <option value="Danish-Swedish Farmdog" @if($pet->breed=='Danish-Swedish Farmdog'){{'selected'}}@endif>Danish-Swedish Farmdog</option>

                                <option value="Deutscher Wachtelhund" @if($pet->breed=='Deutscher Wachtelhund'){{'selected'}}@endif>Deutscher Wachtelhund</option>

                                <option value="Doberman Pinscher" @if($pet->breed=='Doberman Pinscher'){{'selected'}}@endif>Doberman Pinscher</option>

                                <option value="Dogo Argentino" @if($pet->breed=='Dogo Argentino'){{'selected'}}@endif>Dogo Argentino</option>

                                <option value="Dogue de Bordeaux" @if($pet->breed=='Dogue de Bordeaux'){{'selected'}}@endif>Dogue de Bordeaux</option>

                                <option value="Drentsche Patrijshond" @if($pet->breed=='Drentsche Patrijshond'){{'selected'}}@endif>Drentsche Patrijshond</option>

                                <option value="Drever" @if($pet->breed=='Drever'){{'selected'}}@endif>Drever</option>

                                <option value="Dutch Shepherd" @if($pet->breed=='Dutch Shepherd'){{'selected'}}@endif>Dutch Shepherd</option>

                                <option value="English Bulldog" @if($pet->breed=='English Bulldog'){{'selected'}}@endif>English Bulldog</option>

                                <option value="English Cocker Spaniel" @if($pet->breed=='English Cocker Spaniel'){{'selected'}}@endif>English Cocker Spaniel</option>

                                <option value="English Foxhound" @if($pet->breed=='English Foxhound'){{'selected'}}@endif>English Foxhound</option>

                                <option value="English Pointer" @if($pet->breed=='English Pointer'){{'selected'}}@endif>English Pointer</option>

                                <option value="English Setter" @if($pet->breed=='English Setter'){{'selected'}}@endif>English Setter</option>

                                <option value="English Shepherd" @if($pet->breed=='English Shepherd'){{'selected'}}@endif>English Shepherd</option>

                                <option value="English Springer Spaniel" @if($pet->breed=='English Springer Spaniel'){{'selected'}}@endif>English Springer Spaniel</option>

                                <option value="English Toy Spaniel" @if($pet->breed=='English Toy Spaniel'){{'selected'}}@endif>English Toy Spaniel</option>

                                <option value="Entlebucher Mountain Dog" @if($pet->breed=='Entlebucher Mountain Dog'){{'selected'}}@endif>Entlebucher Mountain Dog</option>

                                <option value="Estrela Mountain Dog" @if($pet->breed=='Estrela Mountain Dog'){{'selected'}}@endif>Estrela Mountain Dog</option>

                                <option value="Eurasier" @if($pet->breed=='Eurasier'){{'selected'}}@endif>Eurasier</option>

                                <option value="Field Spaniel" @if($pet->breed=='Field Spaniel'){{'selected'}}@endif>Field Spaniel</option>

                                <option value="Finnish Lapphund" @if($pet->breed=='Finnish Lapphund'){{'selected'}}@endif>Finnish Lapphund</option>

                                <option value="Finnish Spitz" @if($pet->breed=='Finnish Spitz'){{'selected'}}@endif>Finnish Spitz</option>

                                <option value="Flat-Coated Retriever" @if($pet->breed=='Flat-Coated Retriever'){{'selected'}}@endif>Flat-Coated Retriever</option>

                                <option value="French Bulldog" @if($pet->breed=='French Bulldog'){{'selected'}}@endif>French Bulldog</option>

                                <option value="French Spaniel" @if($pet->breed=='French Spaniel'){{'selected'}}@endif>French Spaniel</option>

                                <option value="German Longhaired Pointer" @if($pet->breed=='German Longhaired Pointer'){{'selected'}}@endif>German Longhaired Pointer</option>

                                <option value="German Pinscher" @if($pet->breed=='German Pinscher'){{'selected'}}@endif>German Pinscher</option>

                                <option value="German Shepherd" @if($pet->breed=='German Shepherd'){{'selected'}}@endif>German Shepherd</option>

                                <option value="German Shorthaired Pointer" @if($pet->breed=='German Shorthaired Pointer'){{'selected'}}@endif>German Shorthaired Pointer</option>

                                <option value="German Spitz" @if($pet->breed=='German Spitz'){{'selected'}}@endif>German Spitz</option>

                                <option value="German Wirehaired Pointer" @if($pet->breed=='German Wirehaired Pointer'){{'selected'}}@endif>German Wirehaired Pointer</option>

                                <option value="Giant Schnauzer" @if($pet->breed=='Giant Schnauzer'){{'selected'}}@endif>Giant Schnauzer</option>

                                <option value="Glen of Imaal Terrier" @if($pet->breed=='Glen of Imaal Terrier'){{'selected'}}@endif>Glen of Imaal Terrier</option>

                                <option value="Golden Retriever" @if($pet->breed=='Golden Retriever'){{'selected'}}@endif>Golden Retriever</option>

                                <option value="Gordon Setter" @if($pet->breed=='Gordon Setter'){{'selected'}}@endif>Gordon Setter</option>

                                <option value="Grand Basset Griffon Vendeen" @if($pet->breed=='Grand Basset Griffon Vendeen'){{'selected'}}@endif>Grand Basset Griffon Vendeen</option>

                                <option value="Great Dane" @if($pet->breed=='Great Dane'){{'selected'}}@endif>Great Dane</option>

                                <option value="Great Pyrenees" @if($pet->breed=='Great Pyrenees'){{'selected'}}@endif>Great Pyrenees</option>

                                <option value="Greater Swiss Mountain Dog" @if($pet->breed=='Greater Swiss Mountain Dog'){{'selected'}}@endif>Greater Swiss Mountain Dog</option>

                                <option value="Greyhound" @if($pet->breed=='Greyhound'){{'selected'}}@endif>Greyhound</option>

                                <option value="Hamiltonstovare" @if($pet->breed=='Hamiltonstovare'){{'selected'}}@endif>Hamiltonstovare</option>

                                <option value="Harrier" @if($pet->breed=='Harrier'){{'selected'}}@endif>Harrier</option>

                                <option value="Havanese" @if($pet->breed=='Havanese'){{'selected'}}@endif>Havanese</option>

                                <option value="Hound" @if($pet->breed=='Hound'){{'selected'}}@endif>Hound</option>

                                <option value="Hovawart" @if($pet->breed=='Hovawart'){{'selected'}}@endif>Hovawart</option>

                                <option value="Ibizan Hound" @if($pet->breed=='Ibizan Hound'){{'selected'}}@endif>Ibizan Hound</option>

                                <option value="Icelandic Sheepdog" @if($pet->breed=='Icelandic Sheepdog'){{'selected'}}@endif>Icelandic Sheepdog</option>

                                <option value="Irish Red and White Setter" @if($pet->breed=='Irish Red and White Setter'){{'selected'}}@endif>Irish Red and White Setter</option>

                                <option value="Irish Setter" @if($pet->breed=='Irish Setter'){{'selected'}}@endif>Irish Setter</option>

                                <option value="Irish Terrier" @if($pet->breed=='Irish Terrier'){{'selected'}}@endif>Irish Terrier</option>

                                <option value="Irish Water Spaniel" @if($pet->breed=='Irish Water Spaniel'){{'selected'}}@endif>Irish Water Spaniel</option>

                                <option value="Irish Wolfhound" @if($pet->breed=='Irish Wolfhound'){{'selected'}}@endif>Irish Wolfhound</option>

                                <option value="Italian Greyhound" @if($pet->breed=='Italian Greyhound'){{'selected'}}@endif>Italian Greyhound</option>

                                <option value="Jack Russell Terrier" @if($pet->breed=='Jack Russell Terrier'){{'selected'}}@endif>Jack Russell Terrier</option>

                                <option value="Jagdterrier" @if($pet->breed=='Jagdterrier'){{'selected'}}@endif>Jagdterrier</option>

                                <option value="Japanese Chin" @if($pet->breed=='Japanese Chin'){{'selected'}}@endif>Japanese Chin</option>

                                <option value="Kai Ken" @if($pet->breed=='Kai Ken'){{'selected'}}@endif>Kai Ken</option>

                                <option value="Keeshond" @if($pet->breed=='Keeshond'){{'selected'}}@endif>Keeshond</option>

                                <option value="Kerry Blue Terrier" @if($pet->breed=='Kerry Blue Terrier'){{'selected'}}@endif>Kerry Blue Terrier</option>

                                <option value="Kishu Ken" @if($pet->breed=='Kishu Ken'){{'selected'}}@endif>Kishu Ken</option>

                                <option value="Komondor" @if($pet->breed=='Komondor'){{'selected'}}@endif>Komondor</option>

                                <option value="Korean Jindo" @if($pet->breed=='Korean Jindo'){{'selected'}}@endif>Korean Jindo</option>

                                <option value="Kromfohrlander" @if($pet->breed=='Kromfohrlander'){{'selected'}}@endif>Kromfohrlander</option>

                                <option value="Kuvasz" @if($pet->breed=='Kuvasz'){{'selected'}}@endif>Kuvasz</option>

                                <option value="Labrador Retriever" @if($pet->breed=='Labrador Retriever'){{'selected'}}@endif>Labrador Retriever</option>

                                <option value="Lagotto Romagnolo" @if($pet->breed=='Lagotto Romagnolo'){{'selected'}}@endif>Lagotto Romagnolo</option>

                                <option value="Lakeland Terrier" @if($pet->breed=='Lakeland Terrier'){{'selected'}}@endif>Lakeland Terrier</option>

                                <option value="Lancashire Heeler" @if($pet->breed=='Lancashire Heeler'){{'selected'}}@endif>Lancashire Heeler</option>

                                <option value="Leonberger" @if($pet->breed=='Leonberger'){{'selected'}}@endif>Leonberger</option>

                                <option value="Lhasa Apso" @if($pet->breed=='Lhasa Apso'){{'selected'}}@endif>Lhasa Apso</option>

                                <option value="Löwchen" @if($pet->breed=='Löwchen'){{'selected'}}@endif>Löwchen</option>

                                <option value="Maltese" @if($pet->breed=='Maltese'){{'selected'}}@endif>Maltese</option>

                                <option value="Manchester Terrier" @if($pet->breed=='Manchester Terrier'){{'selected'}}@endif>Manchester Terrier</option>

                                <option value="Mastiff" @if($pet->breed=='Mastiff'){{'selected'}}@endif>Mastiff</option>

                                <option value="Miniature American Shepherd" @if($pet->breed=='Miniature American Shepherd'){{'selected'}}@endif>Miniature American Shepherd</option>

                                <option value="Miniature Bull Terrier" @if($pet->breed=='Miniature Bull Terrier'){{'selected'}}@endif>Miniature Bull Terrier</option>

                                <option value="Miniature Pinscher" @if($pet->breed=='Miniature Pinscher'){{'selected'}}@endif>Miniature Pinscher</option>

                                <option value="Miniature Schnauzer" @if($pet->breed=='Miniature Schnauzer'){{'selected'}}@endif>Miniature Schnauzer</option>

                                <option value="Mixed Breed" @if($pet->breed=='Mixed Breed'){{'selected'}}@endif>Mixed Breed</option>

                                <option value="Mudi" @if($pet->breed=='Mudi'){{'selected'}}@endif>Mudi</option>

                                <option value="Neapolitan Mastiff" @if($pet->breed=='Neapolitan Mastiff'){{'selected'}}@endif>Neapolitan Mastiff</option>

                                <option value="Nederlandse Kooikerhondje" @if($pet->breed=='Nederlandse Kooikerhondje'){{'selected'}}@endif>Nederlandse Kooikerhondje</option>

                                <option value="Newfoundland" @if($pet->breed=='Newfoundland'){{'selected'}}@endif>Newfoundland</option>

                                <option value="Norfolk Terrier" @if($pet->breed=='Norfolk Terrier'){{'selected'}}@endif>Norfolk Terrier</option>

                                <option value="Norrbottenspets" @if($pet->breed=='Norrbottenspets'){{'selected'}}@endif>Norrbottenspets</option>

                                <option value="Norwegian Buhund" @if($pet->breed=='Norwegian Buhund'){{'selected'}}@endif>Norwegian Buhund</option>

                                <option value="Norwegian Elkhound" @if($pet->breed=='Norwegian Elkhound'){{'selected'}}@endif>Norwegian Elkhound</option>

                                <option value="Norwegian Lundehund" @if($pet->breed=='Norwegian Lundehund'){{'selected'}}@endif>Norwegian Lundehund</option>

                                <option value="Norwich Terrier" @if($pet->breed=='Norwich Terrier'){{'selected'}}@endif>Norwich Terrier</option>

                                <option value="Nova Scotia Duck Tolling Retriever" @if($pet->breed=='Nova Scotia Duck Tolling Retriever'){{'selected'}}@endif>Nova Scotia Duck Tolling Retriever</option>

                                <option value="Old English Sheepdog" @if($pet->breed=='Old English Sheepdog'){{'selected'}}@endif>Old English Sheepdog</option>

                                <option value="Otterhound" @if($pet->breed=='Otterhound'){{'selected'}}@endif>Otterhound</option>

                                <option value="Papillon" @if($pet->breed=='Papillon'){{'selected'}}@endif>Papillon</option>

                                <option value="Parson Russell Terrier" @if($pet->breed=='Parson Russell Terrier'){{'selected'}}@endif>Parson Russell Terrier</option>

                                <option value="Patterdale Terrier" @if($pet->breed=='Patterdale Terrier'){{'selected'}}@endif>Patterdale Terrier</option>

                                <option value="Pekingese" @if($pet->breed=='Pekingese'){{'selected'}}@endif>Pekingese</option>

                                <option value="Pembroke Welsh Corgi" @if($pet->breed=='Pembroke Welsh Corgi'){{'selected'}}@endif>Pembroke Welsh Corgi</option>

                                <option value="Perro De Presa Canario" @if($pet->breed=='Perro De Presa Canario'){{'selected'}}@endif>Perro De Presa Canario</option>

                                <option value="Peruvian Inca Orchid" @if($pet->breed=='Peruvian Inca Orchid'){{'selected'}}@endif>Peruvian Inca Orchid</option>

                                <option value="Petit Basset Griffon Vendéen" @if($pet->breed=='Petit Basset Griffon Vendéen'){{'selected'}}@endif>Petit Basset Griffon Vendéen</option>

                                <option value="Pharaoh Hound" @if($pet->breed=='Pharaoh Hound'){{'selected'}}@endif>Pharaoh Hound</option>

                                <option value="Pit Bull" @if($pet->breed=='Pit Bull'){{'selected'}}@endif>Pit Bull</option>

                                <option value="Plott" @if($pet->breed=='Plott'){{'selected'}}@endif>Plott</option>

                                <option value="Plott" @if($pet->breed=='Plott'){{'selected'}}@endif>Plott</option>

                                <option value="Pointer" @if($pet->breed=='Pointer'){{'selected'}}@endif>Pointer</option>

                                <option value="Polish Lowland Sheepdog" @if($pet->breed=='Polish Lowland Sheepdog'){{'selected'}}@endif>Polish Lowland Sheepdog</option>

                                <option value="Pomeranian" @if($pet->breed=='Pomeranian'){{'selected'}}@endif>Pomeranian</option>

                                <option value="Poodle" @if($pet->breed=='Poodle'){{'selected'}}@endif>Poodle</option>

                                <option value="Portuguese Podengo" @if($pet->breed=='Portuguese Podengo'){{'selected'}}@endif>Portuguese Podengo</option>

                                <option value="Portuguese Podengo Pequeno" @if($pet->breed=='Portuguese Podengo Pequeno'){{'selected'}}@endif>Portuguese Podengo Pequeno</option>

                                <option value="Portuguese Pointer" @if($pet->breed=='Portuguese Pointer'){{'selected'}}@endif>Portuguese Pointer</option>

                                <option value="Portuguese Sheepdog" @if($pet->breed=='Portuguese Sheepdog'){{'selected'}}@endif>Portuguese Sheepdog</option>

                                <option value="Portuguese Water Dog" @if($pet->breed=='Portuguese Water Dog'){{'selected'}}@endif>Portuguese Water Dog</option>

                                <option value="Pug" @if($pet->breed=='Pug'){{'selected'}}@endif>Pug</option>

                                <option value="Puli" @if($pet->breed=='Puli'){{'selected'}}@endif>Puli</option>

                                <option value="Puli" @if($pet->breed=='Puli'){{'selected'}}@endif>Puli</option>

                                <option value="Pumi" @if($pet->breed=='Pumi'){{'selected'}}@endif>Pumi</option>

                                <option value="Pyrenean Mastiff" @if($pet->breed=='Pyrenean Mastiff'){{'selected'}}@endif>Pyrenean Mastiff</option>

                                <option value="Pyrenean Shepherd" @if($pet->breed=='Pyrenean Shepherd'){{'selected'}}@endif>Pyrenean Shepherd</option>

                                <option value="Queensland Heeler" @if($pet->breed=='Queensland Heeler'){{'selected'}}@endif>Queensland Heeler</option>

                                <option value="Rafeiro Do Alentejo" @if($pet->breed=='Rafeiro Do Alentejo'){{'selected'}}@endif>Rafeiro Do Alentejo</option>

                                <option value="Rat Terrier" @if($pet->breed=='Rat Terrier'){{'selected'}}@endif>Rat Terrier</option>

                                <option value="Redbone Coonhound" @if($pet->breed=='Redbone Coonhound'){{'selected'}}@endif>Redbone Coonhound</option>

                                <option value="Rhodesian Ridgeback" @if($pet->breed=='Rhodesian Ridgeback'){{'selected'}}@endif>Rhodesian Ridgeback</option>

                                <option value="Rottweiler" @if($pet->breed=='Rottweiler'){{'selected'}}@endif>Rottweiler</option>

                                <option value="Running Walker Foxhound" @if($pet->breed=='Running Walker Foxhound'){{'selected'}}@endif>Running Walker Foxhound</option>

                                <option value="Russian Toy" @if($pet->breed=='Russian Toy'){{'selected'}}@endif>Russian Toy</option>

                                <option value="Russian Tsvetnaya Bolonka" @if($pet->breed=='Russian Tsvetnaya Bolonka'){{'selected'}}@endif>Russian Tsvetnaya Bolonka</option>

                                <option value="Saint Bernard" @if($pet->breed=='Saint Bernard'){{'selected'}}@endif>Saint Bernard</option>

                                <option value="Saluki" @if($pet->breed=='Saluki'){{'selected'}}@endif>Saluki</option>

                                <option value="Samoyed" @if($pet->breed=='Samoyed'){{'selected'}}@endif>Samoyed</option>

                                <option value="Schapendoes" @if($pet->breed=='Schapendoes'){{'selected'}}@endif>Schapendoes</option>

                                <option value="Schipperke" @if($pet->breed=='Schipperke'){{'selected'}}@endif>Schipperke</option>

                                <option value="Schnauzer" @if($pet->breed=='Schnauzer'){{'selected'}}@endif>Schnauzer</option>

                                <option value="Scottish Deerhound" @if($pet->breed=='Scottish Deerhound'){{'selected'}}@endif>Scottish Deerhound</option>

                                <option value="Scottish Terrier" @if($pet->breed=='Scottish Terrier'){{'selected'}}@endif>Scottish Terrier</option>

                                <option value="Sealyham Terrier" @if($pet->breed=='Sealyham Terrier'){{'selected'}}@endif>Sealyham Terrier</option>

                                <option value="Shar-Pei" @if($pet->breed=='Shar-Pei'){{'selected'}}@endif>Shar-Pei</option>

                                <option value="Shetland Sheepdog" @if($pet->breed=='Shetland Sheepdog'){{'selected'}}@endif>Shetland Sheepdog</option>

                                <option value="Shiba Inu" @if($pet->breed=='Shiba Inu'){{'selected'}}@endif>Shiba Inu</option>

                                <option value="Shih Tzu" @if($pet->breed=='Shih Tzu'){{'selected'}}@endif>Shih Tzu</option>

                                <option value="Shikoku" @if($pet->breed=='Shikoku'){{'selected'}}@endif>Shikoku</option>

                                <option value="Siberian Husky" @if($pet->breed=='Siberian Husky'){{'selected'}}@endif>Siberian Husky</option>

                                <option value="Silky Terrie" @if($pet->breed=='Silky Terrie'){{'selected'}}@endif>Silky Terrier</option>

                                <option value="Skye Terrier" @if($pet->breed=='Skye Terrier'){{'selected'}}@endif>Skye Terrier</option>

                                <option value="Sloughi" @if($pet->breed=='Sloughi'){{'selected'}}@endif>Sloughi</option>

                                <option value="Slovensky Cuvac" @if($pet->breed=='Slovensky Cuvac'){{'selected'}}@endif>Slovensky Cuvac</option>

                                <option value="Slovensky Kopov" @if($pet->breed=='Slovensky Kopov'){{'selected'}}@endif>Slovensky Kopov</option>

                                <option value="Small Munsterlander Pointer" @if($pet->breed=='Small Munsterlander Pointer'){{'selected'}}@endif>Small Munsterlander Pointer</option>

                                <option value="Smooth Fox Terrier" @if($pet->breed=='Smooth Fox Terrier'){{'selected'}}@endif>Smooth Fox Terrier</option>

                                <option value="Soft Coated Wheaten Terrier" @if($pet->breed=='Soft Coated Wheaten Terrier'){{'selected'}}@endif>Soft Coated Wheaten Terrier</option>

                                <option value="Spanish Mastiff" @if($pet->breed=='Spanish Mastiff'){{'selected'}}@endif>Spanish Mastiff</option>

                                <option value="Spanish Water Dog" @if($pet->breed=='Spanish Water Dog'){{'selected'}}@endif>Spanish Water Dog</option>

                                <option value="Spinone Italiano" @if($pet->breed=='Spinone Italiano'){{'selected'}}@endif>Spinone Italiano</option>

                                <option value="Stabyhoun" @if($pet->breed=='Stabyhoun'){{'selected'}}@endif>Stabyhoun</option>

                                <option value="Staffordshire Bull Terrier" @if($pet->breed=='Staffordshire Bull Terrier'){{'selected'}}@endif>Staffordshire Bull Terrier</option>

                                <option value="Sussex Spaniel" @if($pet->breed=='Sussex Spaniel'){{'selected'}}@endif>Sussex Spaniel</option>

                                <option value="Swedish Lapphund" @if($pet->breed=='Swedish Lapphund'){{'selected'}}@endif>Swedish Lapphund</option>

                                <option value="Swedish Vallhund" @if($pet->breed=='Swedish Vallhund'){{'selected'}}@endif>Swedish Vallhund</option>

                                <option value="Tamaskan" @if($pet->breed=='Tamaskan'){{'selected'}}@endif>Tamaskan</option>

                                <option value="Terrier Mix" @if($pet->breed=='Terrier Mix'){{'selected'}}@endif>Terrier Mix</option>

                                <option value="Thai Ridgeback" @if($pet->breed=='Thai Ridgeback'){{'selected'}}@endif>Thai Ridgeback</option>

                                <option value="Tibetan Mastiff" @if($pet->breed=='Tibetan Mastiff'){{'selected'}}@endif>Tibetan Mastiff</option>

                                <option value="Tibetan Spaniel" @if($pet->breed=='Tibetan Spaniel'){{'selected'}}@endif>Tibetan Spaniel</option>

                                <option value="Tibetan Terrier" @if($pet->breed=='Tibetan Terrier'){{'selected'}}@endif>Tibetan Terrier</option>

                                <option value="Tornjak" @if($pet->breed=='Tornjak'){{'selected'}}@endif>Tornjak</option>

                                <option value="Tosa" @if($pet->breed=='Tosa'){{'selected'}}@endif>Tosa</option>

                                <option value="Toy Fox Terrier" @if($pet->breed=='Toy Fox Terrier'){{'selected'}}@endif>Toy Fox Terrier</option>

                                <option value="Transylvanian Hound" @if($pet->breed=='Transylvanian Hound'){{'selected'}}@endif>Transylvanian Hound</option>

                                <option value="Treeing Walker Brindle" @if($pet->breed=='Treeing Walker Brindle'){{'selected'}}@endif>Treeing Walker Brindle</option>

                                <option value="Treeing Walker Coonhound" @if($pet->breed=='Treeing Walker Coonhound'){{'selected'}}@endif>Treeing Walker Coonhound</option>

                                <option value="Unknown" @if($pet->breed=='Unknown'){{'selected'}}@endif>Unknown</option>

                                <option value="Vizsla" @if($pet->breed=='Vizsla'){{'selected'}}@endif>Vizsla</option>

                                <option value="Weimaraner" @if($pet->breed=='Weimaraner'){{'selected'}}@endif>Weimaraner</option>

                                <option value="Welsh Springer Spaniel" @if($pet->breed=='Welsh Springer Spaniel'){{'selected'}}@endif>Welsh Springer Spaniel</option>

                                <option value="Welsh Terrier" @if($pet->breed=='Welsh Terrier'){{'selected'}}@endif>Welsh Terrier</option>

                                <option value="West Highland White Terrier" @if($pet->breed=='West Highland White Terrier'){{'selected'}}@endif>West Highland White Terrier</option>

                                <option value="Whippet" @if($pet->breed=='Whippet'){{'selected'}}@endif>Whippet</option>

                                <option value="Wire Fox Terrier" @if($pet->breed=='Wire Fox Terrier'){{'selected'}}@endif>Wire Fox Terrier</option>

                                <option value="Wirehaired Pointing Griffon" @if($pet->breed=='Wirehaired Pointing Griffon'){{'selected'}}@endif>Wirehaired Pointing Griffon</option>

                                <option value="Wolfdog" @if($pet->breed=='Wolfdog'){{'selected'}}@endif>Wolfdog</option>

                                <option value="Working Kelpie" @if($pet->breed=='Working Kelpie'){{'selected'}}@endif>Working Kelpie</option>

                                <option value="Xoloitzcuintli" @if($pet->breed=='Xoloitzcuintli'){{'selected'}}@endif>Xoloitzcuintli</option>

                                <option value="Yorkshire Terrier" @if($pet->breed=='Yorkshire Terrier'){{'selected'}}@endif>Yorkshire Terrier</option>
                            </select>
                        </fieldset>




                        <fieldset class="form-group">
                            <label class="form-label semibold" for="date">
                                Date
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="lost_found_date" value="{{$pet->date}}" id="date" placeholder="Date" >
                        </fieldset>


                        <fieldset class="form-group">
                            <label class="form-label semibold" for="color">
                                Color
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="color" value="{{$pet->color}}" id="color" placeholder="Color" >
                        </fieldset>


                        <fieldset class="form-group">
                            <label class="form-label semibold" for="description">
                                Description
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="description" value="{{$pet->description}}" id="description" placeholder="Description" >
                        </fieldset>


                        <fieldset class="form-group">
                            <label class="form-label semibold" for="last_seen">
                                Last Seen
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="address" value="{{$pet->last_seen}}" id="reportlostaddress" placeholder="Last Seen" >
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="form-label semibold" for="street">
                                Cross Street
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="street" value="{{$pet->street}}" id="street" placeholder="Cross Street" >
                        </fieldset>
                        
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="postal">
                                Postal
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="postal" value="{{$pet->postal}}" id="postal_2" placeholder="Postal" >
                        </fieldset>





                        <fieldset class="form-group">
                            <label class="form-label semibold" for="circumstance">
                                Circumstance

                            </label>
                            <input type="text" class="form-control" name="circumstance" value="{{$pet->circumstance}}" id="circumstance" placeholder="Circumstance" >
                        </fieldset>



                        <fieldset class="form-group">
                            <label class="form-label semibold" for="circumstance">
                                Seo Meta Title

                            </label>
                            <input type="text" class="form-control" name="meta_title" value="{{$pet->meta_title}}" id="circumstance" placeholder="Circumstance" >
                        </fieldset>

                        <fieldset class="form-group">
                            <label class="form-label semibold" for="circumstance">
                                Seo Meta Description

                            </label>
                            <input type="text" class="form-control" name="meta_description" value="{{$pet->meta_description}}" id="circumstance" placeholder="Circumstance" >
                        </fieldset>


                        <fieldset class="form-group">
                            <label class="form-label semibold" for="circumstance">
                                Seo Meta Keyword

                            </label>
                            <input type="text" class="form-control" name="meta_keywords" value="{{$pet->meta_keywords}}" id="circumstance" placeholder="Circumstance" >
                        </fieldset>



                    </div>
                </div><!--.row-->
                <div class="row">
                    <div class="col-lg-12">
                        <a href="{{ url('admin/pets/edit') }}/{{ $pet->id }}" class="btn btn-inline btn-secondary pull-left">
                            <span class="btn-icon">
                                <i class="fa fa-refresh"></i>
                            </span>
                            Refresh
                        </a>
                        <button type="submit" class="btn btn-inline btn-success pull-right">
                            <span class="btn-icon">
                                <i class="fa fa-edit"></i>
                            </span>
                            Update
                        </button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div><!--.box-typical-->
    </div><!--.container-fluid-->
</div><!--.page-content-->
@endsection
