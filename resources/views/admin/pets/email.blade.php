@extends('admin.layout.admin')

@section('content')
@push('css-head')

@endpush
<div class="page-content">
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3> Email Send</h3>
                        <ol class="breadcrumb breadcrumb-simple">
                            <li>
                                <a href="{{ url('admin/pets') }}">Pet List</a>
                            </li>
                            <li class="active">Create New</li>
                        </ol>
                    </div>
                </div>
            </div>
        </header>

        <div class="box-typical box-typical-padding">
            @if (count($errors) > 0)
                <div class="alert alert-danger alert-no-border alert-txt-colored alert-close alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                </div>
            @endif
            {!!Form::open(array('route'=>'admin_pets_emailstore','method'=>'POST', 'enctype'=>'multipart/form-data'))!!}
                <div class="row">

                    <input type="hidden" value="{{ $pet->id }}" name="pet_id">
                    <input type="hidden" value="{{ $pet->user_id }}" name="user_id">

                    <div class="col-lg-12">
                        <!--<fieldset class="form-group">-->
                        <!--    <label class="form-label semibold" for="name">-->
                        <!--        Join Our Network-->
                        <!--        <span class="color-red">*</span>-->
                        <!--    </label>-->
                        <!--    <input type="text" class="form-control" name="shareonfacebook" id="name" placeholder="Name" required="" value="{{$pet->shareonfacebook}}">-->
                        <!--</fieldset>-->

                        <fieldset class="form-group">
                            <label class="form-label semibold" for="name">
                                View Facebook Post
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="viewfacebookpost" id="name" placeholder="Name" required="" value="{{$pet->viewfacebookpost}}">
                        </fieldset>
                        @php
						if($pet->name)	{
							$petName = $pet->name; 
                        } else { 
                            $petName='This Pet'; 
                        } 
                        if($pet->type=='lost'){
                            $dateType='Lost';
                            $type='LOST'; 
                            $contact="$petName's owner"; 
                        } else {
                            $dateType='Found'; 
                            $type='FOUND'; 
                            $contact='the finder'; 
                        }
						@endphp
						<textarea style="width:100%;height:500px">
Let your nearby people know by SHARE/COMMENT/LIKE {{ $pet->type=='lost' ? strtoupper($petName) : 'this pet' }} was {{ strtoupper($pet->type) }} on {{ $pet->date }} in {{$pet->last_seen}}

➤ Pet Description: 
{{ $pet->description }}

➤ Information from the {{ $pet->type=='lost' ? 'Owner' : 'Finder' }}:
Area last seen: {{ $pet->last_seen }}
Cross street: {{ $pet->street_address }}
Species: {{ $pet->specie }}
Color: {{ $pet->color }}
Gender: {{ $pet->gender }}
Breed: {{ $pet->breed }}
{{$dateType}} date: {{ $pet->date }}

➤ To contact {{$contact}}, click on the link:
https://www.pawmaw.com/pet-details/{{ $pet->slug }}

➤ Lost or found a pet? Please report it here:
https://www.pawmaw.com/report/
</textarea>
                        
                    </div>
                </div><!--.row-->
                <div class="row">
                    <div class="col-lg-12">
                        <a href="{{ url('admin/pets/email') }}/{{ $pet->id }}" class="btn btn-inline btn-secondary pull-left">
                            <span class="btn-icon">
                                <i class="fa fa-refresh"></i>
                            </span>
                            Refresh
                        </a>
                        <button type="submit" class="btn btn-inline btn-success pull-right">
                            <span class="btn-icon">
                                <i class="fa fa-save"></i>
                            </span>
                            Submit
                        </button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div><!--.box-typical-->
    </div><!--.container-fluid-->
</div><!--.page-content-->
@endsection