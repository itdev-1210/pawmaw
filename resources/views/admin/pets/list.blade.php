@extends('admin.layout.admin')

@section('content')
@push('css-head')
    <link rel="stylesheet" href="{{ asset('/assets/css/lib/datatables-net/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/datatables-net.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/custom/switch/rcswitcher.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/pnotify.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/lib/bootstrap-sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/sweet-alert-animations.min.css') }}">
@endpush
<div class="page-content">
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h2>Pets Setup</h2>
                        <div class="subtitle">Change status for approval.</div>
                    </div>
                    <div class="tbl-cell">
                        <a href="{{ url('admin/pets/create') }}" class="btn btn-inline btn-secondary pull-right">
                            <span class="btn-icon"> 
                                <i class="fa fa-plus"></i>
                            </span>
                            Create new
                        </a>
                    </div>
                </div>
            </div>
        </header>
        <section class="card">
            <div class="search-form" style="margin: 20px 0; margin-left: 15px; background: #fff;">
                @csrf
                <form action="/admin/pets">
                    <input 
                        type="text" 
                        placeholder="Pet ID | Name | Email"
                        name="search"
                        value="{{ @$_GET['search'] }}"
                        style="border: solid 1px rgba(197,214,222,.7); font-size: 16px; color: black; border-radius: 3px; height: 33px; padding: 0 10px">
                    <button style="background: #45a31f; border: 0; outline: 0; color: #fff; border-radius: 3px; height: 32px; padding: 0 10px;">Search</button>
                </form>
            </div>

            <div class="card-block">
                <table class="display table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Pet ID</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pets as $key => $row)
                        @php 
                          $package = json_decode($row->package);
                          $packageActive = false;
                          if ($package) {
                              $daysPassed = (time() - $package->updated_at) / 3600 / 24;
                              $packageActive = $package && ($package->name == 'basic' && $daysPassed < 3) || ($package->name == 'standard' && $daysPassed < 7) || ($package->name == 'premium' && $daysPassed < 10);
                          }
                        @endphp
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $row->id }}</td>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->type }}</td>
                            <td>{{ $row->contact_email }}</td>
                            <td style="text-align: center;">
                                <input type="checkbox" name="status{{ $row->id }}" @if($row->status == 1) {{ 'checked' }} @endif data-id="{{ $row->id }}">
                            </td>
                            <td style="text-align: center;width:380px">
                                <select class="package_name" pet_id="{{ $row->id }}">
                                    <option value="">Select package</option>
                                    @foreach(array('basic', 'standard', 'premium') as $packageName)
                                    <option value="{{ $packageName }}" @if ($packageActive && $package->name == $packageName)selected @endif>{{ strtoupper($packageName) }}</option>
                                    @endforeach
                                </select> 
                                <span data-link="{{ url('admin/pets/view') }}/{{ $row->id }}" class="btn btn-sm btn-inline btn-primary modal_view" data-toggle="tooltip" title="View" data-placement="top">
                                    <span class="fa fa-eye"></span>
                                </span>
                                <a href="{{ url('admin/pets/edit') }}/{{ $row->id }}" class="btn btn-sm btn-inline btn-warning" data-toggle="tooltip" title="Edit">
                                    <span class="fa fa-edit"></span>
                                </a>

                                <a href="{{ url('admin/pets/email') }}/{{ $row->id }}" class="btn btn-sm btn-inline btn-warning" data-toggle="tooltip" title="Email">
                                    <span class="fa fa-envelope"></span>
                                </a>
                                {!! Form::model($pets, ['method' => 'delete', 'route' => ['admin_pets_delete', $row->id], 'class' =>'btn btn-sm btn-inline btn-danger form-delete', 'data-toggle' => 'tooltip','title' => 'Delete']) !!}
                                {!! Form::hidden('id', $row->id) !!}
                                    <span class="fa fa-trash delete"></span>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $pets->links() }}
            </div>
        </section>
    </div><!--.container-fluid-->
</div><!--.page-content-->

<div class="control-panel-container">
    <ul>
        <li class="add">
            <div class="control-item-header">
                <a href="#" class="icon-toggle no-caret">
                    <span class="icon fa fa-plus"></span>
                </a>
            </div>
        </li>
    </ul>
    <a class="control-panel-toggle">
        <span class="fa fa-angle-double-left"></span>
    </a>
</div>

@push('scripts')
    <script src="{{ asset('/assets/js/lib/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('/assets/custom/switch/rcswitcher.js') }}"></script>
    <script src="{{ asset('/assets/js/lib/datatables-net/datatables.min.js') }}"></script>
    <script src="{{ asset('/assets/js/lib/pnotify/pnotify.js') }}"></script>
    <script src="{{ asset('/assets/custom/js/notify.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
            $("input[type='checkbox']").rcSwitcher();
            $('.table').DataTable();
        });
        $('.modal_view').on('click', function(){
            var link = $(this).data('link');
            modal_call('Pet Details',link); //modal_call(title,route);
        });

        $("input[type='checkbox']").rcSwitcher().on({'toggle.rcSwitcher': function( e, dataObj, changeType ){
                var id = $(this).data('id');
                var urll="{{ url('admin/pets/status') }}/"+id;
                var msg_success = 'Status Updated Successfully.';
                var msg_error = 'Something went wrong!';
                $.ajax({
                    url:urll,
                    success: function(data){
                       notify('success',msg_success);
                    },
                    error: function (data) {
                        notify('error',msg_error);
                    }
                });
            },
        });

        $("select.package_name").change(function() {
        	if (confirm("Are you sure?")) {
        		var msg_success = 'Package Updated Successfully.';
                var msg_error = 'Something went wrong!';
            	var urll="{{ url('admin/pets/package/') }}/"+ $(this).attr('pet_id') + '/' + (this.value?this.value:0);
            	$.ajax({
                    url:urll,
                    success: function(data){
                       notify('success',msg_success);
                    },
                    error: function (data) {
                        notify('error',msg_error);
                    }
                });
        	}
        });
        
    </script>

    <script type="text/javascript">
        $('.form-delete').click(function(e){
            e.preventDefault();
            $form = $(this);
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            });
            $('button.confirm').on('click',function(){
                $form.submit();
            });
        });
    </script>
    @if ($message = Session::get('success'))
    <script type="text/javascript">
        $(document).ready(function(){
            var msg = "{{ $message }}";
            notify('success',msg);
        });
    </script>
    @endif
    @include('admin.elements.info_modal')

    <style>
        table td, table th{
            padding: 0 10px;
        }
    </style>

@endpush
@endsection