<div class="tbl">



    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Pet Id:</div>
        <div class="tbl-cell">{{ $payment->pet_id }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">User Id:</div>
        <div class="tbl-cell">{{ $payment->user_id }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">LI O Name:</div>
        <div class="tbl-cell">{{ $payment->li_0_name }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Sid:</div>
        <div class="tbl-cell">{{ $payment->sid }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Key:</div>
        <div class="tbl-cell">{{ $payment->key }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">State:</div>
        <div class="tbl-cell">{{ $payment->state }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Email:</div>
        <div class="tbl-cell">{{ $payment->email }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Li O Type:</div>
        <div class="tbl-cell">{{ $payment->li_0_type }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Order Number:</div>
        <div class="tbl-cell">{{ $payment->order_number }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Currency Code:</div>
        <div class="tbl-cell">{{ $payment->currency_code }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Lang:</div>
        <div class="tbl-cell">{{ $payment->lang }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Invoice Id:</div>
        <div class="tbl-cell">{{ $payment->invoice_id }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Li O Price:</div>
        <div class="tbl-cell">{{ $payment->li_0_price }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Total:</div>
        <div class="tbl-cell">{{ $payment->total }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Credit Card Processed:</div>
        <div class="tbl-cell">{{ $payment->credit_card_processed }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Zip:</div>
        <div class="tbl-cell">{{ $payment->zip }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Li O Quantity:</div>
        <div class="tbl-cell">{{ $payment->li_0_quantity }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Li O Description:</div>
        <div class="tbl-cell">{{ $payment->li_0__description }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Cart Weight:</div>
        <div class="tbl-cell">{{ $payment->cart_weight }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Fixed:</div>
        <div class="tbl-cell">{{ $payment->fixed }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Last Name:</div>
        <div class="tbl-cell">{{ $payment->last_name }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Li O Product Id:</div>
        <div class="tbl-cell">{{ $payment->li_0_product_id }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Street Address:</div>
        <div class="tbl-cell">{{ $payment->street_address }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">City:</div>
        <div class="tbl-cell">{{ $payment->city }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Li O Tangible:</div>
        <div class="tbl-cell">{{ $payment->li_0_tangible }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Li O Description:</div>
        <div class="tbl-cell">{{ $payment->li_0_description }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Merchant Order Id:</div>
        <div class="tbl-cell">{{ $payment->merchant_order_id }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Country:</div>
        <div class="tbl-cell">{{ $payment->country }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Ip Country:</div>
        <div class="tbl-cell">{{ $payment->ip_country }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Demo:</div>
        <div class="tbl-cell">{{ $payment->demo }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Pay Method:</div>
        <div class="tbl-cell">{{ $payment->pay_method }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Cart Tangible:</div>
        <div class="tbl-cell">{{ $payment->cart_tangible }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Phone:</div>
        <div class="tbl-cell">{{ $payment->phone }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Street Address2:</div>
        <div class="tbl-cell">{{ $payment->street_address2 }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">First Name:</div>
        <div class="tbl-cell">{{ $payment->first_name }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Card Holder Name:</div>
        <div class="tbl-cell">{{ $payment->card_holder_name }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Middle Initial:</div>
        <div class="tbl-cell">{{ $payment->middle_initial }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Created At:</div>
        <div class="tbl-cell">{{ $payment->lang }}</div>
    </div>

    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Updated At:</div>
        <div class="tbl-cell">{{ $payment->updated_at }}</div>
    </div>








</div>


