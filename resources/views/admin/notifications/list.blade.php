@extends('admin.layout.admin')

@section('content')
@push('css-head')
    <link rel="stylesheet" href="{{ asset('/assets/css/lib/datatables-net/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/datatables-net.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
@endpush
<div class="page-content">
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h2>Notifications</h2>
                    </div>
                    <div class="tbl-cell">
                        @if($settings->enable_notifications)
                            <form method="POST" action="{{ url('/admin/notifications/toggle-setting/0') }}">
                                @csrf
                                <button style="float:right" class="btn btn-sm btn-danger">Disable Notifications</button>
                            </form>
                        @else
                            <form method="POST" action="{{ url('/admin/notifications/toggle-setting/1') }}">
                                @csrf
                                <button style="float:right" class="btn btn-sm btn-success">Enable Notifications</button>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </header>
        <section class="card">
            <div class="card-block">
                <form method="POST" id="new_entry" enctype="multipart/form-data" action="{{url('/admin/notifications/create')}}">
                    @csrf
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" maxlength="45" name="name" class="form-control" required />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Image</label>
                                <input type="file" maxlength="45" name="image" class="form-control" required />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Type</label>
                                <select name="lost_or_found" class="form-control">
                                    <option value="0">Lost</option>
                                    <option value="1">Found</option>
                                    <option value="2">Reunited</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>&nbsp;</label>
                            <button class="btn btn-sm btn-primary" style="float:right">Submit</button>
                        </div>
                    </div>
                </form>
                <table class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width="30">#</th>
                            <th>Name</th>
                            <th class="text-center">Image</th>
                            <th class="text-center">Lost/Found</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($notifications))
                            @foreach ($notifications as $key => $item)
                                <tr>
                                    <td class="text-center">{{$key + 1}}</td>
                                    <td>{{$item->name}}</td>
                                    <td class="text-center"><img style="width: 60px; height:60px; border-radius: 200px" src="{{url('/uploads/'.$item->image)}}" /></td>
                                    <td class="text-center">
                                        @switch($item->lost_or_found)
                                            @case(0)
                                                Lost
                                                @break
                                            @case(1)
                                                Found
                                                @break
                                            @case(2)
                                                Reunited
                                                @break
                                            @default
                                                
                                        @endswitch
                                    </td>
                                    <td class="text-center">
                                        <form method="POST" action="{{ url('/admin/notifications/delete/'.$item->id) }}">
                                            @csrf
                                            <button class="btn btn-sm btn-danger delete-item"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                        <tr class="text-center">
                            <td colspan="5">No Data</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </section>
    </div><!--.container-fluid-->
</div><!--.page-content-->
@push('scripts')
    <script src="{{ asset('/assets/js/lib/datatables-net/datatables.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.table').DataTable();
        });
        $('.delete-item').on('click', function(e){
            e.preventDefault();
            var form = $(this).parent();
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $(form).submit();
                }
            })
        })
    </script>
@endpush
@endsection