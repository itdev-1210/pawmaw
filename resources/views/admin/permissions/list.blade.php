@extends('admin.layout.admin')

@section('content')
@push('css-head')
    <link rel="stylesheet" href="{{ asset('assets/css/lib/datatables-net/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/datatables-net.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/custom/switch/rcswitcher.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/pnotify.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/lib/bootstrap-sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/sweet-alert-animations.min.css') }}">
@endpush
<div class="page-content">
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h2>Permission</h2>
                        <div class="subtitle"></div>
                    </div>
                    <div class="tbl-cell">
                        <a href="{{ url('admin/permissions/create') }}" class="btn btn-inline btn-secondary pull-right">
                            <span class="btn-icon"> 
                                <i class="fa fa-plus"></i>
                            </span>
                            Create new
                        </a>
                    </div>
                </div>
            </div>
        </header>
        <section class="card">
            <div class="card-block">
                <table class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($permissions as $key => $row)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $row->name }}</td>
                            <td style="text-align: center;">
                                <span data-link="{{ url('admin/permissions/view') }}/{{ $row->id }}" class="btn btn-sm btn-inline btn-primary modal_view" data-toggle="tooltip" title="View" data-placement="top">
                                    <span class="fa fa-eye"></span>
                                </span>
                                <a href="{{ url('admin/permissions/edit') }}/{{ $row->id }}" class="btn btn-sm btn-inline btn-warning" data-toggle="tooltip" title="Edit">
                                    <span class="fa fa-edit"></span>
                                </a>
                                {!! Form::model($permissions, ['method' => 'delete', 'route' => ['permissions_delete', $row->id], 'class' =>'btn btn-sm btn-inline btn-danger form-delete', 'data-toggle' => 'tooltip','title' => 'Delete']) !!}
                                {!! Form::hidden('id', $row->id) !!}
                                    <span class="fa fa-trash delete"></span>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div><!--.container-fluid-->
</div><!--.page-content-->


@push('scripts')
    <script src="{{ asset('assets/js/lib/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/custom/switch/rcswitcher.js') }}"></script>
    <script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/pnotify/pnotify.js') }}"></script>
    <script src="{{ asset('assets/custom/js/notify.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
            $("input[type='checkbox']").rcSwitcher();
            $('.table').DataTable();
        });
        $('.modal_view').on('click', function(){
            var link = $(this).data('link');
            modal_call('Permission Details',link); //modal_call(title,route);
        });
    </script>

    <script type="text/javascript">
        $('.form-delete').click(function(e){
            e.preventDefault();
            $form = $(this);
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            });
            $('button.confirm').on('click',function(){
                $form.submit();
            });
        });
    </script>
    @if ($message = Session::get('success'))
    <script type="text/javascript">
        $(document).ready(function(){
            var msg = "{{ $message }}";
            notify('success',msg);
        });
    </script>
    @endif
    @include('admin.elements.info_modal')
@endpush
@endsection