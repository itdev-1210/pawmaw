<div class="tbl">
    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Name:</div>
        <div class="tbl-cell">{{ $permission->name }}</div>
    </div>
    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Created at:</div>
        <div class="tbl-cell">
            {{date('F j, y h:i a', strtotime($permission->created_at))}}
        </div>
    </div>
</div>