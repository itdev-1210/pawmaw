@extends('admin.layout.admin')

@section('content')
@push('css-head')

@endpush
<div class="page-content">
    <div class="container-fluid">
        <div class="box-typical box-typical-padding">
            @if (count($errors) > 0)
                <div class="alert alert-danger alert-no-border alert-txt-colored alert-close alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                </div>
            @endif
            <form 
                action="/admin/facebook/edit/{{ $facebook->id }}"
                method="POST">
                @csrf
                <div class="row">
                    <div class="col-lg-12">
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="name">
                                Name
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="name" value="{{ $facebook->name}}" id="name" placeholder="Name">
                        </fieldset>
                    </div>
                    <div class="col-lg-12">    
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="country">
                                Country
                            </label>
                            <input type="text" class="form-control" name="country" value="{{ $facebook->country }}" id="name" placeholder="country"> 
                        </fieldset>
                    </div>
                    <div class="col-lg-12">    
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="link">
                                Link
                            </label>
                            <input type="text" class="form-control" name="link" value="{{ $facebook->link }}" id="name" placeholder="Link"> 
                        </fieldset>
                    </div>
                    <div class="col-lg-12">    
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="facebook_id">
                                Facebook id
                            </label>
                            <input type="text" class="form-control" name="facebook_id" value="{{ $facebook->facebook_id }}" placeholder="Facebook id"> 
                        </fieldset>
                    </div>
                    <div class="col-lg-12">    
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="app_id">
                                App id
                            </label>
                            <input type="text" class="form-control" name="app_id" value="{{ $facebook->app_id }}" placeholder="App id"> 
                        </fieldset>
                    </div>
                    <div class="col-lg-12">    
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="app_secret">
                                App secret
                            </label>
                            <input type="text" class="form-control" name="app_secret" value="{{ $facebook->app_secret }}" placeholder="App secret"> 
                        </fieldset>
                    </div>
                    <div class="col-lg-12">    
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="access_token">
                                Access token
                            </label>
                            <input type="text" class="form-control" name="access_token" value="{{ $facebook->access_token }}" placeholder="Access token"> 
                        </fieldset>
                    </div>
                    <div class="col-lg-12">    
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="app_secret">
                                Zip codes
                            </label>
                            <textarea 
                                class="form-control" 
                                name="zip_codes" 
                                placeholder="Zip codes"
                                style="height: 300px !important;">{{ $facebook->zip_codes }}</textarea>
                        </fieldset>
                    </div>
                </div><!--.row-->
                <div class="row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-inline btn-success pull-right">
                            <span class="btn-icon">
                                <i class="fa fa-edit"></i>
                            </span>
                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div><!--.box-typical-->
    </div><!--.container-fluid-->
</div><!--.page-content-->
@endsection
