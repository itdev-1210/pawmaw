<div class="tbl">



    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Name:</div>
        <div class="tbl-cell">{{ $pet->name }}</div>
    </div>



    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Id:</div>
        <div class="tbl-cell">{{ $pet->id }}</div>
    </div>


    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Type:</div>
        <div class="tbl-cell">{{ $pet->type }}</div>
    </div>


    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">User Email:</div>
        <div class="tbl-cell">{{ $pet->user->email }}</div>
    </div>


    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">User Phone:</div>
        <div class="tbl-cell">{{ $pet->user->phone }}</div>
    </div>



    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Postal:</div>
        <div class="tbl-cell">{{ $pet->postal }}</div>
    </div>




    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Specie:</div>
        <div class="tbl-cell">{{ $pet->specie }}</div>
    </div>


    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Breed:</div>
        <div class="tbl-cell">{{ $pet->breed }}</div>
    </div>


    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Color:</div>
        <div class="tbl-cell">{{ $pet->color }}</div>
    </div>


    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Last Seen:</div>
        <div class="tbl-cell">{{ $pet->last_seen }}</div>
    </div>


    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Date:</div>
        <div class="tbl-cell">{{ $pet->date }}</div>
    </div>



    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Gender:</div>
        <div class="tbl-cell">{{ $pet->gender }}</div>
    </div>


    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Description:</div>
        <div class="tbl-cell">{{ $pet->description }}</div>
    </div>


    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Circumstance:</div>
        <div class="tbl-cell">{{ $pet->circumstance }}</div>
    </div>


    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Created At:</div>
        <div class="tbl-cell">{{ $pet->created_at }}</div>
    </div>


    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Updated At:</div>
        <div class="tbl-cell">{{ $pet->updated_at }}</div>
    </div>





    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Pet Pic:</div>
        <div class="tbl-cell"> <a href="{{ asset('/uploads/pets_image') }}/{{$pet->photo}}" target="_blank">Preview </a>     </div>
    </div>




</div>


