@extends('admin.layout.admin')

@section('content')
@push('css-head')

@endpush
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
<div class="page-content">
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3>Create facebook page</h3>
                    </div>
                </div>
            </div>
        </header>

        <div class="box-typical box-typical-padding">
            @if (count($errors) > 0)
                <div class="alert alert-danger alert-no-border alert-txt-colored alert-close alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                </div>
            @endif
            <form 
                action="/admin/facebook/create"
                method="POST">
                @csrf
                <div class="row">
                    <div class="col-lg-12">
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="name">
                                Name
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="name" value="" id="name" placeholder="Name" required>
                        </fieldset>
                    </div>
                    <div class="col-lg-12">    
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="link">
                                Link
                            </label>
                            <input type="text" class="form-control" name="link" value="" id="name" placeholder="Link"> 
                        </fieldset>
                    </div>
                    <div class="col-lg-12">    
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="facebook_id">
                                Facebook id
                            </label>
                            <input type="text" class="form-control" name="facebook_id" value="" placeholder="Facebook id"> 
                        </fieldset>
                    </div>
                    <div class="col-lg-12">    
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="app_id">
                                App id
                            </label>
                            <input type="text" class="form-control" name="app_id" value="" placeholder="App id"> 
                        </fieldset>
                    </div>
                    <div class="col-lg-12">    
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="app_secret">
                                App secret
                            </label>
                            <input type="text" class="form-control" name="app_secret" value="" placeholder="App secret"> 
                        </fieldset>
                    </div>
                    <div class="col-lg-12">    
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="app_secret">
                                Zip codes
                            </label>
                            <textarea 
                                class="form-control" 
                                name="zip_codes" 
                                placeholder="Zip codes"
                                style="height: 300px !important;"></textarea>
                        </fieldset>
                    </div>
                </div><!--.row-->
                <div class="row">
                    <div class="col-lg-12">

                        <button type="submit" class="btn btn-inline btn-success pull-right">
                            <span class="btn-icon">
                                <i class="fa fa-save"></i>
                            </span>
                            Submit
                        </button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div><!--.box-typical-->
    </div><!--.container-fluid-->
</div><!--.page-content-->
@endsection

@push('css-head')
    <!-- include summernote css/js-->



@endpush
@push('alljs')
    <!-- include summernote css/js-->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('#content').summernote();
        });
    </script>

@endpush


