<div class="tbl">



    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">User Id:</div>
        <div class="tbl-cell">{{ $user->id }}</div>
    </div>



    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">First Name:</div>
        <div class="tbl-cell">{{ $user->fname }}</div>
    </div>


    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Last Name:</div>
        <div class="tbl-cell">{{ $user->lname }}</div>
    </div>


    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Phone:</div>
        <div class="tbl-cell">{{ $user->phone }}</div>
    </div>


    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Address:</div>
        <div class="tbl-cell">{{ $user->address }}</div>
    </div>



    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Email:</div>
        <div class="tbl-cell">{{ $user->email }}</div>
    </div>


    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Create:</div>
        <div class="tbl-cell">{{ $user->created_at }}</div>
    </div>


    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Update:</div>
        <div class="tbl-cell">{{ $user->updated_at }}</div>
    </div>











</div>


