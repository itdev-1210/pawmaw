@extends('admin.layout.admin')

@section('content')
@push('css-head')

@endpush
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
<div class="page-content">
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3>Edit Blog</h3>
                    </div>
                </div>
            </div>
        </header>

        <div class="box-typical box-typical-padding">
            @if (count($errors) > 0)
                <div class="alert alert-danger alert-no-border alert-txt-colored alert-close alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                </div>
            @endif
            {{-- {!!Form::open(array('route'=>"/admin/blog/{$post['id']}/edit",'method'=>'POST', 'enctype'=>'multipart/form-data'))!!} --}}
            <form 
                action="/admin/blog/{{ $post['id'] }}/edit"
                enctype="multipart/form-data"
                method="POST">
                @csrf
                <div class="row">
                    <div class="col-lg-12">
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="name">
                                Title
                                <span class="color-red">*</span>
                            </label>
                            <input 
                                type="text" 
                                class="form-control summernote" 
                                name="title" 
                                id="title" 
                                placeholder="Blog Title" 
                                value="{{ $post['title'] }}"
                                required>
                        </fieldset>
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="name">
                                Url
                            </label>
                            <input type="text" value="{{ $post['slug'] }}" class="form-control summernote" name="slug" id="url" placeholder="Blog Url">
                        </fieldset>
                        <fieldset class="form-group" style="display: flex; flex-direction: column;">
                            <label class="form-label" for="logo">
                                Select Image
                                <span class="color-red">*</span>
                            </label>
                            <a 
                                href="{{ url('/img/' . $post['image']) }}" 
                                target="_blank"
                                style="margin-bottom: 10px; text-decoration: underline;">Current photo</a>
                            <input type="file" name="image" id="image" accept="image/*">
                        </fieldset>
                        <fieldset class="form-group">
                            <label class="form-label" for="logo">
                                Select Cagegory
                                <span class="color-red">*</span>
                            </label>
                            <select name="category_id" class="form-control">
                                <option value="">Select a category</option>
                                @foreach($categories as $category)
                                    <option 
                                        value="{{$category->id}}"
                                        {{ $category['id'] == $post['category_id'] ? 'selected' : '' }}>{{$category->name}}</option>
                                @endforeach
                            </select>
                        </fieldset>
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="excerpt">
                                Short description
                                <span class="color-red">*</span>
                            </label>
                            <textarea 
                                name="excerpt" 
                                id="excerpt" 
                                class="form-control"
                                style="height: 100px">{{ $post['excerpt'] }}</textarea>
                        </fieldset>
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="name">
                                Content
                                <span class="color-red">*</span>
                            </label>
                            <textarea 
                                name="content" 
                                id="content" 
                                class="form-control"
                                style="height: 400px">{{ $post['content'] }}</textarea>
                        </fieldset>
                    </div>
                </div><!--.row-->
                <div class="row">
                    <div class="col-lg-12">

                        <button type="submit" class="btn btn-inline btn-success pull-right">
                            <span class="btn-icon">
                                <i class="fa fa-save"></i>
                            </span>
                            Submit
                        </button>
                    </div>
                </div>
            </form>
            {{-- {!! Form::close() !!} --}}
        </div><!--.box-typical-->
    </div><!--.container-fluid-->
</div><!--.page-content-->
@endsection

@push('css-head')
    <!-- include summernote css/js-->



@endpush
@push('alljs')
    <!-- include summernote css/js-->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('#content').summernote({
                 callbacks: {
                    onImageUpload : function(files, editor, welEditable) {
                           
                           for(var i = files.length - 1; i >= 0; i--) {
                             sendFile(files[i], this);
                        }
                      }
                  }
            });
        });
        function sendFile(file, el) {
            var form_data = new FormData();
            form_data.append('_token', $('meta[name=csrf-token]').attr('content'));
            form_data.append('file', file);
            var urll="{{ url('admin/uploadEditorImage') }}";
            $.ajax({
                data: form_data,
                type: 'POST',
                url: urll,
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    $(el).summernote('editor.insertImage', url);
                }
            });
        }
    </script>

@endpush


