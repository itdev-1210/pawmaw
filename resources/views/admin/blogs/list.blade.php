@extends('admin.layout.admin')

@section('content')
@push('css-head')
    <link rel="stylesheet" href="{{ asset('/assets/css/lib/datatables-net/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/datatables-net.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/custom/switch/rcswitcher.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/pnotify.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/lib/bootstrap-sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/sweet-alert-animations.min.css') }}">
@endpush
<div class="page-content">
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h2>Blog List</h2>
                        <div class="subtitle">Change status for approval.</div>
                    </div>
                    <div class="tbl-cell">
                        <a href="{{ url('admin/blog/create') }}" class="btn btn-inline btn-secondary pull-right">
                            <span class="btn-icon">
                                <i class="fa fa-plus"></i>
                            </span>
                            Create new
                        </a>
                    </div>
                </div>
            </div>
        </header>
        <section class="card">
            <div class="card-block">
                <table class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Short description</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($blogs as $key => $row)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $row->title }}</td>
                                <td>{{ $row->excerpt }}</td>
                                <td style="width: 100px;">
                                    <a href="/admin/blog/{{ $row['id'] }}/edit">Edit</a>
                                    <a 
                                        href="javascript:void(0);" 
                                        style="color: red; margin-left: 10px; border-color: red;"
                                        onclick="deletePost({{ $row['id'] }})">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>`
            </div>
        </section>
    </div><!--.container-fluid-->
</div><!--.page-content-->

<div class="control-panel-container">
    <ul>
        <li class="add">
            <div class="control-item-header">
                <a href="#" class="icon-toggle no-caret">
                    <span class="icon fa fa-plus"></span>
                </a>
            </div>
        </li>
    </ul>
    <a class="control-panel-toggle">
        <span class="fa fa-angle-double-left"></span>
    </a>
</div>

@push('scripts')
    <script src="{{ asset('/assets/js/lib/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('/assets/custom/switch/rcswitcher.js') }}"></script>
    <script src="{{ asset('/assets/js/lib/datatables-net/datatables.min.js') }}"></script>
    <script src="{{ asset('/assets/js/lib/pnotify/pnotify.js') }}"></script>
    <script src="{{ asset('/assets/custom/js/notify.js') }}"></script>
    <script type="text/javascript">

        function deletePost(id){
            let confirmCheck = confirm('Are you sure?');

            if(confirmCheck)
                $.ajax({
                    type: "POST",
                    url: '/admin/blog/' + id + '/delete',
                    data: {
                        _token: $('meta[name=csrf-token]').attr('content')
                    },
                    success: function(){
                        location.reload();
                    }
                });
        }


        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
            $("input[type='checkbox']").rcSwitcher();
            $('.table').DataTable();
        });
        $('.modal_view').on('click', function(){
            var link = $(this).data('link');
            modal_call('User Details',link); //modal_call(title,route);
        });

        $("input[type='checkbox']").rcSwitcher().on({'toggle.rcSwitcher': function( e, dataObj, changeType ){
                var id = $(this).data('id');
                var urll="{{ url('admin/pets/status') }}/"+id;
                var msg_success = 'Status Updated Successfully.';
                var msg_error = 'Something went wrong!';
                $.ajax({
                    url:urll,
                    success: function(data){
                       notify('success',msg_success);
                    },
                    error: function (data) {
                        notify('error',msg_error);
                    }
                });
            },
        });
    </script>

    <script type="text/javascript">
        $('.form-delete').click(function(e){
            e.preventDefault();
            $form = $(this);
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            });
            $('button.confirm').on('click',function(){
                $form.submit();
            });
        });
    </script>
    @if ($message = Session::get('success'))
    <script type="text/javascript">
        $(document).ready(function(){
            var msg = "{{ $message }}";
            notify('success',msg);
        });
    </script>
    @endif
    @include('admin.elements.info_modal')
@endpush
@endsection
