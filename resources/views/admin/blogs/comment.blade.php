@extends('admin.layout.admin')

@section('content')
@push('css-head')
    <link rel="stylesheet" href="{{ asset('/assets/css/lib/datatables-net/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/datatables-net.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/custom/switch/rcswitcher.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/pnotify.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/lib/bootstrap-sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/sweet-alert-animations.min.css') }}">
@endpush
<div class="page-content">
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h2>New comment list</h2>
                    </div>
                </div>
            </div>
        </header>
        <section class="card">
            <div class="card-block">
                <table class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Comment</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Type</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($comments as $key => $comment)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $comment->comment }}</td>
                                <td>{{ $comment->name }}</td>
                                <td>{{ $comment->email }}</td>
                                <td>{{ $comment->comment_id == 0 ? 'comment' : 'reply' }}</td>
                                <td style="width: 140px">
                                    <div class="action-buttons">
                                        <button onclick="confirmComment({{ $comment['id'] }})">Confirm</button>
                                        <button onclick="declineComment({{ $comment['id'] }})">Decline</button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>`
            </div>
        </section>
    </div><!--.container-fluid-->
</div><!--.page-content-->

<div class="control-panel-container">
    <ul>
        <li class="add">
            <div class="control-item-header">
                <a href="#" class="icon-toggle no-caret">
                    <span class="icon fa fa-plus"></span>
                </a>
            </div>
        </li>
    </ul>
    <a class="control-panel-toggle">
        <span class="fa fa-angle-double-left"></span>
    </a>
</div>

@push('scripts')
    <script src="{{ asset('/assets/js/lib/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('/assets/custom/switch/rcswitcher.js') }}"></script>
    <script src="{{ asset('/assets/js/lib/datatables-net/datatables.min.js') }}"></script>
    <script src="{{ asset('/assets/js/lib/pnotify/pnotify.js') }}"></script>
    <script src="{{ asset('/assets/custom/js/notify.js') }}"></script>

    <script type="text/javascript">
        function confirmComment(id){
            let confirmCheck = confirm('Are you sure?');

            if(confirmCheck)
                $.ajax({
                    type: "POST",
                    url: '/admin/blog/comment',
                    data: {
                        _token: $('meta[name=csrf-token]').attr('content'),
                        id: id,
                        type: 1
                    },
                    success: function(){
                        location.reload();
                    }
                });
        }

        function declineComment(id){
            let confirmCheck = confirm('Are you sure?');

            if(confirmCheck)
                $.ajax({
                    type: "POST",
                    url: '/admin/blog/comment',
                    data: {
                        _token: $('meta[name=csrf-token]').attr('content'),
                        id: id,
                        type: 0
                    },
                    success: function(){
                        location.reload();
                    }
                });
        }
    </script>

    @include('admin.elements.info_modal')

@endpush
@endsection
