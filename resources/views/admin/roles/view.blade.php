<div class="tbl">
    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Name:</div>
        <div class="tbl-cell">{{ $role->name }}</div>
    </div>
    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Created at:</div>
        <div class="tbl-cell">
            {{date('F j, y h:i a', strtotime($role->created_at))}}
        </div>
    </div>
    <div class="tbl-row list">
        <div class="tbl-cell tbl-cell-lbl">Permissions:</div>
        <div class="tbl-cell">
            <ul>
                @foreach($permissions as $row)
                <li>{{ $row->name }}</li>
                @endforeach
            </ul>
        </div>
    </div>
</div>