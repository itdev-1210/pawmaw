<?= '<'.'?'.'xml version="1.0" encoding="UTF-8"?>'."\n"; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    
    <url>
        <loc>{{ url('/') }}</loc>
    </url>
    <url>
        <loc>{{ url('blog') }}</loc>
    </url>
    <url>
        <loc>{{ url('faqs') }}</loc>
    </url>
    <url>
        <loc>{{ url('about-us') }}</loc>
    </url>
    <url>
        <loc>{{ url('contact') }}</loc>
    </url>
    <url>
        <loc>{{ url('join-our-network') }}</loc>
    </url>
    <url>
        <loc>{{ url('privacy-policy') }}</loc>
    </url>
    <url>
        <loc>{{ url('refund-policy') }}</loc>
    </url>
    @foreach($pets as $pet)
        <url>
            <loc>{{ url('pet-details') }}/{{$pet->slug}}</loc>
        </url>
    @endforeach
    @foreach($blogs as $blog)
        <url>
            <loc>{{ url('blog') }}/{{$blog->slug}}</loc>
        </url>
    @endforeach
</urlset>