@extends('admin.layout.admin')

@section('content')
@push('css-head')
    <link rel="stylesheet" href="{{ asset('/assets/css/lib/datatables-net/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/datatables-net.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/custom/switch/rcswitcher.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/pnotify.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/lib/bootstrap-sweetalert/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/sweet-alert-animations.min.css') }}">
    <style>
        .onoffswitch {
             position: absolute;
            -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
        }
        .onoffswitch-checkbox {
            display: none;
        }
        .onoffswitch-label {
            display: block; overflow: hidden; cursor: pointer;
            border: 2px solid #999999; border-radius: 20px;    margin-left: 4%;    max-width: 80%;
        }
        .onoffswitch-inner {
            display: block; width: 200%; margin-left: -100%;
            transition: margin 0.3s ease-in 0s;
        }
        .onoffswitch-inner:before, .onoffswitch-inner:after {
            display: block; float: left; width: 50%; height: 28px; padding: 0; line-height: 30px;
            font-size: 10px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
            box-sizing: border-box;
        }
        .onoffswitch-inner:before {
            content: "Accept";
            padding-left: 10px;
            background-color: #34c13f; color: #FFFFFF;
        }
        .onoffswitch-inner:after {
            content: "Reject";
            padding-right: 10px;
            background-color: #dd4b39; color: #FFFFFF;
            text-align: right;
        }
        .onoffswitch-switch {
            display: block; width: 18px; margin: 10px;
            background: #FFFFFF;
            position: absolute; top: 0; bottom: 0;
            right: 68px;
            border: 2px solid #999999; border-radius: 20px;
            transition: all 0.3s ease-in 0s; 
        }
        .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
            margin-left: 0;
        }
        .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
            right: 15px; 
        }
    </style>
@endpush
<div class="page-content">
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h2>Pet Details Email</h2>
                        <div class="subtitle">Change status for approval.</div>
                    </div>
                    <div class="tbl-cell">
                        <a href="#" class="btn btn-inline btn-secondary pull-right">
                            <span class="btn-icon"> 
                                <i class="fa fa-plus"></i>
                            </span>
                            Create new
                        </a>
                    </div>
                </div>
            </div>
        </header>
        <section class="card">
            <div class="card-block">
                <table class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Pet_id</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>message</th>
                            <th>Action</th>

                            {{--<th>Options</th>--}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($contacts as $key => $row)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $row->pet_id }}</td>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->email }}</td>
                            <td>{{ $row->message }}</td>
                            @if($row->status == 0)
                                <td class='text-center'>
                                  <a class='btn btn-success btn-xs waves-effect waves-light  blue darken-4 z-depth-0 less-pad statuschange' data-toggle="accept" title="Accept" data-delid='1' data-delname="" data-id="{{$row->id}}" data-target='#statuschange'><i class='fa fa-check fa-fw'></i></a>
                                  <a class='btn btn-danger btn-xs waves-effect waves-light  blue darken-4 z-depth-0 less-pad statuschange' data-toggle="decline" title="Decline" data-delid='2' data-delname="" data-id="{{$row->id}}" data-target='#statuschange'><i class='fa fa-times fa-fw'></i></a>
                                </td>
                            @elseif($row->status == 1)
                                <td>
                                    <span class="badge badge-success">Accepted</span>
                                </td>
                            @elseif($row->status == 2)
                                <td>
                                    <span class="badge badge-danger">Rejected</span>
                                </td>
                            @endif
                         
                            
    

                            {{--<td style="text-align: center;">--}}
                                {{--<span data-link="{{ url('admin/orders/view') }}/{{ $row->id }}" class="btn btn-sm btn-inline btn-primary modal_view" data-toggle="tooltip" title="View" data-placement="top">--}}
                                    {{--<span class="fa fa-eye"></span>--}}
                                {{--</span>--}}

                            {{--</td>--}}
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div><!--.container-fluid-->
</div><!--.page-content-->

<div class="control-panel-container">
    <ul>
        <li class="add">
            <div class="control-item-header">
                <a href="#" class="icon-toggle no-caret">
                    <span class="icon fa fa-plus"></span>
                </a>
            </div>
        </li>
    </ul>
    <a class="control-panel-toggle">
        <span class="fa fa-angle-double-left"></span>
    </a>
</div>

@push('scripts')
    <script src="{{ asset('/assets/js/lib/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('/assets/custom/switch/rcswitcher.js') }}"></script>
    <script src="{{ asset('/assets/js/lib/datatables-net/datatables.min.js') }}"></script>
    <script src="{{ asset('/assets/js/lib/pnotify/pnotify.js') }}"></script>
    <script src="{{ asset('/assets/custom/js/notify.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="accept"]').tooltip();
            $('[data-toggle="decline"]').tooltip();
            
            $('.statuschange').on('click', function(){
                var status = $(this).attr('data-delid');
                var id = $(this).attr('data-id');
               
                var url = "{{url('admin/ownermailstatus')}}";
                
                var msg_error = 'Something went wrong!';
                
                var flag = confirm("Are you sure to change this contact status?");
                var $this = $(this);
                if(flag) {
                        $this.prop('disabled',true);
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: {
                                _token: $('meta[name=csrf-token]').attr('content'),
                                status: status,
                                id: id
                            },
                            
                            success: function(data){
                                
                                if(data == 'true'){
                                    if(status == 1){
                                        var msg_success = 'Your Request Accepted!';
                                        notify('success',msg_success);
                                        $this.parent().html('<span class="badge badge-success">Accepted</span>');
                                        
                                    }
                                    else if(status == 2){
                                        var msg_success = 'Your Request Rejected!';
                                        notify('error',msg_success);
                                        $this.parent().html('<span class="badge badge-danger">Rejected</span>');
                                    }
                                }
                                else{
                                     notify('error',msg_error);
                                }
                            },
                            error: function (data) {
                                $this.prop('disabled',false);
                                notify('error',msg_error);
                            }
                        });
                    }
            });
            
            
            $("input[type='checkbox']").rcSwitcher();
            $('.table').DataTable();
        });
        $('.modal_view').on('click', function(){
            var link = $(this).data('link');
            modal_call('Order Details',link); //modal_call(title,route);
        });

        $("input[type='checkbox']").rcSwitcher().on({'toggle.rcSwitcher': function( e, dataObj, changeType ){
                var id = $(this).data('id');
                var urll="{{ url('admin/pets/status') }}/"+id;
                var msg_success = 'Status Updated Successfully.';
                var msg_error = 'Something went wrong!';
                $.ajax({
                    url:urll,
                    success: function(data){
                       notify('success',msg_success);
                    },
                    error: function (data) {
                        notify('error',msg_error);
                    }
                });
            },
        });
    </script>

    <script type="text/javascript">
        $('.form-delete').click(function(e){
            e.preventDefault();
            $form = $(this);
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            });
            $('button.confirm').on('click',function(){
                $form.submit();
            });
        });
    </script>
    @if ($message = Session::get('success'))
    <script type="text/javascript">
        $(document).ready(function(){
            var msg = "{{ $message }}";
            notify('success',msg);
        });
    </script>
    @endif
    @include('admin.elements.info_modal')
@endpush
@endsection