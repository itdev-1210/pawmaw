<span class="btn btn-inline btn-primary info_modal hidden"
        data-toggle="modal"
        data-target=".bd-info-modal-lg">  
</span>
<div class="modal info fade bd-info-modal-lg"
     tabindex="-1"
     role="dialog"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                    <i class="font-icon-close-2"></i>
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="modal-result"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function modal_call(title,link){
        $('.info .modal-title').html(title);
        info_modal(link,callback_info);
    }
    function callback_info(){
        $('.info_modal').click();
    }
    function info_modal(link,callback){
        $('.info .modal-result').load(link,callback);
    }
</script>