<span class="btn btn-inline btn-primary input_modal hidden"
        data-toggle="modal"
        data-target=".bd-input-modal-lg"></span>
<div class="modal input fade bd-input-modal-lg"
     tabindex="-1"
     role="dialog"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                    <i class="font-icon-close-2"></i>
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="modal-result"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Close</button>
                <span class="btn btn-rounded btn-primary save">Submit</span>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function modal_input(title,link){
        $('.input .modal-title').html(title);
        input_modal(link,callback_input);
    }
    function callback_input(){
        $('.input_modal').click();
    }
    function input_modal(link,callback){
        $('.input .modal-result').load(link,callback);
    }
    $('.save').on('click', function(){
        if(check_required() == true){
            $(this).closest('.input').find('form').submit();
        }
    });
    function check_required(){
        var check = 0;
        var required = 0;
        $('.modal.input .required').each(function(){
            required = required + 1;
            if($(this).val() == null || $(this).val() == ''){
                $(this).css('border-color','#ff0000');
            }else{
                $(this).css('border-color','#d6e2e8');
                check = check + 1;
            }
        });
        if(check == required){
            return true;
        }else{
            return false;
        }
    }
</script>