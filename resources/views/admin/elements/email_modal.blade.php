<span class="btn btn-inline btn-primary email_modal hidden"
        data-toggle="modal"
        data-target=".bd-email-modal-lg"></span>
<div class="modal email fade bd-email-modal-lg"
     tabindex="-1"
     role="dialog"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                    <i class="font-icon-close-2"></i>
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="modal-result"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Close</button>
                <span class="btn btn-rounded btn-primary send">Send</span>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function modal_email(title,link){
        $('.email .modal-title').html(title);
        email_modal(link,callback_email);
    }
    function callback_email(){
        $('.email_modal').click();
    }
    function email_modal(link,callback){
        $('.email .modal-result').load(link,callback);
    }
    $('.send').on('click', function(){
        $(this).closest('.email').find('form').submit();
    });
</script>