@extends('admin.layout.admin')

@section('content')


<div class="page-content">
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3>Unsubscribe User From Newslatter</h3>
                    </div>
                </div>
            </div>
        </header>

        <div class="col-lg-12 box-typical box-typical-padding offset-md-4">
            @if (count($errors) > 0)
                <div class="alert alert-danger alert-no-border alert-txt-colored alert-close alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                </div>
            @endif
            @if ($message = Session::get('success'))

            <div class="alert alert-success alert-block">
            
            	<button type="button" class="close" data-dismiss="alert">×</button>	
            
            	<strong>{{ $message }}</strong>
            
            </div>
            
            @endif
             @if ($message = Session::get('error'))

            <div class="alert alert-danger alert-block">
            
            	<button type="button" class="close" data-dismiss="alert">×</button>	
            
            	<strong>{{ $message }}</strong>
            
            </div>
            
            @endif
            {!!Form::open(array('url'=>'admin/addunsubscribe','method'=>'POST', 'enctype'=>'multipart/form-data'))!!}
                <div class="row">
                    <div class="col-lg-12 offset-md-4">
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="name">
                                Email
                                <span class="color-red">*</span>
                            </label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="User Email" required="">
                        </fieldset>
                        <button type="submit" class="btn btn-inline btn-success pull-right">
                            <span class="btn-icon">
                                <i class="font-icon font-icon-mail"></i>
                            </span>
                            Unsubscribe
                        </button>
                  </div> 
              
              </div>
            {!! Form::close() !!}
        </div><!--.box-typical-->
    </div><!--.container-fluid-->
</div><!--.page-content-->
@endsection



