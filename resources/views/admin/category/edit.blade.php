@extends('admin.layout.admin')

@section('content')
@push('css-head')

@endpush
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
<div class="page-content">
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3>Edit category</h3>
                    </div>
                </div>
            </div>
        </header>

        <div class="box-typical box-typical-padding">
            @if (count($errors) > 0)
                <div class="alert alert-danger alert-no-border alert-txt-colored alert-close alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                </div>
            @endif
            {{-- {!!Form::open(array('route'=>"/admin/blog/{$post['id']}/edit",'method'=>'POST', 'enctype'=>'multipart/form-data'))!!} --}}
            <form 
                action="/admin/blog/category/{{ $category['id'] }}/edit"
                method="POST">
                @csrf
                <div class="row">
                    <div class="col-lg-12">
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="name">
                                Name
                                <span class="color-red">*</span>
                            </label>
                            <input 
                                type="text" 
                                class="form-control summernote" 
                                name="name" 
                                id="title" 
                                placeholder="Blog Title" 
                                required=""
                                value="{{ $category['name'] }}">
                        </fieldset>
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="name">
                                Url
                            </label>
                            <input type="text" class="form-control summernote" name="slug" id="url" placeholder="Blog Url" value="{{ $category['slug'] }}" required>
                        </fieldset>
                    </div>
                </div><!--.row-->
                <div class="row">
                    <div class="col-lg-12">

                        <button type="submit" class="btn btn-inline btn-success pull-right">
                            <span class="btn-icon">
                                <i class="fa fa-save"></i>
                            </span>
                            Submit
                        </button>
                    </div>
                </div>
            </form>
            {{-- {!! Form::close() !!} --}}
        </div><!--.box-typical-->
    </div><!--.container-fluid-->
</div><!--.page-content-->
@endsection

@push('css-head')
    <!-- include summernote css/js-->



@endpush
@push('alljs')
    <!-- include summernote css/js-->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('#content').summernote();
        });
    </script>

@endpush


