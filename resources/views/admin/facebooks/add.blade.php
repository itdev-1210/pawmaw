@extends('admin.layout.admin')

@section('content')
@push('css-head')
<link rel="stylesheet" href="{{ asset('/assets/css/separate/elements/steps.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/pnotify.min.css') }}">
<style type="text/css">
    .light.red{
        color: #ff0000;
    }
    .light.green{
        color: #00a752;
    }
</style>
@endpush
<div class="page-content">
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3>Create Facebook</h3>
                        <ol class="breadcrumb breadcrumb-simple">
                            <li>
                                <a href="{{ url('admin/facebookshares') }}">Facebook share List</a>
                            </li>
                            <li class="active">Create New</li>
                        </ol>
                    </div>
                </div>
            </div>
        </header>

        <div class="box-typical box-typical-padding">
            @if (count($errors) > 0)
                <div class="alert alert-danger alert-no-border alert-txt-colored alert-close alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                </div>
            @endif
            {!!Form::open(array('route'=>'facebooks_store','method'=>'POST', 'enctype'=>'multipart/form-data'))!!}
                <div class="row">
                    <div class="col-lg-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="zip_code">
                                Zip Code
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="zip_code" id="name" placeholder="Name" required="">
                        </fieldset>



                    </div>

                    <div class="col-lg-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="pageid">
                                Page Id
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="pageid" id="name" placeholder="Name" required="">
                        </fieldset>



                    </div>


                    <div class="col-lg-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="app_id">
                        App id
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="app_id" id="name" placeholder="Name" required="">
                        </fieldset>



                    </div>


                    <div class="col-lg-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="app_secret">
                            App secret
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="app_secret" id="name" placeholder="Name" required="">
                        </fieldset>



                    </div>


                    <div class="col-lg-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="access_token">
                            Access token
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="access_token" id="name" placeholder="Name" required="">
                        </fieldset>



                    </div>

                </div><!--.row-->
                <div class="row">
                    <div class="col-lg-12">
                        <a href="{{ url('admin/facebookshares/create') }}" class="btn btn-inline btn-secondary pull-left">
                            <span class="btn-icon"> 
                                <i class="fa fa-refresh"></i>
                            </span>
                            Refresh
                        </a>
                        <button type="submit" class="btn btn-inline btn-success pull-right">
                            <span class="btn-icon"> 
                                <i class="fa fa-save"></i>
                            </span>
                            Submit
                        </button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div><!--.box-typical-->
    </div><!--.container-fluid-->
</div><!--.page-content-->
@endsection