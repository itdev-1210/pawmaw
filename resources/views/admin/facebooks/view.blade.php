<div class="tbl">
    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Name:</div>
        <div class="tbl-cell">{{ $channel->name }}</div>
    </div>


    <div class="tbl-row">
        <div class="tbl-cell tbl-cell-lbl">Status:</div>
        <div class="tbl-cell">
            @if($channel->status == 1)
            {{ 'Approved' }}
            @else
            {{ 'Pending' }}
            @endif
        </div>
    </div>

</div>


<div class="row view_footer">
    <div class="col-lg-4">
        <center>
            <strong>Added by:</strong>
            {{ $channel->added_by_data->name }}
        </center>
    </div>
    <div class="col-lg-4">
        @if($channel->status_by_data)
            <center>
                <strong>Last Approval:</strong>
                {{ $channel->status_by_data->name }}
            </center>
        @endif
    </div>
    <div class="col-lg-4">
        @if($channel->updated_by_data)
            <center>
                <strong>Last Update:</strong>
                {{ $channel->updated_by_data->name }}
            </center>
        @endif
    </div>
</div>