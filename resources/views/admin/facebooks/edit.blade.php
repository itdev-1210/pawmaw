@extends('admin.layout.admin')

@section('content')
@push('css-head')
<link rel="stylesheet" href="{{ asset('/assets/css/separate/elements/steps.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/pnotify.min.css') }}">
<style type="text/css">
    .light.red{
        color: #ff0000;
    }
    .light.green{
        color: #00a752;
    }
</style>
<div class="page-content">
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3>Update Channel</h3>
                        <ol class="breadcrumb breadcrumb-simple">
                            <li>
                                <a href="{{ url('agent/channels') }}">Channels List</a>
                            </li>
                            <li class="active">Update</li>
                        </ol>
                    </div>
                </div>
            </div>
        </header>

        <div class="box-typical box-typical-padding">
            @if (count($errors) > 0)
                <div class="alert alert-danger alert-no-border alert-txt-colored alert-close alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>Whoops!</strong> There were some problems with your input.
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                </div>
            @endif
            {!!Form::model($facebookshare,['method'=>'PETCH', 'enctype'=>'multipart/form-data', 'route'=>['facebooks_update',$facebookshare->id]])!!}
                <div class="row">

                    <div class="col-lg-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="name">
                                zip_code
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="zip_code" value="{{$facebookshare->zip_code}}" id="name" placeholder="Name" required="">
                        </fieldset>

                    </div>





                    <div class="col-lg-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="name">
                                Page Id
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="pageid" value="{{$facebookshare->pageid}}" id="name" placeholder="Name" required="">
                        </fieldset>

                    </div>






                    <div class="col-lg-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="name">
                                app_id
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="app_id" value="{{$facebookshare->app_id}}" id="name" placeholder="Name" required="">
                        </fieldset>

                    </div>



                    <div class="col-lg-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="name">
                                zip_code
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="app_secret" value="{{$facebookshare->app_secret}}" id="name" placeholder="Name" required="">
                        </fieldset>

                    </div>



                    <div class="col-lg-6">
                        <fieldset class="form-group">
                            <label class="form-label semibold" for="name">
                                access_token
                                <span class="color-red">*</span>
                            </label>
                            <input type="text" class="form-control" name="access_token" value="{{$facebookshare->access_token}}" id="name" placeholder="Name" required="">
                        </fieldset>

                    </div>

                </div><!--.row-->
                <div class="row">
                    <div class="col-lg-12">
                        <a href="{{ url('admin/facebookshares/edit') }}/{{ $facebookshare->id }}" class="btn btn-inline btn-secondary pull-left">
                            <span class="btn-icon"> 
                                <i class="fa fa-refresh"></i>
                            </span>
                            Refresh
                        </a>
                        <button type="submit" class="btn btn-inline btn-success pull-right">
                            <span class="btn-icon"> 
                                <i class="fa fa-edit"></i>
                            </span>
                            Update
                        </button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div><!--.box-typical-->
    </div><!--.container-fluid-->
</div><!--.page-content-->
@endsection