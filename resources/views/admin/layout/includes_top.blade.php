<head lang="en">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Admin || {{ config('app.name') }}</title>

  <link href="{{ asset('/assets/custom/img/favicon.png')}}" rel="icon" type="image/png">

  <link rel="stylesheet" href="{{ asset('/assets/css/lib/lobipanel/lobipanel.min.css')}}">
  <link rel="stylesheet" href="{{ asset('/assets/css/separate/vendor/lobipanel.min.css')}}">
  <link rel="stylesheet" href="{{ asset('/assets/css/lib/jqueryui/jquery-ui.min.css')}}">
  <link rel="stylesheet" href="{{ asset('/assets/css/separate/pages/widgets.min.css')}}">
  <link rel="stylesheet" href="{{ asset('/assets/css/lib/font-awesome/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{ asset('/assets/css/lib/bootstrap/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('/assets/css/lib/bootstrap-datetimepicker/bootstrap-datetimepicker.css')}}">
  <link rel="stylesheet" href="{{ asset('/assets/css/main.css')}}">
  <link rel="stylesheet" href="{{ asset('/assets/custom/css/main.css')}}">
  
  @stack('css-head')

  <script src="{{ asset('/assets/js/lib/jquery/jquery-3.2.1.min.js')}}"></script>
  <script type="text/javascript" src="{{ asset('/assets/js/lib/jqueryui/jquery-ui.min.js')}}"></script>

</head>