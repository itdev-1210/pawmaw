@stack('scripts')
<script src="{{ asset('/assets/js/lib/popper/popper.min.js')}}"></script>
<script src="{{ asset('/assets/js/lib/tether/tether.min.js')}}"></script>
<script src="{{ asset('/assets/js/lib/bootstrap/bootstrap.min.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="{{ asset('/assets/js/lib/bootstrap-datetimepicker/bootstrap-datetimepicker.js')}}"></script>
<script src="{{ asset('/assets/js/plugins.js')}}"></script>

<script type="text/javascript" src="{{ asset('/assets/js/lib/lobipanel/lobipanel.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('/assets/js/lib/match-height/jquery.matchHeight.min.js')}}"></script>
<script src="{{ asset('/assets/js/app.js')}}?{{time()}}"></script>


<script>

    // This example adds a search box to a map, using the Google Place Autocomplete
    // feature. People can enter geographical searches. The search box will return a
    // pick list containing a mix of places and predicted search terms.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
    var map, infowindow, geocoder;

    function initAutocomplete() {
        geocoder = new google.maps.Geocoder();

        infowindow = new google.maps.InfoWindow();

        var input3 = document.getElementById('reportlostaddress');

        var searchBox3 = new google.maps.places.SearchBox(input3);

        //  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.

        searchBox3.addListener('places_changed', function () {

            var places = searchBox3.getPlaces();


            if (places.length == 0) {
                return;
            }
            var myAddress = places[0]["address_components"];
            for (let i = 0; i <myAddress.length ; i++) {
                var aData = myAddress[i]['types'][0];
                if(aData=="postal_code"){
                    jQuery("#postal_2").val(myAddress[i]["long_name"]);
                    break;
                }
            }



        });


    }

    function toggleBounce(marker) {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

</script>



@stack('alljs')
