<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="{{url('admin/downloadmail')}}" method="post">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Emails Filter By Date</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="email">From Date *</label>
                    <input type="" class="form-control" name="fromdate" id="fromdate">
                  </div>
                </div>  
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="pwd">To Date</label>
                        <input type="text" class="form-control" name="todate" id="todate">
                        {{ csrf_field() }}
                     </div>
                </div>    
            </div>    
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">Download</button>
      </div>
       </form>
    </div>
  </div>
</div>
 
