<!-- colors: red,gold,gray,green,purple,blue,magenta,pink,orange-red,blue-dirty,aquamarine -->

<div class="mobile-menu-left-overlay"></div>
<nav class="side-menu">

    <ul class="side-menu-list">
        <li class="red">
            <a href="{{ url('admin/home')}}">
                <i class="font-icon fa fa-tachometer"></i>
                <span class="lbl">Dashboard</span>
            </a>
        </li>

        <li class="green">
            <a href="{{ url('admin/pets')}}">
                <i class="font-icon fa fa-magic"></i>
                <span class="lbl">Pets</span>
            </a>
        </li>

        <li class="purple">
            <a href="{{ url('/admin/orders') }}">
                <i class="font-icon fa fa-money"></i>
                <span class="lbl">Order</span>
            </a>
        </li>

        <li class="red">
            <a href="{{ url('/admin/users') }}">
                <i class="font-icon fa fa-users"></i>
                <span class="lbl">Users</span>
            </a>
        </li>


        <li class="purple">
            <a href="{{ url('/admin/petsemail') }}">
                <i class="font-icon fa fa-money"></i>
                <span class="lbl">Admin Email</span>
            </a>
        </li>



        <li class="red">
            <a href="{{ url('/admin/webemail') }}">
                <i class="font-icon fa fa-users"></i>
                <span class="lbl">Contact Web</span>
            </a>
        </li>


        <li class="red">
            <a href="{{ url('/admin/owneremail') }}">
                <i class="font-icon fa fa-users"></i>
                <span class="lbl">Owner Email</span>
            </a>
        </li>

        <li class="green with-sub">
            <span>
                <i class="font-icon fa fa-key"></i>
                <span class="lbl">Role & Permissions</span>
            </span>
            <ul>
                <li class="">
                    <a href="{{ url('admin/permissions') }}">
                        <span class="lbl">Permissions</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ url('admin/roles') }}">
                        <span class="lbl">Roles</span>
                    </a>
                </li>
            </ul>
        </li>


        <li class="green with-sub">
            <span>
                <i class="font-icon fa fa-key"></i>
                <span class="lbl">Blog</span>
            </span>
            <ul>
                <li class="">
                    <a href="{{ route('adminblog') }}">
                        <span class="lbl">List Blog</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route('adminblogcreate') }}">
                        <span class="lbl">Add Blog</span>
                    </a>
                </li>
                <li class="">
                    <a href="/admin/blog/comment">
                        <span class="lbl">New comments</span>
                    </a>
                </li>
                <li class="">
                    <a href="/admin/blog/category">
                        <span class="lbl">Categories</span>
                    </a>
                </li>
            </ul>
        </li>
         
       <li class="green with-sub">
            <span>
                <i class="font-icon fa fa-key"></i>
                <span class="lbl">Pages</span>
            </span>
            <ul>
                <li class="">
                    <a href="{{ route('adminpage') }}">
                        <span class="lbl">List Pages</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route('adminpagecreate') }}">
                        <span class="lbl">Add Page</span>
                    </a>
                </li>
            </ul>
        </li>
        
        <li class="purple">
            <a href="{{ url('admin/admins') }}">
                <i class="font-icon font-icon-users"></i>
                <span class="lbl">Admins</span>
            </a>
        </li>

        {{-- <li class="blue">
            <a href="{{ url('admin/facebookshares') }}">
                <i class="font-icon font-icon-facebook"></i>
                <span class="lbl">Facebook Shares</span>
            </a>
        </li> --}}
        <li class="blue">
            <a href="{{ url('admin/facebook') }}">
                <i class="font-icon font-icon-facebook"></i>
                <span class="lbl">Facebook Pages</span>
            </a>
        </li>

        <li class="red">
            <a href="{{ url('admin/newsletters') }}">
                <i class="font-icon font-icon-mail"></i>
                <span class="lbl">Newsletters</span>
            </a>
        </li>
         <li class="red">
            <a href="{{ url('admin/unsubscribe') }}">
                <i class="font-icon font-icon-mail"></i>
                <span class="lbl">Unsubscribe</span>
            </a>
        </li>
        <li class="blue">
            <a href="{{ url('admin/sitemap') }}">
                <i class="fa fa-sitemap"></i>
                <span class="lbl">Sitemap</span>
            </a>
        </li>
        <li class="blue">
            <a href="javascript:;" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-download"></i>
                <span class="lbl">Download Emails</span>
            </a>
        </li>
        <li class="blue">
            <a href="{{ url('admin/notifications') }}">
                <i class="fa fa-bell"></i>
                <span class="lbl">Notifications</span>
            </a>
        </li>
    </ul>


</nav><!--.side-menu-->
