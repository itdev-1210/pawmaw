<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  @include('admin.layout.includes_top')
  <body class="with-side-menu control-panel control-panel-compact">
    @include('admin.layout.header')
    @include('admin.layout.nav')
    @yield('content')
    @include('admin.layout.includes_bottom')
    @include('admin.layout.footer')

  </body>
</html>