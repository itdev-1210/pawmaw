<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Post Confirmation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <style type="text/css">
        html {position: relative; min-height: 100%; font-size: 62.5%;}
        body { margin: 0; padding: 0; min-width: 100%; width: 100% !important; height: 100% !important; box-sizing: border-box; background-color: #f2f2f2; font-family: 'Montserrat', sans-serif;}
        h1,h2,h3 {margin: 0; padding: 0;}
        .post-secondary-title {
            font-size: 20px;
            font-weight: 400;
            color: #0b0b0b;
            text-align: center;
        }
        .cdsub-title {
            font-size: 18px;
            font-weight: 700;
            text-align: center;
            color: #0b0b0b;
        }
        .cdsub-title span,
        .cdcontent-info span {
            color: #ea4b4f !important;
        }
        .cdcontent-info {
            font-size: 16px;
            font-weight: 400;
            text-align: center;
            color: #0b0b0b;
        }
        .cdsub-title2 {
            font-size: 24px;
            font-weight: 700;
            text-align: center;
            color: #0b0b0b;
        }
        .cdpet-details-btn {
            position: relative;
            margin-left: auto !important;
            margin-right: auto !important;
            margin-top: 20px !important;
            display: block;
        }
        .cdpet-share-btn {
            float: left;
        }
        .cdpet-post-btn {
            float: right;
        }
        .cdpet-details-btn,
        .cdpet-share-btn,
        .cdpet-post-btn {
            font-size: 18px;
            text-align: center!important;
            text-transform: capitalize;
            color: #ffffff !important;
            background-color: #3a72d9;
            text-decoration: none!important;
            width: 180px;
            padding: 15px;
            border-radius: 100px;
            margin-bottom: 20px!important;
            cursor: pointer !important;
            transition: all .2s;
        }
        .cdpet-details-btn:hover,
        .cdpet-details-btn:active,
        .cdpet-share-btn:hover,
        .cdpet-share-btn:active,
        .cdpet-post-btn:hover,
        .cdpet-post-btn:active {
            background-color: #3b5998;
        }
        .site-link {
            font-size: 18px;
            color: #ea4b4f !important;
            text-decoration: none;
            cursor: pointer;
            transition: all .2s;
        }
        .site-link:hover {
            color: #dc2227 !important;
        }
        .call_now-btn {
            border-radius: 10px;
            text-decoration: none;
            border: none;
            background-color: #e43f44;
            position: relative;
            overflow: hidden;
            cursor: pointer;
            padding: 10px 30px;
            margin-top: 20px;
            margin-bottom: 10px !important;
            transition: all .2s;
        }
        .icon-phone {
            height: 20px;
            width: 20px;
            fill: #ffffff;
            position: absolute;
            bottom: 8px;
            left: 8px;
        }
        .call_now-btn span {
            font-size: 16px;
            font-weight: 700;
            color: #ffffff;
            text-transform: capitalize;
        }
        .call_now-btn:hover,
        .call_now-btn:active {
            background-color: #dc2227;
        }
        .phone-num {
            color: #ffffff !important;
            font-size: 16px;
            font-weight: 400;
        }
        .address-title {
            font-size: 20px;
            color: #ea4b4f !important;
            font-weight: 400;
        }
        .address-details {
            color: #ffffff !important;
            font-size: 16px;
        }
        .address-email {
            margin-left: 10px;
            color: #ffffff !important;
            font-size: 16px;
            text-decoration: none;
        }
        .footer-text {
            color: #ffffff;
            font-size: 16px;
            text-align: center !important;
        }
        .footer-text a {
            color: #ffffff;
            text-decoration: none;
            transition: all .2s;
        }
        .footer-text a:hover {
            color: #ea4b4f;
        }
        .footer-text span {
            color: #ea4b4f !important;
        }
    </style>
</head>
<body>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" background="{{URL::asset('email_image/post-bg.png')}}" style="box-sizing: border-box; padding: 10px 15px !important; background-position: center; background-repeat: no-repeat; border-collapse: separate;">
    <tr>
        <td>
            <img src="{{URL::asset('email_image/logo.png')}}" alt="Pawmaw" width="406" height="96" style="margin: 0 auto; display: block;" />
        </td>
    </tr>
    <tr>
        <td align="center">
            <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="570" style="box-sizing: border-box; padding: 10px; border-radius: 8px;">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="font-size: 0; line-height: 0;" width="95">&nbsp;</td>
                                <td>
                                    <h1 style="font-size: 38px; font-weight: 700; color:#ea4b4f;">
                                        PawMaw Alert
                                    </h1>
                                </td>
                                <td><img src="{{URL::asset('email_image/alert-icon.png')}}" alt="Alert"></td>
                                <td style="font-size: 0; line-height: 0;" width="95">&nbsp;</td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td>
                                    <h2 class="post-secondary-title">
                                        You can see your pet details and facebook post below:
                                    </h2>
                                    <hr>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h2 class="cdsub-title">Here is the <span>Best Way</span> to stay focused on your search:</h2>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p class="cdcontent-info">
                                        Make sure to <span>comment/like</span> on the post so that, you will get  notified when other people <span>comment or found</span> your pet.
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h3 class="cdsub-title2">Example Comment</h3>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p class="cdcontent-info">
                                        We are in need of your help. Our dog/cat went <span>missing</span> last night, if you see him/her please <span>contact me</span> and <span>comment & share</span> as well. We really miss him/her.
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h3 class="cdsub-title2">Share Post</h3>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p class="cdcontent-info">
                                        You should also <span>share ({{$email->pet->name}}) post</span> on your own <span>timeline</span>, if you haven’t share it yet.
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">
            <table border="0" cellpadding="0" cellspacing="0" width="570" style="box-sizing: border-box; padding: 10px;">
                <tr>
                    <td>
                        <a class="cdpet-details-btn" href="{{url('/pet-details/'.$email->pet->id)}}">View pet detials</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center">
            <table border="0" cellpadding="0" cellspacing="0" width="570" style="box-sizing: border-box; padding: 10px;">
                <tr>
                    <td>
                        <a class="cdpet-share-btn" href="{{$email->shareonfacebook}}">Join Our Network</a>
                        <a class="cdpet-post-btn" href="{{$email->viewfacebookpost}}">View Facebook Post</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table bgcolor="#222222" align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="padding: 15px;">
                <tr>
                    <td valign="top" width="400">
                        <p style="color: #fff; font-size: 16px;">Have questions about how PawMaw works?</p>
                        <p style="color: #fff; font-size: 16px;">
                            Check out our website
                            <a href="#" class="site-link">www.PawMaw.com</a>
                        </p>
                        <h3 class="address-title">Address</h3>
                        <p class="address-details">Los Angeles County, California, USA.</p>
                        <p><img src="{{URL::asset('email_image/email-icon.png')}}" alt="E-mail"><a href="#" class="address-email">info@pawmaw.com</a></p>
                    </td>
                    <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                    <td width="180">
                        <a href="#" class="call_now-btn">
                            <img class="icon-phone" src="{{URL::asset('email_image/phone.png')}}" alt="">
                            <span>Call now</span>
                        </a>
                        <p class="phone-num"><span>+1</span> <span>909</span> <span>764</span> <span>0024</span></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>