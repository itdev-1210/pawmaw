<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Download Flyer</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <style type="text/css">
        html {
            position: relative;
            min-height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            min-width: 100%;
            width: 100% !important;
            height: 100% !important;
            box-sizing: border-box;
            background-color: #f2f2f2;
            font-family: 'Roboto', sans-serif;
        }

        h1,
        h2,
        h3,
        h4,
        p,
        a {
            margin: 0;
            padding: 0;
        }

        a {
            text-decoration: none;
        }

        @font-face {
            font-family: 'Hobo Std Medium';
            src: url('fonts/hobo/Hobo Std Medium.ttf');
        }

        .flyer-title {
            color: #e53849;
            font-size: 20px;
            font-family: 'Hobo Std Medium';
            text-align: center;
        }

        .flyer-title_sub {
            color: #242122;
            font-size: 20px;
            font-family: 'Roboto', sans-serif;
            font-weight: 700;
            text-align: center;
            margin: 20px 0 10px 0;
        }

        .download-flyer_text {
            color: #242122;
            font-size: 20px;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            text-align: center;
        }

        .download-flyer_btn {
            color: #ffffff;
            font-size: 20px;
            font-family: 'Hobo Std Medium';
            text-align: center;
            text-transform: capitalize;
            padding: 10px 10px 8px 10px;
            border-radius: 5px;
            background-color: #ff002f;
            width: 35%;
            display: block;
            cursor: pointer;
            box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
        }

        .download-flyer_btn:hover {
            background-color: #df1d41;
        }

        .foot-title,
        .foot-info,
        .foot-address {
            color: #ffffff;
            font-size: 16px;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            text-align: left;
            line-height: 1.6;
        }

        .foot-link {
            color: #df1d41;
            cursor: pointer;
            transition: all 0.4s;
        }

        .foot-link:hover,
        .foot-info:hover,
        .foot-bottom_link a:hover {
            color: #b81f3b;
        }

        .call-now_btn {
            color: #ffffff;
            font-size: 16px;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            text-align: center;
            text-transform: capitalize;
            cursor: pointer;
            background-color: #df1d41;
            padding: 8px 10px 5px 10px;
            border-radius: 5px;
            transition: all 0.4s;
        }

        .call-now_btn:hover {
            background-color: #b81f3b;
        }

        .call-now_btn i {
            margin-right: 5px;
        }

        .contact-num {
            color: #ffffff;
            font-size: 16px;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            text-align: right;
            display: block;
            padding: 10px 0;
        }

        .foot-add_title {
            color: #df1d41;
            font-size: 16px;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            margin-top: 20px;
        }

        .foot-info {
            transition: all 0.2s;
        }

        .foot-info i {
            color: #df1d41;
            font-size: 16px;
            margin-right: 10px;
        }

        .foot-bottom_link {
            color: #ffffff;
            font-size: 16px;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            text-align: center;
            padding: 15px 0;
        }

        .foot-bottom_link a {
            color: #ffffff;
            text-align: center;
            text-transform: capitalize;
            transition: all 0.2s;
        }

        .download_flyer_email {
            width: 100% !important;
        }


        /*      Responsive css start       */

        @media(max-width:1024px) and (min-width:992px) {
            .download_flyer_email {
                width: 60%;
            }
        }

        @media(max-width:991px) and (min-width:768px) {
            .download_flyer_email {
                width: 75%;
            }
        }

        @media(max-width:767px) and (min-width:576px) {
            .download_flyer_email {
                width: 95%;
            }
        }

        @media(max-width:575px) {
            .download_flyer_email {
                width: 95%;
            }

            .download-flyer_btn {
                width: 60%;
            }


            .call-now_btn {
                display: inline-block;
            }
            .foot-info i {
                font-size: 12px;
                margin-right: 4px;

            }

            .foot-info {
                font-size: 12px;
            }

            .foot-title,
            .foot-address {
                font-size: 15px;
            }
            .blog-icon {
                padding-left: 12px;
                width: 100%;

            }
            .blog-btn {
                width: 50%;
            }
        }

        /*      Responsive css end        */

    </style>
</head>

<body>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="download_flyer_email" style="box-sizing: border-box; border-collapse: separate;">
        <tr>
            <td align="center">
                <table bgcolor="#df1d41" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 5px 0 8px 0; box-sizing: border-box; border-collapse: separate;">
                    <!-- HEADER -->
                    <tr>
                        <td align="center">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 10px 0 15px 0; box-sizing: border-box; border-collapse: separate;">
                                <tr>
                                    <td align="center">
                                        <img src="{{ URL::asset('/email-template/logo.png') }}" alt="PawMaw" class="logo">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- HEADER -->

                    <!-- CONTENT -->
                    <tr>
                        <td align="center">
                            <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 10px 0; box-sizing: border-box; border-collapse: separate;">
                                <tr>
                                    <td valign="top" align="center">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 10px; margin: 20px 0 5px 0; box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td valign="top" align="center">
                                                    <h1 class="flyer-title">Flyer Update</h1>
                                                    <h2 class="flyer-title_sub">Download a Printable Lost & Found Flyer of {{ $pet->name }}.</h2>
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                                <!-- /content title -->

                                <tr>
                                    <td valign="middle" align="center">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 10px; margin: 5px 0 20px 0; box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td valign="middle" align="center">
                                                    <img src="{{ url('uploads/pets_image') }}/{{ $pet->photo }}" alt="Download Flyer Icon" class="download-flyer_icon" style="width:200px; height:200px">
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                                <!-- /content icon -->

                                <tr>
                                    <td valign="bottom" align="center">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 10px; margin: 5px 0 20px 0; box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td valign="bottom" align="center">
                                                    <p class="download-flyer_text">Hang this poster in your neighborhood to boost your search. Click the below button to download your flyer.</p>
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                                <!-- /content text -->

                                <tr>
                                    <td valign="bottom" align="center">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 10px; box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td valign="bottom" align="center">
                                                    <a style='color:#fff' href="https://www.pawmaw.com/user/download-flyer/{{ $pet->id}}" class="download-flyer_btn">Download Flyer</a>
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                                <!-- /content button -->

                                <tr>
                                    <td valign="bottom" align="center">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="60%" style="padding-top: 30px; box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td valign="bottom" align="center" valign="bottom">
                                                    <hr style="border: 1px solid #dddddd; margin: 0; padding: 0;">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- /content hr -->
                            </table>
                        </td>
                    </tr>
                    <!-- CONTENT -->
                </table>
            </td>
        </tr>

        <!-- FOOTER -->
        <tr>
            <td align="center">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="box-sizing: border-box; border-collapse: separate;">
                    <tr>
                        <td align="center">
                            <table bgcolor="#222222" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 10px 15px; box-sizing: border-box; border-collapse: separate;">
                                <tr>
                                    <td align="left">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td align="left">
                                                    <h2 class="foot-title">Have questions about how PawMaw works?</h2>
                                                    <h2 class="foot-title">Check out our Website <a href="https://www.pawmaw.com/" style='color:#fff' class="foot-link">www.PawMaw.com</a></h2>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- /footer title -->
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table bgcolor="#222222" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0px 15px 10px 15px; box-sizing: border-box; border-collapse: separate;">

                                <tr>
                                    <td align="left" width="50%">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td align="left">
                                                    <p class="foot-add_title">Address:</p>
                                                    <h2 class="foot-address">Los Angeles County, California, USA.</h2>
                                                    <a href="mailto:info@pawmaw.com" style='color:#fff' class="foot-info"><i class="fas fa-envelope"></i> info@pawmaw.com</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="right" width="50%">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td align="right">
                                                    <a href="tel:+1 909 764 0024" class="call-now_btn" style='color:#fff'><i class="fas fa-phone"></i> Call Now</a>
                                                    <p class="contact-num">+1 909 764 0024</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- /footer address and contact number -->
                            </table>
                        </td>
                    </tr>
                    <!-- /footer top -->

                    <tr>
                        <td align="center">
                            <table bgcolor="#111111" border="0" cellpadding="0" cellspacing="0" width="100%" style="box-sizing: border-box; border-collapse: separate;">
                                <tr>
                                    <td align="center">
                                        <p class="foot-bottom_link"><a href="https://www.pawmaw.com/privacy-policy" style='color:#fff'>Privacy Policy</a> <span>|</span> <a style='color:#fff' href="https://www.pawmaw.com/unsubscribe?email={{ $pet->contact_email }}">Unsubscribe</a></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- /footer bottom-->
                </table>
            </td>
        </tr>
        <!-- FOOTER -->
    </table>
</body>

</html>
