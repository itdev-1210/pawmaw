<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Still Missing</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <style type="text/css">
        html {position: relative; min-height: 100%; font-size: 62.5%;}
        body { margin: 0; padding: 0; min-width: 100%; width: 100% !important; height: 100% !important; box-sizing: border-box; background-color: #f2f2f2; font-family: 'Montserrat', sans-serif;}
        h1,h2,h3 {margin: 0; padding: 0;}
        .email-logo {
            margin: 15px auto; display: block;
        }
        .alert-btn {
            color: #0b0b0b;
            font-size: 18px;
            background-color: #ffffff;
            border-radius: 5px;
            padding: 10px 30px;
            text-align: center;
            text-transform: capitalize;
            text-decoration: none;
            cursor: pointer;
            transition: all .2s;
        }
        .alert-btn:hover {
            color: #000000;
            background-color: #ededed;
        }
        .site-link {
            font-size: 18px;
            color: #ea4b4f !important;
            text-decoration: none;
            cursor: pointer;
            transition: all .2s;
        }
        .site-link:hover {
            color: #dc2227 !important;;
        }
        .call_now-btn {
            border-radius: 10px;
            text-decoration: none;
            border: none;
            background-color: #e43f44;
            position: relative;
            overflow: hidden;
            cursor: pointer;
            padding: 10px 30px;
            margin-top: 20px;
            margin-bottom: 10px !important;
            transition: all .2s;
        }
        .icon-phone {
            height: 20px;
            width: 20px;
            fill: #fff;
            position: absolute;
            bottom: 8px;
            left: 8px;
        }
        .call_now-btn span {
            font-size: 16px;
            font-weight: 700;
            color: #FFFFFF;
            text-transform: capitalize;
        }
        .call_now-btn:hover,
        .call_now-btn:active {
            background-color: #dc2227;
        }
        .phone-num {
            color: #FFFFFF !important;
            font-size: 16px;
            font-weight: 400;
        }
        .address-title {
            font-size: 20px;
            color: #ea4b4f !important;
            font-weight: 400;
        }
        .address-details {
            color: #FFFFFF !important;
            font-size: 16px;
        }
        .address-email {
            margin-left: 10px;
            color: #FFFFFF !important;
            font-size: 16px;
            text-decoration: none;
        }
        .footer-text {
            color: #FFFFFF;
            font-size: 16px;
            text-align: center !important;
        }
        .footer-text a {
            color: #FFFFFF;
            text-decoration: none;
            transition: all .2s;
        }
        .footer-text a:hover {
            color: #ea4b4f;
        }
        .footer-text span {
            color: #ea4b4f !important;
        }
    </style>
</head>
<body style="margin: 0; padding: 0; font-family: 'Montserrat', sans-serif;">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="box-sizing: border-box;">
    <tr>
        <td bgcolor="#ffffff">
            <img src="{{URL::asset('email_image/xs-logo.png')}}" alt="Pawmaw" width="139" height="30" class="email-logo"/>
        </td>
    </tr>

    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="#dad6d6" style="border-collapse: separate;  padding: 10px 0; box-sizing: border-box;"  class="">
                <tr>
                    <td align="center" valign="top">
                        <table width="600" height="464" background="{{URL::asset('email_image/missing-bg.png')}}" style="padding: 20px 5px; box-sizing: border-box; background-position: center; background-repeat: no-repeat;">
                            <tr>
                                <td align="center">
                                    <h1 style="font-size: 35px; font-weight: 700; color:#ffffff; text-transform: uppercase; text-align: center; margin: 20px 0;">
                                        Is  {{$email->name}}  still missing?
                                    </h1>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <img src="{{URL::asset('email_image/missing-circle.png')}}" alt="Missing Circle" width="254" height="254" style="margin-bottom: 20px;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td align="center" valign="top" bgcolor="#EFEFEF">
            <table width="600" style="padding: 20px 2px; box-sizing: border-box;">
                <tr>
                    <td align="center">
                        <h2 style="font-size: 14px; font-weight: 400; color:#0b0b0b; text-transform: uppercase; text-align: center; margin: 20px 0;">
                            We sincerely hope <span style="color: #ec4b50;"> {{$email->name}} </span> is back home with you, safe &amp; sound.
                        </h2>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="middle">
                        <h2 style="font-size: 16px; font-weight: 400; color:#0b0b0b; text-align: center; margin-bottom: 20px;">
                            If you are still searching... <br>
                            Please send PawAlert in your nearest area.
                        </h2>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td align="center" valign="top" bgcolor="ec4b50">
            <table width="600" style="padding: 20px; box-sizing: border-box;">
                <tr>
                    <td align="left" valign="top">
                        <img src="{{URL::asset('email_image/cat-circle.png')}}" alt="Missing Cat" width="125" height="125" style="margin: 20px;">
                    </td>
                    <td align="right" valign="top">
                        <h2 style="font-size: 24px; font-weight: 700; color:#ffffff; text-align: justify-all; margin: 30px 0;">
                            Is  {{$email->name}}  still missing?
                        </h2>
                        <a href="#" class="alert-btn">
                            <strong>Send Alert</strong> in your area
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td align="center" valign="top" bgcolor="30ae30">
            <table width="600" style="padding: 20px; box-sizing: border-box;">
                <tr>
                    <td align="left" valign="top">
                        <img src="{{URL::asset('email_image/dog-circle.png')}}" alt="Missing Dog" width="125" height="125" style="margin: 20px;">
                    </td>
                    <td align="right" valign="top">
                        <h2 style="font-size: 24px; font-weight: 700; color:#ffffff; text-align: justify-all; margin: 30px 0;">
                            Has  {{$email->name}}  been reunited with u?
                        </h2>
                        <a href="{{url('/user/my-account')}}" class="alert-btn">
                            Mark as <strong>reunited</strong>
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td align="center" valign="top" bgcolor="efefef">
            <table width="600" style="padding: 20px; box-sizing: border-box;">
                <tr>
                    <td align="center" valign="top">
                        <h2 style="font-size: 28px; font-weight: 700; color:#000000; text-transform: uppercase; text-align: center; margin-top: 10px; margin-bottom: 20px;">
                            Cheers <span>!!!</span>
                        </h2>
                        <h2 style="font-size: 22px; font-weight: 400; color:#0b0b0b; text-align: center; margin-top: 20px; margin-bottom: 10px;">
                            Sincerely, <br>
                            <strong style="color: #ec4b50;">The PAWMAW Team.</strong>
                        </h2>
                        <hr style="height: 2px; width: 80%; margin-left: auto; margin-right: auto; background-color: #b3b6b9; border: 0;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <table bgcolor="#222222" align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="padding: 15px;">
                <tr>
                    <td valign="top" width="400">
                        <p style="color: #fff; font-size: 16px;">Have questions about how PawMaw works?</p>
                        <p style="color: #fff; font-size: 16px;">
                            Check out our website
                            <a href="#" class="site-link">www.PawMaw.com</a>
                        </p>
                        <h3 class="address-title">Address</h3>
                        <p class="address-details">Los Angeles County, California, USA.</p>
                        <p><img src="{{URL::asset('email_image/email-icon.png')}}" alt="E-mail"><a href="#" class="address-email">info@pawmaw.com</a></p>
                    </td>
                    <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                    <td width="180">
                        <a href="#" class="call_now-btn">
                            <img class="icon-phone" src="{{URL::asset('email_image/phone.png')}}" alt="">
                            <span>Call now</span>
                        </a>
                        <p class="phone-num"><span>+1</span> <span>909</span> <span>764</span> <span>0024</span></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table bgcolor="#111111" align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="padding:2px 15px;">
                <tr>
                    <td>
                        <p class="footer-text"><a href="#">Privacy Policy</a> <span>|</span> <a href="#">Unsubscribe</a></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
