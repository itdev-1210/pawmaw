<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Contact Email</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <style type="text/css">
        .label-1 {
            font-size: 18px; color: #ea4b4f;
        }
        .label-2 {
            font-size: 18px; color: #0b0b0b;
        }
        .site-link {
            font-size: 18px;
            color: #ea4b4f !important;
            text-decoration: none;
            cursor: pointer;
            transition: all .2s;
        }
        .site-link:hover {
            color: #dc2227 !important;
        }
        .call_now-btn {
            border-radius: 10px;
            text-decoration: none;
            border: none;
            background-color: #e43f44;
            position: relative;
            overflow: hidden;
            cursor: pointer;
            padding: 10px 30px;
            margin-top: 20px;
            margin-bottom: 10px !important;
            transition: all .2s;
        }
        .icon-phone {
            height: 20px;
            width: 20px;
            fill: #fff;
            position: absolute;
            bottom: 8px;
            left: 8px;
        }
        .call_now-btn span {
            font-size: 16px;
            font-weight: 700;
            color: #fff;
            text-transform: capitalize;
        }
        .call_now-btn:hover,
        .call_now-btn:active {
            background-color: #dc2227;
        }
        .phone-num {
            color: #fff !important;
            font-size: 16px;
            font-weight: 400;
        }
        .address-title {
            font-size: 20px;
            color: #ea4b4f !important;
            font-weight: 400;
        }
        .address-details {
            color: #fff !important;
            font-size: 16px;
        }
        .address-email {
            margin-left: 10px;
            color: #fff !important;
            font-size: 16px;
            text-decoration: none;
        }
        .footer-text {
            color: #fff;
            font-size: 16px;
            text-align: center !important;
        }
        .footer-text a {
            color: #fff;
            text-decoration: none;
            transition: all .2s;
        }
        .footer-text a:hover {
            color: #ea4b4f;
        }
        .footer-text span {
            color: #ea4b4f !important;
        }
    </style>
</head>
<body style="margin: 0; padding: 0; font-family: 'Montserrat', sans-serif; box-sizing: border-box;">
<table align="center" border="0" cellpadding="0" cellspacing="0" style="box-sizing: border-box;">
    <tr>
        <td align="center">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" background="{{URL::asset('email_image/bg.png')}}" style="box-sizing: border-box; padding: 10px;">
                <tr>
                    <td>
                        <img src="{{URL::asset('email_image/logo.png')}}" alt="Pawmaw" width="406" height="96" style="margin: 15px auto; display: block;" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="570" bgcolor="#ffffff" style="box-sizing: border-box; padding: 2px 10px; border-radius: 5px; margin-left: auto; margin-right: auto; margin-bottom: 20px; border-collapse: separate;">
                            <tr>
                                <td>
                                    <h1 style="font-size: 38px; font-weight: 700; color:#ea4b4f; text-align: center;">
                                        Message From Contact Form
                                        <img src="{{URL::asset('email_image/alert-icon.png')}}" alt="Alert">
                                    </h1>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="570" bgcolor="#ffffff" style="box-sizing: border-box; padding: 5px; border-collapse: separate;border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; margin-left: auto; margin-right: auto;">
                            <tr>
                                <td>
                                    <h2 style="font-size: 20px; font-weight: 700; color: #0b0b0b">
                                        Looks like someone might have found your pet!
                                    </h2>
                                    <h3 style="font-size: 20px; font-weight: 700; color: #ea4b4f">
                                        Information We Received:
                                    </h3>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" style="width: 560px; height: 100%;">
                                        <tr>
                                            <td valign="top" style="width: 80px">
                                                <p class="label-1">Name</p>
                                            </td>
                                            <td valign="top" style="width: 20px">
                                                <p class="label-2">:</p>
                                            </td>
                                            <td valign="top" style="width: 470px">
                                                <p class="label-2">{{$email->name}}</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="width: 80px">
                                                <p class="label-1">Phone</p>
                                            </td>
                                            <td valign="top" style="width: 20px">
                                                <p class="label-2">:</p>
                                            </td>
                                            <td valign="top" style="width: 470px">
                                                <p class="label-2">{{ $email->phone}}</p>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td valign="top" style="width: 80px">
                                                <p class="label-1">Email</p>
                                            </td>
                                            <td valign="top" style="width: 20px">
                                                <p class="label-2">:</p>
                                            </td>
                                            <td valign="top" style="width: 470px">
                                                <p class="label-2">{{ $email->email}}</p>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td valign="top" style="width: 80px">
                                                <p class="label-1">Address</p>
                                            </td>
                                            <td valign="top" style="width: 20px">
                                                <p class="label-2">:</p>
                                            </td>
                                            <td valign="top" style="width: 470px">
                                                <p valign="top" class="label-2">
                                                    {{$email->address}}
                                                </p>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td valign="top" style="width: 80px">
                                                <p class="label-1">Message</p>
                                            </td>
                                            <td valign="top" style="width: 20px">
                                                <p class="label-2">:</p>
                                            </td>
                                            <td valign="top" style="width: 470px">
                                                <p valign="top" class="label-2">
                                                    {{$email->message}}
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <table bgcolor="#222222" align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="padding: 15px;">
                <tr>
                    <td valign="top" width="400">
                        <p style="color: #fff; font-size: 16px;">Have questions about how PawMaw works?</p>
                        <p style="color: #fff; font-size: 16px;">
                            Check out our website
                            <a href="#" class="site-link">www.PawMaw.com</a>
                        </p>
                        <h3 class="address-title">Address</h3>
                        <p class="address-details">Los Angeles County, California, USA.</p>
                        <p><img src="{{URL::asset('email_image/email-icon.png')}}" alt="E-mail"><a href="#" class="address-email">info@pawmaw.com</a></p>
                    </td>
                    <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                    <td width="180">
                        <a href="#" class="call_now-btn">
                            <img class="icon-phone" src="{{URL::asset('email_image/phone.png')}}" alt="">
                            <span>Call now</span>
                        </a>
                        <p class="phone-num"><span>+1</span> <span>909</span> <span>764</span> <span>0024</span></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
