<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>verification</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
	<style type="text/css">
		html {position: relative; min-height: 100%; font-size: 62.5%;}
		body { margin: 0; padding: 0; min-width: 100%; width: 100% !important; height: 100% !important; box-sizing: border-box; background-color: #f2f2f2; font-family: 'Montserrat', sans-serif;}
		h1,h2,h3 {margin: 0; padding: 0;}
		.email-logo {
			margin: 15px auto; display: block;
		}
		.welcome-wrap {
			padding: 10px 15px;
			border-radius: 5px;
			margin-bottom: 20px;
			width: 550px;
			margin-left: auto;
			margin-right: auto;
			display: block;
		}
		.welcome-title {
			font-size: 48px; font-weight: 400; color:#0b0b0b; text-align: center;
		}
		.welcome-details {
			font-size: 20px; font-weight: 400; color: #0b0b0b; text-align: justify; margin-bottom: 25px !important;
		}
		.welcome-con-acc {
			margin-left: auto !important;
			margin-right: auto !important;
			display: block;

			width: 170px;
			border-radius: 10px;
			text-decoration: none;
			border: none;
			background-color: #e43f44;
			color: #FFFFFF !important;
			font-size: 18px;
			font-weight: 700;
			text-transform: capitalize;
			text-align: center;
			position: relative;
			overflow: hidden;
			cursor: pointer;
			padding: 25px 45px;
			margin-bottom: 10px !important;
			transition: all .2s;
		}
		.welcome-con-acc:hover {
			background-color: #dc2227;
		}
		.welcome-cheers {
			font-size: 24px; font-weight: 400; color: #0b0b0b; text-align: left;
		}
		.site-link {
			font-size: 18px;
			color: #ea4b4f !important;
			text-decoration: none;
			cursor: pointer;
			transition: all .2s;
		}
		.site-link:hover {
			color: #dc2227 !important;;
		}
		.call_now-btn {
			border-radius: 10px;
			text-decoration: none;
			border: none;
			background-color: #e43f44;
			position: relative;
			overflow: hidden;
			cursor: pointer;
			padding: 10px 30px;
			margin-top: 20px;
			margin-bottom: 10px !important;
			transition: all .2s;
		}
		.icon-phone {
			height: 20px;
			width: 20px;
			fill: #fff;
			position: absolute;
			bottom: 8px;
			left: 8px;
		}
		.call_now-btn span {
			font-size: 16px;
			font-weight: 700;
			color: #FFFFFF;
			text-transform: capitalize;
		}
		.call_now-btn:hover,
		.call_now-btn:active {
			background-color: #dc2227;
		}
		.phone-num {
			color: #FFFFFF !important;
			font-size: 16px;
			font-weight: 400;
		}
		.address-title {
			font-size: 20px;
			color: #ea4b4f !important;
			font-weight: 400;
		}
		.address-details {
			color: #FFFFFF !important;
			font-size: 16px;
		}
		.address-email {
			margin-left: 10px;
			color: #FFFFFF !important;
			font-size: 16px;
			text-decoration: none;
		}
		.footer-text {
			color: #FFFFFF;
			font-size: 16px;
			text-align: center !important;
		}
		.footer-text a {
			color: #FFFFFF;
			text-decoration: none;
			transition: all .2s;
		}
		.footer-text a:hover {
			color: #ea4b4f;
		}
		.footer-text span {
			color: #ea4b4f !important;
		}
	</style>
</head>
<body style="margin: 0; padding: 0; font-family: 'Montserrat', sans-serif;">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
	<tr>
		<td align="center">
			<table align="center" border="0" cellpadding="0" cellspacing="0" width="600"  background="{{URL::asset('email_image/bg.png')}}" style="padding: 15px; box-sizing: border-box; background-position: center; background-repeat: no-repeat;">
				<tr>
					<td>
						<img src="{{URL::asset('email_image/logo.png')}}" alt="Pawmaw" width="406" height="96" class="email-logo"/>
					</td>
				</tr>
				<tr>
					<td>
						<table width="550" style="border-collapse: separate" bgcolor="#ffffff" class="welcome-wrap">
							<tr>
								<td valign="top">
									<h1 class="welcome-title">
										Welcome!
									</h1>
								</td>
							</tr>
							<tr>
								<td valign="top">
									<p class="welcome-details">
										We're excited to have you get started. First, you need to confirm your account. Just press the button below.
									</p>
								</td>
							</tr>
							<tr>
								<td valign="top">
									<a href="{{route('apiverify',$user->verification_token)}}" class="welcome-con-acc">Confirm Account</a>
								</td>
							</tr>
							<tr>
								<td valign="top">
									<p class="welcome-details">
										If you have any questions, just reply to this email— we're alway happy to help out.
									</p>
								</td>
							</tr>
							<tr>
								<td valign="top">
									<p class="welcome-cheers">
										Cheers, <br>The PawMaw Team.
									</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td>
			<table bgcolor="#222222" align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="padding: 15px;">
				<tr>
					<td valign="top" width="400">
						<p style="color: #fff; font-size: 16px;">Have questions about how PawMaw works?</p>
						<p style="color: #fff; font-size: 16px;">
							Check out our website
							<a href="#" class="site-link">www.PawMaw.com</a>
						</p>
						<h3 class="address-title">Address</h3>
						<p class="address-details">Los Angeles County, California, USA.</p>
						<p><img src="{{URL::asset('email_image/email-icon.png')}}" alt="E-mail"><a href="#" class="address-email">info@pawmaw.com</a></p>
					</td>
					<td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
					<td width="180">
						<a href="#" class="call_now-btn">
							<img class="icon-phone" src="{{URL::asset('email_image/phone.png')}}" alt="">
							<span>Call now</span>
						</a>
						<p class="phone-num"><span>+1</span> <span>909</span> <span>764</span> <span>0024</span></p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
