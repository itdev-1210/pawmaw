<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Delete</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <style type="text/css">
        html {
            position: relative;
            min-height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            min-width: 100%;
            width: 100% !important;
            height: 100% !important;
            box-sizing: border-box;
            background-color: #f2f2f2;
            font-family: 'Roboto', sans-serif;
        }

        h1,
        h2,
        h3,
        h4,
        p,
        a {
            margin: 0;
            padding: 0;
        }

        a {
            text-decoration: none;
        }

        @font-face {
            font-family: 'Hobo Std Medium';
            src: url('fonts/hobo/Hobo Std Medium.ttf');
        }

        .report-again_btn {
            color: #ffffff;
            font-size: 20px;
            font-family: 'Hobo Std Medium';
            text-align: center;
            text-transform: capitalize;
            padding: 10px 10px 8px 10px;
            border-radius: 5px;
            background-color: #ff002f;
            width: 35%;
            display: block;
            margin-top: 30px;
            transition: all 0.4s;
            cursor: pointer;
            box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
        }

        .report-again_btn:hover {
            background-color: #df1d41;
        }

        .delete-text {
            color: #222222;
            font-size: 20px;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            text-align: center;
        }

        .delete-title {
            color: #df1d41;
            font-size: 25px;
            font-family: 'Hobo Std Medium', Roboto;
            border-bottom: 2px solid #b81f3b;
            display: inline-block;
            padding-bottom: 5px;
            text-transform: capitalize;
        }

        .foot-title,
        .foot-info,
        .foot-address {
            color: #ffffff;
            font-size: 16px;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            text-align: left;
            line-height: 1.6;
        }

        .foot-link {
            color: #df1d41;
            cursor: pointer;
            transition: all 0.4s;
        }

        .foot-link:hover,
        .foot-info:hover,
        .foot-bottom_link a:hover {
            color: #b81f3b;
        }

        .call-now_btn {
            color: #ffffff;
            font-size: 16px;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            text-align: center;
            text-transform: capitalize;
            cursor: pointer;
            background-color: #df1d41;
            padding: 8px 10px 5px 10px;
            border-radius: 5px;
            transition: all 0.4s;
        }

        .call-now_btn:hover {
            background-color: #b81f3b;
        }

        .call-now_btn i {
            margin-right: 5px;
        }

        .contact-num {
            color: #ffffff;
            font-size: 16px;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            text-align: right;
            display: block;
            padding: 10px 0;
        }

        .foot-add_title {
            color: #df1d41;
            font-size: 16px;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            margin-top: 20px;
        }

        .foot-info {
            transition: all 0.2s;
        }

        .foot-info i {
            color: #df1d41;
            font-size: 16px;
            margin-right: 10px;
        }

        .foot-bottom_link {
            color: #ffffff;
            font-size: 16px;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            text-align: center;
            padding: 15px 0;
        }

        .foot-bottom_link a {
            color: #ffffff;
            text-align: center;
            text-transform: capitalize;
            transition: all 0.2s;
        }

        .delete_email {
            width: 100%;
        }

        /*      Responsive css start       */

        @media(max-width:1024px) and (min-width:992px) {
            .delete_email {
                width: 60%;
            }
        }

        @media(max-width:991px) and (min-width:768px) {
            .delete_email {
                width: 75%;
            }
        }

        @media(max-width:767px) and (min-width:576px) {
            .delete_email {
                width: 95%;
            }
        }

        @media(max-width:575px) {
            .delete_email {
                width: 95%;
            }
            .report-again_btn {
                width: 75%;
            }

            .call-now_btn {
                display: inline-block;
            }
            .foot-info i {
                font-size: 12px;
                margin-right: 4px;

            }

            .foot-info {
                font-size: 12px;
            }

            .foot-title,
            .foot-address {
                font-size: 15px;
            }
            .blog-icon {
                padding-left: 12px;
                width: 100%;

            }
            .blog-btn {
                width: 50%;
            }
        }

        /*      Responsive css end        */

    </style>
</head>

<body>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="delete_email" style="box-sizing: border-box; border-collapse: separate;">
        <tr>
            <td align="center">
                <table bgcolor="#df1d41" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 5px 0 8px 0; box-sizing: border-box; border-collapse: separate;">
                    <!-- HEADER -->
                    <tr>
                        <td align="center">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 10px 0 15px 0; box-sizing: border-box; border-collapse: separate;">
                                <tr>
                                    <td align="center">
                                        <img src="{{URL::asset('/email-template/logo.png') }}" alt="PawMaw" class="logo">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- HEADER -->

                    <!-- CONTENT -->
                    <tr>
                        <td align="center">
                            <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 10px 0; box-sizing: border-box; border-collapse: separate;">
                                <tr>
                                    <td valign="top" align="center">
                                        <table border="0" cellpadding="0" cellspacing="0" width="55%" style="padding: 20px 0; box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td align="center">
                                                    <h1 class="delete-title">Your Post Has Been Deleted</h1>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- /delete title -->

                                <tr>
                                    <td valign="middle" align="center">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 20px 30px; box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td valign="middle" align="center">
                                                    <p class="delete-text">Unfortunately we have removed your post because your post contains a wrong picture or wrong information.</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- /delete text -->

                                <tr>
                                    <td valign="bottom" align="center">
                                        <table border="0" cellpadding="0" cellspacing="0" width="90%" style="padding: 20px 40px; box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td valign="bottom" align="center">
                                                    <p class="delete-text">You may post again with proper picture & information from here.</p>
                                                    <a href="https://www.pawmaw.com/" class="report-again_btn">Report Again</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- /delete text -->

                                <tr>
                                    <td align="center">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="40%" style="box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td align="center">
                                                    <hr style="border: 1px solid #dddddd; margin: 0; padding: 0;">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- /content hr -->
                            </table>
                        </td>
                    </tr>
                    <!-- CONTENT -->
                </table>
            </td>
        </tr>

  <!-- FOOTER -->
        <tr>
            <td align="center">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="box-sizing: border-box; border-collapse: separate;">
                    <tr>
                        <td align="center">
                            <table bgcolor="#222222" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 10px 15px; box-sizing: border-box; border-collapse: separate;">
                                <tr>
                                    <td align="left">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td align="left">
                                                    <h2 class="foot-title">Have questions about how PawMaw works?</h2>
                                                    <h2 class="foot-title">Check out our Website <a href="https://www.pawmaw.com/" style='color:#fff' class="foot-link">www.PawMaw.com</a></h2>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- /footer title -->
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table bgcolor="#222222" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0px 15px 10px 15px; box-sizing: border-box; border-collapse: separate;">

                                <tr>
                                    <td align="left" width="50%">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td align="left">
                                                    <p class="foot-add_title">Address:</p>
                                                    <h2 class="foot-address">Los Angeles County, California, USA.</h2>
                                                    <a href="mailto:info@pawmaw.com" style='color:#fff' class="foot-info"><i class="fas fa-envelope"></i> info@pawmaw.com</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="right" width="50%">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td align="right">
                                                    <a href="tel:+1 909 764 0024" class="call-now_btn" style='color:#fff'><i class="fas fa-phone"></i> Call Now</a>
                                                    <p class="contact-num">+1 909 764 0024</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- /footer address and contact number -->
                            </table>
                        </td>
                    </tr>
                    <!-- /footer top -->

                    <tr>
                        <td align="center">
                            <table bgcolor="#111111" border="0" cellpadding="0" cellspacing="0" width="100%" style="box-sizing: border-box; border-collapse: separate;">
                                <tr>
                                    <td align="center">
                                        <p class="foot-bottom_link"><a href="https://www.pawmaw.com/privacy-policy" style='color:#fff'>Privacy Policy</a> <span>|</span> <a style='color:#fff' href="https://www.pawmaw.com/unsubscribe?email={{ $pet->contact_email }}">Unsubscribe</a></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- /footer bottom-->
                </table>
            </td>
        </tr>
        <!-- FOOTER -->
    </table>
</body>

</html>
