<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Found Blog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <style type="text/css">
        html {
            position: relative;
            min-height: 100%;
            font-size: 62.5%;
        }
        body {
            margin: 0;
            padding: 0;
            min-width: 100%;
            width: 100% !important;
            height: 100% !important;
            box-sizing: border-box;
            background-color: #f2f2f2;
            font-family: 'Montserrat', sans-serif;
        }

        h1,h2,h3 {margin: 0; padding: 0;}

        .email-logo {
            padding: 1.8rem 1rem;
            margin-bottom: 2rem;
        }

        .email-logo img {
            margin-left: auto;
            margin-right: auto;
            display: block;
        }

        .cdtop-ten {
            color: #ffffff;
            font-size: 5.8rem;
            font-weight: 700;
            text-align: center;
            text-transform: uppercase;
            margin-left: 5rem;
            font-family: 'Roboto', sans-serif;
        }
        .cdtitle {
            color: #ffffff;
            font-size: 3.2rem;
            font-weight: 400;
            text-align: center;
            max-width: 20rem;
            padding-left: 5rem;
            margin-left: auto !important;
            margin-right: auto !important;
            font-family: 'Roboto', sans-serif;
        }

        .blog-step-guide-link {
            color: #323232;
            font-size: 1.6rem;
            line-height: 1.8rem;
            font-weight: 400;
            text-align: center;
            margin-bottom: 1.5rem;
            transition: all .2s;
        }

        .blog-step-guide-link:hover {
            color: #dc2227;
        }

        .blog-dog-img {
            margin: 2rem auto;
            display: block;
        }

        .blog-view-btn {
            font-size: 1.6rem;
            line-height: 1.8rem;
            font-weight: 400;
            text-align: center;
            border-radius: 10px;
            text-transform: capitalize;
            text-decoration: none;
            border: none;
            color: #fff;
            background-color: #e43f44;
            position: relative;
            overflow: hidden;
            cursor: pointer;
            padding: 1.5rem 3.5rem;
            transition: all .2s;
        }

        .blog-view-btn:hover {
            background-color: #dc2227;
        }

        .blog-team {
            color: #323232;
            font-size: 1.4rem;
            line-height: 2rem;
            font-weight: 400;
            text-align: center;
        }

        .site-link {
            font-size: 18px;
            color: #ea4b4f;
            text-decoration: none;
            cursor: pointer;
            transition: all .2s;
        }

        .site-link:hover {
            color: #dc2227;
        }

        .call_now-btn {
            border-radius: 10px;
            text-decoration: none;
            border: none;
            background-color: #e43f44;
            position: relative;
            overflow: hidden;
            cursor: pointer;
            padding: 10px 30px;
            margin-top: 20px;
            margin-bottom: 10px !important;
            transition: all .2s;
        }

        .icon-phone {
            height: 20px;
            width: 20px;
            fill: #fff;
            position: absolute;
            bottom: 8px;
            left: 8px;
        }

        .call_now-btn span {
            font-size: 16px;
            font-weight: 700;
            color: #fff;
            text-transform: capitalize;
        }

        .call_now-btn:hover,
        .call_now-btn:active {
            background-color: #dc2227;
        }

        .phone-num {
            color: #fff !important;
            font-size: 16px;
            font-weight: 400;
        }

        .address-title {
            font-size: 20px;
            color: #ea4b4f !important;
            font-weight: 400;
        }

        .address-details {
            color: #fff !important;
            font-size: 16px;
        }

        .address-email {
            margin-left: 10px;
            color: #fff !important;
            font-size: 16px;
            text-decoration: none;
        }

        .footer-text {
            color: #fff;
            font-size: 16px;
            text-align: center !important;
        }

        .footer-text a {
            color: #fff;
            text-decoration: none;
            transition: all .2s;
        }

        .footer-text a:hover {
            color: #ea4b4f;
        }

        .footer-text span {
            color: #ea4b4f !important;
        }
    </style>
</head>
<body>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600"
       style="box-sizing: border-box; padding: 1rem;">
    <tr>
        <td bgcolor="#ffffff" class="email-logo">
            <img src="{{URL::asset('email_image/xs-logo.png')}}" alt="PawMaw" width="139" height="30">
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" bgcolor="#e1e1e1"
                   style="box-sizing: border-box; padding: 1rem 2rem; border-collapse: separate; border-spacing:0 2.5rem;">
                <tr>
                    <td background="{{URL::asset('email_image/blog-header-bg.png')}}" width="550" height="160"
                        style="box-sizing: border-box; padding: 1rem; margin-bottom: 2rem; background-position: center; background-repeat: no-repeat;">
                        <h1 class="cdtop-ten">Top <span>10</span></h1>
                        <h2 class="cdtitle">Ways to Find Your Lost Pet</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table bgcolor="#ffffff" style="box-sizing: border-box; padding: 1.5rem; border-spacing:0 1rem;">
                            <tr>
                                <td>
                                    <a href="#" class="blog-step-guide-link">
                                        Step by Step guide what you should do if you found any lost pet
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img class="blog-dog-img" src="{{URL::asset('email_image/blog-dog.png')}}" alt="DOG" width="520" height="338">
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 0; line-height: 0;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-size: 0; line-height: 0;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-size: 0; line-height: 0;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-size: 0; line-height: 0;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-size: 0; line-height: 0;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <a class="blog-view-btn" href="#">
                                        View guide
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <p class="blog-team">Cheers, <br>The PawMaw Team.</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table bgcolor="#222222" align="center" border="0" cellpadding="0" cellspacing="0" width="600"
                   style="box-sizing: border-box; padding: 1.5rem;">
                <tr>
                    <td valign="top" width="420">
                        <p style="color: #fff; font-size: 1.6rem;">Have questions about how PawMaw works?</p>
                        <p style="color: #fff; font-size: 1.6rem;">
                            Check out our website
                            <a href="#" class="site-link">www.PawMaw.com</a>
                        </p>
                        <h3 class="address-title">Address</h3>
                        <p class="address-details">Los Angeles County, California, USA.</p>
                        <p><img src="{{URL::asset('email_image/email-icon.png')}}"><a href="#" class="address-email">info@pawmaw.com</a></p>
                    </td>
                    <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                    <td width="160">
                        <a href="#" class="call_now-btn">
                            <img class="icon-phone" src="{{URL::asset('email_image/phone.png')}}" alt="">
                            <span>Call now</span>
                        </a>
                        <p class="phone-num"><span>+1</span> <span>909</span> <span>764</span> <span>0024</span></p>
                    </td>
                </tr>
                <tr>
                    <table bgcolor="#111111" align="center" border="0" cellpadding="0" cellspacing="0" width="600"
                           style="box-sizing: border-box; padding: 0.5rem 1.5rem;">
                        <tr>
                            <td>
                                <p class="footer-text"><a href="#">Privacy Policy</a> <span>|</span> <a href="#">Unsubscribe</a>
                                </p>
                            </td>
                        </tr>
                    </table>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
