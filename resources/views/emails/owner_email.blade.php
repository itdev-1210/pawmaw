<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Contact Owner</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <style type="text/css">
            html {
            position: relative;
            min-height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            min-width: 100%;
            width: 100% !important;
            height: 100% !important;
            box-sizing: border-box;
            background-color: #f2f2f2;
            font-family: 'Roboto', sans-serif;
        }

        h1,
        h2,
        h3,
        h4,
        p,
        a {
            margin: 0;
            padding: 0;
        }

        a {
            text-decoration: none;
        }

        @font-face {
            font-family: 'Hobo Std Medium';
            src: url('fonts/hobo/Hobo Std Medium.ttf');
        }

        .foot-title,
        .foot-info,
        .foot-address {
            color: #ffffff;
            font-size: 16px;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            text-align: left;
            line-height: 1.6;
        }

        .foot-link {
            color: #df1d41;
            cursor: pointer;
            transition: all 0.4s;
        }

        .foot-link:hover,
        .foot-info:hover,
        .foot-bottom_link a:hover {
            color: #b81f3b;
        }

        .call-now_btn {
            color: #ffffff;
            font-size: 16px;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            text-align: center;
            text-transform: capitalize;
            cursor: pointer;
            background-color: #df1d41;
            padding: 8px 10px 5px 10px;
            border-radius: 8px;
            transition: all 0.4s;
        }

        .call-now_btn:hover {
            background-color: #b81f3b;
        }

        .call-now_btn i {
            margin-right: 5px;
        }

        .contact-num {
            color: #ffffff;
            font-size: 16px;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            text-align: right;
            display: block;
            padding: 10px 0;
        }

        .foot-add_title {
            color: #df1d41;
            font-size: 16px;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            margin-top: 20px;
        }

        .foot-info {
            transition: all 0.2s;
        }

        .foot-info i {
            color: #df1d41;
            font-size: 16px;
            margin-right: 10px;
        }

        .foot-bottom_link {
            color: #ffffff;
            font-size: 16px;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            text-align: center;
            padding: 15px 0;
        }

        .foot-bottom_link a {
            color: #ffffff;
            text-align: center;
            text-transform: capitalize;
            transition: all 0.2s;
        }

       

      
        @media only screen and (max-width:575px) {
            .call-now_btn {
                display: inline-block;
            }
            .foot-info i {
                font-size: 12px;
                margin-right: 4px;

            }

            .foot-info {
                font-size: 12px;
            }

            .foot-title,
            .foot-address {
                font-size: 15px;
            }
            .full {
                display: block;
                width: 100%;
            }


        }
        .label-1 {
            font-size: 18px; color: #ea4b4f;
        }
        .label-2 {
            font-size: 18px; color: #0b0b0b;
        }
        .site-link {
            font-size: 18px;
            color: #ea4b4f !important;
            text-decoration: none;
            cursor: pointer;
            transition: all .2s;
        }
        .site-link:hover {
            color: #dc2227 !important;
        }
        .call_now-btn {
            border-radius: 10px;
            text-decoration: none;
            border: none;
            background-color: #e43f44;
            position: relative;
            overflow: hidden;
            cursor: pointer;
            padding: 10px 30px;
            margin-top: 20px;
            margin-bottom: 10px !important;
            transition: all .2s;
        }
        .icon-phone {
            height: 20px;
            width: 20px;
            fill: #fff;
            position: absolute;
            bottom: 8px;
            left: 8px;
        }
        .call_now-btn span {
            font-size: 16px;
            font-weight: 700;
            color: #fff;
            text-transform: capitalize;
        }
        .call_now-btn:hover,
        .call_now-btn:active {
            background-color: #dc2227;
        }
        .phone-num {
            color: #fff !important;
            font-size: 16px;
            font-weight: 400;
        }
        .address-title {
            font-size: 20px;
            color: #ea4b4f !important;
            font-weight: 400;
        }
        .address-details {
            color: #fff !important;
            font-size: 16px;
        }
        .address-email {
            margin-left: 10px;
            color: #fff !important;
            font-size: 16px;
            text-decoration: none;
        }
        .footer-text {
            color: #fff;
            font-size: 16px;
            text-align: center !important;
        }
        .footer-text a {
            color: #fff;
            text-decoration: none;
            transition: all .2s;
        }
        .footer-text a:hover {
            color: #ea4b4f;
        }
        .footer-text span {
            color: #ea4b4f !important;
        }
    </style>
</head>

<body style="margin: 0; padding: 0; font-family: 'Montserrat', sans-serif; box-sizing: border-box;">
<table align="center" border="0" cellpadding="0" cellspacing="0" style="box-sizing: border-box;">
    <tr>
        <td align="center">
            <table bgcolor="#df1d41" align="center" border="0" cellpadding="0" cellspacing="0" width="600" background="{{URL::asset('email_image/bg.png')}}" style="box-sizing: border-box; padding: 10px;">
               <!-- HEADER -->
                    <tr>
                        <td align="center">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 10px 0 15px 0; box-sizing: border-box; border-collapse: separate;">
                                <tr>
                                    <td align="center">
                                        <img src="{{ URL::asset('/email-template/logo.png') }}" alt="PawMaw" class="logo">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- HEADER -->                
                   <tr>
                    <td>
                        <table width="570" bgcolor="#ffffff" style="box-sizing: border-box; padding: 2px 10px; border-radius: 5px; margin-left: auto; margin-right: auto; margin-bottom: 20px; border-collapse: separate;">
                            <tr>
                                <td>
                                    <h1 style="font-size: 38px; font-weight: 700; color:#ea4b4f; text-align: center;">
                                        PawMaw Alert
                                    </h1>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="570" bgcolor="#ffffff" style="box-sizing: border-box; padding: 5px; border-collapse: separate;border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; margin-left: auto; margin-right: auto;">
                            <tr>
                                <td>
                                    <h2 style="font-size: 20px; font-weight: 700; color: #0b0b0b">
                                        Looks like someone might have found your pet!
                                    </h2>
                                    <h3 style="font-size: 20px; font-weight: 700; color: #ea4b4f">
                                        Information We Received:
                                    </h3>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" style="width: 560px; height: 100%;">
                                        <tr>
                                            <td valign="top" style="width: 80px">
                                                <p class="label-1">From</p>
                                            </td>
                                            <td valign="top" style="width: 20px">
                                                <p class="label-2">:</p>
                                            </td>
                                            <td valign="top" style="width: 470px">
                                                <p class="label-2">{{$email->email}}</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="width: 80px">
                                                <p class="label-1">Name</p>
                                            </td>
                                            <td valign="top" style="width: 20px">
                                                <p class="label-2">:</p>
                                            </td>
                                            <td valign="top" style="width: 470px">
                                                <p class="label-2">{{ $email->name}}</p>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td valign="top" style="width: 80px">
                                                <p class="label-1">Message</p>
                                            </td>
                                            <td valign="top" style="width: 20px">
                                                <p class="label-2">:</p>
                                            </td>
                                            <td valign="top" style="width: 470px">
                                                <p valign="top" class="label-2">
                                                    {{$email->message}}
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

           <!-- FOOTER -->
        <tr>
            <td align="center">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="box-sizing: border-box; border-collapse: separate;">
                    <tr>
                        <td align="center">
                            <table bgcolor="#222222" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 10px 15px; box-sizing: border-box; border-collapse: separate;">
                                <tr>
                                    <td align="left">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td align="left">
                                                    <h2 class="foot-title">Have questions about how PawMaw works?</h2>
                                                    <h2 class="foot-title">Check out our Website <a href="https://www.pawmaw.com/" style='color:#fff' class="foot-link">www.PawMaw.com</a></h2>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- /footer title -->
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table bgcolor="#222222" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 0px 15px 10px 15px; box-sizing: border-box; border-collapse: separate;">

                                <tr>
                                    <td align="left" width="50%">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td align="left">
                                                    <p class="foot-add_title">Address:</p>
                                                    <h2 class="foot-address">Los Angeles County, California, USA.</h2>
                                                    <a href="mailto:info@pawmaw.com" style='color:#fff' class="foot-info"><i class="fas fa-envelope"></i> info@pawmaw.com</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="right" width="50%">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="box-sizing: border-box; border-collapse: separate;">
                                            <tr>
                                                <td align="right">
                                                    <a href="tel:+1 909 764 0024" class="call-now_btn" style='color:#fff'><i class="fas fa-phone"></i> Call Now</a>
                                                    <p class="contact-num">+1 909 764 0024</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!-- /footer address and contact number -->
                            </table>
                        </td>
                    </tr>
                    <!-- /footer top -->

                    <tr>
                        <td align="center">
                            <table bgcolor="#111111" border="0" cellpadding="0" cellspacing="0" width="100%" style="box-sizing: border-box; border-collapse: separate;">
                                <tr>
                                    <td align="center">
                                        <p class="foot-bottom_link"><a href="https://www.pawmaw.com/privacy-policy" style='color:#fff'>Privacy Policy</a> <span>|</span> <a style='color:#fff' href="https://www.pawmaw.com/unsubscribe?email={{ $email->email }}">Unsubscribe</a></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- /footer bottom-->
                </table>
            </td>
        </tr>
        <!-- FOOTER -->
</table>
</body>
</html>
