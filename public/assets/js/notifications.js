var pawmaw = pawmaw || {};

function setCookie(cname, cvalue) {
    var d = new Date();
    d.setTime(d.getTime() + 600000);
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

pawmaw.notifications = {
    options: {
        notifications: [],
        times: [
            "About 2 Hours Ago",
            "About 1 Hour Ago",
            "About 45 Minutes Ago",
            "About 30 Minutes Ago",
            "About 5 Minutes Ago",
            "About 31 Minutes Ago",
            "About 34 Minutes Ago",
            "About 36 Minutes Ago",
            "About 37 Minutes Ago",
            "About 21 Minutes Ago",
            "About 22 Minutes Ago",
            "About 25 Minutes Ago",
            "About 26 Minutes Ago",
            "About 27 Minutes Ago",
            "About 29 Minutes Ago",
            "About 41 Minutes Ago",
            "About 46 Minutes Ago",
            "About 42 Minutes Ago",
            "About 47 Minutes Ago",
            "About 48 Minutes Ago",
            "About 52 Minutes Ago",
            "About 56 Minutes Ago",
            "About 1 Minute Ago",
            "About 53 Minutes Ago",
            "About 56 Minutes Ago",
        ],
        timesDays: [
            'After 1 Day',
            'After 2 Days',
            'After 3 Days',
            'After 4 Days',
            'After 5 Days',

        ],
        renderNotificationIndex: 0,
    },
    getNotifications: function(){
        var that = this;
        $.get('/api/notifications', function(data){
            that.options.notifications  = data;
            that.renderNotifications();
        });
    },
    renderNotifications: function(){
        var that = this;
        if(that.options.notifications.length == 0){
            return;
        }
        $('<div class="notificaions-container"></div>').appendTo('body');
        this.renderNotification(0);
        this.interval = setInterval(function(){
            that.options.renderNotificationIndex = that.options.renderNotificationIndex + 1;
            // remove old notification
            $('.notification-content').slideUp("slow", function(){
                $('.notification-content').remove();
            });
            
            if(that.options.notifications.length - 1 < that.options.renderNotificationIndex){
                that.options.renderNotificationIndex = 0;
            }
            that.renderNotification(that.options.renderNotificationIndex)
        }, 14000);
    },
    getContent(type){
        switch(type){
            case 0:
                return 'Just Purchased Lost Pet Alert';
            case 1:
                return 'Just Purchased Found Pet Alert';
            case 2:
                return 'Has Been Reported As Reunited';
        }
    },
    renderNotification: function(index) {
        var that = this;
        setTimeout(function(){
            $(`
                <div class="notification-content" style="display:none;">
                    <img class="notification-image" width="60" height="60" src="/uploads/${that.options.notifications[index].image}" />
                    <div class="notification-body">
                        <div class="notification-title">${that.options.notifications[index].name}</div>
                        <div class="notification-name">${that.getContent(that.options.notifications[index].lost_or_found)}</div>
                        <div class="notification-footer">${that.randomTime(that.options.notifications[index].lost_or_found)}</div>
                        <i class="notification-close-icon">x</i>
                    </div>
                </div>`
            ).appendTo('.notificaions-container');
            $('.notification-content').slideDown();
        }, 5000);
    },
    randomTime(type){
        if(type != 2){
            var key = Math.floor(Math.random() * 25); 
            return this.options.times[key];
        }else{
            var key = Math.floor(Math.random() * 4); 
            return this.options.timesDays[key];
        }
        
    },
    events(){
        var that = this;
        $('body').on('click', '.notification-close-icon', function(){
            clearInterval(that.interval);
            $('.notification-content').remove();
            setCookie('dnd', 1);
        });
    },
    init: function(){
        this.getNotifications();
        this.events();
    }
}

$(document).ready(function(){
    if(!getCookie('dnd')){
        pawmaw.notifications.init();
    }
});