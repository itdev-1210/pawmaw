
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
var map, infowindow, geocoder;

console.log(1);

function initAutocomplete() {
    geocoder = new google.maps.Geocoder();

    infowindow = new google.maps.InfoWindow();

    // Create the search box and link it to the UI element.
    var input1 = document.getElementById('pac-input');
    var input2 = document.getElementById('pac-input2');
    var input3 = document.getElementById('reportlostaddress');
    var input4 = document.getElementById('foundaddress');
    var searchBox = new google.maps.places.SearchBox(input1);
    var searchBox2 = new google.maps.places.SearchBox(input2);
    var searchBox3 = new google.maps.places.SearchBox(input3);
    var searchBox4 = new google.maps.places.SearchBox(input4);
    //  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.


    var markers1 = [];
    var markers2 = [];

    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function () {
        map1 = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -33.8688, lng: 151.2195},
            zoom: 15,
        });
        map1.addListener('bounds_changed', function () {
            searchBox.setBounds(map1.getBounds());
        });
        var places = searchBox.getPlaces();


        if (places.length == 0) {
            return;
        }
        var myAddress = places[0]["address_components"];
        for (let i = 0; i <myAddress.length ; i++) {
            var aData = myAddress[i]['types'][0];
            if(aData=="postal_code"){
                jQuery("#postal_2").val(myAddress[i]["long_name"]);
                break;
            }
        }
        // Clear out the old markers.
        markers1.forEach(function (marker) {
            marker.setMap(null);
        });
        markers1 = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }
            markers1.push(new google.maps.Marker({
                map: map1,
                position: place.geometry.location,
                animation: google.maps.Animation.DROP,
                draggable: true,
            }));
            markers1.forEach(function (marker) {
                google.maps.event.addListener(marker, 'click', function () {
                    toggleBounce(marker);
                    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
                        'Place ID: ' + place.place_id + '<br>' +
                        place.formatted_address + '</div>');
                    infowindow.open(map1, this);
                });

                google.maps.event.addListener(marker, 'dragend', function () {

                    geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {

                            if (results[0]) {
                                var myAddress = results[0]["address_components"];
                                for (let i = 0; i <myAddress.length ; i++) {
                                    var aData = myAddress[i]['types'][0];
                                    if(aData=="postal_code"){
                                        jQuery("#postal_2").val(myAddress[i]["long_name"]);
                                        break;
                                    }
                                }

                                jQuery('#pac-input').val(results[0].formatted_address);
                                infowindow.setContent(results[0].formatted_address);
                                infowindow.open(map1, marker);
                            }
                        }
                    });
                });
            });




            // Create a marker for each place.


            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map1.fitBounds(bounds);
    });


    searchBox2.addListener('places_changed', function () {
        map2 = new google.maps.Map(document.getElementById('map2'), {
            center: {lat: -33.8688, lng: 151.2195},
            zoom: 15,
        });
        map2.addListener('bounds_changed', function () {
            searchBox2.setBounds(map2.getBounds());
        });
        var places = searchBox2.getPlaces();


        if (places.length == 0) {
            return;
        }
        var myAddress = places[0]["address_components"];
        for (let i = 0; i <myAddress.length ; i++) {
            var aData = myAddress[i]['types'][0];
            if(aData=="postal_code"){
                jQuery("#postal_3").val(myAddress[i]["long_name"]);
                break;
            }
        }
        // Clear out the old markers.
        markers2.forEach(function (marker) {
            marker.setMap(null);
        });
        markers2 = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }
            markers2.push(new google.maps.Marker({
                map: map2,
                position: place.geometry.location,
                animation: google.maps.Animation.DROP,
                draggable: true,
            }));
            markers2.forEach(function (marker) {
                google.maps.event.addListener(marker, 'click', function () {
                    toggleBounce(marker);
                    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
                        'Place ID: ' + place.place_id + '<br>' +
                        place.formatted_address + '</div>');
                    infowindow.open(map2, this);
                });

                google.maps.event.addListener(marker, 'dragend', function () {

                    geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {

                            if (results[0]) {
                                var myAddress = results[0]["address_components"];
                                for (let i = 0; i <myAddress.length ; i++) {
                                    var aData = myAddress[i]['types'][0];
                                    if(aData=="postal_code"){
                                        jQuery("#postal_3").val(myAddress[i]["long_name"]);
                                        break;
                                    }
                                }

                                jQuery('#pac-input2').val(results[0].formatted_address);
                                infowindow.setContent(results[0].formatted_address);
                                infowindow.open(map2, marker);
                            }
                        }
                    });
                });
            });




            // Create a marker for each place.


            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map2.fitBounds(bounds);
    });
    searchBox3.addListener('places_changed', function () {

        var places = searchBox3.getPlaces();


        if (places.length == 0) {
            return;
        }
        var myAddress = places[0]["address_components"];
        for (let i = 0; i <myAddress.length ; i++) {
            var aData = myAddress[i]['types'][0];
            if(aData=="postal_code"){
                jQuery("#postal_2").val(myAddress[i]["long_name"]);
                break;
            }
        }



    });

    searchBox4.addListener('places_changed', function () {

        var places = searchBox4.getPlaces();


        if (places.length == 0) {
            return;
        }
        var myAddress = places[0]["address_components"];
        for (let i = 0; i <myAddress.length ; i++) {
            var aData = myAddress[i]['types'][0];
            if(aData=="postal_code"){
                jQuery("#postal_3").val(myAddress[i]["long_name"]);
                break;
            }
        }



    });
}

function toggleBounce(marker) {
    if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
    } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
    }
}
