PNotify.prototype.options.styling = "bootstrap3";

$(document).ready(function(){

});

function notify(type,msg){
	var title = type.toLowerCase().replace(/\b[a-z]/g, function(letter) {
	    return letter.toUpperCase();
	});
	var icon = '';
	if(type == 'success'){
		icon = 'font-icon font-icon-check-circle';
	}else{
		icon = 'font-icon font-icon-warning';
	}
    var stack_bottomright = {
      dir1: 'up',
      dir2: 'left'
    };
	new PNotify({
        title: title,
        text: msg,
        type: type,
        icon: icon,
        stack: stack_bottomright,
        addclass: 'alert-with-icon stack-bottomright'
    });
}

function custom_notify(type,icon,title,msg){
	new PNotify({
        title: title,
        type: type,
        text: msg,
        icon: icon,
        addclass: 'alert-with-icon'
    });
}