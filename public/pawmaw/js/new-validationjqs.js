jQuery(document).ready(function ($) {
    $('#contact-form').validate({
        rules: {
            name: {
                required: true
            },
            phone: {
                required: true,
                number: true
            },
            email: {
                required: true,
                email: true
            },
            message: {
                required: true,
            }
        }
    });
    function validation() {
        var file_size = $('#lost_preview')[0].files[0].size;
        if (file_size > 5242880) {
            $('.error-image').remove();
            $("<span class='error-image'>File size is greater than 5MB</span>").insertAfter($("#lost_preview-img"));
            return false;
        }
        $('.error-image').remove();
        return true;
    }
    function validations() {
        var file_size = $('#found_preview')[0].files[0].size;
        if (file_size > 5242880) {
            $('.error-image').remove();
            $("<span class='error-image'>File size is greater than 5MB</span>").insertAfter($("#found_preview-img"));
            return false;
        }
        $('.error-image').remove();
        return true;
    }
   
    $('#lost-form .next-btn').click(function (e) {
        if (validation() === true) {
            return true;
            
        } else {
            e.preventDefault();
        }
    });
    $('#found-form .next-btn').click(function (e) {
        if (validations() === true) {
            return true;
        } else {
            e.preventDefault();
        }
    });
});


