
function searchFacebookGroup() {
    let zip_code = $('#search-zip-code').val();
    $.ajax({
        type: 'POST',
        url: '/get-facebook-page',
        data: {
            _token: $('meta[name=csrf-token]').attr('content'),
            zip_code: zip_code
        },
        success: function (response) {
            let facebook = response.facebook;

            if (facebook) {
                $('.zip_code_item').show().empty();

                $('.zip_code_item').append(`
					<a href="` + facebook.link + `" target="_blank">` + facebook.name + ` </a>
                    <div class="fb-like" id="fb-button" data-href="` + facebook.link + `" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
				`);

                FB.XFBML.parse();

            } else {
                $('.zip_code_item').hide()
            }
        }
    });
}

(function ($) {
	
	
	
    "use strict";
    $('#lost_found_pop')

    $('#search-zip-code').keyup(function (event) {
        if (event.keyCode === 13) {
            searchFacebookGroup();
        }
    });


    $('#move-to-search-zip-code').keyup(function (event) {
        if (event.keyCode === 13) {
            $('.featured-network_code-btn').trigger('click');
        }
    });

    $('#lost-form').submit(function (e) {
        let check = 1;
        let inputs = $('#lost-form input[required], #lost-form textarea[required]');

        inputs.each(function (index, element) {
            if (!(/\S/.test($(element).val())))
                check = 0;
        });

        if (check) {
            $('#lost-form').submit();
        } else {
            e.preventDefault();
            //alert('Choose correct address');
            $('.error-address').remove();
            $("<span class='error-address'>Choose correct address</span>").insertAfter($("#pac-input"));
        }

    });

    $('#found-form').submit(function (e) {
        let check = 1;
        let inputs = $('#found-form input[required], #found-form textarea[required]');

        inputs.each(function (index, element) {
            if (!(/\S/.test($(element).val())))
                check = 0;
        });

        if (check) {
            $('#found-form').submit();
        } else {
            e.preventDefault();
             //alert('Choose correct address');
            $('.error-address').remove();
            $("<span class='error-address'>Choose correct address</span>").insertAfter($("#pac-input2"));
        }

    });
    
    function initializePetImageConvert() {
        if ($('#image-to-copy').data('check') == '1')
            return;
        let elementToCopy = document.getElementById('petdetails-img-wrap');
        let elementToPut = document.getElementById('copy-image');
        let petId = $('#image-to-copy').data('id');
        domtoimage
                .toPng(elementToCopy)
                .then(function (dataUrl) {
                	
                    let img = new Image();

                    img.src = dataUrl;

                    elementToPut.appendChild(img);

                    $.ajax({
                        type: 'POST',
                        url: '/save-converting-image',
                        data: {
                            _token: $('meta[name=csrf-token]').attr('content'),
                            image: dataUrl,
                            id: petId
                        },
                        success: function (response) {
                            $('#copy-image img').attr('src', response.image);
                        }
                    });
                });
    }

    jQuery(document).ready(function ($) {

        let checkZipCode = $('.joinournetwork-content_inner-search').data('zip-code');


        $('.active-form-button').click(function () {
            $('html, body').animate({
                scrollTop: 0
            }, 700);

            $('#lost-name').focus();
        });
        
        setTimeout(function(){
            initializePetImageConvert();
        },2000);

        
        
        // Hero Pet Lost and Found Form
        $('#lost-form-link').click(function (e) {
            $("#lost-form").delay(100).fadeIn(100);
            $("#found-form").fadeOut(100);
            $('#found-form-link').removeClass('active');
            $(this).addClass('active');
            $("#lost-form-link .form-check-input").prop("checked", true);
            e.preventDefault();
        });
        $('#found-form-link').click(function (e) {
            $("#found-form").delay(100).fadeIn(100);
            $("#lost-form").fadeOut(100);
            $('#lost-form-link').removeClass('active');
            $("#found-form-link .form-check-input").prop("checked", true);
            $(this).addClass('active');
            e.preventDefault();
        });

        // Hero Form Preview Image
        function lostURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                if (input.files[0].size > 8000000) {
                    alert(
                            'Photo exceeds the size limit. Please optimize this photo or choose another.'
                            );
                    return;
                }

                EXIF.getData(input.files[0], function () {
                    $('#lost_preview-img').attr('class', 'preview_img');

                    switch (parseInt(EXIF.getTag(this, "Orientation"))) {
                        case 2:
                            $('#lost_preview-img').addClass('flip');
                            break;
                        case 3:
                            $('#lost_preview-img').addClass('rotate-180');
                            break;
                        case 4:
                            $('#lost_preview-img').addClass('flip-and-rotate-180');
                            break;
                        case 5:
                            $('#lost_preview-img').addClass('flip-and-rotate-270');
                            break;
                        case 6:
                            $('#lost_preview-img').addClass('rotate-90');
                            break;
                        case 7:
                            $('#lost_preview-img').addClass('flip-and-rotate-90');
                            break;
                        case 8:
                            $('#lost_preview-img').addClass('rotate-270');
                            break;
                    }
                });

                reader.onload = function (e) {
                    $('#lost_preview-img').css('background', 'transparent url(' + e.target.result + ') no-repeat center center');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#lost_preview").change(function () {
            lostURL(this);
        });

        function foundURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                if (input.files[0].size > 8000000) {
                    alert(
                            'Photo exceeds the size limit. Please optimize this photo or choose another.'
                            );
                    return;
                }

                EXIF.getData(input.files[0], function () {
                    $('#found_preview-img').attr('class', 'preview_img');

                    switch (parseInt(EXIF.getTag(this, "Orientation"))) {
                        case 2:
                            $('#found_preview-img').addClass('flip');
                            break;
                        case 3:
                            $('#found_preview-img').addClass('rotate-180');
                            break;
                        case 4:
                            $('#found_preview-img').addClass('flip-and-rotate-180');
                            break;
                        case 5:
                            $('#found_preview-img').addClass('flip-and-rotate-270');
                            break;
                        case 6:
                            $('#found_preview-img').addClass('rotate-90');
                            break;
                        case 7:
                            $('#found_preview-img').addClass('flip-and-rotate-90');
                            break;
                        case 8:
                            $('#found_preview-img').addClass('rotate-270');
                            break;
                    }
                });

                reader.onload = function (e) {
                    // $('#found_preview-img').attr('src', e.target.result);
                    $('#found_preview-img').css('background', 'transparent url(' + e.target.result + ') no-repeat center center');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#found_preview").change(function () {
            foundURL(this);
        });

        // Report Another Pet Form
        $('#lost-report-form-link').click(function (e) {
            $("#lost-report-form").delay(100).fadeIn(100);
            $("#found-report-form").fadeOut(100);
            $('#found-report-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });
        $('#found-report-form-link').click(function (e) {
            $("#found-report-form").delay(100).fadeIn(100);
            $("#lost-report-form").fadeOut(100);
            $('#lost-report-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });


        // Login / Sign up
        $('#login-form-link').click(function (e) {
            $("#login-form").delay(100).fadeIn(100);
            $("#signup-form").fadeOut(100);
            $('#signup-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });
        $('#signup-form-link').click(function (e) {
            $("#signup-form").delay(100).fadeIn(100);
            $("#login-form").fadeOut(100);
            $('#login-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });


        // Join network option change

        $('.jns-category_select select').on('change', function () {
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;

            if (valueSelected == 1) {
                $('.jns-category_result').css('display', 'block');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }

            if (valueSelected == 2) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: block !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }


            if (valueSelected == 3) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: block !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }
            if (valueSelected == 4) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: block !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }
            if (valueSelected == 5) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: block !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }


            if (valueSelected == 6) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: block !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }


            if (valueSelected == 7) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: block !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }
            if (valueSelected == 8) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: block !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }
            if (valueSelected == 9) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: block !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }


            if (valueSelected == 10) {
                $('.jns-category_result').css('display', 'no-repeat');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: block !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }


            if (valueSelected == 11) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: block !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }
            if (valueSelected == 12) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: block !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }
            if (valueSelected == 13) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: block !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }


            if (valueSelected == 14) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: block !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }


            if (valueSelected == 15) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: block !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }
            if (valueSelected == 16) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: block !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }
            if (valueSelected == 17) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: block !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }


            if (valueSelected == 18) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: block !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }
            if (valueSelected == 19) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: block !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }
            if (valueSelected == 20) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: block !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }
            if (valueSelected == 21) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: block !important");
                $('.jns-category_result-22').attr("style", "display: none !important");
            }
            if (valueSelected == 22) {
                $('.jns-category_result').css('display', 'none');
                $('.jns-category_result-2').attr("style", "display: none !important");
                $('.jns-category_result-3').attr("style", "display: none !important");
                $('.jns-category_result-4').attr("style", "display: none !important");
                $('.jns-category_result-5').attr("style", "display: none !important");
                $('.jns-category_result-6').attr("style", "display: none !important");
                $('.jns-category_result-7').attr("style", "display: none !important");
                $('.jns-category_result-8').attr("style", "display: none !important");
                $('.jns-category_result-9').attr("style", "display: none !important");
                $('.jns-category_result-10').attr("style", "display: none !important");
                $('.jns-category_result-11').attr("style", "display: none !important");
                $('.jns-category_result-12').attr("style", "display: none !important");
                $('.jns-category_result-13').attr("style", "display: none !important");
                $('.jns-category_result-14').attr("style", "display: none !important");
                $('.jns-category_result-15').attr("style", "display: none !important");
                $('.jns-category_result-16').attr("style", "display: none !important");
                $('.jns-category_result-17').attr("style", "display: none !important");
                $('.jns-category_result-18').attr("style", "display: none !important");
                $('.jns-category_result-19').attr("style", "display: none !important");
                $('.jns-category_result-20').attr("style", "display: none !important");
                $('.jns-category_result-21').attr("style", "display: none !important");
                $('.jns-category_result-22').attr("style", "display: block !important");
            }

        });



        // Blog Tabs
        $('#popular_blog-tab_btn').click(function (e) {
            $("#popular_blog-tab_items").delay(100).fadeIn(100);
            $("#recent_blog-tab_items").fadeOut(100);
            $('#recent_blog-tab_btn').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });
        $('#recent_blog-tab_btn').click(function (e) {
            $("#recent_blog-tab_items").delay(100).fadeIn(100);
            $("#popular_blog-tab_items").fadeOut(100);
            $('#popular_blog-tab_btn').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });


        // home btn hover

        $('#lost_preview').on('click', function () {

            $(".report_upload-title").addClass("report_upload-title-hover");
        });

        $('#found_preview').on('click', function () {

            $(".report_upload-title").addClass("report_upload-title-found-hover");
        });



        // Blog Search Icon hide/show
        $(".blog_post_search input").focus(function () {
            $('.hide_blog_search-icon').hide('slow');
            //return false;
        });


        $('.blog_post_search input').blur(function () {
            if (!$(this).val()) {
                $('.hide_blog_search-icon').show('slow');
            }
        });

        // Arrow down and up of faq page
        $('.card-header').click(function () {
            $(this).addClass('active');
        });



        // hamburger menu 

        $('.hamburger').on('click', function () {
            $('#navbarResponsive').toggleClass("Collapse_ham");
            $(this).toggleClass("cross_sign");
        });

        // mobile footer animaton

        $('.crossi_con').on('click', function () {
            $('.footer_link').fadeIn();
            $('.fixing_view_point').animate({
                'top': '100%'
            }, 400);
        });


        $('#more_btn').on('click', function () {

            $('.link_items').animate({
                'opacity': '1'
            }, 1500);
            $('.fixing_view_point').animate({
                'top': '0%'
            }, 400);
            $('.footer_link').fadeOut();


        });





        // wow initialize
        new WOW().init();

        // scrollup back to top
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn("slow");
                $('.package-wrap_skip_fix').fadeIn("slow");
            } else {
                $('.scrollup').fadeOut("slow");
                $('.package-wrap_skip_fix').fadeOut("slow");
            }
        });

        $('.scrollup').click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });

        // help open/close
        $(".help-wrap_btn").click(function () {
            $("#helpModal").collapse('toggle');
        });

        $(".close-x").click(function () {
            $("#helpModal").collapse('toggle');
        });




        // site preloader
        $(window).on('load', function () {
            setTimeout(function () {
                $('#cover').fadeOut(300);
            }, 300)
        });

        // Image Upload with preview
        $(document).on('change', '.btn-file :file', function () {
            var input = $(this),
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function (event, label) {

            var input = $(this).parents('.input-group').find(':text'),
                    log = label;

            if (input.length) {
                input.val(log);
            } else {
                if (log)
                    alert(log);
            }

        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                if (input.files[0].size > 8000000) {
                    alert(
                            'Photo exceeds the size limit. Please optimize this photo or choose another.'
                            );
                    return;
                }

                $('#img-upload-lost').attr('class', '');

                EXIF.getData(input.files[0], function () {

                    switch (parseInt(EXIF.getTag(this, "Orientation"))) {
                        case 2:
                            $('#img-upload-lost').addClass('flip');
                            break;
                        case 3:
                            $('#img-upload-lost').addClass('rotate-180');
                            break;
                        case 4:
                            $('#img-upload-lost').addClass('flip-and-rotate-180');
                            break;
                        case 5:
                            $('#img-upload-lost').addClass('flip-and-rotate-270');
                            break;
                        case 6:
                            $('#img-upload-lost').addClass('rotate-90');
                            break;
                        case 7:
                            $('#img-upload-lost').addClass('flip-and-rotate-90');
                            break;
                        case 8:
                            $('#img-upload-lost').addClass('rotate-270');
                            break;
                    }
                });

                reader.onload = function (e) {
                    $('#img-upload-lost').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp-lost").change(function () {
            readURL(this);
        });

        function readURLFound(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                if (input.files[0].size > 8000000) {
                    alert(
                            'Photo exceeds the size limit. Please optimize this photo or choose another.'
                            );
                    return;
                }

                $('#img-upload-found').attr('class', '');

                EXIF.getData(input.files[0], function () {
                    switch (parseInt(EXIF.getTag(this, "Orientation"))) {
                        case 2:
                            $('#img-upload-found').addClass('flip');
                            break;
                        case 3:
                            $('#img-upload-found').addClass('rotate-180');
                            break;
                        case 4:
                            $('#img-upload-found').addClass('flip-and-rotate-180');
                            break;
                        case 5:
                            $('#img-upload-found').addClass('flip-and-rotate-270');
                            break;
                        case 6:
                            $('#img-upload-found').addClass('rotate-90');
                            break;
                        case 7:
                            $('#img-upload-found').addClass('flip-and-rotate-90');
                            break;
                        case 8:
                            $('#img-upload-found').addClass('rotate-270');
                            break;
                    }
                });

                reader.onload = function (e) {
                    $('#img-upload-found').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp-found").change(function () {
            readURLFound(this);
        });


        // PopUp animation js
        var myfunction = function ($) {

            if (!localStorage.getItem('found-page-popup')) {
                $('.PopUp_overlay').delay(300).css('display', 'block').animate({
                    'opacity': '1',
                    'display': 'block'
                }, 500);
                $('.pop_up_banner').delay(800).css('display', 'block').animate({
                    'top': '50%',
                    'opacity': '1'
                }, 800);

                localStorage.setItem('found-page-popup', 1);
            } else {
                $('.PopUp_overlay').remove();
            }

            return false;

        };
        myfunction(jQuery);

        $('.PopUp_close').on('click', function () {
            $('.PopUp_overlay').delay(500).css('display', 'none').animate({
                'opacity': '0',
                'display': 'none'
            }, 500);
            $('.pop_up_banner').delay(300).css('display', 'none').animate({
                'top': '0%',
                'opacity': '0',
                'visibility': 'none'
            }, 500);
        });


        $('.PopUp_overlay').on('click', function () {
            $('.PopUp_overlay').delay(500).css('display', 'none').animate({
                'opacity': '0',
                'display': 'none'
            }, 500);
            $('.pop_up_banner').delay(300).css('display', 'none').animate({
                'top': '0%',
                'opacity': '0',
                'visibility': 'none'
            }, 500);
        });

        // $('#lost_found_pop').myfunction();

    });
}(jQuery));
(function ($, window) {

})(window.jQuery, window);
