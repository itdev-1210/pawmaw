<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Mail\LostBlogEmail;
use Illuminate\Support\Facades\Mail;

// for mail test

//Route::get('/facebook','TestController@facebook');
Route::get('/facebook', function () {
    return view('user.facebook');
});
Route::get('/test', 'TestController@testing');

Route::get('/testemail', 'Auth\PawMawMailController@sendNewLostPostEmail');

Route::get('/testeugene', 'EugeneController@index');

Route::get('/postcommand', function () {
    Artisan::call('custom:command');

    return 'post ok';
});

Route::get('/lostcommand', function () {
    Artisan::call('lost:command');

    return 'lost ok';
});

Route::get('/foundcommand', function () {
    Artisan::call('found:command');

    return 'found ok';
});

Route::get('resendactivation', 'UserAuth\ForgotPasswordController@resendActivationCode')->name('resendactivation');
Route::post('resendactivation', 'UserAuth\ForgotPasswordController@resendActivationCodePost')->name('resendactivationPost');

// route for email template test
Route::get('/found', function () {
    // Mail::to('preeti.bisht@catalyst.sh')->send(new LostBlogEmail());
    return view('emails.lostblog_email');
});

Route::get('/lost', function () {
    return view('emails.lost_email');
});
Route::get('/owner', function () {
    return view('emails.owner_email');
});
Route::get('/post', function () {
    return view('emails.post_email');
});

Route::get('/package-email', function () {
    return view('emails.cron.newPost');
});

Route::get('/missing', function () {
    return view('emails.missing_email');
});

Route::get('verify/{token}', 'Auth\RegisterController@verify')->name('apiverify');
// route end for tapos
Route::get('/', 'Front\SiteController@index')->name('homepage');

Route::get('/pet-details/{id}', 'Front\SiteController@petDetails')->name('pet.details');

Route::get('/pet-image/{id}', 'Front\SiteController@imageWaterMark');
Route::get('/map-image/{id}', 'Front\SiteController@mapWaterMark');

//site Page Front
Route::get('/privacy-policy', function () {
    $metaData = [
        'title' => 'Privacy Policy- PawMaw',
        'description' => 'Our privacy policy describes how we collect and use personal data. Read our privacy policy to know how we use your information.',
        'image' => url('pawmaw/img/logo.png'),
    ];

    return view('pawmawFront/site/privacy', compact('metaData'));
});

Route::get('/terms', function () {
    return view('pawmawFront/site/terms');
});

Route::get('/report', function () {
    return redirect('/');
});

// Route::get('/package', function () {
//     return view('pawmawFront/site/package');
// });

Route::get('/blog', 'Front\BlogController@index');
Route::get('/blog/{slug}', 'Front\BlogController@post');
Route::get('/blog/category/{slug}', 'Front\BlogController@category');
Route::post('/blog/create-comment', 'Front\BlogController@createComment');

/* Route::get('/guide-for-found-pets', function () {
    return view('pawmawFront/site/guide-for-found-pets');
});

Route::get('/top-10-ways-to-find-missing-pets', function () {
    return view('pawmawFront/site/top-10-ways-to-find-missing-pets');
});
 */
Route::get('/refund-policy', function () {
    return view('pawmawFront/site/refund');
});
    Route::get('/unsubscribe', function () {
        return view('pawmawFront/site/email_unsubscribe');
    });
   Route::post('/unsubscribe', 'Front\SiteController@unsubscribe');

Route::get('/contact', function () {
    $data['metaData'] = [
        'title' => 'Have a Question? Contact Us- PawMaw',
        'description' => 'Have a question about pawpaw? Fill up the form and we will get back to you as quickly as possible. Also, check out our F&Q page many questions can be answered there.',
        'image' => url('pawmaw/img/logo.png'),
    ];

    return view('pawmawFront/site/contact', $data);
});

// For Development Test purpose

Route::get('/flyer-lost', function () {
    return view('pawmawFront/site/flyer_lost');
});

Route::post('/newsletter/newsletterstore', ['as' => 'newsletter_email_store', 'uses' => 'NewsletterController@store']);

Route::post('/contact/contactstore', ['as' => 'contact_email_store', 'uses' => 'Front\SiteController@contactemailstore']);

//  email pet

Route::get('/pets/details/{id}', ['as' => 'owner_pets_email', 'uses' => 'Front\SiteController@owneremail']);

Route::post('/email/emailstore', ['as' => 'owner_pets_emailstore', 'uses' => 'Front\SiteController@owneremailstore']);

//  contact email

Route::get('/reunited', 'Front\SiteController@reunited')->name('reunited-page');

Route::get('/pet', 'Front\SiteController@search')->name('search');
Route::get('/lost-found-pets', 'Front\SiteController@search')->name('search');
Route::get('/lost-pets', 'Front\SiteController@lostSearch')->name('search');
Route::get('/found-pets', 'Front\SiteController@foundSearch')->name('search');
Route::get('/reunited-pets', 'Front\SiteController@reunitedSearch')->name('search');
Route::get('/how-it-works', 'Front\SiteController@howitworks')->name('howitworks');
Route::get('/join-our-network', 'Front\SiteController@join_network')->name('join_network');
Route::get('/faqs', 'Front\SiteController@faq')->name('faq');
Route::get('/about-us', 'Front\SiteController@about')->name('about');
Route::post('/save-converting-image', 'Front\SiteController@saveConvertingImage');
Route::post('/save-watermark-converting-image', 'Front\SiteController@saveWatermarkConvertingImage');
Route::post('/get-facebook-page', 'Front\SiteController@getFacebookPage');
Route::post('/update-pet-listing', 'Front\SiteController@updatePetListing');
Route::get('/api/notifications', 'Front\SiteController@notifications');

Route::group(['prefix' => 'admin'], function () {
    Route::get('/newsletters', 'NewsletterController@index');

    // blog related work

    Route::get('/blog', ['as' => 'adminblog', 'uses' => 'Admin\BlogController@index']);
    Route::get('/blog/create', ['as' => 'adminblogcreate', 'uses' => 'Admin\BlogController@create']);
    Route::get('/blog/{id}/edit', ['as' => 'adminblogedit', 'uses' => 'Admin\BlogController@edit']);
    Route::get('/blog/comment', 'Admin\BlogController@comment');
    Route::post('/blog/comment', 'Admin\BlogController@commentCheck');
    Route::post('/blog', ['as' => 'adminblogstore', 'uses' => 'Admin\BlogController@store']);
    Route::post('/blog/{id}/edit', 'Admin\BlogController@update');
    Route::post('/blog/{id}/delete', 'Admin\BlogController@destroy');
    Route::post('/uploadEditorImage', 'Admin\BlogController@uploadEditorImage')->name('uploadEditorImage');
    Route::get('/page', ['as' => 'adminpage', 'uses' => 'Admin\PageController@index']);
    Route::get('/page/create', ['as' => 'adminpagecreate', 'uses' => 'Admin\PageController@create']);
    Route::get('/page/{id}/edit', ['as' => 'adminpageedit', 'uses' => 'Admin\PageController@edit']);
    Route::post('/page', ['as' => 'adminpagestore', 'uses' => 'Admin\PageController@store']);
    Route::post('/page/{id}/edit', 'Admin\PageController@update');
    Route::post('/page/{id}/delete', 'Admin\PageController@destroy');

    Route::get('/facebook', ['uses' => 'Admin\FacebookPageController@index']);
    Route::get('/facebook/create', ['uses' => 'Admin\FacebookPageController@create']);
    Route::get('/facebook/edit/{id}', ['uses' => 'Admin\FacebookPageController@edit']);
    Route::post('/facebook/create', ['uses' => 'Admin\FacebookPageController@store']);
    Route::post('/facebook/edit/{id}', 'Admin\FacebookPageController@update');
    Route::post('/facebook/{id}/delete', 'Admin\FacebookPageController@destroy')->name('delete-facebook-page');

    Route::get('/blog/category', 'Admin\CategoryController@index');
    Route::get('/blog/category/create', 'Admin\CategoryController@create');
    Route::get('/blog/category/{id}/edit', 'Admin\CategoryController@edit');
    Route::post('/blog/category/create', 'Admin\CategoryController@store');
    Route::post('/blog/category/{id}/edit', 'Admin\CategoryController@update');
    Route::post('/blog/category/{id}/delete', 'Admin\CategoryController@destroy');

    // end blog related work
    Route::get('/', 'AdminAuth\LoginController@showLoginForm');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');
    Route::get('/logout', function () {
        return response()->view('errors.404', [], 404);
    });

    //  Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
    //  Route::post('/register', 'AdminAuth\RegisterController@register');

    Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');

    //  web email

    Route::get('/sitemap', 'Admin\DownloadController@sitemap');
    Route::get('/unsubscribe', 'Admin\AdminController@unsubscribe');
    Route::post('/addunsubscribe', 'Admin\AdminController@addunsubscribe');
    Route::post('/downloadmail', 'Admin\DownloadController@emails');
    Route::get('/downloadmail', function () {
        return response()->view('errors.404', [], 404);
    });
    Route::get('/webemail', 'Admin\PetController@webmessage');

    Route::get('/owneremail', 'Admin\PetController@petmessage');

    //  admin email

    Route::get('/petsemail', 'Admin\PetController@adminmessage');

    Route::get('/petsemail/view/{id}', 'Admin\PetController@adminmessageshow');

    //  admin pet

    Route::get('/pets', 'Admin\PetController@index');
    Route::get('/pets/create', 'Admin\PetController@create');
    Route::post('/pets/store', ['as' => 'admin_pets_store', 'uses' => 'Admin\PetController@store']);
    Route::get('/pets/view/{id}', 'Admin\PetController@show');
    Route::get('/pets/edit/{id}', 'Admin\PetController@edit');
    Route::post('/pets/update/{id}', ['as' => 'admin_pets_update', 'uses' => 'Admin\PetController@update']);
    Route::delete('/pets/delete/{id}', ['as' => 'admin_pets_delete', 'uses' => 'Admin\PetController@destroy']);
    Route::get('/pets/status/{id}', 'Admin\PetController@statusUpdate');
    Route::get('/pets/package/{id}/{name}', 'Admin\PetController@packageUpdate');

    Route::post('/ownermailstatus', 'Admin\PetController@ownermailstatus')->name('ownermailstatus');

    //  email pet

    Route::get('/pets/email/{id}', ['as' => 'admin_pets_email', 'uses' => 'Admin\PetController@email']);

    Route::post('/email/emailstore', ['as' => 'admin_pets_emailstore', 'uses' => 'Admin\PetController@emailstore']);

    // notifications
    Route::get('/notifications', 'Admin\NotificationsController@index');
    Route::post('/notifications/create', 'Admin\NotificationsController@store');
    Route::post('/notifications/delete/{id}', 'Admin\NotificationsController@delete');
    Route::post('/notifications/toggle-setting/{value}', 'Admin\NotificationsController@changeSetting');

    //  admin order history

    Route::get('/orders', 'Admin\PaymentController@index');

    Route::get('/orders/view/{id}', 'Admin\PaymentController@show');

    // for facebook share

    Route::get('/facebookshares', 'Admin\FacebookshareController@index');
    Route::get('/facebookshares/create', 'Admin\FacebookshareController@create');
    Route::post('/facebookshares/store', ['as' => 'facebooks_store', 'uses' => 'Admin\FacebookshareController@store']);
    Route::get('/facebookshares/view/{id}', 'Admin\FacebookshareController@show');
    Route::get('/facebookshares/edit/{id}', 'Admin\FacebookshareController@edit');
    Route::post('/facebookshares/update/{id}', ['as' => 'facebooks_update', 'uses' => 'Admin\FacebookshareController@update']);
    Route::delete('/facebookshares/delete/{id}', ['as' => 'facebooks_delete', 'uses' => 'Admin\FacebookshareController@destroy']);
    Route::get('/facebookshares/status/{id}', 'Admin\FacebookshareController@statusUpdate');

    //  admin user

    Route::get('/users', 'Admin\UserController@index');

    Route::get('/users/view/{id}', 'Admin\UserController@show');
    Route::post('/users/{id}/delete', 'Admin\UserController@delete');

    Route::get('/permissions', 'Admin\PermissionController@index');
    Route::get('/permissions/create', 'Admin\PermissionController@create');
    Route::post('/permissions/store', ['as' => 'permissions_store', 'uses' => 'Admin\PermissionController@store']);
    Route::get('/permissions/view/{id}', 'Admin\PermissionController@show');
    Route::get('/permissions/edit/{id}', 'Admin\PermissionController@edit');
    Route::post('/permissions/update/{id}', ['as' => 'permissions_update', 'uses' => 'Admin\PermissionController@update']);
    Route::delete('/permissions/delete/{id}', ['as' => 'permissions_delete', 'uses' => 'Admin\PermissionController@destroy']);

    Route::get('/roles', 'Admin\RoleController@index');
    Route::get('/roles/create', 'Admin\RoleController@create');
    Route::post('/roles/store', ['as' => 'roles_store', 'uses' => 'Admin\RoleController@store']);
    Route::get('/roles/view/{id}', 'Admin\RoleController@show');
    Route::get('/roles/edit/{id}', 'Admin\RoleController@edit');
    Route::post('/roles/update/{id}', ['as' => 'roles_update', 'uses' => 'Admin\RoleController@update']);
    Route::delete('/roles/delete/{id}', ['as' => 'roles_delete', 'uses' => 'Admin\RoleController@destroy']);

    Route::get('/admins', 'Admin\AdminController@index');
    Route::get('/admins/create', 'Admin\AdminController@create');
    Route::post('/admins/store', ['as' => 'admins_store', 'uses' => 'Admin\AdminController@store']);
    Route::get('/admins/view/{id}', 'Admin\AdminController@show');
    Route::get('/admins/edit/{id}', 'Admin\AdminController@edit');
    Route::post('/admins/update/{id}', ['as' => 'admins_update', 'uses' => 'Admin\AdminController@update']);
    Route::delete('/admins/delete/{id}', ['as' => 'admins_delete', 'uses' => 'Admin\AdminController@destroy']);
    Route::get('/admins/edit_password/{id}', 'Admin\AdminController@edit_password');
    Route::post('/admins/admin_password/{id}', ['as' => 'admin_password', 'uses' => 'Admin\AdminController@change_password']);
});
        Route::get('/cache', function () {
            $exitCode = Artisan::call('cache:clear');

            return 'Cache cleared';
        });
            Route::get('/mail', function () {
                $d = getcwd();
                /* //Queue::push('log message');
                $job=(new SendFoundPostEmail())
                ->delay(Carbon::now()->addSeconds(30));
                dispatch($job);  */
                return 'Cache cleared'.$d;
            });

Route::group(['prefix' => 'user'], function () {
    Route::get('/login', 'UserAuth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'UserAuth\LoginController@login');
    Route::post('/logout', 'UserAuth\LoginController@logout')->name('logout');

    Route::get('/logout', function () {
        return response()->view('errors.404', [], 404);
    });
    Route::get('/register', 'UserAuth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'UserAuth\RegisterController@register');

    Route::post('/password/email', 'UserAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'UserAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'UserAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'UserAuth\ResetPasswordController@showResetForm');

    Route::get('/next', 'Front\UserAccountController@reg_next');
    Route::post('/updatepetinfo', 'Front\UserAccountController@updatepetinfo')->name('updatepetinfo');
    Route::get('/updatepetinfo', 'Front\UserAccountController@updatepetinfo')->name('updatepetinfo');

    Route::group(['middleware' => ['checkOnFinishPost', 'user']], function () {
        Route::get('/faqs/{id}', 'Front\UserAccountController@faq');
        Route::get('/my-account', 'Front\UserAccountController@index')->name('my-account');
        Route::get('/report', 'Front\UserAccountController@report')->name('report-add');
        Route::post('/report-store', 'Front\UserAccountController@store_report')->name('report-store');
        Route::get('/report/edit/{id}', 'Front\UserAccountController@edit');
        Route::post('/report/update/{id}', ['as' => 'reports_update', 'uses' => 'Front\UserAccountController@update']);
        Route::delete('/report/delete/{id}', ['as' => 'reports_delete', 'uses' => 'Front\UserAccountController@destroy']);
        Route::get('/report/reunited/{id}', ['as' => 'reunited_update', 'uses' => 'Front\UserAccountController@reunited']);
        Route::get('/pdf-report/{id}', 'Front\UserAccountController@pdf');
        Route::get('/pdf-report/{id}/test', 'Front\UserAccountController@pdfTest');
        Route::get('/promote/{id}', 'Front\UserAccountController@promote');
        Route::get('/history', 'Front\UserAccountController@history');
        Route::get('/edit-profile', 'Front\UserAccountController@edit_profile');

        Route::get('/pet_alert/{id}', 'Front\UserAccountController@facebook_tools');
        Route::get('/join_network/{id}', 'Front\UserAccountController@join_network');
        Route::get('/join_network', 'Front\UserAccountController@join_network_without_map');

        Route::get('/pet-details/{id}', ['as' => 'owner_pets_details', 'uses' => 'Front\UserAccountController@petDetails']);
        Route::post('/updateProfile', 'Front\UserAccountController@update_profile')->name('updateProfile');
        Route::get('/changePassword', 'Front\UserAccountController@showChangePasswordForm');
        Route::post('/changePassword', 'Front\UserAccountController@changePassword')->name('changePassword');
    });

    Route::get('/download-flyer/{id}', 'Front\UserAccountController@download_flayer');
    Route::get('/alerts/{id}', 'Front\UserAccountController@alerts');
    Route::get('/update_listing/{id}', 'Front\UserAccountController@update_listing');
    Route::get('/pdf-test', function () {
        return view('user.pdf-test');
    });

    // for payment
    Route::get('/checkout', 'Front\PaymentController@checkout');
    //Route::get('/callback', 'Front\PaymentController@callback');
    //Route::get('/complete_payment', 'Front\PaymentController@callback');
    //Route::post('/complete_payment', 'Front\PaymentController@callback');
});
Route::get('/complete_payment', 'Front\PaymentController@callback');
Route::get('/payment_ins', 'Front\PaymentController@ins');
Route::post('/payment_ins', 'Front\PaymentController@ins');
Route::get('/dd', function () {
    phpinfo();
});

Route::get('/{slug}', 'Front\SiteController@index')->name('homepage2');
